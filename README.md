
[![pipeline status](https://gitlab.inria.fr/mpfq/mpfq/badges/master/pipeline.svg)](https://gitlab.inria.fr/mpfq/mpfq/-/pipelines?ref=master)
[![coverage report](https://gitlab.inria.fr/mpfq/mpfq/badges/master/coverage.svg)](https://gitlab.inria.fr/mpfq/mpfq/-/jobs/artifacts/master/file/coverage/index.html?job=coverage+tests)


MPFQ - A finite field library
=============================

See the [releases](../../releases) section for MPFQ releases. Note however that
most of the Mpfq development since several years has been happening in
the git repository only.

You may also browse the on-line documentation, either in
[multi-page](http://mpfq.gitlabpages.inria.fr/doc/index.html) or
[single-page](http://mpfq.gitlabpages.inria.fr/doc/doc.html) format.
