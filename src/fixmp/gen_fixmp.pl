#!/usr/bin/perl

# This source file handles the generation of the different variants of
# the fixmp headers.
#
#  - one which contains fallbacks for all functions (the "longlong" version).
#
#  - one which is specific to x86_64
#
#  - one which is specific to i386
#
# We also know how to create a "merged" header file, with fallbacks
# removed. This implies tinkering with the ingredients of the generation
# process below.

use strict;
use warnings;
use File::Spec;
use Storable qw/dclone/;
use Data::Dumper;

my $dirname;

BEGIN {
    $dirname = (File::Spec->splitpath($0))[1];
    unshift @INC, "$dirname/../perl-lib";
}

use Mpfq::engine::utils qw($debuglevel debug);
use Mpfq::engine::conf qw/parse_api_rhs/;
use Mpfq::fixmp;

sub usage_and_die {
    die "usage: ./gen_fixmp.pl\n"; 
}

# This evaluates to a hash which may be used as \$api
sub fixmp_api {
    my $nhw = shift;
    (my $nd = $nhw) =~ s/_/./;
    (my $n = $nhw) =~ s/(\d+)((?:[_\.]\d+)?)/($1+1)/e;
    (my $np1d = $nd) =~ s/(\d+)((?:[_\.]\d+)?)/($1+1).$2/e;
    my $nn1 = 2*$n; $nn1-- if $nhw =~ /_5$/;
    my $nn1p1 = $nn1 + 1;
    my $np1 = $n + 1;
    my @meta_api = ();

    push @meta_api, (
    add => [
        "mp_limb_t <- mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x, y, and z have $nd words. Result in z. Return carry bit"],
    sub => [
        "mp_limb_t <- mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x, y, and z have $nd words. Result in z. Return borrow bit"],
    add_nc => [
        "mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x, y, and z have $nd words. Result in z. Carry bit is lost."],
    sub_nc => [
        "mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x, y, and z have $nd words. Result in z. Borrow bit is lost."],
    add_ui => [
        "mp_limb_t <- mp_limb_t* const-mp_limb_t* mp_limb_t",
        "x, y, and z have $nd words. Result in z. Return carry bit"],
    sub_ui => [
        "mp_limb_t <- mp_limb_t* const-mp_limb_t* mp_limb_t",
        "x, y, and z have $nd words. Result in z. Return borrow bit"],
    add_ui_nc => [
        "mp_limb_t* const-mp_limb_t* mp_limb_t",
        "x, y, and z have $nd words. Result in z. Carry bit is lost."],
    sub_ui_nc => [
        "mp_limb_t* const-mp_limb_t* mp_limb_t",
        "x, y, and z have $nd words. Result in z. Borrow bit is lost."],
    cmp => [
        "int <- const-mp_limb_t* const-mp_limb_t*",
        "x and y have $nd words. Return sign of difference x-y."],
    cmp_ui => [
        "int <- const-mp_limb_t* mp_limb_t",
        "x has $nd words. Return sign of difference x-y."],

    addmul1 => [
        "mp_limb_t <- mp_limb_t* const-mp_limb_t* mp_limb_t",
        "x has $nd words, z has $np1.\nPut (z+x*c) in z. Return carry bit."],
    addmul1_nc => [
        "mp_limb_t* const-mp_limb_t* mp_limb_t",
        "x has $nd words, z has $np1.\nPut (z+x*c) in z. Carry bit is lost."],
    addmul1_shortz => [
        "mp_limb_t <- mp_limb_t* const-mp_limb_t* mp_limb_t",
        "x has $nd words, z has $n.\nPut (z+x*c) in z. Return carry word."],

    mul => [
        "mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x and y have $nd words, z has $nn1. Put x*y in z."],

    sqr => [
        "mp_limb_t* const-mp_limb_t*",
        "x has $nd words, z has $nn1. Put x*y in z."],

    mul1 => [
        "mp_limb_t* const-mp_limb_t* mp_limb_t",
        "x has $nd words, z has $np1. Put x*y in z."],

    shortmul => [
        "mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x and y have $nd words, z has $n.\nPut the low $n words of x*y in z."],
    );

    if ($nhw =~ /_5$/) {
        # We have a collection of mul05 routines, too
        push @meta_api, (
        addmul05 => [
            "mp_limb_t <- mp_limb_t* const-mp_limb_t* mp_limb_t",
            "x has $nd words, z has $n. c is 0.5 word.\n".
            "Put (z+x*c) in z. Return carry bit."],
        addmul05_nc => [
            "mp_limb_t* const-mp_limb_t* mp_limb_t",
            "x has $nd words, z has $n. c is 0.5 word.\n".
            "Put (z+x*c) in z. Carry bit is lost."],
        mul05 => [
            "mp_limb_t* const-mp_limb_t* mp_limb_t",
            "x has $nd words, z has $n. c is 0.5 word.\n".
            "Put (x*c) in z. No carry."],
        );
    }

    # Now we have functions which depend on a prime p. Documenting these
    # properly is important, as there are some assumptions on
    # the relative magnitude of p and the numbers considered, and it's
    # bad to have them implicit. So we spell things out here.
    
    push @meta_api, (
    mod => [
        "mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x has $nn1 words. z and p have $nd words, and the high word of p is non-zero.\n".
        "Put x mod p in z."],

    invmod => [
        "int <- mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x, z, and p have $nd words. Put inverse of x mod p in z.\n".
        "Return non-zero if an inverse could be found.\n".
        "If no inverse could be found, return 0 and set z to zero.\n"
    ],

    redc => [
        "mp_limb_t* mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x has $nn1 words, z and p have $nd words.\n".
        "only one word is read from invp.\n".
        "Assuming R=W^$n is the redc modulus, we expect that x verifies:\n".
        "  x < R*p,\n".
        "so that we have eventually z < p, z congruent to x/R mod p.\n" .
        "The contents of the area pointed by x are clobbered by this call.\n" .
        "Note also that x may alias z.\n"
    ],
    mulredc => [
        "mp_limb_t* const-mp_limb_t* const-mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x, y, z and p have $nd words.\n".
        "only one word is read from invp.\n".
        "We expect that x and y are both < p.\n".
        "Assuming R=W^$n is the redc modulus, we return x*y/R mod p in z"
    ],

    redc_ur => [
        "mp_limb_t* mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x has $nn1p1 words, z and p have $nd words.\n".
        "only one word is read from invp.\n".
        "Assuming R=W^$n is the redc modulus, we expect that x verifies:\n".
        " x < W*W^$nd*p = W^0.5*R*p or the hw case, W*R*p otherwise,\n".
        "so that we have eventually z < p, z congruent to x/R mod p.\n" .
        "The contents of the area pointed by x are clobbered by this call.\n" .
        "Note also that x may alias z.\n"
    ],

    mgy_encode => [
        "mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x, z, and p have $nd words.\n".
        "Assuming R=W^$n is the redc modulus, we compute z=R*x mod p."
    ],

    mgy_decode => [
        "mp_limb_t* const-mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
        "x, z, invR, and p have $nd words.\n".
        "Assuming R=W^$n is the redc modulus, we compute z=x/R mod p."
    ],

    lshift => [
        "mp_limb_t* int",
        "a has $nd words. Shift it in place by cnt bits to the left.\n".
        "The shift count cnt must not exceed the word size.\n".
        "Note that no carry is returned for the bits shifted out."],
    rshift => [
        "mp_limb_t* int",
        "a has $nd words. Shift it in place by cnt bits to the right.\n".
        "The shift count cnt must not exceed the word size.\n".
        "Note that no carry is returned for the bits shifted out."],

    long_lshift => [
        "mp_limb_t* int int",
        "a has $nd words. Shift it in place by off words plus cnt bits to the left.\n".
        "Note that no carry is returned for the bits shifted out."],

    long_rshift => [
        "mp_limb_t* int int",
        "a has $nd words. Shift it in place by off words plus cnt bits to the left.\n".
        "Note that no carry is returned for the bits shifted out."],
    );


    my $api = {};
    while (scalar @meta_api) {
        my $f = shift @meta_api;
        my $a = shift @meta_api;
        push @{$api->{'order'}}, $nhw . "_" . $f . "\@_fixmp_$f";
        $api->{'functions'}->{$nhw . "_" . $f} = parse_api_rhs(undef, $a);
    };

    return $api;
}

MAIN: {
    # Deal with command-line arguments
    my $tag = 'fixmp';
    my $output_path=".";

    while (scalar @ARGV) {
        $_ = $ARGV[0];
        if (/^output_path=(.*)$/) { $output_path=$1; shift @ARGV; next; }
        if (/^-d=?(\d+)$/) { $debuglevel=$1; shift @ARGV; next; }
        if (/^-d$/) { $debuglevel++; shift @ARGV; next; }
        last;
    }

    # Build an ad-hoc API.
    my @fixmp_functions=qw/
            add add_ui add_nc add_ui_nc
            sub sub_ui sub_nc sub_ui_nc
            cmp cmp_ui
            addmul1
            addmul1_nc
            addmul1_shortz
            mul
            mul1
            sqr
            shortmul
            mod
            redc
            redc_ur
            mgy_encode
            mgy_decode
            lshift long_lshift
            rshift long_rshift
            invmod
            mulredc
            /;
    my @fixmp_hw_more_functions=qw/
            addmul05
            addmul05_nc
            mul05
            /;

    my $api = {};
    for my $n (1..15) {
        my $a = fixmp_api($n);
        # merge with existing.
        push @{$api->{'order'}}, @{$a->{'order'}};
        $api->{'functions'}->{$_} = $a->{'functions'}->{$_} for keys %{$a->{'functions'}};
    }
    for my $n (0..14) {
        my $a = fixmp_api($n . "_5");
        # merge with existing.
        push @{$api->{'order'}}, @{$a->{'order'}};
        $api->{'functions'}->{$_} = $a->{'functions'}->{$_} for keys %{$a->{'functions'}};
    }

    # generate x86_64
    {
        my $options = { w=>64, tag=>$tag,
            features=>{ gcc_inline_assembly => 1, } };
        # my $api = { order => eval {my @x=@order; \@x;}, };
        my $code = {
            includes => [ qw{ <gmp.h> <limits.h> } ],
            # prefix => 'fixmp_',
            filebase => "mpfq_${tag}_x86_64",
        };
        @Mpfq::fixmp::parents=qw/ Mpfq::fixmp::x86_64/;
        my $object = Mpfq::fixmp->new();
        $object->create_code(dclone($api), $code, $options);
    
        $object->document_all_functions_in_source();
        $object->trigger_hashdefine_for_functions('native_');


        $object->create_files($output_path, $tag);
    }

    # generate i386
    {
        my $options = { w=>32, tag=>$tag,
            features=>{ gcc_inline_assembly => 1, } };
        # my $api = { order => eval {my @x=@order; \@x;}, };
        my $code = {
            includes => [ qw{ <gmp.h> <limits.h> } ],
            # prefix => 'fixmp_',
            filebase => "mpfq_${tag}_i386",
        };
        @Mpfq::fixmp::parents=qw/ Mpfq::fixmp::i386/;
        my $object = Mpfq::fixmp->new();
        $object->create_code(dclone($api), $code, $options);
    
        $object->document_all_functions_in_source();
        $object->trigger_hashdefine_for_functions('native_');

        $object->create_files($output_path, $tag);
    }

    # generate longlong
    {
        my $options = { w=>0, tag=>$tag, features => {} };
        # my $api = { order => eval {my @x=@order; \@x;}, };
        my $code = {
            includes => [ qw{ <gmp.h> <limits.h> } ],
            # prefix => 'fixmp_',
            filebase => "mpfq_${tag}_longlong",
        };
        @Mpfq::fixmp::parents=qw/ Mpfq::fixmp::longlong/;
        my $object = Mpfq::fixmp->new();

        $object->create_code(dclone($api), $code, $options);
    
        $object->document_all_functions_in_source();
        $object->trigger_hashif_for_functions( sub { return "!defined(HAVE_native_@!" . shift(@_) . ")"; });
        # print STDERR Dumper($object->{'code'}->{'1_5_addmul05_nc'});


        $object->create_files($output_path, $tag);
    }
}
