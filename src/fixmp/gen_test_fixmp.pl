#!/usr/bin/perl

use strict;
use warnings;

# See "COMMON INPUT DATA" below wrt what are x, y, z, and wx
sub test_fixmp_add { # {{{ add and add_nc
    my ($n, $P) = @_;
    return <<EOF;
    // add
    u[$n] = 0xdeadbeef;
    v[$n] = 0xdeadbeef;
    c1 = mpn_add_n(s, x, y, $n);
    c2 = @!${P}_add(u, x, y);
    @!${P}_add_nc(v, x, y);
    assert (u[$n] == 0xdeadbeef);
    assert (v[$n] == 0xdeadbeef);
    assert (c1 == c2);
    assert (mpn_cmp(s,u,$n) == 0);
    assert (mpn_cmp(s,v,$n) == 0);
    /* check aliasing x==y */
    c1 = mpn_add_n(s, x, x, $n);
    c2 = @!${P}_add(u, x, x);
    @!${P}_add_nc(v, x, x);
    assert (c1 == c2);
    assert (mpn_cmp(s,u,$n) == 0);
    assert (mpn_cmp(s,v,$n) == 0);
    /* check aliasing z==x */
    c1 = mpn_add_n(s, x, y, $n);
    mpfq_copy(u, x, $n);
    mpfq_copy(v, x, $n);
    c2 = @!${P}_add(v, v, y);
    @!${P}_add_nc(u, u, y);
    assert (c1 == c2);
    assert (mpn_cmp(s,v,$n) == 0);
    assert (mpn_cmp(s,u,$n) == 0);
EOF
}
# }}}
sub test_fixmp_sub { # {{{ sub and sub_nc
    my ($n, $P) = @_;
    return <<EOF;
    // sub
    u[$n] = 0xdeadbeef;
    v[$n] = 0xdeadbeef;
    c1 = mpn_sub_n(s, x, y, $n);
    c2 = @!${P}_sub(u, x, y);
    @!${P}_sub_nc(v, x, y);
    assert (u[$n] == 0xdeadbeef);
    assert (v[$n] == 0xdeadbeef);
    assert (c1 == c2);
    assert (mpn_cmp(s,u,$n) == 0);
    assert (mpn_cmp(s,v,$n) == 0);
    /* check aliasing x==y */
    c1 = mpn_sub_n(s, x, x, $n);
    c2 = @!${P}_sub(u, x, x);
    @!${P}_sub_nc(v, x, x);
    assert (c1 == c2);
    assert (mpn_cmp(s,u,$n) == 0);
    assert (mpn_cmp(s,v,$n) == 0);
    /* check aliasing z==x */
    c1 = mpn_sub_n(s, x, y, $n);
    mpfq_copy(u, x, $n);
    mpfq_copy(v, x, $n);
    c2 = @!${P}_sub(v, v, y);
    @!${P}_sub_nc(u, u, y);
    assert (c1 == c2);
    assert (mpn_cmp(s,v,$n) == 0);
    assert (mpn_cmp(s,u,$n) == 0);
EOF
}
# }}}

sub test_fixmp_add_ui { # {{{ add_ui and add_ui_nc
    my ($n, $P) = @_;
    return <<EOF;
    // add_ui
    u[$n] = 0xdeadbeef;
    c1 = mpn_add_1(s, x, $n, wx);
    c2 = @!${P}_add_ui(u, x, wx);
    @!${P}_add_ui_nc(v, x, wx);
    assert (u[$n] == 0xdeadbeef);
    assert (c1 == c2);
    assert (mpn_cmp(s,u,$n) == 0);
    assert (mpn_cmp(s,v,$n) == 0);
    /* check aliasing z==x */
    mpfq_copy(u, x, $n);
    mpfq_copy(v, x, $n);
    c1 = mpn_add_1(s, x, $n, wx);
    c2 = @!${P}_add_ui(u, u, wx);
    @!${P}_add_ui_nc(v, v, wx);
    assert (c1 == c2);
    assert (mpn_cmp(s,u,$n) == 0);
    assert (mpn_cmp(s,v,$n) == 0);
EOF
}
# }}}
sub test_fixmp_sub_ui { # {{{ sub_ui and sub_ui_nc
    my ($n, $P) = @_;
    return <<EOF;
    // sub_ui
    u[$n] = 0xdeadbeef;
    c1 = mpn_sub_1(s, x, $n, wx);
    c2 = @!${P}_sub_ui(u, x, wx);
    @!${P}_sub_ui_nc(v, x, wx);
    assert (u[$n] == 0xdeadbeef);
    assert (c1 == c2);
    assert (mpn_cmp(s,u,$n) == 0);
    assert (mpn_cmp(s,v,$n) == 0);
    /* check aliasing z==x */
    mpfq_copy(u, x, $n);
    mpfq_copy(v, x, $n);
    c1 = mpn_sub_1(s, x, $n, wx);
    c2 = @!${P}_sub_ui(u, u, wx);
    @!${P}_sub_ui_nc(v, v, wx);
    assert (c1 == c2);
    assert (mpn_cmp(s,u,$n) == 0);
    assert (mpn_cmp(s,v,$n) == 0);
EOF
}
# }}}

sub test_fixmp_addmul1 { # {{{ addmul1 addmul1_nc addmul1_shortz
    my ($n, $P) = @_;
    my $hw = $P =~ /_5$/;
    my $nn = 2*$n;
    my $nm1 = $n-1;
    my $np1 = $n+1;
    my $code = <<EOF;
    // addmul1
    mpfq_copy(s, z, $np1);
    mpfq_copy(u, z, $np1);
    mpfq_copy(v, z, $np1);
    mpfq_copy(w, z, $np1);
    c1 = mpn_addmul_1(s, x, $n, wx);
    s[$n] += c1;
    c3 = s[$n] < c1;
    u[$np1]=0xdeadbeef;
    v[$n]=0xdeadbeef;
    w[$np1]=0xdeadbeef;
    c4 = @!${P}_addmul1(u, x, wx);
    c2 = @!${P}_addmul1_shortz(v, x, wx);
    @!${P}_addmul1_nc(w, x, wx);
    assert (u[$np1] == 0xdeadbeef);
    assert (v[$n] == 0xdeadbeef);
    assert (w[$np1] == 0xdeadbeef);
    assert (mpn_cmp(s, u, $np1) == 0);
    assert (mpn_cmp(s, v, $n) == 0);
    assert (mpn_cmp(s, w, $np1) == 0);
    assert (c1 == c2);
    assert (c3 == c4);
    /* check aliasing z==x */
    mpfq_copy(s, x, $n);
    mpfq_copy(u, x, $n);
    c1 = mpn_addmul_1(s, s, $n, wx);
    c2 = @!${P}_addmul1_shortz(u, u, wx);
    assert (mpn_cmp(s, u, $n) == 0);
    assert (c1 == c2);
EOF
    if ($hw) {
        $code .= <<EOF;
        // addmul05
        mpfq_copy(s, z, $n);
        mpfq_copy(u, z, $n);
        mpfq_copy(w, z, $n);
        c1 = mpn_addmul_1(s, x, $n, y[$nm1]);
        u[$n]=0xdeadbeef;
        w[$n]=0xdeadbeef;
        c2 = @!${P}_addmul05(u, x, y[$nm1]);
        @!${P}_addmul05_nc(w, x, y[$nm1]);
        assert (u[$n] == 0xdeadbeef);
        assert (w[$n] == 0xdeadbeef);
        assert (mpn_cmp(s, u, $n) == 0);
        assert (mpn_cmp(s, w, $n) == 0);
        assert (c1 == c2);
        /* check aliasing z==x */
        mpfq_copy(s, x, $n);
        mpfq_copy(u, x, $n);
        c1 = mpn_addmul_1(s, s, $n, y[$nm1]);
        c2 = @!${P}_addmul05(u, u, y[$nm1]);
        assert (mpn_cmp(s, u, $n) == 0);
        assert (c1 == c2);
EOF
    }
    return $code;
}
# }}}

sub test_fixmp_mul1 { # {{{
    my ($n, $P) = @_;
    my $hw = $P =~ /_5$/;
    my $nn = 2*$n;
    my $nm1 = $n-1;
    my $np1 = $n+1;
    my $code .= <<EOF;
    // mul1
    s[$n] = mpn_mul_1(s, x, $n, wx);
    u[$np1] = 0xdeadbeef;
    @!${P}_mul1(u, x, wx);
    assert (u[$np1] == 0xdeadbeef);
    assert (mpn_cmp(s,u,$np1) == 0);
    /* check aliasing z==x */
    mpfq_copy(t, x, $n);
    mpfq_copy(v, x, $n);
    t[$n] = mpn_mul_1(t, t, $n, wx);
    @!${P}_mul1(v, v, wx);
    assert (mpn_cmp(t, v, $n + 1) == 0);
EOF
    if ($hw) {
        $code .= <<EOF;
        // mul05
        s[$n] = mpn_mul_1(s, x, $n, y[$nm1]);
        u[$n] = 0xdeadbeef;
        @!${P}_mul05(u, x, y[$nm1]);
        assert (u[$n] == 0xdeadbeef);
        assert (mpn_cmp(s,u,$n) == 0);
        assert (s[$n] == 0);
        /* check aliasing z==x */
        mpfq_copy(t, x, $n);
        mpfq_copy(v, x, $n);
        t[$n] = mpn_mul_1(t, t, $n, y[$nm1]);
        @!${P}_mul05(v, v, y[$nm1]);
        assert (mpn_cmp(t, v, $n) == 0);
EOF
    }
    return $code;
}
# }}}

sub test_fixmp_mul { # {{{
    my ($n, $P) = @_;
    my $hw = $P =~ /_5$/;
    my $nn = 2*$n;
    $nn-- if $hw;
    # no operand aliasing is allowed here.
    my $code = <<EOF;
    // mul
    mpn_mul_n(s, x, y, $n);
    u[$nn]=0xdeadbeef;
    @!${P}_mul(u, x, y);
    assert (u[$nn]==0xdeadbeef);
    assert (mpn_cmp(s,u,$nn) == 0);
EOF
    $code .= "assert (s[$nn] == 0);\n" if $hw;
    return $code;
}
# }}}

sub test_fixmp_shortmul { # {{{
    my ($n, $P) = @_;
    # no operand aliasing is allowed here.
    return <<EOF;
    // shortmul
    mpn_mul_n(s, x, y, $n);
    u[$n] = 0xdeadbeef;
    @!${P}_shortmul(u, x, y);
    assert (u[$n] == 0xdeadbeef);
    assert (mpn_cmp(s, u, $n) == 0);
EOF
}
# }}}

sub test_fixmp_sqr { # {{{
    my ($n, $P) = @_;
    # no operand aliasing is allowed here.
    my $hw = $P =~ /_5$/;
    my $nn = 2*$n;
    $nn-- if $hw;
    my $code = <<EOF;
    // sqr
    mpn_mul_n(s, x, x, $n);
    u[$nn]=0xdeadbeef;
    @!${P}_sqr(u, x);
    assert (u[$nn]==0xdeadbeef);
    assert (mpn_cmp(s,u,$nn) == 0);
EOF
    $code .= "assert (s[$nn] == 0);\n" if $hw;
    return $code;
}
# }}}

sub test_fixmp_cmp { # {{{
    my ($n, $P) = @_;
    my $nm1 = $n-1;
    my $code = <<EOF;
    // cmp
    j = mpn_cmp(x, y, $n);
    k = @!${P}_cmp(x, y);
    assert (j==k);
    mpfq_copy(u, x, $n);
    j = @!${P}_cmp(x, u);
    assert (j==0);
    mpfq_zero(u+1, $nm1);
    j = @!${P}_cmp_ui(u, x[0]);
    assert (j==0);
    j = @!${P}_cmp_ui(u, ~x[0]);
    assert (j!=0);
EOF
    if ($n > 1) {
        $code .= <<EOF;
    u[1]=1;
    j = @!${P}_cmp_ui(u, x[0]);
    assert (j>0);
EOF
    }
    return $code;
}
# }}}

sub test_fixmp_mod { # {{{
    my ($n, $P) = @_;
    my $hw = $P =~ /_5$/;
    my $np1 = $n+1;
    my $nn = 2*$n;
    $nn-- if $hw;
    return <<EOF;
    // mod
    mpfq_copy(v, y, $n);
    v[$n-1] += !v[$n-1];
    mpn_tdiv_qr(s+$n, s, 0, z, $nn, v, $n);
    @!${P}_mod(u, z, v);
    assert(mpn_cmp(s, u, $n) == 0);
EOF
}
# }}}

sub test_fixmp_inv { # {{{
    my ($n, $P) = @_;
    return <<EOF;
    // inv
    @!${P}_mod(u, z, P);
    u[0] += !u[0];
    @!${P}_invmod(v, u, P);
    @!${P}_invmod(v, v, P);
    assert(mpn_cmp(v, u, $n) == 0);
    /* also test the degenerate case invmod(0) or invmod(non invertible) */
    mpfq_zero(u, $n);
    mpfq_zero(v, $n);
    v[0] = ~0UL;
    c1 = @!${P}_invmod(v, u, P);
    assert(c1 == 0);
    assert(v[0] == 0);
    v[0] = ~0UL;
    c1 = @!${P}_invmod(v, P, P);
    assert(c1 == 0);
    assert(v[0] == 0);
    /* non invertible ; We'll make it slightly artificial */
    mpfq_zero(u, $n);
    memset(u, ~0, $n * sizeof(mp_limb_t)); /* multiple of 257 */
    mpfq_copy(v, x, $n);
    /* This creates an n-limb multiple of 257.  */
    v[$n] = mpn_lshift(v, x, $n, 8);
    v[$n] += @!${P}_add(v, v, x);
    /* the add_ui below can't overflow since it would imply (for k=257)
     * k*x + cy >= k*2^(n*w).
     * Since cy <= k-1 and x <= 2^(n*w)-1, that can't be. */
    /* note though that v == 2^(n*w)-1 is possible... */
    @!${P}_add_ui(v, v, v[$n]);
    w[0] = 1UL;
    c1 = @!${P}_invmod(w, v, u);
    assert(c1 == 0);
    assert(mpn_mod_1(w, $n, 257) == 0);
EOF
}
# }}}

sub test_fixmp_redc { # {{{ tests mgy_{en,de}code redc redc_ur
    my ($n, $P) = @_;
    my $hw = $P =~ /_5$/;
    my $nn = 2*$n;
    my $nn1 = $nn;
    $nn1-- if $hw;
    my $n1 = $n + 1;
    $n1-- if $hw;
    my $code = <<EOF;
    //redc
    {
      mp_limb_t p[$n], mip[$n];
      mp_limb_t xe[$n], ye[$n], ze[$n];
      mp_limb_t invR[$n];
      
      // x[$n-1] &= (1UL<<(GMP_LIMB_BITS-1)) -1;
      // y[$n-1] &= (1UL<<(GMP_LIMB_BITS-1)) -1;
      mpfq_zero(mip, $n);
      mpn_random2(p, $n);
      p[$n-1] &= (1UL<<(GMP_LIMB_BITS-1)) -1;
      if (x[$n-1] > p[$n-1])
        p[$n-1] = x[$n-1];
      if (y[$n-1] > p[$n-1])
        p[$n-1] = y[$n-1];
      p[0] |= 1UL;
EOF
    $code .= "p[$n-1] &= (1UL<<(GMP_LIMB_BITS>>1))-1;\n" if $hw;
    $code .= <<EOF;
      p[$n-1] += !p[$n-1];
EOF
    if ($P ne '0_5') {
        $code .= <<EOF;
      mpfq_zero(w, 2*$n);
      w[$n]=1;
      @!${P}_mod(w, w, p);
      @!${P}_invmod(invR, w, p);
EOF
    } else {
        # 0.5 case is a bit tricky, since {0,1} is beyond what _mod takes.
        $code .= <<EOF;
        mpfq_zero(w, 2*$n);
        w[0] = 1UL << (GMP_LIMB_BITS>>1);
        @!${P}_mod(w, w, p);
        @!${P}_mul(v, w, w);
        @!${P}_mod(w, v, p);
        @!${P}_invmod(invR, w, p);
EOF
    }
    $code .= <<EOF;
      // compute inverse of opposite of p mod R, 
      // with iterated  x <- x + x*(p*x+1) mod R
      mip[0] = 1UL;
      do{ 
        mpfq_copy(v, mip, $n);
        @!${P}_shortmul(t, mip, p);
	mpn_add_1(t, t, $n, 1);
	@!${P}_shortmul(u, t, mip);
	@!${P}_add(mip, u, mip);
      } while (mpn_cmp(v, mip, $n));
      @!${P}_mgy_encode(xe, x, p);
      @!${P}_mgy_encode(ye, y, p);
      // encode x*y mod p
      @!${P}_mul(s, x, y);
      @!${P}_mod(t, s, p);
      @!${P}_mgy_encode(ze, t, p);
      // do the product in Mgy form
      @!${P}_mul(s, xe, ye);
      @!${P}_mod(t, s, p);
      @!${P}_mgy_decode(u, t, invR, p);
      assert(mpn_cmp(ze, u, $n) == 0);
      @!${P}_redc(t, s, mip, p);
      assert(mpn_cmp(ze, t, $n) == 0);
      /* test redc_ur too.
       * We should be able to accumulate many additions here.
       * Let's cheat a bit, and accumulate a number of additions which
       * is simulated. */
      @!${P}_mul(t, xe, ye);
      s[$nn1] = mpn_mul_1(s, t, $nn1, wx);
      mpfq_zero(t, 2*$n+1);
      @!${P}_redc_ur(t, s, mip, p); /* input is 2n+1-hw words */
      @!${P}_mod(s, t, p);
      @!${P}_mul1(w, ze, wx);       /* output is n+1-hw/2 words == n+1 */
      /* we can't do @!${P}_mod(v, w, p) because that would only eat 2n-hw
       * input limbs, and we want n+1 (which is larger for n=1 and hw=1) */
      mpn_tdiv_qr(v+$n, v, 0, w, $n+1, p, $n);
      assert(mpn_cmp(v, s, $n) == 0);
      /* Do the same for something which is carry-friendly */
      mp_limb_t sat = wx;
      /* Saturate the high bits of the random word. This is a very strong
       * degradation of randomness, but its intent is to pinpoint corner
       * cases (we could as well set sat=~0UL, that is). */
      for(j = GMP_LIMB_BITS >> 1 ; j >= 2 ; j >>= 1) {
        sat |= sat << j;
      }
      /* first for plain redc */
      for(int i = 0 ; i < $nn1 ; u[i++] = 1) ;
      mpn_mul_1(v, u, $nn1, sat);
      @!${P}_redc(s, u, mip, p);
      @!${P}_mul1(w, s, sat);
      mpn_tdiv_qr(s+$n, s, 0, w, $n+1, p, $n);
      mpfq_zero(w, 2*$n+1);
      @!${P}_redc(w, v, mip, p);
      mpn_tdiv_qr(t+$n, t, 0, w, $n+1, p, $n);
      assert(mpn_cmp(s, t, $n) == 0);
      /* then for redc_ur */
      for(int i = 0 ; i < $nn1 + 1 ; u[i++] = 1) ;
      mpn_mul_1(v, u, $nn1 + 1, sat);
      @!${P}_redc_ur(s, u, mip, p);
      @!${P}_mul1(w, s, sat);
      mpn_tdiv_qr(s+$n, s, 0, w, $n+1, p, $n);
      mpfq_zero(w, 2*$n+1);
      @!${P}_redc_ur(w, v, mip, p);
      mpn_tdiv_qr(t+$n, t, 0, w, $n+1, p, $n);
      assert(mpn_cmp(s, t, $n) == 0);
#ifdef  HAVE_native_${P}_mulredc
      @!${P}_mul(u, x, y);
      @!${P}_redc(s, u, mip, p);
      @!${P}_mulredc(t, x, y, mip, p);
      assert(mpn_cmp(s, t, $n) == 0);
#endif
    }
EOF
}
# }}}

sub test_fixmp_lshift { # {{{
    my ($n, $P) = @_;
    return <<EOF;
    // lshift
    j = wx % GMP_LIMB_BITS;
    if (j) 
        mpn_lshift(s, x, $n, j);
    else
        mpfq_copy(s, x, $n);
    mpfq_copy(u, x, $n);
    @!${P}_lshift(u, j);
    assert (mpn_cmp(s, u, $n) == 0);
EOF
}
# }}}

sub test_fixmp_rshift { # {{{
    my ($n, $P) = @_;
    return <<EOF;
    // lshift
    j = wx % GMP_LIMB_BITS;
    if (j)
        mpn_rshift(s, x, $n, j);
    else
        mpfq_copy(s, x, $n);
    mpfq_copy(u, x, $n);
    @!${P}_rshift(u, j);
    assert (mpn_cmp(s, u, $n) == 0);
EOF
}
# }}}

sub test_fixmp_long_lshift { # {{{
    my ($n, $P) = @_;
    return <<EOF;
    // long_lshift
    j = wx % ($n * GMP_LIMB_BITS);
    mpfq_zero(s, $n);
    k = j / GMP_LIMB_BITS;
    j = j % GMP_LIMB_BITS;
    if (j)
        mpn_lshift(s + k, x, $n - k, j);
    else
        mpfq_copy(s + k, x, $n - k);
    mpfq_copy(u, x, $n);
    @!${P}_long_lshift(u, k, j);
    assert (mpn_cmp(s, u, $n) == 0);
EOF
}
# }}}

sub test_fixmp_long_rshift { # {{{
    my ($n, $P) = @_;
    return <<EOF;
    // long_rshift
    j = wx % ($n * GMP_LIMB_BITS);
    mpfq_zero(s, $n);
    k = j / GMP_LIMB_BITS;
    j = j % GMP_LIMB_BITS;
    if (j)
        mpn_rshift(s, x + k, $n - k, j);
    else
        mpfq_copy(s, x + k, $n - k);
    mpfq_copy(u, x, $n);
    @!${P}_long_rshift(u, k, j);
    assert (mpn_cmp(s, u, $n) == 0);
EOF
}
# }}}

# This wraps everything together.
sub test_fixmp_code { # {{{
    my ($n, $P, $p32, $p64) = @_;
    my $na = 2*$n + 1;
    my $code = <<EOF;
void test_fixmp_$P() {
  mp_limb_t s[$na];
  mp_limb_t t[$na];
  mp_limb_t u[$na];
  mp_limb_t v[$na];
  mp_limb_t w[$na];
  mp_limb_t c1, c2, c3, c4;
#if GMP_LIMB_BITS == 32
  mp_limb_t P[$n] = $p32;
#elif GMP_LIMB_BITS == 64
  mp_limb_t P[$n] = $p64;
#endif
  int j, k;
EOF
    # memcpy(z, y+17, 10*sizeof(mp_limb_t);
    # mpfq_copy(t, z, 10);

    $code .= test_fixmp_add($n, $P);      # add add_nc
    $code .= test_fixmp_add_ui($n, $P);   # add_ui add_ui_nc
    $code .= test_fixmp_sub($n, $P);      # sub sub_nc
    $code .= test_fixmp_sub_ui($n, $P);   # sub_ui sub_ui_nc

    $code .= test_fixmp_addmul1($n, $P);
    $code .= test_fixmp_mul1($n, $P);
    $code .= test_fixmp_mul($n, $P);
    $code .= test_fixmp_shortmul($n, $P);
    $code .= test_fixmp_sqr($n, $P);
    $code .= test_fixmp_cmp($n, $P);
    $code .= test_fixmp_mod($n, $P);
    $code .= test_fixmp_inv($n, $P);
    $code .= test_fixmp_redc($n, $P);
    $code .= test_fixmp_lshift($n, $P);
    $code .= test_fixmp_rshift($n, $P);
    $code .= test_fixmp_long_lshift($n, $P);
    $code .= test_fixmp_long_rshift($n, $P);

    $code .= "}\n";
    return $code;
}
# }}}




print <<EOF;
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <string.h>

/* The tests are based on assert() */
#ifdef NDEBUG
#  undef NDEBUG
#endif

#include <assert.h>
#include "mpfq_fixmp.h"

EOF

my $primes64 = {
1 => "{4929763703639915597UL}",
2 => "{14425133756266440979UL, 7028776506806380750UL}",
3 => "{9758664018554848775UL, 108797327114284110UL, 3855934483758865187UL}",
4 => "{12011675740079661751UL, 3294090837287775300UL, 9673935898323528142UL, 6244774036521541631UL }",
5 => "{11766063743839433833UL, 17023338808517031849UL, 6384879829007101141UL, 9814014250957810811UL,5856459693223253397UL }",
6 => "{9332489496020345727UL, 13059118375404545793UL, 543826843599586942UL, 568657921352937073UL, 8714542686157595041UL, 8377129812810584371UL }",
7 => "{8305431681600953837UL, 8511912794376737076UL, 5827616680491403508UL, 11764963549898802560UL, 9952224619298044241UL, 2593919323804169004UL, 5707166315511930231UL }",
8 => "{16672777963903890445UL, 14321543724342516978UL, 5190009058579841038UL, 16894467406687282692UL, 5579682454395466331UL, 3120279582727612446UL, 2933066969036697885UL, 2125779597467003446UL }",
9 => "{6272071724723397689UL, 7097496403184731472UL, 6722451164852552420UL, 2557895735561628759UL, 11466998160538807963UL, 18232042263112599551UL, 4641538801156436724UL, 16426483130014462608UL, 7262099965674661736UL }",
"0_5" => "{665100797UL }",
"1_5" => "{3107852032399716593UL, 561560781UL }",
"2_5" => "{3022170862732092607UL, 2943310936735432501UL, 2389540284UL }",
"3_5" => "{3365129921614404491UL, 3435152464474079436UL, 15222848675840898267UL, 757047498UL }",
"4_5" => "{7237051979824649275UL, 4624810970717405228UL, 1884741607445120980UL, 13362986272152613141UL, 4289448930UL }",
"5_5" => "{6517825047235477399UL, 10380803732985570388UL, 10617540373906134554UL, 9832882603074502134UL, 17768816977972787415UL, 3347008897UL }",
"6_5" => "{14474939553991838547UL, 13588299506894413430UL, 2138988088321967398UL, 4885350755630930435UL, 11400929609603244885UL, 9580305905884609731UL, 3424080812UL }",
"7_5" => "{16041218152472538879UL, 7399024743779162188UL, 13475004769486012038UL, 11599672397031013245UL, 8334617143536304840UL, 914620958498863738UL, 7941356306452725792UL, 2100729329UL }",
"8_5" => "{9735718323941594257UL, 17406359535604746461UL, 12157468420192567345UL, 1042532625931608455UL, 13551854600515349299UL, 5602118475269555012UL, 9091328115459418722UL, 12828508824304788877UL, 3205924263UL }"
};

my $primes32 = {
1 => "{ 2041087589UL, }",
2 => "{ 1737731653UL, 3654705850UL, }",
3 => "{ 3826833745UL, 2279976717UL, 3984871455UL, }",
4 => "{ 3662469475UL, 2096692762UL, 4151755841UL, 4009865730UL, }",
5 => "{ 4034459419UL, 3797792253UL, 1419478273UL, 2675749510UL, 3664727098UL, }",
6 => "{ 2813719779UL, 3907769622UL, 704006380UL, 1485932037UL, 661860009UL, 2968664580UL, }",
7 => "{ 3784709369UL, 269443326UL, 4028649229UL, 2906318846UL, 1307656400UL, 167308958UL, 3095675918UL, }",
8 => "{ 4093166397UL, 205402748UL, 1827875733UL, 2591432089UL, 498572719UL, 2575114975UL, 3040974997UL, 3977792999UL, }",
9 => "{ 3897809411UL, 1993283498UL, 867915630UL, 886471665UL, 3987868346UL, 2967702854UL, 1194285669UL, 1588068146UL, 928806807UL, }",
"0_5" => "{ 51157UL }",
"1_5" => "{ 3980356625UL, 58899UL }",
"2_5" => "{ 2705370375UL, 3814976481UL, 12767UL }",
"3_5" => "{ 1824034149UL, 3737757448UL, 3181502520UL, 10185UL }",
"4_5" => "{ 855954195UL, 1867864209UL, 238425956UL, 1079706229UL, 46658UL }",
"5_5" => "{ 1374420825UL, 2881636544UL, 355812594UL, 997066857UL, 3035701496UL, 32232UL }",
"6_5" => "{ 4269688555UL, 2942999559UL, 634598091UL, 3827993518UL, 782471155UL, 4124854894UL, 45865UL }",
"7_5" => "{ 1034369339UL, 1441735391UL, 87723275UL, 1402427089UL, 2414785066UL, 3785364497UL, 3077063242UL, 5378UL }",
"8_5" => "{ 1576205429UL, 1707228548UL, 3046795374UL, 846023214UL, 3621694389UL, 53080496UL, 1314150003UL, 322039988UL, 42563UL }"
};

print <<EOF;
/* COMMON INPUT DATA
 * All test routines work on data areas whose name prescribes the length.
 * In order to provide fresh random inputs to all routines, we work with
 * read-only areas on input. These are all accessed via const pointers
 * which are global variables in the test file, and owned by main() only.
 * 
 * x, y are n-word long.
 * z is 2n-word long.
 * wx is a full limb.
 * 
 * For the hw routines, the following modifications apply:
 * 
 * the top word of x and y is a half-word.
 * z is only max(n+1, 2n-1)-word long.
 * in order to access a random half-limb, y[n-1] is recommended.
 * 
 * 
 * the output buffers are s, t, u, v, w. All have space allocated for 2n+1
 * words. s and t are generally used for the reference data computed by
 * gmp.
 */
const mp_limb_t * x, * y, * z;
mp_limb_t wx;
mp_limb_t * gx, * gy, * gz;



EOF

for my $i (1..9) {
    my $P = $i;
  my $c = test_fixmp_code($i, $P, $primes32->{$P}, $primes64->{$P});
  $c =~ s/@!/mpfq_fixmp_/g;
  print "$c\n\n";
}
for my $i (1..9) {
    my $P = $i-1;
    $P .= "_5";
  my $c = test_fixmp_code($i, $P, $primes32->{$P}, $primes64->{$P});
  $c =~ s/@!/mpfq_fixmp_/g;
  print "$c\n\n";
}

print <<EOF;



void do_test(int N, int k, int hw, void (*func)())
{
    int kk = 2*k-hw;
    if (kk < k+1) kk = k + 1;

    gx = (mp_limb_t *) malloc(k * sizeof(mp_limb_t));
    gy = (mp_limb_t *) malloc(k * sizeof(mp_limb_t));
    gz = (mp_limb_t *) malloc(2*k * sizeof(mp_limb_t));

    for(int i = 0 ; i < N ; i++) {
        mpn_random2(gx, k);
        mpn_random2(gy, k);
        mpn_random2(gz, kk);

        if (hw) {
            mp_limb_t hwmask = (1UL<<(GMP_LIMB_BITS>>1))-1;
            gx[k-1] &= hwmask; gy[k-1] &= hwmask;
            if (k>1) { gz[2*k-1] = 0xbada55; }
        }

        mpn_random2(&wx, 1);
        x=gx; y=gy; z=gz;
        (*func)();

        if (hw && k>1) { assert(gz[2*k-1] == 0xbada55); }
    }

    free(gx);
    free(gy);
    free(gz);
    printf("."); fflush(stdout);
}
EOF

print <<EOF;
int main(int argc, char **argv) {
  int k=100;

  if (argc==2) {
    // coverity[tainted_data_transitive]
    k = atoi(argv[1]);
  }

  do_test(k, 1, 0, &test_fixmp_1);
  do_test(k, 2, 0, &test_fixmp_2);
  do_test(k, 3, 0, &test_fixmp_3);
  do_test(k, 4, 0, &test_fixmp_4);
  do_test(k, 5, 0, &test_fixmp_5);
  do_test(k, 6, 0, &test_fixmp_6);
  do_test(k, 7, 0, &test_fixmp_7);
  do_test(k, 8, 0, &test_fixmp_8);
  do_test(k, 9, 0, &test_fixmp_9);
  do_test(k, 1, 1, &test_fixmp_0_5);
  do_test(k, 2, 1, &test_fixmp_1_5);
  do_test(k, 3, 1, &test_fixmp_2_5);
  do_test(k, 4, 1, &test_fixmp_3_5);
  do_test(k, 5, 1, &test_fixmp_4_5);
  do_test(k, 6, 1, &test_fixmp_5_5);
  do_test(k, 7, 1, &test_fixmp_6_5);
  do_test(k, 8, 1, &test_fixmp_7_5);
  do_test(k, 9, 1, &test_fixmp_8_5);
  printf("\\n");
  return 0;
}
EOF
