
#include "SpMV.h"



void RNSBasis_mpz_init (RnsBasis_MPZ * r,
						mpz_t q, 
						int moduli_nb, 
						int modulus_length,
						gmp_randstate_t rnd_state)
{
	printf("Initialising RNS basis ...\n");

	mpz_t temp_0, temp_1;
	mpz_init(temp_0);
	mpz_init(temp_1);

	int moduloIsValid;

	r->moduli_nb = moduli_nb;
	r->modulus_length = modulus_length;

	r->p = malloc(r->moduli_nb * sizeof(mpz_t));
	r->p_c = malloc(r->moduli_nb * sizeof(mpz_t));
	r->P_inv = malloc(r->moduli_nb * sizeof(mpz_t));
	r->P_p = malloc(r->moduli_nb * sizeof(mpz_t));
	r->P_p_inv = malloc(r->moduli_nb * sizeof(mpz_t));
	r->P_p_mod_q = malloc(r->moduli_nb * sizeof(mpz_t));
	r->alpha_P_mod_q = malloc((r->moduli_nb - 1) * sizeof(mpz_t));

	mpz_init(r->P);
	for (uint i = 0; i < r->moduli_nb; ++i) {
		mpz_init(r->p[i]);
		mpz_init(r->p_c[i]);
		mpz_init(r->P_inv[i]);
		mpz_init(r->P_p[i]);
		mpz_init(r->P_p_inv[i]);
		mpz_init(r->P_p_mod_q[i]);
	}

	for (unsigned int i = 0; i < r->moduli_nb - 1; ++i) {
		mpz_init(r->alpha_P_mod_q[i]);
	}

	mpz_ui_pow_ui (temp_0, 2, modulus_length);

	for (uint i = 0; i < r->moduli_nb; ++i) {
		moduloIsValid = 0;
		while (!moduloIsValid) {
	    	mpz_urandomb(r->p_c[i], rnd_state, 8);
	    	mpz_sub(r->p[i], temp_0, r->p_c[i]);
	    	moduloIsValid = 1;
	    	if (mpz_cmp_ui(r->p_c[i], 0) == 0)
				moduloIsValid = 0;
	    	for (uint j = 0; j < i; j++) {
				mpz_gcd(temp_1, r->p[i], r->p[j]);
				if (mpz_cmp_ui(temp_1, 1) != 0) {
					moduloIsValid = 0;
					break;
				}
	    	}	

	    	mpz_gcd( temp_1, r->p[i], q);
	    	if (mpz_cmp_ui(temp_1, 1) != 0) {
				moduloIsValid = 0;
	    	}
	    	if (mpz_divisible_ui_p(r->p[i], 2) != 0) {
				moduloIsValid = 0;
	    	}
		}
    }

	// all threads take the choice of thread 0 for p and p_c
	for (uint i = 0; i < r->moduli_nb; ++i) {
		ulong temp0;
		mpz_export (&temp0, NULL, -1, sizeof(ulong), -1, 0, r->p [i]);
		MPI_Bcast (&temp0, 1, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
		mpz_set_ui (r->p [i], temp0);
		mpz_export (&temp0, NULL, -1, sizeof(ulong), -1, 0, r->p_c [i]);
		MPI_Bcast (&temp0, 1, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
		mpz_set_ui (r->p_c [i], temp0);
	}

	mpz_set_ui (r->P, 1);
	for (uint i = 0; i < r->moduli_nb; ++i) {
		mpz_mul(r->P, r->P, r->p[i]);
	}

	mpz_t limit;
	mpz_init (limit);						

	mpz_mul (limit, q, temp_0);
	mpz_mul_ui (limit, limit, r->moduli_nb);

	for (unsigned int i = 0; i < r->moduli_nb; ++i) {
		mpz_divexact(r->P_p[i], r->P, r->p[i]);
		mpz_invert(r->P_inv[i], r->P_p[i], r->p[i]);
		mpz_mul(r->P_p_inv[i], r->P_inv[i], r->P_p[i]);
	}

	for (unsigned int i = 0; i < r->moduli_nb; ++i) {
		mpz_mod(r->P_p_mod_q[i], r->P_p[i], q);
	}

	for (unsigned int i = 0; i < r->moduli_nb - 1; i++) {
		mpz_mul_ui(r->alpha_P_mod_q[i], r->P, i + 1);
		mpz_mod(r->alpha_P_mod_q[i], r->alpha_P_mod_q[i], q);
	}

	printf("	Size of q : %zu bits\n", mpz_sizeinbase(q, 2));
	printf("	Length of RNS basis : %u (%u-bit moduli)\n", r->moduli_nb, r->modulus_length);
	printf("	Size of P : %zu bits\n", mpz_sizeinbase(r->P, 2));
	printf("	Size of limit : %zu bits\n", mpz_sizeinbase(limit, 2));

	nbSpMVsPerRed =	(uint) (((mpz_sizeinbase(r->P, 2) - 1) - mpz_sizeinbase(limit, 2)) / 9.22);
	printf("	#(SpMVs) / reduction : %u\n", nbSpMVsPerRed);
	nbMksolsPerRed = (uint) (((mpz_sizeinbase(r->P, 2) - 1) - mpz_sizeinbase(limit, 2)) / 10.22);
	printf("	#(Mksols) / reduction : %u\n", nbMksolsPerRed);

	gmp_printf("q = %Zd\n", q);

	mpz_clear(temp_0);
	mpz_clear(temp_1);
    
   	mpz_clear (limit);
}

void RNSBasis_mpz_clear (RnsBasis_MPZ * r)
{
	free (r->p);
	free (r->p_c);
	free (r->P_inv);
	free (r->P_p);
	free (r->P_p_inv);
	free (r->P_p_mod_q);
	free (r->alpha_P_mod_q);
}

///////////////////////////////////
// Arithmetic functions with AVX //
///////////////////////////////////

void RnsElement_AVX_init (RnsBasis_AVX r,
					  	  RnsElement_AVX * A_RNS)
{
	int x = posix_memalign ((void**)A_RNS, 32, (NB_RNS_LEAVES_AVX/4) * sizeof (__m256i) );
}					  

void RNSBasis_AVX_init (RnsBasis_MPZ r,
					    RnsBasis_AVX * r_AVX)
{
	r_AVX->moduli_nb = r.moduli_nb;
	r_AVX->modulus_length = r.modulus_length;

	float epsilon = 0;
	float delta = 0;
	int k = r.modulus_length;
	int s = 10;

	for (uint i = 0; i < r.moduli_nb; ++i) {
		uint p_c;
		mpz_export(&p_c, NULL, -1, sizeof(uint), -1, 0, r.p_c[i]);
		epsilon += (p_c);
	}
	epsilon = (float) (epsilon / pow(2, k));
	delta =	r.modulus_length * (float) ((float) (pow(2, k - s)) / pow(2, k));

	r_AVX->Delta = 0.001 + delta + epsilon;
	r_AVX->kMinuss = k - s;
	r_AVX->twoPowers = 2 << (s - 1);

	RnsElement_AVX_init (*r_AVX, &r_AVX->p);
	RnsElement_AVX_init (*r_AVX, &r_AVX->p_c_2);
	RnsElement_AVX_init (*r_AVX, &r_AVX->P_inv);
	
	int x = posix_memalign ((void**)&(r_AVX->P_p_mod_q), 32, r_AVX->moduli_nb * sizeof (RnsElement_AVX));	
	for (int i = 0; i < r_AVX->moduli_nb; ++i) {
		RnsElement_AVX_init (*r_AVX, r_AVX->P_p_mod_q + i);
	}
	
	x = posix_memalign ((void**)&(r_AVX->alpha_P_mod_q), 32, ((r_AVX->moduli_nb)-1) * sizeof (RnsElement_AVX));
	for (int i = 0; i < (r_AVX->moduli_nb)-1; ++i) {	
		RnsElement_AVX_init (*r_AVX, r_AVX->alpha_P_mod_q + i);
    }
    
	for (int i = 0; i < (r_AVX->moduli_nb/4); ++i) {
		r_AVX->p [i] [3] = mpz_get_si (r.p [4*i+3]);
		r_AVX->p [i] [2] = mpz_get_si (r.p [4*i+2]);
		r_AVX->p [i] [1] = mpz_get_si (r.p [4*i+1]);
		r_AVX->p [i] [0] = mpz_get_si (r.p [4*i]);
	}
    
	for (int i = 0; i < (r_AVX->moduli_nb/4); ++i) {
		r_AVX->p_c_2 [i] [3] = 2*mpz_get_si (r.p_c [4*i+3]);
		r_AVX->p_c_2 [i] [2] = 2*mpz_get_si (r.p_c [4*i+2]);
		r_AVX->p_c_2 [i] [1] = 2*mpz_get_si (r.p_c [4*i+1]);
		r_AVX->p_c_2 [i] [0] = 2*mpz_get_si (r.p_c [4*i]);	
	}
    
	for (int i = 0; i < (r_AVX->moduli_nb/4); ++i) {
		ulong temp0, temp1, temp2, temp3;
		mpz_export (&temp0, NULL, -1, sizeof(unsigned long), -1, 0, r.P_inv [4*i]);
		mpz_export (&temp1, NULL, -1, sizeof(unsigned long), -1, 0, r.P_inv [4*i+1]);
		mpz_export (&temp2, NULL, -1, sizeof(unsigned long), -1, 0, r.P_inv [4*i+2]);
		mpz_export (&temp3, NULL, -1, sizeof(unsigned long), -1, 0, r.P_inv [4*i+3]);
		r_AVX->P_inv [i] [0] = temp0;
		r_AVX->P_inv [i] [1] = temp1;
		r_AVX->P_inv [i] [2] = temp2;
		r_AVX->P_inv [i] [3] = temp3;   	
	}

	for (int i = 0; i < r_AVX->moduli_nb; ++i) {
		rns_AVX_set_mpz (r_AVX->P_p_mod_q [i], r.P_p_mod_q [i], r);
	}

	for (int i = 0; i < (r_AVX->moduli_nb)-1; ++i) {
		rns_AVX_set_mpz (r_AVX->alpha_P_mod_q [i], r.alpha_P_mod_q [i], r);
	}
}

void rns_AVX_set_mpz (RnsElement_AVX A_RNS,
					  mpz_t A_MPZ, 
					  RnsBasis_MPZ r)
{ 
	mpz_t temp_0;
	mpz_t temp_1;
	mpz_t temp_2;
	mpz_t temp_3;

	mpz_init (temp_0); 
	mpz_init (temp_1); 
	mpz_init (temp_2); 
	mpz_init (temp_3); 
  
	for (uint i=0; i<(r.moduli_nb/4); ++i) {
	  	mpz_mod (temp_0, A_MPZ, r.p [4*i]);
	  	mpz_mod (temp_1, A_MPZ, r.p [4*i+1]);
	  	mpz_mod (temp_2, A_MPZ, r.p [4*i+2]);
	  	mpz_mod (temp_3, A_MPZ, r.p [4*i+3]);
		A_RNS [i] [0] = mpz_get_si (temp_0);
		A_RNS [i] [1] = mpz_get_si (temp_1);
		A_RNS [i] [2] = mpz_get_si (temp_2); 
		A_RNS [i] [3] = mpz_get_si (temp_3);  
	}
  
	mpz_clear (temp_0);
	mpz_clear (temp_1);
	mpz_clear (temp_2);
	mpz_clear (temp_3);
}

void mpz_set_rns_AVX (mpz_t A_MPZ, 
					  RnsElement_AVX A_RNS,
				      RnsBasis_MPZ r)
{
	long int temp_0;
	long int temp_1;
	long int temp_2;
	long int temp_3;

	mpz_set_ui( A_MPZ, 0 );  

	for (uint i = 0; i < (r.moduli_nb/4); ++i) {
		temp_0 = (long int) A_RNS [i] [0];
		temp_1 = (long int) A_RNS [i] [1];
		temp_2 = (long int) A_RNS [i] [2];
		temp_3 = (long int) A_RNS [i] [3];
		mpz_addmul_ui (A_MPZ, r.P_p_inv [4*i], temp_0);
		mpz_addmul_ui (A_MPZ, r.P_p_inv [4*i+1], temp_1);
		mpz_addmul_ui (A_MPZ, r.P_p_inv [4*i+2], temp_2);
		mpz_addmul_ui (A_MPZ, r.P_p_inv [4*i+3], temp_3);
	}

	mpz_mod( A_MPZ, A_MPZ, r.P);
}

void RNSBasis_AVX_clear (RnsBasis_AVX * r) {
	free (r->p);
	free (r->p_c_2);
	free (r->P_inv);
	for (int i = 0; i < r->moduli_nb; ++i)
		free (r->P_p_mod_q [i]);
	free (r->P_p_mod_q);
	for (int i = 0; i < (r->moduli_nb)-1; ++i)	
		free (r->alpha_P_mod_q [i]);
	free (r->alpha_P_mod_q);	
}

void static inline mod_AVX (__m256i* c, 
							__m256i p)
{
	__m256i temp;
		
	temp = _mm256_cmpgt_epi64 (_mm256_setzero_si256(), *c);
	
	temp = _mm256_and_si256 (p, temp);

	*c = _mm256_sub_epi64 (*c, temp);	
}

void static inline modadd_AVX (__m256i a,
							   __m256i b, 
							   __m256i * c, 
							   __m256i p)
{
	__m256i temp, res;

	res = _mm256_add_epi64 (a, b);
		
	temp = _mm256_cmpgt_epi64 (_mm256_setzero_si256(), res);
	
	temp = _mm256_and_si256 (p, temp);

	*c = _mm256_sub_epi64 (res, temp);	
}

void static inline modsub_AVX (__m256i a, 
							   __m256i b, 
							   __m256i* c, 
							   __m256i p)
{

	__m256i temp;

	*c = _mm256_sub_epi64 (a, b);
	
	temp = _mm256_cmpgt_epi64 (_mm256_setzero_si256(), *c);

	temp = _mm256_and_si256 (p, temp);	

	*c = _mm256_add_epi64 (*c, temp);	
}

void static inline modmuladd_AVX (__m256i a, 
							      __m256i b, 
							      __m256i lambda, 
							      __m256i* c, 
							      __m256i p, 
							      __m256i p_c_2)
{
	__m256i temp, temp1;

	*c = _mm256_mul_epu32 (a, lambda);

	temp = _mm256_mul_epu32 (_mm256_srli_epi64 (a, 32), lambda);

	temp = _mm256_or_si256 (_mm256_mul_epu32 (_mm256_srli_epi64 (temp, 32), p_c_2), _mm256_slli_epi64 (temp, 32) );

	temp1 = _mm256_cmpgt_epi64 (_mm256_setzero_si256(), temp);
	
	temp1 = _mm256_and_si256(p, temp1);

	temp = _mm256_sub_epi64 (temp, temp1);	

	modadd_AVX (temp, *c, c, p); 

	modadd_AVX (b, *c, c, p); 
}

void static inline modmulsub_AVX (__m256i a, 
							      __m256i b, 
							      __m256i lambda, 
							      __m256i* c, 
							      __m256i p, 
							      __m256i p_c_2)
{
	__m256i temp, temp1;

	*c = _mm256_mul_epu32(a, lambda);

	temp = _mm256_mul_epu32 (_mm256_srli_epi64 (a, 32), lambda);

	temp = _mm256_or_si256 (_mm256_mul_epu32(_mm256_srli_epi64 (temp, 32), p_c_2), _mm256_slli_epi64 (temp, 32) );

	temp1 = _mm256_cmpgt_epi64 (_mm256_setzero_si256(), temp);
	
	temp1 = _mm256_and_si256(p, temp1);

	temp = _mm256_sub_epi64 (temp, temp1);	

	modadd_AVX (temp, *c, c, p); 

	modsub_AVX (b, *c, c, p); 
}

void static inline modmulscal_AVX (__m256i a, 
								   __m256i lambda, 
								   __m256i* c, 
								   __m256i p, 
								   __m256i p_c_2)

{
	__m256i temp, temp1;

	*c = _mm256_mul_epu32 (a, lambda);

	temp = _mm256_mul_epu32 (_mm256_srli_epi64 (a, 32), lambda);

	temp = _mm256_or_si256 (_mm256_mul_epu32(_mm256_srli_epi64 (temp, 32), p_c_2), _mm256_slli_epi64 (temp, 32) );

	temp1 = _mm256_cmpgt_epi64 (_mm256_setzero_si256(), temp);
	
	temp1 = _mm256_and_si256 (p, temp1);

	temp = _mm256_sub_epi64 (temp, temp1);	

	modadd_AVX (temp, *c, c, p);

}

void static inline modmul_AVX (__m256i a, 
							   __m256i b, 
							   __m256i* c, 
							   __m256i p, 
							   __m256i p_c_2)
{
	__m256i a_h = _mm256_srli_epi64 (a, 32);
	__m256i a_l = _mm256_srli_epi64 (_mm256_slli_epi64 (a, 32),32);
	__m256i b_h = _mm256_srli_epi64 (b, 32);
	__m256i b_l = _mm256_srli_epi64 (_mm256_slli_epi64 (b, 32),32);
	
	__m256i a_lb_l = _mm256_mul_epu32 (a_l, b_l);
	mod_AVX (&a_lb_l, p);
	__m256i a_hb_h = _mm256_mul_epu32 (a_h, b_h);
	
	__m256i a_hb_l = _mm256_mul_epu32 (a_h, b_l);
	__m256i a_lb_h = _mm256_mul_epu32 (a_l, b_h);
	
	__m256i a_hb_lpa_lb_h; modadd_AVX (a_hb_l, a_lb_h, &a_hb_lpa_lb_h, p);	
	
	__m256i	I_h = _mm256_srli_epi64 (a_hb_lpa_lb_h, 32);
	__m256i I_l = _mm256_slli_epi64 (a_hb_lpa_lb_h, 32);	
	mod_AVX(&I_l, p);
	
	modadd_AVX (a_lb_l, I_l, c, p);
	__m256i temp = _mm256_add_epi64 (a_hb_h, I_h);
	modmulscal_AVX (temp, p_c_2, &temp, p, p_c_2);
	modadd_AVX (*c, temp, c, p);	
}

void static inline modaddmul_AVX (__m256i a, 
							   __m256i b, 
							   __m256i d, 
							   __m256i* c, 
							   __m256i p, 
							   __m256i p_c_2)
{

	__m256i a_h = _mm256_srli_epi64 (a, 32);
	__m256i a_l = _mm256_srli_epi64 (_mm256_slli_epi64 (a, 32),32);
	__m256i b_h = _mm256_srli_epi64 (b, 32);
	__m256i b_l = _mm256_srli_epi64 (_mm256_slli_epi64 (b, 32),32);
	
	__m256i a_lb_l = _mm256_mul_epu32 (a_l, b_l);
	mod_AVX (&a_lb_l, p);
	__m256i a_hb_h = _mm256_mul_epu32 (a_h, b_h);
	
	__m256i a_hb_l = _mm256_mul_epu32 (a_h, b_l);
	__m256i a_lb_h = _mm256_mul_epu32 (a_l, b_h);
	
	__m256i a_hb_lpa_lb_h; modadd_AVX (a_hb_l, a_lb_h, &a_hb_lpa_lb_h, p);
	
	__m256i	I_h = _mm256_srli_epi64 (a_hb_lpa_lb_h, 32);
	__m256i I_l = _mm256_slli_epi64 (a_hb_lpa_lb_h, 32);	
	mod_AVX (&I_l, p);
	
	modadd_AVX (a_lb_l, I_l, c, p);
	__m256i temp = _mm256_add_epi64 (a_hb_h, I_h);
	modmulscal_AVX (temp, p_c_2, &temp, p, p_c_2);
	modadd_AVX (*c, temp, c, p);
	modadd_AVX (*c, d, c, p);
}

void static inline rns_set_zero_n_AVX (RnsElement_AVX A_RNS)
{
	for (int i=0; i<(NB_RNS_LEAVES_AVX/4); ++i) {
		A_RNS [i] = _mm256_setzero_si256 ();				
	}
}

void static inline rns_add_n_AVX (RnsElement_AVX A_RNS, 
								  RnsElement_AVX B_RNS, 
								  RnsElement_AVX p)
{  	
	for (int i=0; i<(NB_RNS_LEAVES_AVX/4); ++i) {
		modadd_AVX (A_RNS [i], B_RNS [i], A_RNS + i, p [i]);
	}
}

void static inline rns_sub_n_AVX (RnsElement_AVX A_RNS, 
								  RnsElement_AVX B_RNS, 
								  RnsElement_AVX p)
{  	
	for (int i=0; i<(NB_RNS_LEAVES_AVX/4); ++i) {
		modsub_AVX (A_RNS [i], B_RNS [i], A_RNS + i, p [i]);
	}
}

void static inline rns_muladd_n_AVX (RnsElement_AVX A_RNS, 
									 RnsElement_AVX B_RNS,
									 __m256i lambda, 
									 RnsElement_AVX p,
									 RnsElement_AVX p_c_2)
{
	for (int i=0; i<(NB_RNS_LEAVES_AVX/4); ++i) {
		modmuladd_AVX (B_RNS [i], A_RNS [i], lambda, A_RNS + i, p [i], p_c_2 [i]);
	}
}

void static inline rns_mulsub_n_AVX (RnsElement_AVX A_RNS, 
									 RnsElement_AVX B_RNS,
									 __m256i lambda, 
									 RnsElement_AVX p,
									 RnsElement_AVX p_c_2)
{
	for (int i=0; i<(NB_RNS_LEAVES_AVX/4); ++i) {
		modmulsub_AVX (B_RNS [i], A_RNS [i], lambda, A_RNS + i, p [i], p_c_2 [i]);
	}
}

void static inline rns_mul_n_AVX (RnsElement_AVX A_RNS, 
								  RnsElement_AVX B_RNS,
								  RnsElement_AVX C_RNS, 
							 	  RnsElement_AVX p,
								  RnsElement_AVX p_c_2)
{  
	for (int i=0; i<(NB_RNS_LEAVES_AVX/4); ++i) {
		modmul_AVX (A_RNS [i], B_RNS [i], C_RNS +i, p [i], p_c_2[i] );
	}
}

void static inline rns_mulscalar_n_AVX (RnsElement_AVX A_RNS, 
								  		__m256i B_RNS,
								  		RnsElement_AVX C_RNS, 
							 	  		RnsElement_AVX p,
								  		RnsElement_AVX p_c_2) 
{  
	for (int i=0; i<(NB_RNS_LEAVES_AVX/4); ++i) {
		modmul_AVX (A_RNS [i], B_RNS, C_RNS +i, p [i], p_c_2[i] );
	}
}

void static inline rns_addmulscalar_n_AVX (RnsElement_AVX A_RNS, 
								  		   __m256i B_RNS,
								  		   RnsElement_AVX D_RNS,
								  		   RnsElement_AVX C_RNS, 
							 	  		   RnsElement_AVX p,
								  		   RnsElement_AVX p_c_2) 
{ 
	for (int i=0; i<(NB_RNS_LEAVES_AVX/4); ++i) {
		modaddmul_AVX (A_RNS [i], B_RNS, D_RNS [i], C_RNS +i, p [i], p_c_2[i] );
	}	
}

void static inline rns_modq_n_AVX (RnsElement_AVX gamma_RNS, 
								   RnsElement_AVX A_RNS, 
								   RnsElement_AVX C_RNS, 
								   RnsBasis_AVX r)
{	  	
	rns_mul_n_AVX (A_RNS, r.P_inv, gamma_RNS, r.p, r.p_c_2);
	
	float alpha = r.Delta;
	
	for (uint i=0; i<(NB_RNS_LEAVES_AVX/4); i++) {
		alpha += (float)((float)(_mm256_extract_epi64 (gamma_RNS [i],0) >> (int)r.kMinuss)/(int)r.twoPowers);
		alpha += (float)((float)(_mm256_extract_epi64 (gamma_RNS [i],1) >> (int)r.kMinuss)/(int)r.twoPowers);
		alpha += (float)((float)(_mm256_extract_epi64 (gamma_RNS [i],2) >> (int)r.kMinuss)/(int)r.twoPowers);
		alpha += (float)((float)(_mm256_extract_epi64 (gamma_RNS [i],3) >> (int)r.kMinuss)/(int)r.twoPowers);
	}
		
	rns_mulscalar_n_AVX (r.P_p_mod_q [0], _mm256_set1_epi64x (_mm256_extract_epi64 (gamma_RNS [0],0)), C_RNS, r.p, r.p_c_2);
	   	
   	for (uint i=1; i<NB_RNS_LEAVES_AVX; i++) {
		if ((i%4) == 0) 
			rns_addmulscalar_n_AVX (r.P_p_mod_q [i], _mm256_set1_epi64x (_mm256_extract_epi64 (gamma_RNS [(i/4)],0)), C_RNS, C_RNS, r.p, r.p_c_2);
		else 	if ((i%4) == 1)
				rns_addmulscalar_n_AVX (r.P_p_mod_q [i], _mm256_set1_epi64x (_mm256_extract_epi64 (gamma_RNS [(i/4)],1)), C_RNS, C_RNS, r.p, r.p_c_2);
			else 	if ((i%4) == 2)
					rns_addmulscalar_n_AVX (r.P_p_mod_q [i], _mm256_set1_epi64x (_mm256_extract_epi64 (gamma_RNS [(i/4)],2)), C_RNS, C_RNS, r.p, r.p_c_2);
				else 	
					rns_addmulscalar_n_AVX (r.P_p_mod_q [i], _mm256_set1_epi64x (_mm256_extract_epi64 (gamma_RNS [(i/4)],3)), C_RNS, C_RNS, r.p, r.p_c_2);	
	}
	
   	if (alpha >=1)
		rns_sub_n_AVX (C_RNS, r.alpha_P_mod_q [(int)(alpha-1)], r.p);
}

/////////////////////////////
// SpMV functions with AVX //
/////////////////////////////


RnsElement_AVX p_AVX_Global;

void combine_AVX (RnsElement_AVX in, 
			      RnsElement_AVX inout,
			      int * len, 
			      MPI_Datatype * dptr)
{	
	RnsElement_AVX a; int x = posix_memalign ((void**)&a, 32, (NB_RNS_LEAVES_AVX/4) * sizeof (__m256i));
	RnsElement_AVX b; x = posix_memalign ((void**)&b, 32, (NB_RNS_LEAVES_AVX/4) * sizeof (__m256i));
	
	for (int l=0; l<(*len/(NB_RNS_LEAVES_AVX/4)); ++l) {
		memcpy (a, in + l * (NB_RNS_LEAVES_AVX/4), (NB_RNS_LEAVES_AVX/4) * sizeof (__m256i));
		memcpy (b, inout + l * (NB_RNS_LEAVES_AVX/4), (NB_RNS_LEAVES_AVX/4) * sizeof (__m256i));		
		rns_add_n_AVX (b, a, p_AVX_Global);
		memcpy (inout + l * (NB_RNS_LEAVES_AVX/4), b, (NB_RNS_LEAVES_AVX/4) * sizeof (__m256i));
	}

	free (a);
	free (b);	
}				  

void spmv_MulCores_2D_split_AVX_FFS (RnsElement_AVX src, 
									 RnsElement_AVX dst,
									 int work_begin, 
									 int work_end,
									 uint nb_iter, 
									 Matrix_csr_compressed m,
									 RnsBasis_AVX r)
{
    unsigned long int row_start, row_end, row_end_end, ptr_data_local;
    int i,j;

	RnsElement_AVX Gamma; int x = posix_memalign ((void**)&Gamma, 32, (NB_RNS_LEAVES_AVX/4) * sizeof (__m256i));

    for (i=work_begin; i<work_end; ++i) {

		row_start = m.ptr [i];
        row_end_end = m.ptr [i] + m.data [0 + m.ptr_data [i]];
		row_end = m.ptr [i+1];

		rns_set_zero_n_AVX (dst + i * (NB_RNS_LEAVES_AVX/4));

		for (j = row_start; j < row_end_end; ++j ) {
			rns_add_n_AVX (dst + i*(NB_RNS_LEAVES_AVX/4), src + m.Id[j]*(NB_RNS_LEAVES_AVX/4), r.p);
		}

		row_start = row_end_end;
		row_end_end = row_start + m.data [1 + m.ptr_data [i]];

		for (j = row_start; j < row_end_end; ++j ) {
			rns_sub_n_AVX (dst + i*(NB_RNS_LEAVES_AVX/4), src + m.Id[j]*(NB_RNS_LEAVES_AVX/4), r.p);
		}

		row_start = row_end_end;
		ptr_data_local = 2 + m.ptr_data [i];

		for (j = row_start; j < row_end; ++j ) {		
			if (m.data [ptr_data_local] > 1) {
				rns_muladd_n_AVX (dst + i*(NB_RNS_LEAVES_AVX/4), src + m.Id[j]*(NB_RNS_LEAVES_AVX/4), _mm256_set1_epi64x (m.data [ptr_data_local]), r.p, r.p_c_2);			
			}	
			else { 
				rns_mulsub_n_AVX (dst + i*(NB_RNS_LEAVES_AVX/4), src + m.Id[j]*(NB_RNS_LEAVES_AVX/4), _mm256_set1_epi64x (-m.data [ptr_data_local]), r.p, r.p_c_2);
			}
			ptr_data_local ++; 			
		}

		if ((nb_iter + 1) % nbSpMVsPerRed == 0)
			rns_modq_n_AVX (Gamma, dst + i*(NB_RNS_LEAVES_AVX/4), dst + i*(NB_RNS_LEAVES_AVX/4), r);
						
    }
    
    free (Gamma);	   
}

void mksol_MulCores_2D_split_AVX_FFS (uint* z, 
									  RnsElement_AVX F,
									  RnsElement_AVX src, 
									  RnsElement_AVX dst, 
									  int work_begin, 
									  int work_end,
									  uint nb_iter, 
									  Matrix_csr_compressed m, 
									  int v,
									  RnsBasis_AVX r)
{
    unsigned long int row_start, row_end, row_end_end, ptr_data_local;
    int i,j;

	RnsElement_AVX Gamma; int x = posix_memalign ((void**)&Gamma, 32, (NB_RNS_LEAVES_AVX/4) * sizeof (__m256i));

    for (i=work_begin; i<work_end; ++i) {

		row_start = m.ptr [i];
        row_end_end = m.ptr [i] + m.data [0 + m.ptr_data [i]];
		row_end = m.ptr [i+1];

		rns_set_zero_n_AVX (dst + i * (NB_RNS_LEAVES_AVX/4));

		for (j = row_start; j < row_end_end; ++j ) {
			rns_add_n_AVX (dst + i*(NB_RNS_LEAVES_AVX/4), src + m.Id[j]*(NB_RNS_LEAVES_AVX/4), r.p);
		}

		row_start = row_end_end;
		row_end_end = row_start + m.data [1 + m.ptr_data [i]];

		for (j = row_start; j < row_end_end; ++j ) {
			rns_sub_n_AVX (dst + i*(NB_RNS_LEAVES_AVX/4), src + m.Id[j]*(NB_RNS_LEAVES_AVX/4), r.p);
		}

		row_start = row_end_end;
		ptr_data_local = 2 + m.ptr_data [i];

		for (j = row_start; j < row_end; ++j ) {		
			if (m.data [ptr_data_local] > 1) {
				rns_muladd_n_AVX (dst + i*(NB_RNS_LEAVES_AVX/4), src + m.Id[j]*(NB_RNS_LEAVES_AVX/4), _mm256_set1_epi64x (m.data [ptr_data_local]), r.p, r.p_c_2);			
			}	
			else { 
				rns_mulsub_n_AVX (dst + i*(NB_RNS_LEAVES_AVX/4), src + m.Id[j]*(NB_RNS_LEAVES_AVX/4), _mm256_set1_epi64x (-m.data [ptr_data_local]), r.p, r.p_c_2);
			}
			ptr_data_local ++; 			
		}
		
		if (v==0) 
			rns_addmulscalar_n_AVX (F + nb_iter*(NB_RNS_LEAVES_AVX/4), _mm256_set1_epi64x (z [i]), dst + i*(NB_RNS_LEAVES_AVX/4), dst + i*(NB_RNS_LEAVES_AVX/4), r.p, r.p_c_2);

		if ((nb_iter + 1) % nbMksolsPerRed == 0)
			rns_modq_n_AVX (Gamma, dst + i*(NB_RNS_LEAVES_AVX/4), dst + i*(NB_RNS_LEAVES_AVX/4), r);
						
    }
    
    free (Gamma);
}

void Krylov_FFS_AVX_MulCores_2D_split (mpz_t* src,
									   mpz_t* dst,
									   mpz_t* dst_seq, 
									   Matrix_csr_compressed m,
									   const char * dataDir,
									   unsigned int nRow,
									   mpz_t q,
									   uint* coordinates,
									   uint nCoordinates,
									   int sequence_id,
									   int u,
									   int v, 
									   int nThreads,
									   MPI_Comm row_comm, 
									   MPI_Comm col_comm, 
									   int nb_iters,
									   gmp_randstate_t rnd_state) 
{
	printf ("Krylov-FFS on CPU using AVX: %d iterations ...\n", nb_iters);
	fflush(stdout);
	
	MPI_Barrier (MPI_COMM_WORLD);
	
	int myid = u*nThreads + v;

    char outputFile [100];
    
    float timeExp; 
    struct timeval start, end;
						
    MPI_Op myOp_AVX;

    MPI_Op_create ((void*)combine_AVX, false, &myOp_AVX);

    int bTestResult = 1;
    
    mpz_t* c0;
    mpz_t* cIteration;
    
    mpz_t c0dst, cIterationsrc;
    mpz_t* temp;     

    mpz_t* temp_src;
    mpz_t* temp_dst;        
    
    mpz_t zero;
    mpz_init (zero);
    mpz_set_ui (zero, 0);   
    
    if (myid == 0) {    
		c0 = (mpz_t*) malloc( nRow * sizeof( mpz_t ) );
		cIteration = (mpz_t*) malloc( nRow * sizeof( mpz_t ) );

		for (uint i=0; i<nRow; ++i) {
		   	mpz_init( c0 [i] );
		   	mpz_init( cIteration [i] );
		}

		strcpy (outputFile, dataDir);
		strcat (outputFile, "/c0.txt");
		readVector_dec (c0, outputFile, nRow);
		strcpy (outputFile, dataDir);
		strcat (outputFile, "/cIteration.txt");
		readVector_dec (cIteration, outputFile, nRow);

	    mpz_init (c0dst);
	    mpz_init (cIterationsrc);

    	temp = malloc (nRow * sizeof( mpz_t ) ); 
        for (int i=0; i<nRow; ++i) { 
			mpz_init (temp [i]);
    	}

    	for (int i=0; i<nRow; ++i) { 
			mpz_set (temp [i], src [i]);
    	}

		temp_src = (mpz_t*) malloc (nRow * sizeof (mpz_t));
		temp_dst = (mpz_t*) malloc (nRow * sizeof (mpz_t));
   	
		for (int i=0; i<nRow; ++i) { 
			mpz_init (temp_src [i]);
			mpz_init (temp_dst [i]);	   	
    	}
    }		

	RnsBasis_MPZ r;
	RNSBasis_mpz_init (&r, q, NB_RNS_LEAVES_AVX, MODULI_LENGTH_AVX, rnd_state);

	RnsBasis_AVX r_AVX;
	RNSBasis_AVX_init (r, &r_AVX);
	
   	int x = posix_memalign ((void**)&p_AVX_Global, 32, (NB_RNS_LEAVES_AVX/4) * sizeof (__m256i));
   	
	for (int i=0; i<(NB_RNS_LEAVES_AVX/4); i++)
		_mm256_store_si256 (p_AVX_Global + i, r_AVX.p [i]);
   	
	RnsElement_AVX src_AVX; x = posix_memalign ((void**)&src_AVX, 32, nRow*(r_AVX.moduli_nb/4)*sizeof (__m256i));
		
	RnsElement_AVX dst_AVX; x = posix_memalign ((void**)&dst_AVX, 32, nRow*(r_AVX.moduli_nb/4)*sizeof (__m256i));	
		
	RnsElement_AVX * dst_seq_AVX; x = posix_memalign ((void**)&dst_seq_AVX, 32, (nb_iters+1) * nCoordinates * sizeof (RnsElement_AVX));

	for (int i=0; i<(nb_iters+1) * nCoordinates; ++i) {
		x = posix_memalign ((void**)dst_seq_AVX + i, 32, (r_AVX.moduli_nb/4)*sizeof (__m256i));
	}

	if (myid == 0) {
	   	for (int i=0; i<nRow; ++i) {
	   		rns_AVX_set_mpz (src_AVX + i * (r_AVX.moduli_nb/4), src [i], r);
   		}
   	}
	
	for (int i=0; i<nRow; ++i) {
		rns_AVX_set_mpz (dst_AVX + i * (r_AVX.moduli_nb/4), zero, r);
	}	
	
	for (int i=0; i<(nb_iters+1) * nCoordinates; ++i) {
		rns_AVX_set_mpz (dst_seq_AVX [i], zero, r);    
    }
    	
	MPI_Type_contiguous (4, MPI_UNSIGNED_LONG_LONG, &AVXtype);
   	MPI_Type_commit (&AVXtype);
   	
   	MPI_Bcast (src_AVX, nRow*(r_AVX.moduli_nb/4), AVXtype, 0, MPI_COMM_WORLD);
	
	RnsElement_AVX Gamma; x = posix_memalign ((void**)&Gamma, 32, (NB_RNS_LEAVES_AVX/4) * sizeof (__m256i));

	for (int j=0; j<nCoordinates; ++j) {
		memcpy (dst_seq_AVX [j], src_AVX + coordinates [j] * (r_AVX.moduli_nb/4), (r_AVX.moduli_nb/4) * sizeof (__m256i));
	}

    MPI_Barrier (MPI_COMM_WORLD);

    gettimeofday( &start, NULL );
	
    for (int f=0; f<nb_iters; ++f) {
    	    	
		spmv_MulCores_2D_split_AVX_FFS (src_AVX, dst_AVX + u*m.nRow*(r_AVX.moduli_nb/4), 0, m.nRow, f, m, r_AVX);		 			
		    	
		MPI_Reduce (dst_AVX + u*m.nRow*(r_AVX.moduli_nb/4), src_AVX + u*m.nRow*(r_AVX.moduli_nb/4), m.nRow*(r_AVX.moduli_nb/4), AVXtype, myOp_AVX, u, row_comm);
    	    	
	    MPI_Bcast (src_AVX + v*m.nRow*(r_AVX.moduli_nb/4), m.nRow*(r_AVX.moduli_nb/4), AVXtype, v, col_comm);	
	    	
		for (int j=0; j<nCoordinates; ++j) {
			memcpy (dst_seq_AVX [j + (f+1) * nCoordinates], src_AVX + coordinates [j] * (r_AVX.moduli_nb/4), (r_AVX.moduli_nb/4) * sizeof (__m256i));
		}
        	
		if ((f+1)% CHECKSUM_FREQUENCY == 0) {
    		
			// we broadcat all the pieces to all the threads
			for (int i=0; i<nThreads; ++i) {
				MPI_Bcast (src_AVX + i*m.nRow*(r_AVX.moduli_nb/4), m.nRow*(r_AVX.moduli_nb/4), AVXtype, i, row_comm);
    		}

			if (myid == 0) {
				
				// AVX -> MPZ and final modq
				for (int i=0; i<nRow; ++i) {
					rns_modq_n_AVX (Gamma, src_AVX + i * (r_AVX.moduli_nb/4), src_AVX + i *(r_AVX.moduli_nb/4), r_AVX);
					mpz_set_rns_AVX (dst [i], src_AVX + i * (r_AVX.moduli_nb/4), r);
					mpz_mod (dst [i], dst [i], q);
				}
				
				redistibuteEvenly (temp_dst, dst, nRow, nThreads);
				redistibuteEvenly (temp_src, temp, nRow, nThreads);
				
				prod_vec (c0, temp_dst, c0dst, nRow, q);
				prod_vec (cIteration, temp_src, cIterationsrc, nRow, q);
				
				bTestResult = 1;
				gmp_printf ("tc0 * temp_dst = %Zd\n", c0dst);
				gmp_printf ("tcIteration * temp_src = %Zd\n", cIterationsrc);
				fflush (stdout);
				
				if (mpz_cmp( c0dst, cIterationsrc )!= 0) {
					bTestResult = 0;
				}			
    			
				printf("Verification after %u iterations through dot product? %s\n", (f+1), (bTestResult==1) ? "PASSED" : "FAILED");
				fflush (stdout);
			}
					
			MPI_Bcast (&bTestResult, 1, MPI_INT, 0, MPI_COMM_WORLD);	
		
			if (bTestResult==1) {
				if (myid == 0) {
					for (int i=0; i<nRow; ++i) { 
						mpz_set (temp [i], dst [i]);
	   				}
	   			}					
			}
			else {
				if (myid == 0) {
					for (int i=0; i<nRow; ++i) {
						rns_AVX_set_mpz (src_AVX + i * (r_AVX.moduli_nb/4), temp [i], r);
   					}				
				}
				MPI_Bcast (src_AVX, nRow*(r_AVX.moduli_nb/4), AVXtype, 0, MPI_COMM_WORLD);
				f -= CHECKSUM_FREQUENCY;		    				
    		}		
    	} 
    	    	   	
    }  

    MPI_Barrier (MPI_COMM_WORLD);

    // we measure the timing in ms
    gettimeofday( &end, NULL);
    computeTiming( start, end, &timeExp );
    printf( "	sequence_id = %d, myid = %d, SpMVs time: %f (ms) -> iteration: %f (ms)\n", sequence_id, myid, timeExp, timeExp/nb_iters);

	// we broadcat all the pieces to all the threads
    for (int i=0; i<nThreads; ++i) {
		MPI_Bcast (src_AVX + i*m.nRow*(r_AVX.moduli_nb/4), m.nRow*(r_AVX.moduli_nb/4), AVXtype, i, row_comm);
    }
       
    if (myid  == 0) {
    	for (int i=0; i<nRow; ++i) {
			rns_modq_n_AVX (Gamma, src_AVX + i * (r_AVX.moduli_nb/4), src_AVX + i *(r_AVX.moduli_nb/4), r_AVX);
			mpz_set_rns_AVX (dst [i], src_AVX + i * (r_AVX.moduli_nb/4), r);
			mpz_mod (dst [i], dst [i], q);
		}
    	
		for (int i=0; i<(nb_iters+1); ++i) {
			for (int j=0; j<nCoordinates; ++j) {
				rns_modq_n_AVX (Gamma, dst_seq_AVX [nCoordinates * i + j], dst_seq_AVX [nCoordinates * i + j], r_AVX);
				mpz_set_rns_AVX (dst_seq [nCoordinates * i + j], dst_seq_AVX [nCoordinates * i + j], r);
				mpz_mod (dst_seq [nCoordinates * i + j], dst_seq [nCoordinates * i + j], q);
			}
		}  
	}	
      
   if (myid == 0) {
		for (uint i=0; i<nRow; ++i) {
			mpz_clear( c0 [i] );
			mpz_clear( cIteration [i] );
		}    
    
		free( c0 );
   		free( cIteration );

		for (int i=0; i<nRow; ++i) { 
    		mpz_clear (temp [i]);
    	}	
    	free (temp);

   	    mpz_clear (c0dst);
	    mpz_clear (cIterationsrc);  	
	    
		for (int i=0; i<nRow; ++i) { 
			mpz_clear (temp_src [i]);
			mpz_clear (temp_dst [i]);	   	
		}
		free (temp_src);
		free (temp_dst);	
    }

    for (uint i=0; i<(nb_iters+1) * nCoordinates; ++i)
    	 free (dst_seq_AVX [i]);
    free (dst_seq_AVX);	

	free (src_AVX);

	free (dst_AVX);

	RNSBasis_mpz_clear (&r);
	
	RNSBasis_AVX_clear (&r_AVX);
	
    mpz_clear (zero);
    
    free (p_AVX_Global);

	free (Gamma);
}

void Mksol_FFS_AVX_MulCores_2D_split (mpz_t* z, 
									  mpz_t* F,
									  mpz_t* src,
									  mpz_t* dst,
									  Matrix_csr_compressed m,
									  const char * dataDir,
									  const char * matrixFile,
									  unsigned int nRow,
									  ulong nNZ,
									  mpz_t q,
									  uint* coordinates,
									  uint nCoordinates,
									  int sequence_id,
									  int u,
									  int v, 
									  int nThreads,
									  MPI_Comm row_comm, 
									  MPI_Comm col_comm, 
									  int nb_iters,
									  int nb_iters_Krylov,
									  gmp_randstate_t rnd_state)
{
	printf ("Mksol-FFS on CPU using AVX: %d iterations ...\n", nb_iters);
	fflush(stdout);

	MPI_Barrier (MPI_COMM_WORLD);
	
	int myid = u*nThreads + v;

    char outputFile [100];
	char suffixFile [40];

    float timeExp; 
    struct timeval start, end;
    
    MPI_Op myOp_AVX;

    MPI_Op_create ((void*)combine_AVX, false, &myOp_AVX);

    mpz_t zero;
    mpz_init (zero);
    mpz_set_ui (zero, 0);   
    
    mpz_t* temp_temp = (mpz_t*) malloc (nRow * sizeof (mpz_t));

    for (int i=0; i<nRow; ++i) {
		mpz_init (temp_temp [i]);
    }	
	
	mpz_t xMv_dst, dst_seqF;
	mpz_t* dst_seq;
	    				
    int bTestResult = 1;

    mpz_t* temp;
    if (myid == 0) {
    	temp = (mpz_t*) malloc (nRow * sizeof( mpz_t ) );
    	for (int i=0; i<nRow; ++i) { 
			mpz_init (temp [i]);
    	}

    	for (int i=0; i<nRow; ++i) { 
			mpz_set (temp [i], src [i]);
    	}
    	
	    mpz_init (xMv_dst);
    	mpz_init (dst_seqF);
	
		dst_seq = (mpz_t*) malloc (nCoordinates * (nb_iters_Krylov+1) * sizeof (mpz_t));
		for (uint i=0; i<(nCoordinates*(nb_iters_Krylov+1)); ++i) {
			mpz_init (dst_seq [i]);
		}

		strcpy (outputFile, dataDir);
		strcat (outputFile, "/sequence.");
		sprintf (suffixFile, "%d.txt", sequence_id);
		strcat (outputFile, suffixFile);
		readSequences_dec (dst_seq, (nb_iters_Krylov+1), nCoordinates, outputFile);
    }
    		
	RnsBasis_MPZ r;
	RNSBasis_mpz_init (&r, q, NB_RNS_LEAVES_AVX, MODULI_LENGTH_AVX, rnd_state);

	RnsBasis_AVX r_AVX;
	RNSBasis_AVX_init (r, &r_AVX);
	
   	int x = posix_memalign ((void**)&p_AVX_Global, 32, (NB_RNS_LEAVES_AVX/4) * sizeof (__m256i));
   	
	for (int i=0; i<(NB_RNS_LEAVES_AVX/4); i++)
		_mm256_store_si256 (p_AVX_Global + i, r_AVX.p [i]);

	RnsElement_AVX src_AVX; x = posix_memalign ((void**)&src_AVX, 32, nRow*(r_AVX.moduli_nb/4)*sizeof (__m256i));
		
	RnsElement_AVX dst_AVX; x = posix_memalign ((void**)&dst_AVX, 32, nRow*(r_AVX.moduli_nb/4)*sizeof (__m256i));	

	uint * z_AVX = malloc (nRow * sizeof (uint));

	RnsElement_AVX F_AVX; x = posix_memalign ((void**)&F_AVX, 32, nb_iters*(r_AVX.moduli_nb/4)*sizeof (__m256i));

	if (myid == 0) {
	   	for (int i=0; i<nRow; ++i) {
	   		rns_AVX_set_mpz (src_AVX + i * (r_AVX.moduli_nb/4), src [i], r);
   		}

    	for (int i=0; i<nb_iters; ++i) {
			rns_AVX_set_mpz (F_AVX + i * (r_AVX.moduli_nb/4), F [i], r);
		}

		for (int i=0; i<nRow; ++i) {
			mpz_export (z_AVX + i, NULL, -1, sizeof(uint), -1, 0, z [i]);
    	}
   	}
	
	for (int i=0; i<nRow; ++i) {
		rns_AVX_set_mpz (dst_AVX + i * (r_AVX.moduli_nb/4), zero, r);
	}	

	MPI_Type_contiguous (4, MPI_UNSIGNED_LONG_LONG, &AVXtype);
   	MPI_Type_commit (&AVXtype);
   	
   	MPI_Bcast (src_AVX, nRow*(r_AVX.moduli_nb/4), AVXtype, 0, MPI_COMM_WORLD);
	MPI_Bcast (F_AVX, nb_iters*(r_AVX.moduli_nb/4), AVXtype, 0, MPI_COMM_WORLD);
    MPI_Bcast (z_AVX, nRow, MPI_INT, 0, MPI_COMM_WORLD);
	
	RnsElement_AVX Gamma; x = posix_memalign ((void**)&Gamma, 32, (NB_RNS_LEAVES_AVX/4) * sizeof (__m256i));
   			
	Matrix_coo m_coo_dist;
	
	if (myid == 0) {
	
		Matrix_csr m_csr;
	
		Matrix_csr_init (&m_csr, nRow, nNZ);

		// fill matrix in format csr
		fill_matrix_csr (matrixFile, &m_csr, nRow, nNZ);	
	
		Matrix_csr m_csr_dist;
	
		Matrix_csr_init (&m_csr_dist, nRow, nNZ);
	
		// distribute matrix evenly over threads
		distibuteEvenly_csr (m_csr, &m_csr_dist, nThreads);
	
		Matrix_csr_clear (&m_csr);
	
    	Matrix_coo_init (&m_coo_dist, nNZ);
    
    	// re make distributed coo matrix from distributed csr matrix
    	make_coo_from_csr (m_csr_dist, &m_coo_dist);
    
    	Matrix_csr_clear (&m_csr_dist);	
    
	}

    MPI_Barrier (MPI_COMM_WORLD);

    gettimeofday (&start, NULL );
    	
    for (int f=0; f<nb_iters; ++f) {
	    	
		mksol_MulCores_2D_split_AVX_FFS (z_AVX + u*m.nRow, F_AVX, src_AVX, dst_AVX + u*m.nRow*(r_AVX.moduli_nb/4), 0, m.nRow, f, m, v, r_AVX);

		MPI_Reduce (dst_AVX + u*m.nRow*(r_AVX.moduli_nb/4), src_AVX + u*m.nRow*(r_AVX.moduli_nb/4), m.nRow*(r_AVX.moduli_nb/4), AVXtype, myOp_AVX, u, row_comm);

	    MPI_Bcast (src_AVX + v*m.nRow*(r_AVX.moduli_nb/4), m.nRow*(r_AVX.moduli_nb/4), AVXtype, v, col_comm);	
		    	    		
		if ((f+1)% CHECKSUM_FREQUENCY == 0) { 
   	
			// we broadcat all the pieces to all the threads
			for (int i=0; i<nThreads; ++i) {
				MPI_Bcast (src_AVX + i*m.nRow*(r_AVX.moduli_nb/4), m.nRow*(r_AVX.moduli_nb/4), AVXtype, i, row_comm);
    		}

			if (myid == 0) {
		
				// AVX -> MPZ and final modq
				for (int i=0; i<nRow; ++i) {
					rns_modq_n_AVX (Gamma, src_AVX + i * (r_AVX.moduli_nb/4), src_AVX + i *(r_AVX.moduli_nb/4), r_AVX);
					mpz_set_rns_AVX (dst [i], src_AVX + i * (r_AVX.moduli_nb/4), r);
					mpz_mod (dst [i], dst [i], q);
				}
				
				spmvs (dst, temp_temp, m_coo_dist, nRow, q, DELTA_CHECKSUM_MKSOL);
				
				bTestResult = 1;
				
				for (int k=0; k< nCoordinates; ++k) {
					mpz_set (xMv_dst, temp_temp [coordinates [k]] );
					gmp_printf ("xMv_dst=%Zd\n", xMv_dst );

					mpz_set_ui (dst_seqF, 0);
					for (int i=0; i<(f+1); ++i) {
						mpz_addmul (dst_seqF, dst_seq [(DELTA_CHECKSUM_MKSOL + f - i) * nCoordinates + k], F [i]);
					}
				
					mpz_mod (dst_seqF, dst_seqF, q);
					gmp_printf ("dst_seqF=%Zd\n", dst_seqF);	

					if (mpz_cmp( xMv_dst, dst_seqF )!= 0) {
						bTestResult = 0;
					}
				}
				
				printf("Verification of Mksol after %d iterations? %s\n", (f+1), ( bTestResult) ? "PASSED" : "FAILED");
			
			}
			
			MPI_Bcast (&bTestResult, 1, MPI_INT, 0, MPI_COMM_WORLD);	
		
			if (bTestResult==1) {
				if (myid == 0) {
					for (int i=0; i<nRow; ++i) { 
						mpz_set (temp [i], dst [i]);
	   				}
	   			}					
			}
			else {
				if (myid == 0) {
					for (int i=0; i<nRow; ++i) {
						rns_AVX_set_mpz (src_AVX + i * (r_AVX.moduli_nb/4), temp [i], r);
   					}				
				}
				MPI_Bcast (src_AVX, nRow*(r_AVX.moduli_nb/4), AVXtype, 0, MPI_COMM_WORLD);
				f -= CHECKSUM_FREQUENCY;		    				
    		}	
			
		}
		
    }    
    
    // we broadcat all the pieces to all the threads
    for (int i=0; i<nThreads; ++i) {
		MPI_Bcast (src_AVX + i*m.nRow*(r_AVX.moduli_nb/4), m.nRow*(r_AVX.moduli_nb/4), AVXtype, i, row_comm);
    }
       
	if (myid == 0) {
	
		for (int i=0; i<nRow; ++i) {
			rns_modq_n_AVX (Gamma, src_AVX + i * (r_AVX.moduli_nb/4), src_AVX + i *(r_AVX.moduli_nb/4), r_AVX);
			mpz_set_rns_AVX (dst [i], src_AVX + i * (r_AVX.moduli_nb/4), r);			
			mpz_mod( dst[i], dst[i], q);
    	}

		spmvs (dst, temp_temp, m_coo_dist, nRow, q, DELTA_CHECKSUM_MKSOL);

		bTestResult = 1;
			
		for (int k=0; k< nCoordinates; ++k) {
			mpz_set (xMv_dst, temp_temp [coordinates [k]] );
			gmp_printf ("xMv_dst=%Zd\n", xMv_dst );

			mpz_set_ui (dst_seqF, 0);
			for (int i=0; i<nb_iters; ++i) {
				mpz_addmul (dst_seqF, dst_seq [(DELTA_CHECKSUM_MKSOL + (nb_iters -1) - i) * nCoordinates + k], F [i]);
			}
				
			mpz_mod (dst_seqF, dst_seqF, q);
			gmp_printf ("dst_seqF=%Zd\n", dst_seqF);	

			if (mpz_cmp( xMv_dst, dst_seqF )!= 0) {
				bTestResult = 0;
				exit (EXIT_FAILURE);
			}
		}
					
		printf("Verification of Mksol after %d iterations? %s\n", nb_iters, ( bTestResult) ? "PASSED" : "FAILED");

	}

	MPI_Barrier (MPI_COMM_WORLD);

    // we measure the timing in ms
    gettimeofday( &end, NULL);
    computeTiming( start, end, &timeExp );
    printf( "	sequence_id = %d, myid = %d, Mksols time: %f (ms) -> iteration: %f (ms)\n", sequence_id, myid, timeExp, timeExp/nb_iters);

    if (myid == 0) {

		for (int i=0; i<nRow; ++i) { 
    		mpz_clear (temp [i]);
    	}
		free (temp);
    
    	Matrix_coo_clear (&m_coo_dist);

		for (uint i=0; i<(nCoordinates*(nb_iters_Krylov+1)); ++i) {
			mpz_clear (dst_seq [i]);
		}		
		free (dst_seq);
		
	}
    
	free (src_AVX);

	free (dst_AVX);

	free (F_AVX);

    free (z_AVX);

	RNSBasis_mpz_clear (&r);
	
	RNSBasis_AVX_clear (&r_AVX);
	
    mpz_clear (zero);
    
    free (p_AVX_Global);

	free (Gamma);
}




