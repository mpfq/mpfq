#include <immintrin.h>

// number of bits of the RNS modulus using AVX, should be 63
#define MODULI_LENGTH_AVX 63 
// number of moduli in the RNS basis using AVX, should be multiple of 4
#if (((Q_LENGTH + MODULI_LENGTH_AVX - 1) / MODULI_LENGTH_AVX) + 1) % 4 == 0 
#define NB_RNS_LEAVES_AVX (((Q_LENGTH + MODULI_LENGTH_AVX - 1) / MODULI_LENGTH_AVX) + 1)
#endif
#if (((Q_LENGTH + MODULI_LENGTH_AVX - 1) / MODULI_LENGTH_AVX) + 1) % 4 == 3
#define NB_RNS_LEAVES_AVX (((Q_LENGTH + MODULI_LENGTH_AVX - 1) / MODULI_LENGTH_AVX) + 2)
#endif
#if (((Q_LENGTH + MODULI_LENGTH_AVX - 1) / MODULI_LENGTH_AVX) + 1) % 4 == 2
#define NB_RNS_LEAVES_AVX (((Q_LENGTH + MODULI_LENGTH_AVX - 1) / MODULI_LENGTH_AVX) + 3)
#endif
#if (((Q_LENGTH + MODULI_LENGTH_AVX - 1) / MODULI_LENGTH_AVX) + 1) % 4 == 1
#define NB_RNS_LEAVES_AVX (((Q_LENGTH + MODULI_LENGTH_AVX - 1) / MODULI_LENGTH_AVX) + 4)
#endif

///////////////////////////
// Arithmetic structures //
///////////////////////////

typedef __m256i* RnsElement_AVX;
MPI_Datatype RnsElementAVXtype;
MPI_Datatype AVXtype;

typedef struct {
	uint moduli_nb;
	uint modulus_length;
	mpz_t * p;
	mpz_t * p_c;
	mpz_t * P_inv;
	mpz_t * P_p;
	mpz_t * P_p_inv;
	mpz_t P;
	mpz_t * P_p_mod_q;
	mpz_t * alpha_P_mod_q;
} RnsBasis_MPZ;

typedef struct {
	uint moduli_nb;
	uint modulus_length;
	float Delta;
	int kMinuss;
	int twoPowers;
	RnsElement_AVX p;
	RnsElement_AVX p_c_2;
	RnsElement_AVX P_inv;
	RnsElement_AVX * P_p_mod_q;
	RnsElement_AVX * alpha_P_mod_q;
} RnsBasis_AVX;

void RNSBasis_mpz_init (RnsBasis_MPZ * r,
		      			mpz_t q,
		       			int moduli_nb,
		       			int modulus_length, 
		       			gmp_randstate_t rnd_state);

void RNSBasis_mpz_clear (RnsBasis_MPZ * r);

///////////////////////////////////
// Arithmetic functions with AVX //
///////////////////////////////////

void RnsElement_AVX_init (RnsBasis_AVX r,
					  	  RnsElement_AVX * A_RNS);

void RNSBasis_AVX_init (RnsBasis_MPZ r,
						RnsBasis_AVX * r_AVX);

void RNSBasis_AVX_clear (RnsBasis_AVX * r);
						
void rns_AVX_set_mpz (RnsElement_AVX A_RNS,
					  mpz_t A_MPZ, 
					  RnsBasis_MPZ r);						

void mpz_set_rns_AVX (mpz_t A_MPZ,
				      RnsElement_AVX A_RNS,
				      RnsBasis_MPZ r);

void RNSBasis_AVX_clear (RnsBasis_AVX * r);

void static inline mod_AVX (__m256i* c, 
							__m256i p);

void static inline modadd_AVX (__m256i a,
							   __m256i b, 
							   __m256i * c, 
							   __m256i p);

void static inline modsub_AVX (__m256i a, 
							   __m256i b,
							   __m256i * c, 
							   __m256i p);

void static inline modmuladd_AVX (__m256i a,
								  __m256i b, 
								  __m256i lambda, 
								  __m256i* c, 
								  __m256i p, 
								  __m256i p_c_2);

void static inline modmulsub_AVX (__m256i a, 
								  __m256i b, 
								  __m256i lambda, 
								  __m256i* c, 
								  __m256i p, 
								  __m256i p_c_2); 

void static inline modmulscal_AVX (__m256i a, 
								   __m256i lambda, 
								   __m256i* c, 
								   __m256i p, 
								   __m256i p_c_2);

void static inline modmul_AVX (__m256i a, 
							   __m256i b, 
							   __m256i* c, 
							   __m256i p, 
							   __m256i p_c_2);

void static inline modaddmul_AVX (__m256i a, 
								  __m256i b, 
								  __m256i d, 
								  __m256i* c, 
								  __m256i p, 
								  __m256i p_c_2);

void static inline rns_set_zero_n_AVX (RnsElement_AVX A_RNS);

void static inline rns_add_n_AVX (RnsElement_AVX A_RNS, 
								  RnsElement_AVX B_RNS, 
								  RnsElement_AVX p);

void static inline rns_sub_n_AVX (RnsElement_AVX A_RNS, 
								  RnsElement_AVX B_RNS, 
								  RnsElement_AVX p);

void static inline rns_muladd_n_AVX (RnsElement_AVX A_RNS, 
									 RnsElement_AVX B_RNS, 
									 __m256i lambda, 
									 RnsElement_AVX p, 
									 RnsElement_AVX p_c_2);

void static inline rns_mulsub_n_AVX (RnsElement_AVX A_RNS, 
									 RnsElement_AVX B_RNS, 
									 __m256i lambda, 
									 RnsElement_AVX p, 
									 RnsElement_AVX p_c_2);

void static inline rns_mul_n_AVX (RnsElement_AVX A_RNS, 
								  RnsElement_AVX B_RNS, 
								  RnsElement_AVX C_RNS, 
								  RnsElement_AVX p, 
								  RnsElement_AVX p_c_2);

void static inline rns_mulscalar_n_AVX (RnsElement_AVX A_RNS, 
									    __m256i B_RNS, 
									    RnsElement_AVX C_RNS, 
									    RnsElement_AVX p, 
									    RnsElement_AVX p_c_2);

void static inline rns_addmulscalar_n_AVX (RnsElement_AVX A_RNS, 
										   __m256i B_RNS, 
										   RnsElement_AVX D_RNS, 
										   RnsElement_AVX C_RNS, 
										   RnsElement_AVX p, 
										   RnsElement_AVX p_c_2);

void static inline rns_modq_n_AVX (RnsElement_AVX gamma_RNS, 
								   RnsElement_AVX A_RNS, 
								   RnsElement_AVX C_RNS, 
								   RnsBasis_AVX r);								  

/////////////////////////////
// SpMV functions with AVX //
/////////////////////////////

void combine_AVX (RnsElement_AVX in, 
			      RnsElement_AVX inout,
			      int * len, 
			      MPI_Datatype * dptr);			  	
				  
void spmv_MulCores_2D_split_AVX_FFS (RnsElement_AVX src, 
									 RnsElement_AVX dst,
									 int work_begin, 
									 int work_end,
									 uint nb_iter, 
									 Matrix_csr_compressed m,
									 RnsBasis_AVX r);

void mksol_MulCores_2D_split_AVX_FFS (uint* z, 
									  RnsElement_AVX F,
									  RnsElement_AVX src, 
									  RnsElement_AVX dst, 
									  int work_begin, 
									  int work_end,
									  uint nb_iter, 
									  Matrix_csr_compressed m, 
									  int v,
									  RnsBasis_AVX r);

void Krylov_FFS_AVX_MulCores_2D_split (mpz_t* src,
									   mpz_t* dst,
									   mpz_t* dst_seq, 
									   Matrix_csr_compressed m_split,
									   const char * dataDir,
									   unsigned int nRow,
									   mpz_t q,
									   uint* coordinates,
									   uint nCoordinates,
									   int sequence_id,
									   int u,
									   int v, 
									   int nThreads,
									   MPI_Comm row_comm, 
									   MPI_Comm col_comm, 
									   int nb_iters, 
									   gmp_randstate_t rnd_state);
									  
void Mksol_FFS_AVX_MulCores_2D_split (mpz_t* z, 
									  mpz_t* F,
									  mpz_t* src,
									  mpz_t* dst,
									  Matrix_csr_compressed m,
									  const char * dataDir,
									  const char * matrixFile,
									  unsigned int nRow,
									  ulong nNZ,
									  mpz_t q,
									  uint* coordinates,
									  uint nCoordinates,
									  int sequence_id,
									  int u,
									  int v, 
									  int nThreads,
									  MPI_Comm row_comm, 
									  MPI_Comm col_comm, 
									  int nb_iters,
									  int nb_iters_Krylov,
									  gmp_randstate_t rnd_state);

