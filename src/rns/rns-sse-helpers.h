#ifndef RNS_SSE_HELPERS_H_
#define RNS_SSE_HELPERS_H_

#include <xmmintrin.h>
#include <smmintrin.h>
#include <nmmintrin.h>
#include <immintrin.h>
#include <emmintrin.h>

#ifdef __cplusplus
extern "C" {
#endif

static inline void modadd_128(__m128i a, __m128i b, __m128i * c, __m128i p);
static inline void modmulscal_128(__m128i a, __m128i lambda, __m128i * c, __m128i p,
		    __m128i p_c_times_2);
static inline void modmul_128(__m128i a, __m128i b, __m128i * c, __m128i p,
		__m128i p_c_times_2);
static inline void modaddmul_128(__m128i a, __m128i b, __m128i d, __m128i * c, __m128i p,
		   __m128i p_c_times_2);
static inline void modsub_128(__m128i a, __m128i b, __m128i * c, __m128i p);
static inline void modmuladd_128(__m128i a, __m128i b, __m128i lambda, __m128i * c,
		   __m128i p, __m128i p_c_times_2);
static inline void modmulsub_128(__m128i a, __m128i b, __m128i lambda, __m128i * c,
		   __m128i p, __m128i p_c_times_2);
static inline void modneg_128(__m128i a, __m128i * c, __m128i p);

#ifdef __cplusplus
}
#endif

/* puts in (*c)[i] the numbers (a[i]+b[i])%p[i] for i=0,1 */
static inline void modadd_128(__m128i a, __m128i b, __m128i * c, __m128i p)
{
    __m128i temp;

    *c = _mm_add_epi64(a, b);

    temp = _mm_cmpgt_epi64(_mm_setzero_si128(), *c);

    temp = _mm_and_si128(p, temp);

    *c = _mm_sub_epi64(*c, temp);
}

/* puts in (*c)[i] the numbers (a[i]*lambda[i])%p[i] for i=0,1, lambda[i]*p_c_times_2[i] should not exceed 2^64 */
static inline void modmulscal_128(__m128i a, __m128i lambda, __m128i * c, __m128i p,
		    __m128i p_c_times_2)
{
    __m128i temp, temp1;

    *c = _mm_mul_epu32(a, lambda);

    temp = _mm_mul_epu32(_mm_srli_epi64(a, 32), lambda);

    temp =
	_mm_or_si128(_mm_mul_epu32(_mm_srli_epi64(temp, 32), p_c_times_2),
		     _mm_slli_epi64(temp, 32));

    temp1 = _mm_cmpgt_epi64(_mm_setzero_si128(), temp);

    temp1 = _mm_and_si128(p, temp1);

    temp = _mm_sub_epi64(temp, temp1);

    modadd_128(temp, *c, c, p);
}

/* puts in (*c)[i] the numbers (a[i]*b[i])%p[i] for i=0,1 */
static inline void modmul_128(__m128i a, __m128i b, __m128i * c, __m128i p,
		__m128i p_c_times_2)
{
    __m128i temp, temp3, temp5, temp4;

    *c = _mm_mul_epu32(a, b);

    temp = _mm_cmpgt_epi64(_mm_setzero_si128(), *c);

    temp = _mm_and_si128(p, temp);

    *c = _mm_sub_epi64(*c, temp);

    modadd_128(_mm_mul_epu32(_mm_srli_epi64(a, 32), b),
	       _mm_mul_epu32(a, _mm_srli_epi64(b, 32)), &temp3, p);

    temp4 = _mm_mul_epu32(_mm_srli_epi64(temp3, 32), p_c_times_2);

    temp3 = _mm_slli_epi64(temp3, 32);

    temp = _mm_cmpgt_epi64(_mm_setzero_si128(), temp3);

    temp = _mm_and_si128(p, temp);

    temp3 = _mm_sub_epi64(temp3, temp);

    modadd_128(temp4, temp3, &temp3, p);

    modadd_128(*c, temp3, c, p);

    modmulscal_128(_mm_mul_epu32
		   (_mm_srli_epi64(a, 32), _mm_srli_epi64(b, 32)),
		   p_c_times_2, &temp5, p, p_c_times_2);

    modadd_128(*c, temp5, c, p);
}

/* puts in (*c)[i] the numbers (a[i]*b[i]+d[i])%p[i] for i=0,1 */
static inline void modaddmul_128(__m128i a, __m128i b, __m128i d, __m128i * c, __m128i p,
		   __m128i p_c_times_2)
{
    __m128i temp, temp3, temp5, temp4;

    *c = _mm_mul_epu32(a, b);

    temp = _mm_cmpgt_epi64(_mm_setzero_si128(), *c);

    temp = _mm_and_si128(p, temp);

    *c = _mm_sub_epi64(*c, temp);

    modadd_128(_mm_mul_epu32(_mm_srli_epi64(a, 32), b),
	       _mm_mul_epu32(a, _mm_srli_epi64(b, 32)), &temp3, p);

    temp4 = _mm_mul_epu32(_mm_srli_epi64(temp3, 32), p_c_times_2);

    temp3 = _mm_slli_epi64(temp3, 32);

    temp = _mm_cmpgt_epi64(_mm_setzero_si128(), temp3);

    temp = _mm_and_si128(p, temp);

    temp3 = _mm_sub_epi64(temp3, temp);

    modadd_128(temp4, temp3, &temp3, p);

    modadd_128(*c, temp3, c, p);

    modmulscal_128(_mm_mul_epu32
		   (_mm_srli_epi64(a, 32), _mm_srli_epi64(b, 32)),
		   p_c_times_2, &temp5, p, p_c_times_2);

    modadd_128(*c, temp5, c, p);

    modadd_128(*c, d, c, p);
}


/* puts in (*c)[i] the numbers (a[i]-b[i])%p[i] for i=0,1 */
static inline void modsub_128(__m128i a, __m128i b, __m128i * c, __m128i p)
{

    __m128i temp;
    *c = _mm_sub_epi64(a, b);
    temp = _mm_cmpgt_epi64(_mm_setzero_si128(), *c);

    temp = _mm_and_si128(p, temp);

    *c = _mm_add_epi64(*c, temp);
}

/* puts in (*c)[i] the numbers (-a[i])%p[i] for i=0,1*/
static inline void modneg_128(__m128i a, __m128i * c, __m128i p)
{

    __m128i x = _mm_sub_epi64(_mm_setzero_si128(), a);
    __m128i flags = _mm_cmpgt_epi64(_mm_setzero_si128(), x);
    *c = _mm_add_epi64(x, _mm_and_si128(p, flags));
}

/* puts in (*c)[i] the numbers (a[i]*lambda[i]+b[i])%p[i] for i=0,1, lambda[i]*p_c_times_2[i] should not exceed 2^64 */
static inline void modmuladd_128(__m128i a, __m128i b, __m128i lambda, __m128i * c,
		   __m128i p, __m128i p_c_times_2)
{
    __m128i temp, temp1;

    *c = _mm_mul_epu32(a, lambda);

    temp = _mm_mul_epu32(_mm_srli_epi64(a, 32), lambda);

    temp =
	_mm_or_si128(_mm_mul_epu32(_mm_srli_epi64(temp, 32), p_c_times_2),
		     _mm_slli_epi64(temp, 32));

    temp1 = _mm_cmpgt_epi64(_mm_setzero_si128(), temp);

    temp1 = _mm_and_si128(p, temp1);

    temp = _mm_sub_epi64(temp, temp1);

    modadd_128(temp, *c, c, p);

    modadd_128(b, *c, c, p);
}

/* puts in (*c)[i] the numbers (a[i]*lambda[i]-b[i])%p[i] for i=0,1, lambda[i]*p_c_times_2[i] should not exceed 2^64 */
static inline void modmulsub_128(__m128i a, __m128i b, __m128i lambda, __m128i * c,
		   __m128i p, __m128i p_c_times_2)
{
    __m128i temp, temp1;

    *c = _mm_mul_epu32(a, lambda);

    temp = _mm_mul_epu32(_mm_srli_epi64(a, 32), lambda);

    temp =
	_mm_or_si128(_mm_mul_epu32(_mm_srli_epi64(temp, 32), p_c_times_2),
		     _mm_slli_epi64(temp, 32));

    temp1 = _mm_cmpgt_epi64(_mm_setzero_si128(), temp);

    temp1 = _mm_and_si128(p, temp1);

    temp = _mm_sub_epi64(temp, temp1);

    modadd_128(temp, *c, c, p);

    modsub_128(b, *c, c, p);
}


#endif	/* RNS_SSE_HELPERS_H_ */
