#ifndef MPFQ_RNS_COMMON_H_
#define MPFQ_RNS_COMMON_H_


#include <gmp.h>
#include <xmmintrin.h>
#include <smmintrin.h>
#include <nmmintrin.h>
#include <immintrin.h>
#include <emmintrin.h>

/*
#define _mm_extract_epi64(x, imm) \
_mm_cvtsi128_si64(_mm_srli_si128((x), 8 * (imm)))

#define _mm_extract_epi32(x, imm) \
_mm_cvtsi128_si32(_mm_srli_si128((x), 4 * (imm)))
*/

typedef __m128i *RnsElement;
typedef const __m128i *constRnsElement;

typedef struct {
    unsigned int moduli_nb;
    RnsElement base;
    unsigned int size;
    RnsElement base_c_2;
    RnsElement Ppinv;
    RnsElement *Ppmodq;
    RnsElement *alphaPmodq;
    int k;
    int s;
    float Delta;
} Rns;


typedef struct {
    unsigned int moduli_nb;
    mpz_t *base;
    unsigned int size;
    mpz_t *base_c;
    mpz_t P;
    mpz_t *Pp;
    mpz_t *Ppinv;
    mpz_t *Ppmodq;
    mpz_t *alphaPmodq;
} Rns_mpz;

#ifdef __cplusplus
extern "C" {
#endif

///////////////////////
// RNS initialisation functions
///////////////////////

void RNS_mpz_set_q(Rns_mpz * R, mpz_srcptr q);
void RNS_mpz_init(Rns_mpz * rnsmpzStruct, int moduli_nb, int modulus_length);
void RNS_init_from_mpz(Rns * rnsStruct, Rns_mpz * rnsmpzStruct);
void RNS_set_from_mpz(Rns * rnsStruct, Rns_mpz * rnsmpzStruct);
void RNS_setq_from_mpz(Rns * rnsStruct, Rns_mpz * rnsmpzStruct);
void RNS_mpz_clear(Rns_mpz * rnsmpzStruct);
void RNS_clear(Rns * rnsStruct);

void rns_set_mpz(RnsElement A_RNS, mpz_srcptr A_MPZ, mpz_t * base, unsigned int moduli_nb);
void mpz_set_rns(mpz_ptr A_MPZ, constRnsElement A_RNS, Rns_mpz * rnsmpzStruct);
void mpz_set_rns_ur(mpz_ptr A_MPZ, constRnsElement A_RNS, Rns_mpz * rnsmpzStruct);
void RNS_mpz_print(Rns_mpz * Rmpz);
void RNS_print(Rns * R);
void RnsElement_print(constRnsElement A, const unsigned int moduli_nb);

void rns_add_n_128(Rns * rns, RnsElement C_RNS, constRnsElement A_RNS,
		   constRnsElement B_RNS);
void rns_sub_n_128(Rns * rns, RnsElement C_RNS, constRnsElement A_RNS,
		   constRnsElement B_RNS);
void rns_neg_n_128(Rns * rns, RnsElement C_RNS, constRnsElement A_RNS);		   
void rns_mul_n_128(Rns * rns, RnsElement C_RNS, constRnsElement A_RNS,
		   constRnsElement B_RNS);
void rns_muladd_n_128(Rns * rns, RnsElement A_RNS, constRnsElement B_RNS,
		      long l);
void rns_mulscalar_n_128(Rns * rns, RnsElement C_RNS, constRnsElement A_RNS,
			 const __m128i B);
void rns_addmulscalar_n_128(Rns * rns, RnsElement C_RNS,
			    constRnsElement A_RNS, const __m128i B,
			    constRnsElement D_RNS);
void rns_modq_n_128(Rns * rns, RnsElement C_RNS, constRnsElement A_RNS);


/*
///////////////////////
// RNS arithmetic operations functions using sse4.2
///////////////////////

void static inline rns_set_zero_n_128 (Rns* rns, RnsElement A_RNS);
void static inline rns_add_n_128 (Rns* rns, RnsElement A_RNS, RnsElement B_RNS);
void static inline rns_sub_n_128 (Rns* rns, RnsElement A_RNS, RnsElement B_RNS);
void static inline rns_muladd_n_128 (Rns* rns, RnsElement A_RNS, RnsElement B_RNS, long lambda);
void static inline rns_mulsub_n_128 (Rns* rns, RnsElement A_RNS, RnsElement B_RNS, long lambda);
void static inline rns_mul_n_128 (Rns* rns, RnsElement C_RNS, RnsElement A_RNS, RnsElement B_RNS);
//void static inline rns_mulscalar_n_128 (RnsSSE* rns, __m128i* A_RNS, __m128i B_RNS, __m128i* C_RNS, __m128i* p, __m128i* p_c_times_2);
//void static inline rns_addmulscalar_n_128 (RnsSSE* rns, __m128i* A_RNS, __m128i B_RNS, __m128i* D_RNS, __m128i* C_RNS, __m128i* p, __m128i* p_c_times_2);



void static inline rns_element_clear(Rns* rns, RnsElement e);
void static inline rns_vec_init(Rns* rns, RnsElement* v, unsigned int);
void static inline rns_vec_add(Rns*, RnsElement* c, constRnsElement* a, constRnsElement* b, unsigned int n);

*/

/* Object-oriented interface */
//#define abase_p_4_oo_impl_name(v)     "RNS"
//static inline
//void rns_clear(abase_vbase_ptr);
// void rns_init(abase_vbase_ptr vbase);

#ifdef __cplusplus
}
#endif

#endif	/* MPFQ_RNS_COMMON_H_ */

