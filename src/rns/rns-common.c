//#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <nmmintrin.h>
#include <math.h>
#include <gmp.h>
#include <time.h>
#include <sys/time.h>
#include <malloc.h>
#include <stdlib.h>

#include "rns-common.h"
#include "rns-sse-helpers.h"

/* set an sse-2 element from two immediate mpz_t's */
static inline __m128i sse_set_mpz(mpz_srcptr mpz_element2, mpz_srcptr mpz_element1)
{
    return _mm_set_epi64(_mm_cvtsi64_m64(mpz_get_si(mpz_element2)),
			 _mm_cvtsi64_m64(mpz_get_si(mpz_element1)));
}
/* compute Ppmodq and alphaPmodq */
void RNS_mpz_set_q(Rns_mpz * R, mpz_srcptr q)
{
    mpz_t temp_1;
    mpz_init(temp_1);
    for (unsigned int i = 0; i < R->moduli_nb; ++i) {
        mpz_gcd(temp_1, R->base[i], q);
        if (mpz_cmp_ui(temp_1, 1) != 0) {
            gmp_fprintf(stderr, "The modulus %Zd is inappropriate for working with this RNS basis\n");
            abort();
        }
    }
    for (unsigned int i = 0; i < R->moduli_nb; ++i) {
	mpz_mod(R->Ppmodq[i], R->Pp[i], q);
    }

    for (unsigned int i = 0; i < R->moduli_nb - 1; i++) {
	mpz_mul_ui(R->alphaPmodq[i], R->P, i + 1);
	mpz_mod(R->alphaPmodq[i], R->alphaPmodq[i], q);
    }
    mpz_clear(temp_1);
}

/* alloc and init the RNS_object fields, set coprime moduli p and compute P, Pp and Ppinv */ 
void RNS_mpz_init(Rns_mpz * R,
                  int moduli_nb,
		  int modulus_length)
{
    mpz_t temp_1;
    mpz_init(temp_1);
    R->moduli_nb = moduli_nb;
    R->size = modulus_length;

    R->base = malloc(R->moduli_nb * sizeof(mpz_t));
    R->base_c = malloc(R->moduli_nb * sizeof(mpz_t));
    R->Pp = malloc(R->moduli_nb * sizeof(mpz_t));
    R->Ppinv = malloc(R->moduli_nb * sizeof(mpz_t));
    R->Ppmodq = malloc(R->moduli_nb * sizeof(mpz_t));
    R->alphaPmodq = malloc((R->moduli_nb - 1) * sizeof(mpz_t));

    mpz_init(R->P);
    for (unsigned int i = 0; i < R->moduli_nb; ++i) {
	mpz_init(R->base[i]);
	mpz_init(R->base_c[i]);
	mpz_init(R->Pp[i]);
	mpz_init(R->Ppinv[i]);
	mpz_init(R->Ppmodq[i]);
    }

    for (unsigned int i = 0; i < R->moduli_nb - 1; ++i) {
	mpz_init(R->alphaPmodq[i]);
    }

    /* Now set the moduli */
    mpz_set_ui(R->P, 1);
    for (unsigned int i = 0, c=1; i < R->moduli_nb; ++i, c+=2) {
        for( ; ; c+=2) {
            mpz_ui_pow_ui(R->base[i], 2, R->size);
	    mpz_sub_ui(R->base[i], R->base[i], c);
            int ok = 1;
	    for (unsigned int j = 0; ok && j < i; j++) {
		mpz_gcd(temp_1, R->base[i], R->base[j]);
		ok = mpz_cmp_ui(temp_1, 1) == 0;
            }
            if (!ok) continue;  /* take next c */
            mpz_set_ui(R->base_c[i], c);
            break;
        }
	mpz_mul(R->P, R->P, R->base[i]);
    }

    for (unsigned int i = 0; i < R->moduli_nb; ++i) {
	mpz_divexact(R->Pp[i], R->P, R->base[i]);
	mpz_invert(R->Ppinv[i], R->Pp[i], R->base[i]);
    }

    mpz_clear(temp_1);
}

/* Initialize the Rns object compatibly from the Rns_mpz object */
void RNS_init_from_mpz(Rns * R, Rns_mpz * RZ)
{
    mpz_t temp_0, temp_1;
    mpz_init(temp_0);
    mpz_init(temp_1);

    R->moduli_nb = RZ->moduli_nb / 2;
    R->size = RZ->size;
    R->k = RZ->size;
    R->s = 10;

    R->base = malloc(R->moduli_nb * sizeof(__m128i));
    R->base_c_2 = malloc(R->moduli_nb * sizeof(__m128i));
    R->Ppinv = malloc(R->moduli_nb * sizeof(__m128i));
    R->Ppmodq = malloc(RZ->moduli_nb * sizeof(RnsElement));
    for (unsigned int i = 0; i < RZ->moduli_nb; ++i)
	R->Ppmodq[i] = malloc(R->moduli_nb * sizeof(__m128i));
    R->alphaPmodq =
	malloc((RZ->moduli_nb - 1) * sizeof(RnsElement));
    for (unsigned int i = 0; i < RZ->moduli_nb - 1; ++i)
	R->alphaPmodq[i] =
	    malloc(R->moduli_nb * sizeof(__m128i));


    for (unsigned int i = 0; i < R->moduli_nb; ++i)
	R->base[i] =
	    sse_set_mpz(RZ->base[2 * i + 1],
			RZ->base[2 * i]);

    for (unsigned int i = 0; i < R->moduli_nb; ++i) {
	mpz_mul_ui(temp_0, RZ->base_c[2 * i], 2);
	mpz_mul_ui(temp_1, RZ->base_c[2 * i + 1], 2);
	R->base_c_2[i] = sse_set_mpz(temp_1, temp_0);
    }
    for (unsigned int i = 0; i < R->moduli_nb; ++i)
	R->Ppinv[i] =
	    sse_set_mpz(RZ->Ppinv[2 * i + 1],
			RZ->Ppinv[2 * i]);

    float epsilon = 0;
    float delta = 0;

    for (unsigned int i = 0; i < R->moduli_nb; ++i) {
	epsilon +=
	    ((_mm_extract_epi64(R->base_c_2[i], 1) +
	      _mm_extract_epi64(R->base_c_2[i], 0)) / 2);
    }

    epsilon = (float) (epsilon / pow(2, R->k));
    delta =
	RZ->moduli_nb *
	(float) ((float) (pow(2, R->k - R->s)) /
		 pow(2, R->k));

    R->Delta = 0.001 + delta + epsilon;

    mpz_clear(temp_0);
    mpz_clear(temp_1);
}

/* Initialize the Rns object fields Ppmodq and alphaPmodq compatibly from the Rns_mpz object*/
void RNS_setq_from_mpz(Rns * R, Rns_mpz * RZ)
{
    for (unsigned int i = 0; i < RZ->moduli_nb; ++i)
	rns_set_mpz(R->Ppmodq[i], RZ->Ppmodq[i],
		    RZ->base, RZ->moduli_nb);
    for (unsigned int i = 0; i < RZ->moduli_nb - 1; ++i)
	rns_set_mpz(R->alphaPmodq[i], RZ->alphaPmodq[i],
		    RZ->base, RZ->moduli_nb);
}

/* Initialize the Rns object compatibly from the Rns_mpz object */
void RNS_set_from_mpz(Rns * R, Rns_mpz * RZ)
{
    RNS_init_from_mpz(R, RZ);
    RNS_setq_from_mpz(R, RZ);
}

/* clear the Rns_mpz object */
void RNS_mpz_clear(Rns_mpz * rnsmpzStruct)
{
    mpz_clear(rnsmpzStruct->P);
    for (unsigned int i = 0; i < rnsmpzStruct->moduli_nb; ++i) {
	mpz_clear(rnsmpzStruct->base[i]);
	mpz_clear(rnsmpzStruct->base_c[i]);
	mpz_clear(rnsmpzStruct->Pp[i]);
	mpz_clear(rnsmpzStruct->Ppinv[i]);
	mpz_clear(rnsmpzStruct->Ppmodq[i]);
    }

    for (unsigned int i = 0; i < rnsmpzStruct->moduli_nb - 1; ++i) {
	mpz_clear(rnsmpzStruct->alphaPmodq[i]);
    }

    free(rnsmpzStruct->base);
    free(rnsmpzStruct->base_c);
    free(rnsmpzStruct->Pp);
    free(rnsmpzStruct->Ppinv);
    free(rnsmpzStruct->Ppmodq);
    free(rnsmpzStruct->alphaPmodq);
}

/* clear the Rns object */
void RNS_clear(Rns * rnsStruct)
{
    free(rnsStruct->base);
    free(rnsStruct->base_c_2);
    free(rnsStruct->Ppinv);
    for (unsigned int i = 0; i < 2 * rnsStruct->moduli_nb; ++i)
	free(rnsStruct->Ppmodq[i]);
    free(rnsStruct->Ppmodq);

    for (unsigned int i = 0; i < 2 * rnsStruct->moduli_nb - 1; ++i)
	free(rnsStruct->alphaPmodq[i]);
    free(rnsStruct->alphaPmodq);
}

/* print the fields of Rns_mpz object */
void RNS_mpz_print(Rns_mpz * Rmpz)
{
    printf("Moduli number : %u \n", Rmpz->moduli_nb);
    printf("Size : %u \n", Rmpz->size);
    printf("Base = [");
    for (unsigned int i = 0; i < Rmpz->moduli_nb; ++i)
	gmp_printf("%Zd, ", Rmpz->base[i]);
    printf("]\n");
    printf("Base_c = [ ");
    for (unsigned int i = 0; i < Rmpz->moduli_nb; ++i)
	gmp_printf("%Zd ", Rmpz->base_c[i]);
    printf("]\n");
    gmp_printf("P = %Zd \n", Rmpz->P);
    gmp_printf("Pp = [");
    for (unsigned int i = 0; i < Rmpz->moduli_nb; ++i)
	gmp_printf("%Zd ", Rmpz->Pp[i]);
    printf("]\n");
    gmp_printf("Ppinv = [");
    for (unsigned int i = 0; i < Rmpz->moduli_nb; ++i)
	gmp_printf("%Zd ", Rmpz->Ppinv[i]);
    printf("]\n");
    gmp_printf("Ppmodq = [");
    for (unsigned int i = 0; i < Rmpz->moduli_nb; ++i)
	gmp_printf("%Zd ", Rmpz->Ppmodq[i]);
    printf("]\n");
    gmp_printf("alphaPmodq = [");
    for (unsigned int i = 0; i < Rmpz->moduli_nb - 1; ++i)
	gmp_printf("%Zd ", Rmpz->alphaPmodq[i]);
    printf("]\n");
}

/* print the fields of Rns object */
void RNS_print(Rns * R)
{
    printf("Moduli number : %u \n", R->moduli_nb);
    printf("Size : %u \n", R->size);
    printf("Base : ");
    for (unsigned int i = 0; i < R->moduli_nb; ++i)
	printf("(%lld, %lld) ", _mm_extract_epi64(R->base[i], 0),
	       _mm_extract_epi64(R->base[i], 1));
    printf("\n");
    printf("Base_c_2 : ");
    for (unsigned int i = 0; i < R->moduli_nb; ++i)
	printf("(%lld, %lld) ", _mm_extract_epi64(R->base_c_2[i], 0),
	       _mm_extract_epi64(R->base_c_2[i], 1));
    printf("\n");
    printf("Ppinv : ");
    for (unsigned int i = 0; i < R->moduli_nb; ++i)
	printf("(%lld, %lld) ", _mm_extract_epi64(R->Ppinv[i], 0),
	       _mm_extract_epi64(R->Ppinv[i], 1));
    printf("\n");
    printf("Ppmodq : \n");
    for (unsigned int i = 0; i < 2 * (R->moduli_nb); ++i) {
	for (unsigned int j = 0; j < R->moduli_nb; ++j)
	    printf("(%lld, %lld) ", _mm_extract_epi64(R->Ppmodq[i][j], 0),
		   _mm_extract_epi64(R->Ppmodq[i][j], 1));
	printf("\n");
    }
    printf("alphaPmodq : \n");
    for (unsigned int i = 0; i < 2 * (R->moduli_nb) - 1; ++i) {
	for (unsigned int j = 0; j < R->moduli_nb; ++j)
	    printf("(%lld, %lld) ", _mm_extract_epi64(R->alphaPmodq[i][j], 0),
		   _mm_extract_epi64(R->alphaPmodq[i][j], 1));
	printf("\n");
    }
    printf("k : %u \n", R->k);
    printf("s : %u \n", R->s);
    printf("Delta : %f \n", R->Delta);
}

/* print the constRnsElement object */
void RnsElement_print(constRnsElement A, const unsigned int moduli_nb)
{
    for (unsigned int j = 0; j < (moduli_nb / 2); ++j)
	printf("(%lld, %lld) ", _mm_extract_epi64(A[j], 0),
	       _mm_extract_epi64(A[j], 1));
    printf("\n");
}

/* put in A_RNS the RNS form of A_MPZ */ 
void rns_set_mpz(RnsElement A_RNS, mpz_srcptr A_MPZ, mpz_t * base, unsigned int moduli_nb)
{
    mpz_t temp_0;
    mpz_t temp_1;

    mpz_init(temp_0);
    mpz_init(temp_1);

    for (unsigned int i = 0; i < (moduli_nb / 2); ++i) {
	mpz_mod(temp_0, A_MPZ, base[2 * i]);
	mpz_mod(temp_1, A_MPZ, base[2 * i + 1]);
	A_RNS[i] =
	    _mm_set_epi64(_mm_cvtsi64_m64(mpz_get_si(temp_1)),
			  _mm_cvtsi64_m64(mpz_get_si(temp_0)));
    }

    mpz_clear(temp_0);
    mpz_clear(temp_1);
}

/* reconstruct in A_MPZ the integer form of A_RNS */
void mpz_set_rns(mpz_ptr A_MPZ, constRnsElement A_RNS, Rns_mpz * rnsmpzStruct)
{
    long int temp_0;
    long int temp_1;

    mpz_t temp;

    mpz_set_ui(A_MPZ, 0);

    for (unsigned int i = 0; i < (rnsmpzStruct->moduli_nb / 2); ++i) {
	mpz_init(temp);
	temp_0 = _mm_extract_epi64(A_RNS[i], 0);
	temp_1 = _mm_extract_epi64(A_RNS[i], 1);
	mpz_mul_ui(temp, rnsmpzStruct->Ppinv[2 * i], temp_0);
	mpz_mod(temp, temp, rnsmpzStruct->base[2 * i]);
	mpz_addmul(A_MPZ, temp, rnsmpzStruct->Pp[2 * i]);
	mpz_mul_ui(temp, rnsmpzStruct->Ppinv[2 * i + 1], temp_1);
	mpz_mod(temp, temp, rnsmpzStruct->base[2 * i + 1]);
	mpz_addmul(A_MPZ, temp, rnsmpzStruct->Pp[2 * i + 1]);
	mpz_clear(temp);

    }

    mpz_mod(A_MPZ, A_MPZ, rnsmpzStruct->P);

}



////////////////////////////////////////////////////////////////////////////////
// modular arithmetic operations functions written on inline assembly
////////////////////////////////////////////////////////////////////////////////

/* {C[i]} = {0} */
void rns_set_zero_n_128(Rns * rns, RnsElement A_RNS)
{
    for (unsigned int i = 0; i < rns->moduli_nb; ++i) {
	A_RNS[i] = _mm_setzero_si128();
    }
}

// {C[i]} = {A[i]} + {B[i]}
void rns_add_n_128(Rns * rns, RnsElement C_RNS, constRnsElement A_RNS,
		   constRnsElement B_RNS)
{
    for (unsigned int i = 0; i < rns->moduli_nb; ++i) {
	modadd_128(A_RNS[i], B_RNS[i], C_RNS + i, rns->base[i]);
    }
}

// {C[i]} = {A[i]} - {B[i]}
void rns_sub_n_128(Rns * rns, RnsElement C_RNS, constRnsElement A_RNS,
		   constRnsElement B_RNS)
{
    for (unsigned int i = 0; i < rns->moduli_nb; ++i) {
	modsub_128(A_RNS[i], B_RNS[i], C_RNS + i, rns->base[i]);
    }
}

// {C[i]} = {-A[i]}
void rns_neg_n_128(Rns * rns, RnsElement C_RNS, constRnsElement A_RNS)
{
    for (unsigned int i = 0; i < rns->moduli_nb; ++i) {
	modneg_128(A_RNS[i], C_RNS + i, rns->base[i]);
    }
}

//(_mm_cvtsi64_m64 (mpz_get_si (mpz_element1)), _mm_cvtsi64_m64 (mpz_get_si (mpz_element2)));
// {A[i]} = {B[i]} * Z, with Z[i] = l for i even, = 0 for i odd.
void rns_muladd_n_128(Rns * rns, RnsElement A_RNS, constRnsElement B_RNS,
		      long l)
{
    __m128i lambda = _mm_set_epi64(_mm_cvtsi64_m64(0L), _mm_cvtsi64_m64(l));
    for (unsigned int i = 0; i < rns->moduli_nb; ++i) {
	modmuladd_128(B_RNS[i], A_RNS[i], lambda, A_RNS + i, rns->base[i],
		      rns->base_c_2[i]);
    }
}

// {C[i]} = {A[i]} * {B[i]}
void rns_mul_n_128(Rns * rns, RnsElement C_RNS, constRnsElement A_RNS,
		   constRnsElement B_RNS)
{
    for (unsigned int i = 0; i < rns->moduli_nb; ++i) {
	modmul_128(A_RNS[i], B_RNS[i], C_RNS + i, rns->base[i],
		   rns->base_c_2[i]);
    }
}

// {C[i]} = {A[i]} % [[RNS.Q]]
void rns_modq_n_128(Rns * rns, RnsElement C_RNS, constRnsElement A_RNS)
{
    RnsElement gamma_RNS = malloc(rns->moduli_nb * sizeof(__m128i));

    rns_mul_n_128(rns, gamma_RNS, A_RNS, rns->Ppinv);

    double alpha = (double) rns->Delta;

    for (unsigned int i = 0; i < rns->moduli_nb; ++i) {
	unsigned long long temp1=_mm_extract_epi64(gamma_RNS[i], 0);
	unsigned long long temp1bis=temp1>>(rns->k - rns->s);
	unsigned long long temp2=( 1ULL << rns->s);
	double temp3=(double) temp1bis;
	double temp4=(double) temp2;
	alpha = alpha +temp3/temp4;
	temp1=_mm_extract_epi64(gamma_RNS[i], 1);
	temp1bis=temp1>>(rns->k - rns->s);
	temp2=( 1ULL << rns->s);
	temp3=(double) temp1bis;
	temp4=(double) temp2;
	alpha = alpha +temp3/temp4;
    }
    rns_mulscalar_n_128(rns, C_RNS, rns->Ppmodq[0],
			_mm_set1_epi64(_mm_cvtsi64_m64
				       (_mm_extract_epi64(gamma_RNS[0], 0))));

    for (unsigned int i = 1; i < 2 * rns->moduli_nb; i++) {
	if ((i % 2) == 0)
	    rns_addmulscalar_n_128(rns, C_RNS, rns->Ppmodq[i],
				   _mm_set1_epi64(_mm_cvtsi64_m64
						  (_mm_extract_epi64
						   (gamma_RNS[(i / 2)], 0))),
				   C_RNS);
	else
	    rns_addmulscalar_n_128(rns, C_RNS, rns->Ppmodq[i],
				   _mm_set1_epi64(_mm_cvtsi64_m64
						  (_mm_extract_epi64
						   (gamma_RNS[(i / 2)], 1))),
				   C_RNS);
    }
    if (alpha >= 1)
	rns_sub_n_128(rns, C_RNS, C_RNS, rns->alphaPmodq[(int) (alpha - 1)]);
}

/* {C[i]} = {A[i]} * {B} */
void rns_mulscalar_n_128(Rns * rns, RnsElement C_RNS, constRnsElement A_RNS,
			 const __m128i B)
{
    for (unsigned int i = 0; i < rns->moduli_nb; ++i) {
	modmul_128(A_RNS[i], B, C_RNS + i, rns->base[i], rns->base_c_2[i]);
    }
}

/* {C[i]} = {A[i]} * {B} + {D[i]} */
void rns_addmulscalar_n_128(Rns * rns, RnsElement C_RNS,
			    constRnsElement A_RNS, const __m128i B,
			    constRnsElement D_RNS)
{

    for (unsigned int i = 0; i < rns->moduli_nb; ++i) {
	modaddmul_128(A_RNS[i], B, D_RNS[i], C_RNS + i, rns->base[i],
		      rns->base_c_2[i]);
    }

}
