#!/usr/bin/perl -w

use warnings;
use strict;
use File::Spec;
my $dirname;

BEGIN {
    $dirname = (File::Spec->splitpath($0))[1];
    unshift @INC, "$dirname/../perl-lib";
    unshift @INC, "$dirname/perl";
    unshift @INC, "$dirname";
}

use Mpfq::engine::conf qw(read_api);
# use Mpfq::engine::handler qw(create_code create_files);
# used by OO example (currently commented out):
# use Mpfq::engine::oo qw(create_abstract_bases_headers);
use Mpfq::engine::utils qw($debuglevel debug);

# use Mpfq::gfp;
# use Mpfq::gfpmgy;
# use p127_1;
# use p127_735;
# use p25519;
# use pz

use rns;

my @api_extensions = qw/POLY/;

my $api_file = "$dirname/../api.pl";

sub usage_and_die {
    print STDERR "usage: ./gen_rns.pl w=<w> nmoduli=<nmoduli> where\n"; 
    print STDERR "    <w> is the word size (32 or 64)\n";
    print STDERR "    2*<nmoduli> is the number of moduli (that means that only an even number of moduli can be given)\n";
    die @_;
}

MAIN: {
    # Deal with command-line arguments
    my $w;
    my $nmoduli;
    my $tag;
    my $output_path=".";

    while (scalar @ARGV) {
        $_ = $ARGV[0];
        if (/^w=(.*)$/) { $w=$1; shift @ARGV; next; }
        if (/^nmoduli=(.*)$/) { $nmoduli=$1; shift @ARGV; next; }
        if (/^output_path=(.*)$/) { $output_path=$1; shift @ARGV; next; }
        if (/^-d=?(\d+)$/) { $debuglevel=$1; shift @ARGV; next; }
        if (/^-d$/) { $debuglevel++; shift @ARGV; next; }
        last;
    }

    if (!defined($w) || !defined($nmoduli)) {
        usage_and_die "Missing arguments.\n";
    }
    

    my $object;

    if ($nmoduli=~ /^[1-9]$/) {
	$object = rns->new();
	$tag = "rns_$nmoduli";
    } else {
	usage_and_die "Invalid value for nmoduli.\n";
    }

    my $api;
        $api = Mpfq::engine::conf::read_api $api_file, @api_extensions;
    
    my $nonure=0;

    my $code = {
        includes => [ qw{
              <stdio.h>
              <stdlib.h>
              <gmp.h>
              <string.h>
              <ctype.h>
              <limits.h>
              "mpfq.h"
          } ],
      };

    my $options = { w=>$w, nmoduli=>$nmoduli, fieldtype=>"prime", tag=>$tag, nonure=>$nonure };

    # This is an example of the OO code generation mechanism. One must
    # enable the relevant "use Mpfq::engine::oo;" line in this script's
    # header.
#     $options->{'tag'} = $tag;
#     my $vbase_interface_substitutions = [];
#     push @$vbase_interface_substitutions,
#         [ qr/@!$_ \*/, "void *" ],
#         [ qr/@!src_$_\b/, "const void *" ],
#         [ qr/@!$_\b/, "void *" ],
#         [ qr/@!dst_$_\b/, "void *" ]
#         for (qw/elt elt_ur vec vec_ur poly/);
#     $options->{'virtual_base'} = {
#         name => "gfp_vbase",
#         filebase => "gfp_vbase",
#         substitutions => $vbase_interface_substitutions,
#     };
#     $options->{'family'} = [ qw/p_1 p_2/ ];
#     $options->{'prefix'} = "gfp_";
#     create_abstract_bases_headers($output_path, $api, $options);

    $object->create_code($api, $code, $options);
    $object->create_files($output_path, $tag, $api, $code);
}
