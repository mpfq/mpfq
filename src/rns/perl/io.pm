package io;

use strict;
use warnings;


sub code_for_asprint {
    my $opt = shift @_;
    my $n = $opt->{'nmoduli'};
    my $w = $opt->{'w'};
    my $proto = 'function(k,pstr,x)';
    my $code = <<EOF;
mpz_t xint;
mpz_init(xint);
mpz_set_rns(xint, x, &(k->rnsz));
long int i=xint->_mp_size;
assert(i>=0);
while ((i>=0)&&((xint->_mp_d)[i]==0)) {
    i--;
}
i++;
long int size_xint=i;
mp_limb_t y[size_xint];
for (i = 0; i<size_xint; ++i) {
    y[i]=(xint->_mp_d)[i];
}
// allocate enough room for base 2 conversion.
// mpn_get_str may produce one extra byte

*pstr = (char *)mpfq_malloc_check(size_xint * GMP_LIMB_BITS+2);
long int n = mpn_get_str((unsigned char*)(*pstr), k->io_base, (mp_limb_t *) y, size_xint);
for (i = 0; i < n; ++i)
    (*pstr)[i] += '0';
(*pstr)[n] = '\\0';
// Remove leading 0s
/* Note that gmp source says: There are no leading zeros on the digits
 * generated at str, but that's not currently a documented feature.
 * This implies that we won't do much here... */
int shift = 0;
while (((*pstr)[shift] == '0') && ((*pstr)[shift+1] != '\\0')) 
    shift++;
if (shift>0) {
    memmove(*pstr, (*pstr) + shift, n + 1 - shift);
    n -= shift;
}
// Return '0' instead of empty string for zero element
if ((*pstr)[0] == '\\0') {
    (*pstr)[0] = '0';
    (*pstr)[1] = '\\0';
    n = 1;
}
return n;
EOF
    return [ $proto, $code ];
}

sub code_for_sscan {
    my $opt = shift @_;
    my $n = $opt->{'nmoduli'};
    my $w = $opt->{'w'};
    my $proto = 'function(k,z,str)';
    my $code = <<EOF;
mpz_t zz;
mpz_init(zz);
int nread;
if (gmp_sscanf(str, "%Zd%n", zz, &nread) != 1) {
    mpz_clear(zz);
    return 0;
}
rns_set_mpz(z, zz, (k->rnsz).base,(k->rnsz).moduli_nb);
mpz_clear(zz);
return nread;
EOF
    return [ $proto, $code ];
}

sub code_for_fscan {
    my $opt = shift @_;
    my $n = $opt->{'nmoduli'};
    my $w = $opt->{'w'};
    my $proto = 'function(k,file,z)';
    my $code = <<EOF;
char *tmp;
int allocated, len=0;
int c, start=0;
allocated=100;
tmp = (char *)mpfq_malloc_check(allocated);
for(;;) {
    c = fgetc(file);
    if (c==EOF)
        break;
    if (isspace((int)(unsigned char)c)) {
        if (start==0)
            continue;
        else
            break;
    } else {
        if (len==allocated) {
            allocated+=100 + allocated / 4;
            tmp = (char*)realloc(tmp, allocated);
        }
        tmp[len]=c;
        len++;
        start=1;
    }
}
if (len==allocated) {
    allocated+=1;
    tmp = (char*)realloc(tmp, allocated);
}
tmp[len]='\\0';
int ret=@!sscan(k,z,tmp);
free(tmp);
return ret ? len : 0;
EOF
    return [ $proto, $code ];
}

sub code_for_cxx_in {
    my $opt = shift @_;
    my $n = $opt->{'nmoduli'};
    my $w = $opt->{'w'};
    my $proto = 'function(k,is,z)';
    my $code = <<EOF;
char *tmp;
int allocated, len=0;
int start=0;
allocated=100;
tmp = (char *)mpfq_malloc_check(allocated);
for(;;) {
    char c;
    if (!(is.get(c)))
        break;
    if (isspace(c)) {
        if (start==0)
            continue;
        else
            break;
    } else {
        if (len==allocated) {
            allocated+=100 + allocated / 4;
            tmp = (char*)realloc(tmp, allocated);
        }
        tmp[len]=c;
        len++;
        start=1;
    }
}
if (len==allocated) {
    allocated+=1;
    tmp = (char*)realloc(tmp, allocated);
}
tmp[len]='\\0';
int ret=@!sscan(k,z,tmp);
if (ret != len)
    is.setstate(std::ios::failbit);
free(tmp);
return is;
EOF
    return [ $proto, $code ];
}

1;
