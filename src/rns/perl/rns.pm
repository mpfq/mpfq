package rns;

use strict;
use warnings;

use Mpfq::engine::handler;
our @ISA = qw/Mpfq::engine::handler/;
sub new { return bless({}, shift); }

use Mpfq::defaults;
use Mpfq::defaults::vec;
use Mpfq::defaults::flatdata;
use Mpfq::defaults::poly;
use elt;
use field;
use io;
use conv;

our @parents = qw/
    Mpfq::defaults
    Mpfq::defaults::vec
    Mpfq::defaults::flatdata
    Mpfq::defaults::poly
    elt
    field
    io
    conv
/;

our $resolve_conflicts = {
       vec_set => 'Mpfq::defaults::flatdata',
       vec_ur_set => 'Mpfq::defaults::flatdata',
       is_zero => 'elt',
       cmp => 'elt'
};


sub init_handler {
    my ($opt) = @_;
    my $n = $opt->{'nmoduli'};
    my $types = {
       field => "
// Info for Tonelli-Shanks. 
// Let q be the field cardinality (odd).
// Write q - 1 = h*2^e, with h odd.
// A value of e=0 means that the data is not initialized
typedef struct {
    int e;
    void * z; // a Generator of the 2-Sylow, castable into an elt.
    mpz_t hh;
//    mp_limb_t * hh; // (h-1)/2, stored as an mpn of same length as q.
} ts_info_struct;

struct @!field_struct {
   mpz_t p;
   Rns rns;
   Rns_mpz rnsz;
   ts_info_struct ts_info;
   mpz_t factor;
   int io_base;

};

typedef struct @!field_struct @!field[1];",
       src_field  =>	'typedef const struct @!field_struct * @!src_field;',
       dst_field  =>	'typedef struct @!field_struct * @!dst_field;',

       elt =>	"typedef __m128i @!elt\[$n\];",
       dst_elt =>	"typedef __m128i * @!dst_elt;",
       src_elt =>	"typedef const __m128i * @!src_elt;",

    };
    my $includes = [ qw {
              "rns-common.h"
              "rns-sse-helpers.h"
              } ];
    return { types => $types, includes=>$includes, };
}



1;
