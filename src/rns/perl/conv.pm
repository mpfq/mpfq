package conv;

use strict;
use warnings;


 sub code_for_vec_conv {
     my $proto = 'inline(K!,w,u,n,v,m)';
     my $code = <<EOF;
 unsigned int i, k;
 @!elt z;
 @!init(K, &z);
 // swap pointers to have n <= m
 @!src_vec uu, vv;
 if (n <= m) {
     uu = u; vv = v;
 } else {
    uu = v; vv = u;
     unsigned int tmp = n;
     n = m; m = tmp;
 }
 for(k = 0; k < n; ++k) {
     @!mul(K, w[k], uu[0], vv[k]);
     for(i = 1; i <= k; ++i) {
         @!mul(K, z, uu[i], vv[k-i]);
         @!add(K, w[k], w[k], z);
     }
     @!normalize(K, w[k]);
 }
 for(k = n; k < m; ++k) {
     @!mul(K, w[k], uu[0], vv[k]);
     for(i = 1; i < n; ++i) {
         @!mul(K, z, uu[i], vv[k-i]);
         @!add(K, w[k], w[k], z);
     }
     @!normalize(K, w[k]);
 }
 for(k = m; k < n+m-1; ++k) {
     @!mul(K, w[k], uu[k-m+1], vv[m-1]);
     for(i = k-m+2; i < n; ++i) {
         @!mul(K, z, uu[i], vv[k-i]);
         @!add(K, w[k], w[k], z);
     }
     @!normalize(K, w[k]);
 }
 @!clear(K, &z);
EOF
     return [ $proto, $code ];
}

1;
