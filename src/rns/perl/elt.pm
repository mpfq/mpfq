package elt;

use strict;
use warnings;


sub code_for_mul {
    return [ 'inline(k, z, x, y)', "rns_mul_n_128(&(k->rns), z, x, y);" ];
}

sub code_for_sqr {
    return [ 'macro(k, z, x)', "@!mul(k, z, x, x)" ];
}

sub code_for_add {
    return [ 'inline(k, z, x, y)', "rns_add_n_128(&(k->rns), z, x, y);" ];
}

sub code_for_sub {
    return [ 'inline(k, z, x, y)', "rns_sub_n_128(&(k->rns), z, x, y);" ];
}


sub code_for_inv {
    my $opt = shift @_;
    my $proto = 'inline(k,z,x)';
    my $type = $opt->{'type'};
    my $code = <<EOF;
mpz_t x_mpz;
mpz_init(x_mpz);
@!get_mpz(k,x_mpz,x);
int ret=mpz_invert(x_mpz, x_mpz, (k->p));
rns_set_mpz(z, x_mpz,(k->rnsz).base,(k->rnsz).moduli_nb);
mpz_clear(x_mpz); 
return ret;
EOF
    return [ $proto, $code ];
}



sub code_for_normalize {
    return [ 'inline(k, z)', "rns_modq_n_128(&(k->rns), z, z);" ];
}

##########################################################"

sub code_for_set_ui {
    my $opt = shift @_;
    my $n = $opt->{'nmoduli'};
    my $code = <<EOF;
    unsigned int i;
    for(i = 0 ; i < $n ; ++i) {
	__m128i zero =_mm_setzero_si128();
        __m128i x = _mm_set1_epi64(_mm_cvtsi64_m64(a));
        __m128i flags = _mm_cmpgt_epi64(zero, x);
        x =_mm_sub_epi64(x, _mm_and_si128(flags, ((k->rns).base)[i]));
        flags = _mm_cmpgt_epi64(zero, x);
        r[i] =_mm_sub_epi64(x, _mm_and_si128(flags, ((k->rns).base)[i]));
    }
EOF
    return [ 'inline(k!, r, a)', $code ];
}

sub code_for_get_ui {
    my $code = <<EOF;
mpz_t z;
mpz_init(z);
mpz_set_rns(z, r, &(k->rnsz));
mpz_mod(z,z,k->p);
unsigned long a=mpz_get_ui(z);
mpz_clear(z);
return a;

EOF
    return [ 'inline(k!, r)', $code ];
}

#Is here for the api, but is awful as we normalize everywhere
sub code_for_pow {
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $proto = 'inline(k,res,r,x,n)';
    my $code = <<EOF;
@!elt u, a,b;
long i, j, lead;     /* it is a signed type */
unsigned long mask;

/* get the correct (i,j) position of the most significant bit in x */
for(i = ((long)n)-1; i>=0 && x[i]==0; i--)
    ;
if (i < 0) {
    /* power zero gets 1 */
    @!set_ui(k, res, 1);
    return;
}
j = GMP_LIMB_BITS - 1;
mask = (1UL<<j);
for( ; (x[i]&mask)==0 ;j--, mask>>=1)
    ;
lead = i*GMP_LIMB_BITS+j;      /* Ensured. */

@!init(k, &u);
@!init(k, &a);
@!init(k, &b);
@!set(k, a, r);
@!normalize(k,a);
@!set(k, b, a);
for( ; lead > 0; lead--) {
    if (j-- == 0) {
        i--;
        j = GMP_LIMB_BITS-1;
        mask = (1UL<<j);
    } else {
        mask >>= 1;
    }
    if (x[i]&mask) {
        @!sqr(k, u, a);
        @!normalize(k, u);
        @!mul(k, a, u, b);
        @!normalize(k, a);
    } else {
        @!sqr(k, a,a);
        @!normalize(k, a);
    }
}
@!set(k, res, a);
@!clear(k, &u);
@!clear(k, &a);
@!clear(k, &b);
EOF
    return [ $proto, $code ];
}


#Same remark as  pow
# Getting the correct way to reduce the exponent modulo 2, even when
# doing it roughly, is not exactly trivial.
sub code_for_powz {
    my $opt = shift @_;
    my $proto = 'function(k,y,x,z)';
    my $code = <<EOF;
    if (mpz_sgn(z) < 0) {
        mpz_t mz;
        mpz_init(mz);
        mpz_neg(mz, z);
        @!powz(k, y, x, mz);
        @!inv(k, y, y);
        @!normalize(k, y);
        mpz_clear(mz);
    } else if (mpz_cmp(z,k->p)>0) {
        mpz_t zr;
        mpz_init(zr);
        mpz_t ppz;
        mpz_init(ppz);
        @!field_characteristic(k, ppz);
        mpz_sub_ui(ppz,ppz,1);
        mpz_fdiv_r(zr, z, ppz);
        @!powz(k, y, x, zr);
        mpz_clear(ppz);
        mpz_clear(zr);
    } else {
        @!pow(k, y, x, z->_mp_d, mpz_size(z));
    }
EOF
    return [ $proto, $code ];
}

sub code_for_init_ts {
    my $proto = 'function(k)';
    my $requirements = 'dst_field';
    my $name = 'init_ts';
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $n = $opt->{'nmoduli'};
    my $code = <<EOF;
gmp_randstate_t rstate;
gmp_randinit_default(rstate);
mpz_t pp,s;
mpz_init(pp);
mpz_init(s);
mpz_sub_ui(pp, k->p, 1);
mp_bitcnt_t e=mpz_scan1(pp,0);
mpz_tdiv_q_2exp(pp,pp,e);
mpz_ui_pow_ui(s, 2, (e-1));
k->ts_info.e = e;

k->ts_info.z = mpfq_malloc_check(sizeof(@!elt));

@!elt z, r;
mpz_t mz, mr,mpp,ms;
mpz_init(mz);
mpz_init(mpp);
mpz_init(ms);
mpz_init(mr);
@!init(k, &z);
@!init(k, &r);

do {
    @!random(k, z, rstate);
    @!powz(k,z,z,pp);
    @!powz(k,r,z,s);
    @!add_ui(k, r, r, 1);
    @!normalize(k,r);
} while (@!cmp_ui(k,r, 0)!=0);
@!set(k, (@!dst_elt)k->ts_info.z, z);

@!clear(k, &z);
@!clear(k, &r);

mpz_sub_ui(pp, pp, 1);
mpz_tdiv_q_2exp(k->ts_info.hh,pp, 1);

mpz_clear(mz);
mpz_clear(mpp);
mpz_clear(ms);
mpz_clear(mr);
mpz_clear(pp);
mpz_clear(s);
gmp_randclear(rstate);
EOF
    return { kind=>$proto,
        requirements=>$requirements,
        name=>$name,
        code=>$code,};
}



sub code_for_sqrt {
    my $proto = 'function(k,z,a)';
    my $opt = shift @_;
    my $n = $opt->{'nmoduli'};
    my $code = <<EOF;
if (@!cmp_ui(k, a, 0) == 0) {
    @!set_ui(k, z, 0);
    return 1;
}
if (k->ts_info.e == 0)
    @!init_ts(k);
@!elt b, x, y;
@!init(k, &x);
@!init(k, &y);
@!init(k, &b);
mp_limb_t r = k->ts_info.e;
mp_limb_t s; //= (1UL<<(r-1)); not needed...
@!set(k, x, a);
@!set(k, y, k->ts_info.z);

@!powz(k, x, a, k->ts_info.hh);
@!sqr(k, b, x);
@!normalize(k,b);
@!mul(k, x, x, a);
@!normalize(k,x);
@!mul(k, b, b, a);
@!normalize(k,b);

@!elt t;
@!init(k, &t);
mp_limb_t m;

for(;;) {
    @!set(k, t, b);
    for(m=0; @!cmp_ui(k, t, 1)!=0; m++)
        @!sqr(k, t, t);
        @!normalize(k,t);

    assert(m<=r);
    
    if (m==0 || m==r)
        break;
    
    s = 1UL<<(r-m-1);
    r = m;
    
    @!pow(k, t, y, &s, 1);
    @!sqr(k, y, t);
@!normalize(k,y);
    @!mul(k, x, x, t);
@!normalize(k,x);
    @!mul(k, b, b, y);
@!normalize(k,b);
}
@!set(k, z, x);
@!clear(k, &t);
@!clear(k, &x);
@!clear(k, &y);
@!clear(k, &b);
return (m==0);
EOF
    return [ $proto, $code ], code_for_init_ts($opt);
}

sub code_for_hadamard {
    my $opt = shift @_;
    my $proto = 'inline(k,x,y,z,t)';
    my $code = <<EOF;
@!elt tmp;
@!init(k, &tmp);
@!add(k, tmp, x, y);
@!sub(k, y, x, y);
@!set(k, x, tmp);
@!add(k, tmp, z, t);
@!sub(k, t, z, t);
@!set(k, z, tmp);
@!sub(k, tmp, x, z);
@!add(k, x, x, z);
@!add(k, z, y, t);
@!sub(k, t, y, t);
@!set(k, y, tmp);
@!clear(k, &tmp); 
EOF
    return [ $proto, $code ];
}

sub code_for_frobenius { return [ 'macro(k,x,y)', '@!set(k, x, y)']; }



sub code_for_is_sqr {
    my $proto = 'inline(k,x)';
    my $opt = shift @_;
    my $n = $opt->{'nmoduli'};
    my $code = <<EOF;
mpz_t pp;
mpz_init(pp);
@!elt y;
mpz_sub_ui(pp, k->p, 1);
mpz_tdiv_q_2exp(pp, pp, 1);
@!init(k, &y);
@!powz(k, y, x, pp);
int res = @!cmp_ui(k, y, 1);
@!clear(k, &y);
mpz_clear(pp);
if (res == 0)
    return 1;
else 
    return 0;
EOF
    return [ $proto, $code ];
}

sub code_for_set_mpz {
    my $opt = shift @_;
    my $proto = 'inline(k,r,z)';
    my $code = <<EOF;
rns_set_mpz(r, z, (k->rnsz).base, (k->rnsz).moduli_nb);
EOF
    return [ $proto, $code ];
}

sub code_for_set_mpn {
    my $opt = shift @_;
    my $n = $opt->{'nmoduli'};
    my $proto = 'inline(k,r,x,n)';
    my $code = <<EOF;

    mp_limb_t temp_0;
    mp_limb_t temp_1;
    for (unsigned int i = 0; i < ((1+$n)/2); ++i) {
	temp_0=mpn_mod_1(x, n, (mp_limb_t) mpz_get_si((k->rnsz).base[2 * i]));
	temp_1=mpn_mod_1(x, n, (mp_limb_t) mpz_get_si((k->rnsz).base[2 * i + 1]));
	r[i] =
	    _mm_set_epi64(_mm_cvtsi64_m64(temp_1),
			  _mm_cvtsi64_m64(temp_0));
    }
EOF
    return [ $proto, $code ];
}

#There is a problem for this one because one can't say how long is the copy room
# sub code_for_get_mpn {
#     my $opt = shift @_;
#     my $n = $opt->{'nmoduli'};
#     my $proto = 'inline(k!,r,x)';
#     my $code =<<EOF;
# mpz_t temp;
# mpz_init(temp);
# @!get_mpz(k, temp, x);
# //TO_SOLVEmpfq_copy(r, temp->_mp_d, $n);
# EOF
#     return [ $proto, $code ];
# }
# 
sub code_for_get_mpz {
    my $opt = shift @_;
    my $n = $opt->{'nmoduli'};
    my $proto = 'inline(k!,z,y)';
    my $code = <<EOF;
mpz_set_rns(z, y, &(k->rnsz));
mpz_mod(z,z,k->p);
EOF
    return [ $proto, $code ];
}

sub code_for_cmp {
    my $opt = shift @_;
    my $code=<<EOF;
mpz_t temp_0;
mpz_t temp_1;
mpz_init(temp_0);
mpz_init(temp_1);
mpz_set_rns(temp_0, x, &(k->rnsz));
mpz_mod(temp_0, temp_0,k->p);
mpz_set_rns(temp_1, y, &(k->rnsz));
mpz_mod(temp_1, temp_1,k->p);
int res= mpz_cmp(temp_0, temp_1);
mpz_clear(temp_0);
mpz_clear(temp_1);
return res;
EOF
    return [ 'inline(k!,x,y)', $code ];
}

sub code_for_cmp_ui {
    my $opt = shift @_;
    my $n = $opt->{'nmoduli'};
    my $proto = 'inline(k!,x,y)';
    my $code=<<EOF;
mpz_t temp_0;
mpz_t temp_1;
mpz_init(temp_0);
mpz_init(temp_1);
mpz_set_rns(temp_0, x, &(k->rnsz));
mpz_mod(temp_0, temp_0,k->p);
mpz_set_ui(temp_1,y);
mpz_mod(temp_1, temp_1,k->p);
int res= mpz_cmp(temp_0, temp_1);
mpz_clear(temp_0);
mpz_clear(temp_1);
return res;
EOF
    return [ $proto, $code ];
}


sub code_for_is_zero {
    my $code = <<EOF;
return @!cmp_ui(k,r,0);
EOF
    return [ 'inline(k!, r)', $code ];
}



sub code_for_add_ui {
    my $code = <<EOF;
    @!elt aa;
    @!init(K, aa);
    @!set_ui(K, aa, a);
    @!add(K, s, r, aa);
    @!clear(K, aa);
EOF
    return [ 'inline(K!, s, r, a)', $code ];
}

sub code_for_sub_ui {
    my $code = <<EOF;
    @!elt aa;
    @!init(K, aa);
    @!set_ui(K, aa, a);
    @!sub(K, s, r, aa);
    @!clear(K, aa);
EOF
    return [ 'inline(K!, s, r, a)', $code ];
}

sub code_for_mul_ui {
    my $code = <<EOF;
    @!elt aa;
    @!init(K, aa);
    @!set_ui(K, aa, a);
    @!mul(K, s, r, aa);
    @!clear(K, aa);
EOF
    return [ 'inline(K!, s, r, a)', $code ];
}




sub code_for_random {
    my $opt = shift @_;
    my $n = $opt->{'nmoduli'};
    my $proto = 'inline(k, x, state)';
    my $code = <<EOF;
  mpz_t z;
  mpz_init(z);
  mpz_urandomm(z, state, k->p);
  @!set_mpz(k,x, z);
  mpz_clear(z);
EOF
    return [ $proto, $code ];
}

#The same as random. One have to solve the problem of having
#big sequences of 0 and 1
sub code_for_random2 {
    my $opt = shift @_;
    my $n = $opt->{'nmoduli'};
    my $proto = 'inline(k, x, state)';
    my $code = <<EOF;
  mpz_t z;
  mpz_init(z);
  mpz_urandomm(z, state, k->p);
  rns_set_mpz(x, z, (k->rnsz).base, 2*$n);
  mpz_clear(z);
EOF
    return [ $proto, $code ];
}

sub code_for_neg {
    return [ 'inline(k, z, x)', "rns_neg_n_128(&(k->rns), z, x);" ];
}



1;
