package field;

use strict;
use warnings;

#This one gives a bound for p to make sure that the normalize function gives the good result.
#Some other bound may need to be added to make other operators, such as multiplication, compute the good result.
sub code_for_impl_max_characteristic_bits {
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $n = $opt->{'nmoduli'};
    my $tmp=0;
    while (2**$tmp<$n) {$tmp+=1};
    my $max = (2*$n-1)*($w-1)-3-$tmp;
#    return [ 'function(k)', "return $max;"];
  return[ 'macro()', $max ];
    
}

sub code_for_impl_max_degree {
    return [ 'macro()', '1' ];
}

sub code_for_field_init { 
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $w1 = $opt->{'w'}-1;
    my $n = $opt->{'nmoduli'};
    my $code = <<EOF;
mpz_init(k->p);
k->io_base = 10;
mpz_init(k->factor);
k->ts_info.e=0;
mpz_init(k->ts_info.hh);
    RNS_mpz_init(&(k->rnsz), 2*$n, $w1);
    RNS_init_from_mpz(&(k->rns), &(k->rnsz));
EOF
    return [ 'function(k)', $code ];
}

sub code_for_field_specify {
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $code = <<EOF;
    if (dummy == MPFQ_PRIME_MPZ) {
        mpz_set(k->p,vp);
        mpz_srcptr p = (mpz_srcptr) vp;
        RNS_mpz_set_q(&(k->rnsz), p);
        RNS_setq_from_mpz(&(k->rns), &(k->rnsz));
    } else {
        abort();
    }
EOF
    return [ 'function(k,dummy,vp)' , $code ]; 
}

sub code_for_field_clear {
    my $code = <<EOF;
    mpz_clear(k->p);
    if (k->ts_info.e > 0) {
        free(k->ts_info.z);
    }
mpz_clear(k->ts_info.hh);
    mpz_clear(k->factor);
    RNS_mpz_clear(&(k->rnsz));
    RNS_clear(&(k->rns));
EOF
    return [ 'function(k)', $code ];
}



sub code_for_field_degree { return [ 'macro(K!)', '1' ]; }


sub code_for_field_characteristic {
    my $proto = 'inline(k,z)';
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $code = <<EOF;
    mpz_set(z, k->p);
EOF
    return [ $proto, $code ];
}

sub code_for_field_characteristic_srcptr {
    return [ "macro(k)", "return k->p;" ];
}

#This one is weird for rns
sub code_for_field_characteristic_bits {
    my $proto = 'inline(k)';
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $code = <<EOF;
    return mpz_sizeinbase(k->p, 2);
EOF
    return [ $proto, $code ];
}

sub code_for_field_setopt { return [ 'macro(f,x,y)' , '' ]; }



1;
