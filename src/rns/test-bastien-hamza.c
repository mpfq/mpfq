#include <stdio.h>
#include <time.h>
#include "rns-common.h"

#define ASSERT_ALWAYS(x) do {						\
    if (!(x)) {								\
        fprintf(stderr, "Assertion failed: " #x " at %s:%d",		\
                __FILE__, __LINE__);					\
        abort();							\
    }									\
} while (0)

int main()
{
    mpz_t Q, x, y, z, xPlusy, xMinusy, xMuly, xMulscalar, xModq;

    mpz_init(Q);
    mpz_init(x);
    mpz_init(y);
    mpz_init(z);
    mpz_init(xPlusy);
    mpz_init(xMinusy);
    mpz_init(xMuly);
    mpz_init(xMulscalar);
    mpz_init(xModq);

    uint random_seed = time(NULL);
    //uint random_seed = 12;
    srand(random_seed);
    gmp_randstate_t rnd_state;
    gmp_randinit_default(rnd_state);
    gmp_randseed_ui(rnd_state, random_seed);

    int moduli_nb = 6;
    int modulus_length = 63;


    mpz_set_str(Q,
		"1766847064778384329583297500742918515827483896875618958121606201292619891",
		10);

    Rns_mpz Rmpz;
    Rns R;

    printf("Init RNS_mpz \n");

    //RNS_mpz_init(&Rmpz, Q, moduli_nb, modulus_length);
    RNS_mpz_init(&Rmpz, moduli_nb, modulus_length);
    RNS_mpz_set_q(&Rmpz, Q);
    printf("Init RNS_mpz : OK\n");

    RNS_mpz_print(&Rmpz);

    printf("Init Rns \n");
    RNS_set_from_mpz(&R, &Rmpz);
    printf("Init Rns : OK\n");

    RNS_print(&R);

    mpz_urandomm(x, rnd_state, Q);
    mpz_urandomm(y, rnd_state, Q);
    //mpz_set_ui (x, 2500000);
    //mpz_set_ui (y, 1000000);

    gmp_printf("Q = %Zd\n", Q);
    gmp_printf("x = %Zd\n", x);
    gmp_printf("y = %Zd\n", y);

    RnsElement x_RNS = malloc((moduli_nb / 2) * sizeof(__m128i));
    RnsElement y_RNS = malloc((moduli_nb / 2) * sizeof(__m128i));
    RnsElement z_RNS = malloc((moduli_nb / 2) * sizeof(__m128i));

    rns_set_mpz(x_RNS, x, Rmpz.base, moduli_nb);
    rns_set_mpz(y_RNS, y, Rmpz.base, moduli_nb);

    printf("x_RNS = ");
    RnsElement_print(x_RNS, moduli_nb);
    printf("y_RNS = ");
    RnsElement_print(y_RNS, moduli_nb);


    printf("\n**** Addition *****\n");

    rns_add_n_128(&R, z_RNS, x_RNS, y_RNS);

    printf("z_RNS = ");
    RnsElement_print(z_RNS, moduli_nb);

    mpz_set_rns(z, z_RNS, &Rmpz);
    gmp_printf("z = %Zd\n", z);
    mpz_add(xPlusy, x, y);
    gmp_printf("x+y = %Zd\n", xPlusy);
    ASSERT_ALWAYS(mpz_cmp(z, xPlusy) == 0);

    printf("\n**** Subtraction *****\n");

    rns_sub_n_128(&R, z_RNS, x_RNS, y_RNS);

    printf("z_RNS = ");
    RnsElement_print(z_RNS, moduli_nb);

    mpz_set_rns(z, z_RNS, &Rmpz);
    gmp_printf("z = %Zd\n", z);
    mpz_sub(xMinusy, x, y);
    mpz_mod(xMinusy, xMinusy, Rmpz.P);
    gmp_printf("x-y = %Zd\n", xMinusy);
    ASSERT_ALWAYS(mpz_cmp(z, xMinusy) == 0);

    printf("\n**** Multiplication *****\n");

    rns_mul_n_128(&R, z_RNS, x_RNS, y_RNS);

    printf("z_RNS = ");
    RnsElement_print(z_RNS, moduli_nb);

    mpz_set_rns(z, z_RNS, &Rmpz);
    gmp_printf("z = %Zd\n", z);
    mpz_mul(xMuly, x, y);
    mpz_mod(xMuly, xMuly, Rmpz.P);
    gmp_printf("x*y = %Zd\n", xMuly);
    ASSERT_ALWAYS(mpz_cmp(z, xMuly) == 0);


    printf("\n**** Multiplication by a scalar *****\n");

    rns_mulscalar_n_128(&R, z_RNS, x_RNS,
			_mm_set1_epi64(_mm_cvtsi64_m64(10023)));

    printf("z_RNS = ");
    RnsElement_print(z_RNS, moduli_nb);
    mpz_set_rns(z, z_RNS, &Rmpz);
    gmp_printf("z = %Zd\n", z);
    mpz_mul_ui(xMulscalar, x, 10023);
    mpz_mod(xMulscalar, xMulscalar, Rmpz.P);
    gmp_printf("x*scalar = %Zd\n", xMulscalar);
    ASSERT_ALWAYS(mpz_cmp(z, xMulscalar) == 0);

    printf("\n**** Multiplication by a scalar then addition *****\n");

    rns_addmulscalar_n_128(&R, z_RNS, x_RNS,
			   _mm_set1_epi64(_mm_cvtsi64_m64(10023)), y_RNS);

    printf("z_RNS = ");
    RnsElement_print(z_RNS, moduli_nb);


    mpz_set_rns(z, z_RNS, &Rmpz);
    gmp_printf("z = %Zd\n", z);
    mpz_mul_ui(xMulscalar, x, 10023);
    mpz_add(xMulscalar, xMulscalar, y);
    mpz_mod(xMulscalar, xMulscalar, Rmpz.P);
    gmp_printf("x*scalar + y = %Zd\n", xMulscalar);
    ASSERT_ALWAYS(mpz_cmp(z, xMulscalar) == 0);

    printf("\n**** Reduction mod q *****\n");

    z_RNS = x_RNS;

    rns_modq_n_128(&R, x_RNS, x_RNS);
    //rns_modq_n_128(&R, z_RNS, z_RNS);

    printf("z_RNS = ");
    RnsElement_print(z_RNS, moduli_nb);

    printf("x_RNS = ");
    RnsElement_print(x_RNS, moduli_nb);

    mpz_set_rns(z, x_RNS, &Rmpz);
    mpz_mod(z, z, Q);
    gmp_printf("z = %Zd\n", z);
    mpz_mod(xModq, x, Q);
    gmp_printf("x % Q = %Zd\n", xModq);
    ASSERT_ALWAYS(mpz_cmp(z, xModq) == 0);

    free(x_RNS);
    free(y_RNS);
    free(z_RNS);

    mpz_clear(Q);
    mpz_clear(x);
    mpz_clear(y);
    mpz_clear(z);
    mpz_clear(xPlusy);
    mpz_clear(xMinusy);
    mpz_clear(xMuly);
    mpz_clear(xMulscalar);
    mpz_clear(xModq);

    RNS_mpz_clear(&Rmpz);
    RNS_clear(&R);

    return 0;
}
