package simd_noflat;

use strict;
use warnings;

die("NO");

# u64n was the only consumer of this interface, and it has been
# deprecated. See commit e610256. So there's no point in keeping the
# stuff here up to dae, and it hasn't been tested for ages.
#
# Ideally, we should have something that works in the same way as the pz
# layer (which we did succeed in making work with mpi, so there's no
# reason why we couldn't do it here as well).

########################################################################

# beware. noflat means that elt is blah* (not blah[1]), and dst_elt is
# blah* as well. Thus some important routines are still unchanged here.
# Only the vec-related stuff changes a lot.
# Another change which appears in all routines is related to the
# calculation of storage differs.

my $btype = "uint64_t";
my $bytes = "(*K)*sizeof($btype)";

sub code_for_init {         return [ 'inline(K,px)', "*px = malloc($bytes);"]; }
sub code_for_clear {        return [ 'inline(K,px)', "free(*px); *px=NULL;"]; }
sub code_for_elt_ur_init {  return [ 'inline(K,px)', "*px = malloc($bytes);"]; }
sub code_for_elt_ur_clear { return [ 'inline(K,px)', "free(*px); *px=NULL;"]; }


sub code_for_vec_init {
    my $proto = 'inline(K!,v,n)';
    my $code = <<EOF;
unsigned int i;
*v = (@!vec) malloc (n*sizeof(@!elt));
$btype * d = malloc (n*$bytes);
for(i = 0; i < n; i++) { (*v)[i] = d; d += *K; }
EOF
    return [ $proto, $code ];
}

sub code_for_vec_reinit {
    my $proto = 'inline(K!,v,n,m)';
    my $code = <<EOF;
unsigned int i;
*v = (@!vec) realloc (*v, m * sizeof(@!elt));
$btype * d = realloc ((*v)[0], m*$bytes);
for(i = n; i < m; i++) { (*v)[i] = d; d += *K; }
EOF
    return [ $proto, $code ];
}

sub code_for_vec_clear {
    my $proto = 'inline(K!,v,n)';
    my $code = <<EOF;
free((*v)[0]);
free(*v);
*v=NULL;
EOF
    return [ $proto, $code ];
}

# Here we have default wrappers for most basic operations. These comply
# with the api, although it would be possible to implement them in a
# shorter way. Code-wise, it is expected that in the trivial case, all
# loops in the generated code fold down to nothing when nothing has to be
# done.
sub code_for_set {
    return [ 'inline(K!,r,s)', "if (r != s) memmove(r,s,$bytes);" ];
}

sub code_for_elt_ur_set {
    return [ 'inline(K!,r,s)', "if (r != s) memmove(r,s,$bytes);" ];
}

sub code_for_set_zero {
    return [ 'inline(K!,r)', "memset(r, 0, $bytes);" ];
}

sub code_for_is_zero {
    my $code = <<EOF;
    unsigned int i;
    for(i = 0 ; i < $bytes/sizeof(r[0]) ; i++) {
        if (r[i]) return 0;
    }
    return 1;
EOF
    return [ 'inline(K!,r)', $code ];
}

# note that memcmp makes little sense for the simd interface, as we
# rather case about the per-member comparison and not about the whole
# thing.
# sub code_for_cmp {
# return [ 'inline(K!,r,s)', "return memcmp(r,s,$bytes);" ];
# }


sub code_for_random {
    # FIXME. This looks fairly stupid.
    my $code = <<EOC;
    for(unsigned int i = 0 ; i < $bytes ; i++) {
        ((unsigned char*)r)[i] = gmp_urandomb_ui(state, 8);
    }
EOC
    return [ 'inline(K!,r)', $code ];
}

sub code_for_add {
    my $code = <<EOF;
    for(unsigned int i = 0 ; i < *K ; i++) r[i] = s1[i] ^ s2[i];
EOF
    return [ 'inline(K!,r,s1,s2)', $code ];
}

sub code_for_elt_ur_add { return code_for_add(@_); }

# This is probably very suboptimal in many cases, e.g. if we have sse2 
sub code_for_simd_hamming_weight {
    my $code=<<EOF;
    int w = 0;
#if GNUC_VERSION_ATLEAST(3,4,0)
    unsigned long * xp = (unsigned long *) p;
    assert(@!elt_stride(K) % sizeof(unsigned long) == 0);
    for(size_t b = 0 ; b < @!elt_stride(K) / sizeof(unsigned long) ; b++) {
        w += __builtin_popcountl(xp[b]);
    }
#else
    int tab[16] = { 0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4 };
    uint8_t * xp = (uint8_t *) p;
    for(size_t b = 0 ; b < @!elt_stride(K) ; b++) {
        w += tab[xp[b]&15] + tab[xp[b]>>4];
    }
#endif
    return w;
EOF
    return [ 'inline(K!,p)', $code ];
}

sub code_for_simd_find_first_set {
    my $opt = shift @_;
    my $wordsize = $opt->{'w'};
    my $code=<<EOF;
    size_t bmax = @!elt_stride(K) / sizeof(@!elt);
    assert(@!simd_groupsize(K) % bmax == 0);
    int g = @!simd_groupsize(K) / bmax;
    int f = 0;
    for(size_t b = 0 ; b < bmax ; b++, f+=g) {
        if (!p[b]) continue;
#if GNUC_VERSION_ATLEAST(3,4,0)
        unsigned long * xp = (unsigned long *) (p + b);
        for(size_t c = 0 ; c < sizeof(@!elt) / sizeof(unsigned long) ; c++, f += $wordsize) {
            if (!xp[c]) continue;
            return f + __builtin_ctzl(xp[c]);
        }
#else
        uint8_t * xp = (uint8_t *) (p + b);
        int tab[16] = { -1,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0 };
        for(size_t c = 0 ; c < sizeof(@!elt) ; c++, f += 8) {
            if (!xp[c]) continue;
            if (xp[c] & 15) return f + tab[xp[c] & 15];
            return f + 4 + tab[xp[c] >> 4];
        }
#endif
        abort();
    }
    return -1;
EOF
    return [ 'inline(K!,p)', $code ];
}

sub code_for_simd_get_ui_at {
    my $code=<<EOF;
    assert(k < @!simd_groupsize(K));
    uint64_t * xp = (uint64_t *) p;
    uint64_t mask = ((uint64_t)1) << (k%64);
    return (xp[k/64] & mask) != 0;
EOF
    return [ 'inline(K!,p,k)', $code ];
}

sub code_for_simd_set_ui_at {
    my $code=<<EOF;
    assert(k < @!simd_groupsize(K));
    uint64_t * xp = (uint64_t *) p;
    uint64_t mask = ((uint64_t)1) << (k%64);
    xp[k/64] = (xp[k/64] & ~mask) | ((((uint64_t)v) << (k%64))&mask);
EOF
    return [ 'inline(K!,p,k,v)', $code ];
}

sub code_for_simd_add_ui_at {
    my $code=<<EOF;
    @!set(K, p, p0);
    assert(k < @!simd_groupsize(K));
    uint64_t * xp = (uint64_t *) p;
    uint64_t mask = ((uint64_t)(v&1)) << (k%64);
    xp[k/64] ^= mask;
EOF
    return [ 'inline(K!,p,p0,k,v)', $code ];
}

sub code_for_simd_set_ui_all {
    my $code=<<EOF;
    for(unsigned int i = 0 ; i < sizeof(@!elt)/sizeof(*r) ; i++) {
        r[i] = -v;
    }
EOF
    return [ 'inline(K!,r,v)', $code ];
}
#sub code_for_elt_ur_simd_get_ui_at { return code_for_simd_get_ui_at(@_); }
#sub code_for_elt_ur_simd_set_ui_at { return code_for_simd_set_ui_at(@_); }
#sub code_for_elt_ur_simd_set_ui_all { return code_for_simd_set_ui_all(@_); }


########################################################################

# the vec_add from Mpfq::defaults::vec::addsub is fine in this case.

1;
