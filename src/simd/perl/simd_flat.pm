package simd_flat;

use strict;
use warnings;
use Mpfq::defaults::flatdata;

our @parents = qw/Mpfq::defaults::flatdata/;

########################################################################
# Not that stuff here is also almost valid for non-flat data. Only the
# calculation of storage differs.

sub code_for_random {
    my $code = <<EOC;
    mpz_t ugly;
    ugly->_mp_d = (mp_limb_t *) r;
    ugly->_mp_alloc = sizeof(@!elt) / (sizeof(mp_limb_t));
    ugly->_mp_size = sizeof(@!elt) / (sizeof(mp_limb_t));
    mpz_urandomb(ugly, state, @!simd_groupsize(K));
EOC
    return [ 'inline(K!,r,state)', $code ];
}

sub code_for_random2 {
    my $code = <<EOC;
    mpz_t ugly;
    ugly->_mp_d = (mp_limb_t *) r;
    ugly->_mp_alloc = sizeof(@!elt) / (sizeof(mp_limb_t));
    ugly->_mp_size = sizeof(@!elt) / (sizeof(mp_limb_t));
    mpz_rrandomb(ugly, state, @!simd_groupsize(K));
EOC
    return [ 'inline(K!,r,state)', $code ];
}

sub code_for_set_zero {
    return [ 'inline(K!,r)', "memset(r, 0, sizeof(@!elt));" ];
}

sub code_for_set {
    my $code=<<EOF;
    for(unsigned int i = 0 ; i < sizeof(@!elt)/sizeof(*s) ; i++) {
        r[i] = s[i];
    }
EOF
    return [ 'inline(K!,r,s)', $code ];
}

sub code_for_elt_ur_set {
    my $code=<<EOF;
    for(unsigned int i = 0 ; i < sizeof(@!elt_ur)/sizeof(*s) ; i++) {
        r[i] = s[i];
    }
EOF
    return [ 'inline(K!,r,s)', $code ];
}

# This is probably very suboptimal in many cases, e.g. if we have sse2 
sub code_for_simd_hamming_weight {
    my $code=<<EOF;
    int w = 0;
#if GNUC_VERSION_ATLEAST(3,4,0)
    unsigned long * xp = (unsigned long *) p;
    assert(@!elt_stride(K) % sizeof(unsigned long) == 0);
    // parentheses in the divisor are mandatory here, see
    // https://gcc.gnu.org/pipermail/gcc-patches/2020-September/553888.html
    for(size_t b = 0 ; b < @!elt_stride(K) / (sizeof(unsigned long)) ; b++) {
        w += __builtin_popcountl(xp[b]);
    }
#else
    int tab[16] = { 0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4 };
    uint8_t * xp = (uint8_t *) p;
    for(size_t b = 0 ; b < @!elt_stride(K) ; b++) {
        w += tab[xp[b]&15] + tab[xp[b]>>4];
    }
#endif
    return w;
EOF
    return [ 'inline(K!,p)', $code ];
}

sub code_for_simd_find_first_set {
    my $opt = shift @_;
    my $wordsize = $opt->{'w'};
    my $code=<<EOF;
    size_t bmax = @!elt_stride(K) / (sizeof(@!elt));
    assert(@!simd_groupsize(K) % bmax == 0);
    int g = @!simd_groupsize(K) / bmax;
    int f = 0;
    for(size_t b = 0 ; b < bmax ; b++, f+=g) {
        if (!p[b]) continue;
#if GNUC_VERSION_ATLEAST(3,4,0)
        unsigned long * xp = (unsigned long *) (p + b);
        // parentheses in the divisor are mandatory here, see
        // https://gcc.gnu.org/pipermail/gcc-patches/2020-September/553888.html
        for(size_t c = 0 ; c < sizeof(@!elt) / (sizeof(unsigned long)) ; c++, f += $wordsize) {
            if (!xp[c]) continue;
            return f + __builtin_ctzl(xp[c]);
        }
#else
        uint8_t * xp = (uint8_t *) (p + b);
        int tab[16] = { -1,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0 };
        for(size_t c = 0 ; c < sizeof(@!elt) ; c++, f += 8) {
            if (!xp[c]) continue;
            if (xp[c] & 15) return f + tab[xp[c] & 15];
            return f + 4 + tab[xp[c] >> 4];
        }
#endif
        abort();
    }
    return -1;
EOF
    return [ 'inline(K!,p)', $code ];
}

sub code_for_simd_get_ui_at {
    my $code=<<EOF;
    assert(k < @!simd_groupsize(K));
    uint64_t * xp = (uint64_t *) p;
    uint64_t mask = ((uint64_t)1) << (k%64);
    return (xp[k/64] & mask) != 0;
EOF
    return [ 'inline(K!,p,k)', $code ];
}

sub code_for_simd_set_ui_at {
    my $code=<<EOF;
    assert(k < @!simd_groupsize(K));
    uint64_t * xp = (uint64_t *) p;
    uint64_t mask = ((uint64_t)1) << (k%64);
    xp[k/64] = (xp[k/64] & ~mask) | ((((uint64_t)v) << (k%64))&mask);
EOF
    return [ 'inline(K!,p,k,v)', $code ];
}

sub code_for_simd_add_ui_at {
    my $code=<<EOF;
    @!set(K, p, p0);
    assert(k < @!simd_groupsize(K));
    uint64_t * xp = (uint64_t *) p;
    uint64_t mask = ((uint64_t)(v&1)) << (k%64);
    xp[k/64] ^= mask;
EOF
    return [ 'inline(K!,p,p0,k,v)', $code ];
}

sub code_for_simd_set_ui_all {
    my $code=<<EOF;
    for(unsigned int i = 0 ; i < sizeof(@!elt)/sizeof(*r) ; i++) {
        r[i] = -v;
    }
EOF
    return [ 'inline(K!,r,v)', $code ];
}

#sub code_for_elt_ur_simd_hamming_weight { return code_for_simd_hamming_weight(@_); }
#sub code_for_elt_ur_simd_find_first_set { return code_for_simd_find_first_set(@_); }
#sub code_for_elt_ur_simd_get_ui_at { return code_for_simd_get_ui_at(@_); }
#sub code_for_elt_ur_simd_set_ui_at { return code_for_simd_set_ui_at(@_); }
#sub code_for_elt_ur_simd_set_ui_all { return code_for_simd_set_ui_all(@_); }
#

########################################################################

# the vec_add from Mpfq::defaults::vec::addsub is fine. Normally the
# compiler understands that this all folds down to simple arithmetic.

1;
