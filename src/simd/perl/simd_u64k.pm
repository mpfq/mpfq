package simd_u64k;

use strict;
use warnings;

use Mpfq::engine::handler;
use Mpfq::engine::oo;

use Mpfq::defaults;
use Mpfq::defaults::vec;
use simd_dotprod;
use io;
use trivialities;
use simd_char2;
use Data::Dumper;

our @parents = qw/
    Mpfq::defaults
    Mpfq::defaults::vec
    simd_dotprod
    io
    trivialities
    simd_char2
/;

our $resolve_conflicts = {
    vec_set => 'simd_char2',
    vec_ur_set => 'simd_char2',
    print => 'io',
    fprint => 'io',
    cxx_out => 'io',
    cxx_in => 'io',
};

our @ISA = qw/
    Mpfq::engine::handler
    Mpfq::engine::oo
/;

my $eltwidth;
my $groupsize;

sub new { return bless({}, shift); }

sub code_for_simd_groupsize { return [ 'macro(K!)', $groupsize ]; }
sub code_for_field_specify {
    my ($opt) = @_;
    my $kind = 'function(K!,tag,x!)';
    my $groupsize = $opt->{'k'} * 64;
    my $code = <<EOF;
    if (tag == MPFQ_SIMD_GROUPSIZE) {
        assert(*(int*)x == $groupsize);
    } else if (tag == MPFQ_PRIME_MPZ) {
        assert(mpz_cmp_ui((mpz_srcptr)x, 2) == 0);
    } else {
        fprintf(stderr, "Unsupported field_specify tag %ld\\n", tag);
    }
EOF
    return [ $kind, $code ];
}

sub init_handler {
    my ($opt) = @_;

    my $banner = "/* Automatically generated code  */\n";

    $eltwidth = $opt->{'k'};
    my $elt_urwidth = $eltwidth;
    $groupsize = 64*$eltwidth;
    my $types = {
	elt =>	"typedef uint64_t @!elt\[$eltwidth\];",
	dst_elt =>	"typedef uint64_t * @!dst_elt;",
	src_elt =>	"typedef const uint64_t * @!src_elt;",

	elt_ur =>	"typedef uint64_t @!elt_ur\[$elt_urwidth\];",
	dst_elt_ur =>	"typedef uint64_t * @!dst_elt_ur;",
	src_elt_ur =>	"typedef const uint64_t * @!src_elt_ur;",
    };

    return {
            banner => $banner,
            types => $types,
        };
}

sub output_deps {
    my ($self, $f) = @_;
    open F, ">$f" or die "$f: $!";
    print F "$_\n" for (grep { !m{^/} } values %INC);
    close F;
}

1;

