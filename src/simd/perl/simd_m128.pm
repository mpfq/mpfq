package simd_m128;

# Warning: this actually requires sse4.1 and popcnt, because of
# _mm_popcnt_u64 and _mm_extract_epi64

use strict;
use warnings;

use Mpfq::engine::handler;
use Mpfq::engine::oo;

use Mpfq::defaults;
use Mpfq::defaults::vec;
use simd_char2;
use simd_dotprod;
use io;
use trivialities;
use Data::Dumper;

our @parents = qw/
    Mpfq::defaults
    Mpfq::defaults::vec
    simd_char2
    simd_dotprod
    io
    trivialities
/;

our $resolve_conflicts = {
    vec_set => 'simd_char2',
    vec_ur_set => 'simd_char2',
    print => 'io',
    fprint => 'io',
    cxx_out => 'io',
    cxx_in => 'io',
};

our @ISA = qw/
    Mpfq::engine::handler
    Mpfq::engine::oo
/;

my $groupsize;

sub new { return bless({}, shift); }

sub code_for_simd_groupsize { return [ 'macro(K!)', $groupsize ]; }
sub code_for_field_specify {
    my ($opt) = @_;
    my $kind = 'function(K!,tag,x!)';
    my $groupsize = $opt->{'k'} * 64;
    my $code = <<EOF;
    if (tag == MPFQ_SIMD_GROUPSIZE) {
        assert(*(int*)x == $groupsize);
    } else if (tag == MPFQ_PRIME_MPZ) {
        assert(mpz_cmp_ui((mpz_srcptr)x, 2) == 0);
    } else {
        fprintf(stderr, "Unsupported field_specify tag %ld\\n", tag);
    }
EOF
    return [ $kind, $code ];
}

sub code_for_inv {
    my $code = <<EOF;
    *r = *s;
    return 1;
EOF
    return [ 'inline(K!,r,s)', $code ];
}

sub code_for_mul { return [ 'inline(K!,r,s1,s2)', '*r = _mm_and_si128(*s1, *s2);' ]; }
sub code_for_mul_ur { return [ 'inline(K!,r,s1,s2)', '*r = _mm_and_si128(*s1, *s2);' ]; }
sub code_for_reduce { return [ 'inline(K!,r,s)', "*r=*s;" ]; }

sub code_for_set_zero { return [ 'inline(K!,r)', "*r=_mm_setzero_si128();" ]; }
sub code_for_set { return [ 'inline(K!,r,s)', "*r=*s;" ]; }
sub code_for_neg { return [ 'inline(K!,r,s)', "*r=*s;" ]; }
sub code_for_add { return [ "inline(K!,r,s1,s2)", "*r = _mm_xor_si128(*s1, *s2);" ]; }
sub code_for_sub { return [ "inline(K!,r,s1,s2)", "*r = _mm_xor_si128(*s1, *s2);" ]; }
sub code_for_elt_ur_set { return [ 'inline(K!,r,s)', "*r=*s;" ]; }
sub code_for_elt_ur_neg { return [ 'inline(K!,r,s)', "*r=*s;" ]; }
sub code_for_elt_ur_add { return [ "inline(K!,r,s1,s2)", "*r = _mm_xor_si128(*s1, *s2);" ]; }
sub code_for_elt_ur_sub { return [ "inline(K!,r,s1,s2)", "*r = _mm_xor_si128(*s1, *s2);" ]; }
sub code_for_is_zero {
    my $code = <<EOF;
        return _mm_extract_epi64(*r, 0) == 0 && 
               _mm_extract_epi64(*r, 1) == 0;
EOF
    return [ "inline(K!, r)", $code ];
}
sub code_for_simd_hamming_weight {
    my $code = <<EOF;
        return _mm_popcnt_u64(_mm_extract_epi64(*r, 0)) +
               _mm_popcnt_u64(_mm_extract_epi64(*r, 1));
EOF
    return [ "inline(K!, r)", $code ];
}
sub code_for_simd_set_ui_all { 
    my $code=<<EOF;
    *r = _mm_set1_epi64(_mm_cvtsi64_m64(-(v != 0)));
EOF
    return [ "inline(K!, r, v)", $code ];
}
sub code_for_simd_find_first_set {
    my $opt = shift @_;
    my $wordsize = $opt->{'w'};
    my $code=<<EOF;
    int f = 0;
#if GNUC_VERSION_ATLEAST(3,4,0)
        unsigned long * xp = (unsigned long *) p;
        for(size_t c = 0 ; c < 2 ; c++, f += $wordsize) {
            if (!xp[c]) continue;
            return f + __builtin_ctzl(xp[c]);
        }
#else
        uint8_t * xp = (uint8_t *) p;
        int tab[16] = { -1,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0 };
        for(size_t c = 0 ; c < 16 ; c++, f += 8) {
            if (!xp[c]) continue;
            if (xp[c] & 15) return f + tab[xp[c] & 15];
            return f + 4 + tab[xp[c] >> 4];
        }
#endif
    return -1;
EOF
    return [ 'inline(K!,p)', $code ];
}

sub init_handler {
    my ($opt) = @_;

    my $banner = "/* Automatically generated code  */\n";

    $groupsize = 128;

    my $types = {
        elt =>	"typedef __m128i @!elt\[1\];",
        dst_elt =>	"typedef __m128i * @!dst_elt;",
        src_elt =>	"typedef const __m128i * @!src_elt;",

        elt_ur =>	"typedef __m128i @!elt_ur\[1\];",
        dst_elt_ur =>	"typedef __m128i * @!dst_elt_ur;",
        src_elt_ur =>	"typedef const __m128i * @!src_elt_ur;",
    };

    # return { includes=> [qw/"mpfq_u64n.h"/], banner => $banner, types => $types };
    return {
        includes => [ qw/<x86intrin.h>/ ],
        banner => $banner,
        types => $types,
    };
}

sub output_deps {
    my ($self, $f) = @_;
    open F, ">$f" or die "$f: $!";
    print F "$_\n" for (grep { !m{^/} } values %INC);
    close F;
}

1;

