package simd_gfp;

use strict;
use warnings;

use Mpfq::engine::handler;
use Mpfq::engine::oo;
our @ISA = qw/
    Mpfq::engine::handler
    Mpfq::engine::oo
/;
sub new { return bless({}, shift); }


use Mpfq::gfp;
our @parents = qw/
    Mpfq::gfp
/;

my $groupsize = 1;      # This layer does not allow modification of the
                        # group size.

# set_zero: keep
sub code_for_simd_hamming_weight {
    die unless $groupsize == 1;
    return [ 'inline(K!,p)', "return !@!is_zero(K,p);" ];
}
sub code_for_simd_find_first_set {
    die unless $groupsize == 1;
    return [ 'inline(K!,p)', "return @!simd_hamming_weight(K,p) - 1;" ];
}
sub code_for_simd_get_ui_at {
    die unless $groupsize == 1;
    return [ 'inline(K!,p,k!)', "return @!get_ui(K,p);" ];
}

sub code_for_simd_set_ui_at {
    die unless $groupsize == 1;
    return [ 'inline(K!,p,k!,v)', "@!set_ui(K,p,v);" ];
}

sub code_for_simd_add_ui_at {
    die unless $groupsize == 1;
    return [ 'inline(K!,p,p0,k!,v)', "@!add_ui(K,p,p0,v);" ];
}

sub code_for_simd_set_ui_all {
    die unless $groupsize == 1;
    return [ 'inline(K!,p,v)', "@!set_ui(K,p,v);" ];
}

#sub code_for_elt_ur_simd_hamming_weight {
#    die unless $groupsize == 1;
#    return [ 'inline(K!,p)', "return !@!elt_ur_is_zero(K,p);" ];
#}
#sub code_for_elt_ur_simd_find_first_set {
#    die unless $groupsize == 1;
#    return [ 'inline(K!,p)', "return @!elt_ur_hamming_weight(K,p) - 1;" ];
#}
#sub code_for_elt_ur_simd_get_ui_at { return code_for_simd_get_ui_at(@_); }
#sub code_for_elt_ur_simd_set_ui_at { return code_for_simd_set_ui_at(@_); }
#sub code_for_elt_ur_simd_set_ui_all { return code_for_simd_set_ui_all(@_); }
#
############################################################
# --- dot products now.
#

# Here we are in a context where the SIMD group size is 1, for the
# moment. So the output of add_dotprod is simply a scalar. Easy.
sub code_for_add_dotprod {
    die unless $groupsize == 1;
    my $kind = "function(K!,xw,xu1,xu0,n)";
    my $code = <<EOF;
    @!elt_ur s,t;
    @!elt_ur_init(K, &s);
    @!elt_ur_init(K, &t);
    @!elt_ur_set_elt(K, s, xw[0]);
    for(unsigned int i = 0 ; i < n ; i++) {
        @!mul_ur(K, t, xu0[i], xu1[i]);
        @!elt_ur_add(K, s, s, t);
    }
    @!reduce(K, xw[0], s);
    @!elt_ur_clear(K, &s);
    @!elt_ur_clear(K, &t);
EOF
    return [ $kind, $code ];
}

# As above, we do not support for the moment anything different from simd
# group size being one. So easy again.
sub code_for_member_template_add_dotprod {
    die unless $groupsize == 1;
    my $kind = "function(K0!,K1!,xw,xu1,xu0,n)";
    my $code = <<EOF;
    @!elt_ur s,t;
    @!elt_ur_init(K0, &s);
    @!elt_ur_init(K0, &t);
    @!elt_ur_set_elt(K0, s, xw[0]);
    for(unsigned int i = 0 ; i < n ; i++) {
        @!mul_ur(K0, t, xu0[i], xu1[i]);
        @!elt_ur_add(K0, s, s, t);
    }
    @!reduce(K0, xw[0], s);
    @!elt_ur_clear(K0, &s);
    @!elt_ur_clear(K0, &t);
EOF
    return [ $kind, $code ];
}

# Once again...
sub code_for_member_template_addmul_tiny {
    die unless $groupsize == 1;
    my $kind = "function(K!,L!,w,u,v,n)";
    my $code = <<EOF;
    @!elt s;
    @!init(K, &s);
    for(unsigned int i = 0 ; i < n ; i++) {
        @!mul(K, s, u[i], v[0]);
        @!add(K, w[i], w[i], s);
    }
    @!clear(K, &s);
EOF
    return [ $kind, $code ];
}

sub code_for_member_template_transpose {
    # Note that member templates don't like macros !
    die unless $groupsize == 1;
    return [ "function(K!,L!,w,u)", "@!set(K, w[0], u[0]);" ];
}

########################################################################
# --- This is part of the syntactic sugar around the whole thing.

# Here we're not requesting compatibility with other implementations.
# sub code_for_field_init_oo_change_groupsize {
#     my $kind = "function(K!,f,v)";
#     my $code = <<EOF;
# assert(v == $groupsize); */
# mpfq_p16_field_init_oo(NULL, f);
# (f->set_groupsize)(f, v);
# EOF
#     return [ $kind, $code ];
# }


sub code_for_simd_groupsize { return [ 'macro(K!)', $groupsize ]; }
# sub code_for_set_groupsize { return [ 'macro(K!,n)', "assert(n==$groupsize)" ]; } 
# sub code_for_offset { return [ 'macro(K!,n)', 'n /* TO BE DEPRECATED */' ]; }
# sub code_for_stride { return [ 'macro(K!)', '1 /* TO BE DEPRECATED */' ]; }

sub code_for_addmul_si_ur {
    # This would be a feature request. For the moment the code we give
    # here sucks.
    my $kind = 'inline(K!, w, u, v)';
    my $code = <<EOF;
    @!elt_ur s;
    @!elt vx;
    @!elt_ur_init(K, &s);
    @!init(K, &vx);
    if (v>0) {
        @!set_ui(K, vx, v);
        @!mul_ur(K, s, u, vx);
        @!elt_ur_add(K, w, w, s);
    } else {
        @!set_ui(K, vx, -v);
        @!mul_ur(K, s, u, vx);
        @!elt_ur_sub(K, w, w, s);
    }
    @!clear(K, &vx);
    @!elt_ur_clear(K, &s);
EOF
    return [ $kind, $code ];
}

sub init_handler {
    my ($opt) = @_;

    my $banner = "/* Automatically generated code  */\n";

    die unless $opt->{'tag'} =~ /^p_\d+$/;

    my $tag = $opt->{'tag'};

    return {
        banner => $banner,
        'th:includes' => [ qq{"mpfq_$tag.h"} ],
        'c:includes' => [qw/<inttypes.h>/]};
}

1;
