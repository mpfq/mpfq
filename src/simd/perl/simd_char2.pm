package simd_char2;

use strict;
use warnings;

use simd_flat;
our @parents = qw/
    simd_flat
    /;

# This gathers everything which follows completely trivially from the fact
# that we are essentially a replica of GF(2)

sub code_for_impl_max_characteristic_bits { return [ 'macro()', 2 ]; }
sub code_for_impl_max_degree { return [ 'macro()', '1' ]; }

sub code_for_add {
    my $code = <<EOF;
    for(unsigned int i = 0 ; i < sizeof(@!elt)/sizeof(*r) ; i++) {
        r[i] = s1[i] ^ s2[i];
    }
EOF
    return [ 'inline(K!,r,s1,s2)', $code ];
}

sub code_for_mul {
    my $code = <<EOF;
    for(unsigned int i = 0 ; i < sizeof(@!elt)/sizeof(*r) ; i++) {
        r[i] = s1[i] & s2[i];
    }
EOF
    return [ 'inline(K!,r,s1,s2)', $code ];
}


sub code_for_inv {
    my $code = <<EOF;
    int rc = 0;
    for(unsigned int i = 0 ; i < sizeof(@!elt)/sizeof(*r) ; i++) {
        r[i] = s[i];
        if (r[i]) rc = 1;
    }
    return rc;
EOF
    return [ 'inline(K!,r,s)', $code ];
}

sub code_for_mul_ur { return code_for_mul(@_); }
sub code_for_elt_ur_add { return code_for_add(@_); }
sub code_for_reduce { return simd_flat::code_for_set(@_); }
sub code_for_field_degree { return [ 'macro(K!)', '1' ]; }
sub code_for_field_characteristic { return [ 'macro(K!,z)', 'mpz_set_ui(z,2)' ]; }
sub code_for_field_characteristic_srcptr {
    my $code = <<EOF;
    /* yes, this is ugly */
    static mp_limb_t limbs[1] = {2};
    static __mpz_struct a = { 1, 1, limbs };
    return &a;
EOF
    return [ 'function(K!)', $code ];
}
sub code_for_field_clear { return [ 'macro(K!)', '' ]; }
sub code_for_field_init {       return [ 'macro(f!)', '((@!dst_field) (f))->c=0' ]; }
sub code_for_field_setopt { return [ 'macro(f,x!,y)', '' ]; }


sub code_for_neg { return [ 'macro(K!,r,s)', "@!set(K,r,s)" ]; }
sub code_for_elt_ur_neg { return [ 'macro(K!,r,s)', "@!elt_ur_set(K,r,s)" ]; }
sub code_for_elt_ur_sub {
    return [ "macro(K!,r,s1,s2)", "@!elt_ur_add(K,r,s1,s2)" ];
}
sub code_for_sub { return [ "macro(K!,r,s1,s2)", "@!add(K,r,s1,s2)" ]; }

# We are missing them in the automatically generated code. Let's just put
# placeholders, as no real code is programmed to use them yet.
sub init_handler {
    my $opt = shift;
    my $elt_types = {};
    $elt_types->{"poly"} = <<EOF;
typedef struct {
  @!vec c;
  unsigned long alloc;
  unsigned long size;
} @!poly_struct;
typedef @!poly_struct @!poly [1];
EOF
    $elt_types->{"dst_poly"} = "typedef @!poly_struct * @!dst_poly;";
    $elt_types->{"src_poly"} = "typedef const @!poly_struct * @!src_poly;";
    $elt_types->{"field"} = <<EOF;
typedef struct {
    /* empty struct is not allowed in C. No, really it's not allowed. */
    char c;
} @!field_struct;
typedef @!field_struct @!field [1];
EOF
    $elt_types->{"dst_field"} = "typedef @!field_struct * @!dst_field;";
    $elt_types->{"src_field"} = "typedef const @!field_struct * @!src_field;";
    return { types => $elt_types };
}

1;

