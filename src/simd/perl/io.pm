package io;

use strict;
use warnings;

sub code_for_asprint {
    my $code = <<EOF;
    /* Hmm, this has never been tested, right ? Attempting a quick fix... */
    const uint64_t * y = (const uint64_t *) x;
    const unsigned int stride = @!elt_stride(K)/(sizeof(uint64_t));
    *ps = mpfq_malloc_check(stride * 16 + 1);
    memset(*ps, ' ', stride * 16);
    int n;
    for(unsigned int i = 0 ; i < stride ; i++) {
        n = snprintf((*ps) + i * 16, 17, "%016" PRIx64, y[i]);
        (*ps)[i*16 + n]=',';
    }
    (*ps)[(stride-1) * 16 + n]='\\0';
    return (stride-1) * 16 + n;
EOF
    return [ 'function(K!, ps, x)', $code ];
}

sub code_for_fprint {
    my $proto = 'function(k,file,x)';
    my $code = <<EOF;
char *str;
int rc;
@!asprint(k,&str,x);
rc = fprintf(file,"%s",str);
free(str);
return rc;
EOF
    return [ $proto, $code ];
}

sub code_for_cxx_out {
    my $proto = 'function(k,os,x)';
    my $code = <<EOF;
char *str;
@!asprint(k,&str,x);
os << str;
free(str);
return os;
EOF
    return [ $proto, $code ];
}

sub code_for_print {
    return [ 'macro(k,x)', '@!fprint(k,stdout,x)' ];
}

sub code_for_sscan {
    my $opt = shift @_;
    my $proto = 'function(k!,z,str)';
    my $code = <<EOF;
    char tmp[17];
    uint64_t * y = (uint64_t *) z;
    const unsigned int stride = @!elt_stride(K)/(sizeof(uint64_t));
    assert(strlen(str) >= 1 * 16);
    int r = 0;
    for(unsigned int i = 0 ; i < stride ; i++) {
        memcpy(tmp, str + i * 16, 16);
        tmp[16]=0;
        if (sscanf(tmp, "%" SCNx64, &(y[i])) == 1) {
            r+=16;
        } else {
            return r;
        }
    }
    return r;
EOF
    return [ $proto, $code ];
}

sub code_for_fscan {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $w = $opt->{'w'};
    my $proto = 'function(k,file,z)';
    my $code = <<EOF;
char *tmp;
int allocated, len=0;
int c, start=0;
allocated=100;
tmp = (char *)mpfq_malloc_check(allocated);
for(;;) {
    c = fgetc(file);
    if (c==EOF)
        break;
    if (isspace((int)(unsigned char)c)) {
        if (start==0)
            continue;
        else
            break;
    } else {
        if (len==allocated) {
            allocated+=100;
            tmp = (char*)realloc(tmp, allocated);
        }
        tmp[len]=c;
        len++;
        start=1;
    }
}
if (len==allocated) {
    allocated+=1;
    tmp = (char*)realloc(tmp, allocated);
}
tmp[len]='\\0';
int ret=@!sscan(k,z,tmp);
free(tmp);
return ret ? len : 0;
EOF
    return [ $proto, $code ];
}

sub code_for_cxx_in {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $w = $opt->{'w'};
    my $proto = 'function(k,is,z)';
    my $code = <<EOF;
char *tmp;
int allocated, len=0;
int start=0;
allocated=100;
tmp = (char *)mpfq_malloc_check(allocated);
for(;;) {
    char c;
    if (!(is.get(c)))
        break;
    if (isspace(c)) {
        if (start==0)
            continue;
        else
            break;
    } else {
        if (len==allocated) {
            allocated+=100;
            tmp = (char*)realloc(tmp, allocated);
        }
        tmp[len]=c;
        len++;
        start=1;
    }
}
if (len==allocated) {
    allocated+=1;
    tmp = (char*)realloc(tmp, allocated);
}
tmp[len]='\\0';
int ret=@!sscan(k,z,tmp);
if (ret != len)
    is.setstate(std::ios::failbit);
free(tmp);
return is;
EOF
    return [ $proto, $code ];
}

sub init_handler {
    return { 'c:includes' => [qw/<inttypes.h>/]};
}

1;
