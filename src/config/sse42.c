#include <stdint.h>
#include <nmmintrin.h>

int main() {
    volatile __m128i x = _mm_set_epi64(_m_from_int64(42), _m_from_int64(17));
    volatile __m128i y = _mm_set_epi64(_m_from_int64(41), _m_from_int64(17));
    // oops -- previous code was testing with _mm_cmpeq_epi64 == PCMPEQQ,
    // which is sse-4.1, not sse-4.2 ... sigh.
//    x = _mm_cmpeq_epi64(x, y);
    x = _mm_cmpgt_epi64(x,y);
    /* the following test is for emulated 32-bit on physical 64-bit */
    if (sizeof(unsigned long) != 8)
      abort ();
    return 0;
}

