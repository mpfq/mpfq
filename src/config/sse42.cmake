
# SSE-4.2
message(STATUS "Testing whether sse-4.2 code can be used")
try_run(sse42_runs sse42_compiles
            ${PROJECT_BINARY_DIR}/config
            ${PROJECT_SOURCE_DIR}/config/sse42.c)
if(sse42_compiles)
    if (sse42_runs MATCHES FAILED_TO_RUN)
        message(STATUS "Testing whether sse-4.2 code can be used -- No")
        set (HAVE_SSE42 0)
    else (sse42_runs MATCHES FAILED_TO_RUN)
        message(STATUS "Testing whether sse-4.2 code can be used -- Yes")
        set (HAVE_SSE42 2)
    endif (sse42_runs MATCHES FAILED_TO_RUN)
else(sse42_compiles)
    try_run(sse42_runs sse42_compiles
        ${PROJECT_BINARY_DIR}/config
        ${PROJECT_SOURCE_DIR}/config/sse42.c
        COMPILE_DEFINITIONS -msse4.2)
    if(sse42_compiles)
        if (sse42_runs MATCHES FAILED_TO_RUN)
            message(STATUS "Testing whether sse-4.2 code can be used -- No")
            set (HAVE_SSE42 0)
        else (sse42_runs MATCHES FAILED_TO_RUN)
            message(STATUS "Testing whether sse-4.2 code can be used -- Yes, with -msse4.2")
            set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -msse4.2")
            set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -msse4.2")
            set (HAVE_SSE42 1)
        endif (sse42_runs MATCHES FAILED_TO_RUN)
    else(sse42_compiles)
        message(STATUS "Testing whether sse-4.2 code can be used -- No")
        set (HAVE_SSE42 0)
    endif(sse42_compiles)
endif(sse42_compiles)
