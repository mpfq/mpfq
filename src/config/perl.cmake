find_program(PERL_EXECUTABLE NAMES perl)

if(NOT PERL_EXECUTABLE)
    message(FATAL_ERROR "Perl interpreter not found")
endif()

# Test that the version is something sane
# First get the version string from $^V
execute_process(COMMAND "${PERL_EXECUTABLE}" "-e" "print \$^V;"
    OUTPUT_VARIABLE PERL_OUT)
string(REGEX REPLACE "^v" "" PERL_VERSION "${PERL_OUT}")
message(STATUS "Perl version is ${PERL_VERSION}")

# Minumum acceptable perl version. Well, to be honest we have so minimal
# requirements that probably any perl5 will do.
set(PERL_MINIMUM_ACCEPTED 5.10)

# Check that the interpreter is one of the accepted versions
if (PERL_VERSION VERSION_LESS PERL_MINIMUM_ACCEPTED)
    message(FATAL_ERROR "Could not find a Perl interpreter of version at least ${PERL_MINIMUM_ACCEPTED}.")
endif()
    
mark_as_advanced(PERL_EXECUTABLE)

