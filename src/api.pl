
# This file documents and specifies the public mpfq API.
'#ID'	=> 'MPFQ Field API',

# This file has the form of a parseable perl input file, but clearly it
# needs not be so. Parsing is done in Mpfq/conf.pm


# The first line is magical, and specifies the types that have to be defined.
'#TYPES'        => [ qw/field dst_field src_field/ ],

'#TYPES'        => [ qw/elt dst_elt src_elt/ ],

restrict('+URE'),
'#TYPES'        => [ qw/elt_ur dst_elt_ur src_elt_ur/ ],
restrict('-URE'),

'#COMMON_ARGS'  => [ ],

# These are treated in a somewhat special way in the OO context, since no
# instance is needed (class methods).
'impl_name' => [ 'const-char* <- ' ],

'impl_max_characteristic_bits' => [ 'ulong <-', 'return the max size in bits of the characteristic of fields which may be covered by this implementation' ],

'impl_max_degree' => [ 'ulong <-', 'return the maximum extension degree of fields which may be covered by this implementation' ],



# All functions take a dst_field element as first argument, which is
# only mentioned once and for all here.
'#COMMON_ARGS'  => [ 'dst_field' ],



# Gory details of writing interface descriptions:
# 
# An interface is described with a pair ``name'' (LHS), ``args info'' (RHS).
#
# The name (left-hand side) may be prepended by
# - A ``API_EXT:'' prefix (colon matters), indicating that this interface
#   is considered only for the corresponding API extension
# - A *, indicating that the interface is optional.
#
# The args info (right-hand side) can either be:
# - A string, with argument types separated by spaces. Argument types
#   that do include spaces cause trouble here, so replace spaces by
#   dashes (const ulong * -> const-ulong-*, or const-ulong*).
# - A string as above, but starting with ``<type> <- '', indicating that
#   the interface shall have the mentioned return type.
# - An array ``arg string'', ``terse doc string''.


##################################################
hdr('Functions operating on the field structure'),

'#COMMON_ARGS'  => [ 'src_field' ],
'field_characteristic'	=> [ 'mpz_ptr', 'may be a macro.' ],
'field_characteristic_srcptr'	=> [ 'mpz_srcptr <-', 'may be a macro.' ],
'field_characteristic_bits'	=> [ 'ulong <-', 'may be a macro.' ],
'field_degree'	=> [ 'int <-', 'may be a macro.' ],

'#COMMON_ARGS'  => [ 'dst_field' ],
'field_init'	=> [ '', 'may be a macro.' ],
'field_clear'	=> [ '', 'may be a macro.' ],
'field_specify'	=> [ 'ulong const-void*', 
        "May be a macro. In the case where the TAG corresponds to several
        fields, it is necessary to _specify_ a field with this function.
        The first ulong argument gives the type of specification. It can
        be either MPFQ_PRIME_MPZ, MPFQ_POLYNOMIAL, or MPFQ_DEGREE
        (defined in mpfq.h). Then the second void* argument is the data.
        In the case of MPFQ_PRIME_MPZ, this is an mpz.  In the case of
        MPFQ_DEGREE, this is an ulong*.  In the case of MPFQ_POLYNOMIAL,
        the format will depend on the base field." ],
'field_setopt'	=> [ 'ulong void*', 'may be a macro.' ],

####################################
hdr('Element allocation functions'),

'init'		=> [ 'elt*', 'may be a macro.' ],
'clear'		=> [ 'elt*', 'may be a macro.' ],

'elt_stride' => [ 'ptrdiff_t <-', "Most often, this returns sizeof(elt), really (i.e. the size in bytes taken by an element). There are cases though where it's not that simple. When the field structure embodies an element length which is not known at compile-time (see the tag \"pz\", for instance), this depends on runtime data. Second, when the type is abstracted to the point where the using code no longer knows about the compile-time characteristics, we have to find a way to do proper allocation, which can be done with this function." ],


#######################################
hdr('Elementary assignment functions'),

'set'		=> 'dst_elt src_elt',
'set_ui'	=> 'dst_elt ulong',
'set_zero'	=> 'dst_elt',

'get_ui'	=> [ 'ulong <- src_elt',
	"returns the element as an integer in the prime subfield, if this
	makes sense (if the element isn't in the prime subfield, or if
	its representative exceeds the machine word size, this is
	ill-defined)." ],

'set_mpn'	=> [ 'dst_elt mp_limb_t* size_t',
        "Does the same as set_ui, but with an arbitrary precision
        integer. The size_t argument may equal zero.", ],

'set_mpz'	=> 'dst_elt mpz_srcptr',
'get_mpn'	=> 'mp_limb_t* src_elt',
'get_mpz'	=> 'mpz_ptr src_elt',

restrict('+CHAR2'),

'set_uipoly'	=> [ 'dst_elt ulong',
	"Behaves differently from set(): implements the reverse counting map
	from <math>[0\\ldots\\#K-1]\\rightarrow K</math>." ],

'set_uipoly_wide'	=> [ 'dst_elt const-ulong* uint',
	"Same as set_uipoly, with an arbitrary precision argument. The
        number of ulongs is given by the last argument" ],

'get_uipoly'	=> [ 'ulong <- src_elt',
	"Implements the counting map from
	<math>K\\rightarrow[0\\ldots\\#K-1]</math>."
	],

'get_uipoly_wide'	=> [ 'ulong* src_elt',
	"Same as get_uipoly_wide, with an arbitrary precision. Assume
        that ouput is already allocated with enough room for a full
        element." ],

restrict('-CHAR2'),

###################################
hdr('Assignment of random values'),

'random'	=> 'dst_elt gmp_randstate_t',
'random2'	=> ['dst_elt gmp_randstate_t',
	"fills the target element with random data, in the spirit of what
	mpz_rrandomb or mpn_random2 does." ],

#########################################
hdr('Arithmetic operations on elements'),

'add'		=> 'dst_elt src_elt src_elt',
'sub'		=> 'dst_elt src_elt src_elt',
'neg'		=> 'dst_elt src_elt',
'mul'		=> 'dst_elt src_elt src_elt',
'sqr'		=> 'dst_elt src_elt',
'is_sqr'	=> 'int <- src_elt',
'sqrt'		=> 'int <- dst_elt src_elt',
'pow'          => [ 'dst_elt src_elt ulong* size_t',
        'last argument must be >0' ],
'powz'          => [ 'dst_elt src_elt mpz_srcptr',
        'last argument must be >0.' ],

'frobenius'	=> [ 'dst_elt src_elt', "computes x^p" ],

'add_ui'	=> 'dst_elt src_elt ulong',
'sub_ui'	=> 'dst_elt src_elt ulong',
'mul_ui'	=> 'dst_elt src_elt ulong',


'*normalize'     => [ 'dst_elt',
        "reduces the element in place. This function is different from
        reduce(): it does not operate on an object of elt_ur type. Use
        cases are when element data has undergone some out-of-the-api
        change." ],


'CHAR2:add_uipoly'	=> 'dst_elt src_elt ulong',
'CHAR2:sub_uipoly'	=> 'dst_elt src_elt ulong',
'CHAR2:mul_uipoly'	=> 'dst_elt src_elt ulong',

'inv'		=> [ 'int <- dst_elt src_elt',
        "the return value is 1 in case of success. The result is
        undefined if the input is not invertible" ],

'CHAR2:as_solve'	=> [ 'dst_elt src_elt',
	"gives the solution of the equation x^p-x = a, if there is one
	(undefined behaviour otherwise)." ],

'CHAR2:trace'		=> 'ulong <- src_elt',

'*hadamard'     => [ 'dst_elt dst_elt dst_elt dst_elt',
        "Apply Hadamard matrix to the input vector of size 4. Very
        optional, since it is only used in Kummer surface arithmetic." ],

###############################################
restrict('+URE'),
hdr('Operations involving unreduced elements'),

'elt_ur_init'	=> 'elt_ur*',
'elt_ur_clear'	=> 'elt_ur*',

'elt_ur_stride' => [ 'ptrdiff_t <-', "Most often, this returns sizeof(elt_ur), really (i.e. the size in bytes taken by an element). There are cases though where it's not that simple. When the field structure embodies an element length which is not known at compile-time (see the tag \"pz\", for instance), this depends on runtime data. Second, when the type is abstracted to the point where the using code no longer knows about the compile-time characteristics, we have to find a way to do proper allocation, which can be done with this function." ],

'elt_ur_set'	=> 'dst_elt_ur src_elt_ur',
'elt_ur_set_elt'	=> 'dst_elt_ur src_elt',
'elt_ur_set_zero'	=> 'dst_elt_ur',
'elt_ur_set_ui'	=> 'dst_elt_ur ulong',
'elt_ur_add'	=> 'dst_elt_ur src_elt_ur src_elt_ur',
'elt_ur_neg'	=> 'dst_elt_ur src_elt_ur',
'elt_ur_sub'	=> 'dst_elt_ur src_elt_ur src_elt_ur',

'mul_ur'	=> 'dst_elt_ur src_elt src_elt',
'sqr_ur'	=> 'dst_elt_ur src_elt',

'reduce'	=> [ 'dst_elt dst_elt_ur',
	"reduces the dst_elt_ur operand, store the reduced operand in the dst_elt
	operand. Note that the unreduced operand is clobbered." ],

'*addmul_si_ur'	=> [ 'dst_elt_ur src_elt long',
        "Let (w,u,v) be the arguments. This adds u times v to w" ],

restrict('-URE'),

############################
hdr('Comparison functions'),

'cmp'		=> [ 'int <- src_elt src_elt', 
        "returns 0 if elts are equal, and -1 or 1 according to some
        arbitrary order otherwise" ],
'cmp_ui'	=> 'int <- src_elt ulong',
'is_zero'	=> 'int <- src_elt',

####################################
restrict('+MGY'),

hdr('Montgomery representation conversion functions'),

'mgy_enc'       => [ 'dst_elt src_elt',
        'encodes an element to Montgomery representation. This means
        exactly multiplying by 2^n mod p, where n is the number of bits 
        of p, rounded to a multiple of the machine word.' ],

'mgy_dec'       => [ 'dst_elt src_elt',
        'decode an element from Montgomery representation. This means
        dividing the element by 2^n modulo p.' ],
        
restrict('-MGY'),

####################################
hdr('Input/output functions'),

'asprint'       => [ 'int <- char** src_elt', 
        "print the element in the given string, which is allocated by
        asprint so as to accomodate the result + ending \0 char" ],
'fprint'        => 'int <- FILE* src_elt',
'cxx_out'       => 'std::ostream& <- std::ostream& src_elt',
'print'         => 'int <- src_elt',

'sscan'         => [ 'int <- dst_elt const-char*',
        "returns the number of characters parsed if the parsing was succesful, 0 otherwise."],
'fscan'         => [ 'int <- FILE* dst_elt', "returns the number of characters parsed if the parsing was succesful, 0 otherwise."],
'cxx_in'        => [ 'std::istream& <- std::istream& dst_elt', "sets the input stream flags according to whether the parsing was succesful or not."],
'scan'          => [ 'int <- dst_elt', "returns the number of characters parsed if the parsing was succesful, 0 otherwise."],

'read'          => [ 'int <- FILE* dst_elt',
        "read an element in raw binary format, assuming the writer had same endianness and bit width settings" ],
'importdata'        => [ 'int <- FILE* dst_elt int int',
        "read an element in raw binary format, using the provided endianness and bit width settings (mpfq.h)" ],
'write'         => [ 'int <- FILE* src_elt',
        "write an element in raw binary format, so that it can be read again provided that the reader has same endianness and bit width" ],
'exportdata'        => [ 'int <- FILE* src_elt int int',
        "write an element in raw binary format, using the provided endianness and bit width settings (mpfq.h)" ],

####################################
hdr('Vector functions'),

'#TYPES'        => [ qw/vec dst_vec src_vec/ ],

restrict('+URE'),
'#TYPES'        => [ qw/vec_ur dst_vec_ur src_vec_ur/ ],
restrict('-URE'),

'vec_init'              => [ 'vec* ulong', "initialize a vector of
        elements of given size" ],
'vec_reinit'            => [ 'vec* ulong ulong', "reinitialize a vector to
        increase or decrease the size" ],
'vec_clear'             => [ 'vec* ulong', "clear a vector of elements 
        of given size. The size must be the same as the one given in
        vec_init" ],

# NOTE Except for the purpose of init/reinit/clear operations, all vector
# operations are also valid on sub-vectors, as e.g. considered via
# vec_subvec

'vec_set'               => [ 'dst_vec src_vec ulong', "copy a vector" ],
'vec_set_zero'          => [ 'dst_vec ulong', "zeroes out a vector" ],
'vec_setcoeff'           => [ 'dst_vec src_elt ulong', "set a coeff of the
        vector" ],
'vec_setcoeff_ui'        => [ 'dst_vec ulong ulong', "set a coeff of the
        vector" ],
'vec_getcoeff'           => [ 'dst_elt src_vec ulong', "get a coeff of the
        vector" ],
'vec_add'               => [ 'dst_vec src_vec src_vec ulong', "sum of
        vectors of same size" ],
'vec_neg'               => 'dst_vec src_vec ulong',
'vec_rev'               => [ 'dst_vec src_vec ulong', "revert coefficients
of the vector; can be in place" ],
'vec_sub'               => [ 'dst_vec src_vec src_vec ulong', "subtract
        vectors of same size" ],
'vec_scal_mul'          => [ 'dst_vec src_vec src_elt ulong', "scalar
        multiplication of a vector" ],
'vec_conv'              => [ 'dst_vec src_vec ulong src_vec ulong', 
        "Convolution of a vector of size n and a vector of size m, to get
        a vector of size n+m-1" ],
'vec_random'            => 'dst_vec ulong gmp_randstate_t',
'vec_random2'          => 'dst_vec ulong gmp_randstate_t',
'vec_cmp'               => 'long <- src_vec src_vec ulong',
'vec_is_zero'           => 'long <- src_vec ulong',

'vec_subvec'           => ['dst_vec <- dst_vec long',
                            'returns the sub-vector shifted by the given number of positions' ],

'vec_subvec_const'     => ['src_vec <- src_vec long',
                            'const variant of vec_subvec' ],

'vec_coeff_ptr'        => ['dst_elt <- dst_vec long',
                            'returns a compatible elt object corresponding to the (writable) coefficient at given index within the vector. Note that not all implementations promise to provide this function !'],
'vec_coeff_ptr_const'  => ['src_elt <- src_vec long',
                            'see vec_coeff_ptr. This one is the const variant.'],

'vec_asprint'           => 'long <- char** src_vec ulong',
'vec_fprint'            => 'long <- FILE* src_vec ulong',
'vec_print'             => 'long <- src_vec ulong',
'vec_sscan'             => 'long <- vec* ulong* const-char*',
'vec_fscan'             => 'long <- FILE* vec* ulong*',
'vec_scan'              => 'long <- vec* ulong*',
'vec_cxx_out'           => 'std::ostream& <- std::ostream& src_vec ulong',
'vec_cxx_in'            => 'std::istream& <- std::istream& vec* ulong*',


'vec_read'          => 'long <- FILE* dst_vec ulong',
'vec_write'         => 'long <- FILE* src_vec ulong',
'vec_import'        => 'long <- dst_vec ulong long long',
'vec_export'        => 'long <- src_elt ulong long long',
'vec_hamming_weight'        => [ 'long <- src_vec ulong', "Given a vector v of n elements, returns the number of indices i such that v[i] is not zero. Note that when each item is SIMD data, \"not zero\" counts only as 1, irrespective of the number of inner SIMD items that are non-zero. Use vec_simd_hamming_weight if you want a weighted sum, use vec_simd_hamming_weight." ],
'vec_find_first_set'        => [ 'long <- src_vec ulong', "Given a vector v of n elements, returns the first index i such that v[i] is not zero. Return -1 if the vector is zero. Note that when each item is SIMD data, \"not zero\" can mean *any* of the inner SIMD items being non-zero. Use vec_simd_find_first_set if you want a deeper look." ],

restrict('+SIMD'),
'vec_simd_hamming_weight'        => [ 'long <- src_vec ulong', "weighted sum of simd_hamming_weight for all vector entries" ],
'vec_simd_find_first_set'        => [ 'long <- src_vec ulong', "if the vector is 0, return -1. Otherwise return vec_simd_find_first_set(v,n)+groupsize()*simd_find_first_set(v[vec_simd_find_first_set(v,n)])." ],
restrict('-SIMD'),


restrict('+URE'),

'vec_ur_init'           => 'vec_ur* ulong',
'vec_ur_set_zero'       => 'dst_vec_ur ulong',
'vec_ur_set_vec'	=> 'dst_vec_ur src_vec ulong',
'vec_ur_reinit'         => 'vec_ur* ulong ulong',
'vec_ur_clear'          => 'vec_ur* ulong',

'vec_ur_set'            => 'dst_vec_ur src_vec_ur ulong',
'vec_ur_setcoeff'        => 'dst_vec_ur src_elt_ur ulong',
'vec_ur_getcoeff'        => 'dst_elt_ur src_vec_ur ulong',

'vec_ur_add'            => 'dst_vec_ur src_vec_ur src_vec_ur ulong',
'vec_ur_sub'            => 'dst_vec_ur src_vec_ur src_vec_ur ulong',
'vec_ur_neg'            => 'dst_vec_ur src_vec_ur ulong',
'vec_ur_rev'            => 'dst_vec_ur src_vec_ur ulong',

'vec_scal_mul_ur'       => 'dst_vec_ur src_vec src_elt ulong',
'vec_conv_ur'           => 'dst_vec_ur src_vec ulong src_vec ulong', 
'vec_reduce'            => 'dst_vec dst_vec_ur ulong', 

'vec_ur_subvec'           => 'dst_vec_ur <- dst_vec_ur long',
'vec_ur_subvec_const'     => 'src_vec_ur <- src_vec_ur long',
'vec_ur_coeff_ptr'        => 'dst_elt <- dst_vec_ur long',
'vec_ur_coeff_ptr_const'  => 'src_elt <- src_vec_ur long',


restrict('-URE'),
'vec_elt_stride' => [ 'ptrdiff_t <- long', "Most often, this returns n*sizeof(elt), really (i.e. the size in bytes taken by a vector of n elements). There are cases though where it's not that simple. When the field structure embodies an element length which is not known at compile-time (see the tag \"pz\", for instance), this depends on runtime data. Second, when the type is abstracted to the point where the using code no longer knows about the compile-time characteristics, we have to find a way to do proper allocation, which can be done with this function." ],


restrict('+URE'),
'vec_ur_elt_stride' => [ 'ptrdiff_t <- long', "Most often, this returns n*sizeof(elt), really (i.e. the size in bytes taken by a vector of n elements). There are cases though where it's not that simple. When the field structure embodies an element length which is not known at compile-time (see the tag \"pz\", for instance), this depends on runtime data. Second, when the type is abstracted to the point where the using code no longer knows about the compile-time characteristics, we have to find a way to do proper allocation, which can be done with this function." ],
restrict('-URE'),

# There are also weirder situations.:
# When vector elements are packed. In this case,
# there might be values of n for which asking for the striding
# makes no sense. In such cases, the returned value is zero,
# indicating that a bogus question has been asked (e.g. for a
# vector of bits whose base type is 64-bit wide, asking how
# many bytes are taken by 142 bits is nonsensical.  The
# returned value is therefore 0 in this case).


####################################
restrict("+POLY"),
hdr('Polynomial functions'),

'#TYPES'        => [ qw/poly dst_poly src_poly/ ],

'poly_init'             => [ 'poly uint', "initialize a polynomial to 0,
       and reserve space for given size" ],
'poly_clear'            => [ 'poly', "clear a polynomial" ],
'poly_set'              => [ 'dst_poly src_poly', 'copy a polynomial' ],
'poly_setmonic'         => [ 'dst_poly src_poly', 'make lc = 1' ],
'poly_setcoeff'          => 'dst_poly src_elt uint',
'poly_setcoeff_ui'       => 'dst_poly ulong uint',
'poly_getcoeff'          => 'dst_elt src_poly uint',
'poly_deg'              => 'int <- src_poly',
'poly_add'              => 'dst_poly src_poly src_poly',
'poly_sub'              => 'dst_poly src_poly src_poly',
'poly_set_ui'           => 'dst_poly ulong',
'poly_add_ui'           => 'dst_poly src_poly ulong',
'poly_sub_ui'           => 'dst_poly src_poly ulong',
'poly_neg'              => 'dst_poly src_poly',
'poly_scal_mul'         => 'dst_poly src_poly src_elt',
'poly_mul'              => 'dst_poly src_poly src_poly',
'poly_divmod'           => [ 'int <- dst_poly dst_poly src_poly src_poly', "return zero for division by zero" ],
'poly_precomp_mod'      => 'dst_poly src_poly',
'poly_mod_pre'          => 'dst_poly src_poly src_poly src_poly',
'poly_gcd'              => 'dst_poly src_poly src_poly',
'poly_xgcd'             => 'dst_poly dst_poly dst_poly src_poly src_poly',

'poly_random'           => 'dst_poly uint gmp_randstate_t',
'poly_random2'          => 'dst_poly uint gmp_randstate_t',
'poly_cmp'              => 'int <- src_poly src_poly',

'poly_asprint'          => 'int <- char** src_poly',
'poly_fprint'           => 'int <- FILE* src_poly',
'poly_print'            => 'int <- src_poly',
'poly_sscan'            => 'int <- dst_poly const-char*',
'poly_fscan'            => 'int <- FILE* dst_poly',
'poly_scan'             => 'int <- dst_poly',
'poly_cxx_out'          => 'std::ostream& <- std::ostream& src_poly',
'poly_cxx_in'           => 'std::istream& <- std::istream& dst_poly',

restrict("-POLY"),

###################################
restrict('+SIMD'),
hdr('Functions related to SIMD operation'),

'simd_groupsize'     => [ 'int <-',
        "Indicates how many elements are considered together. This might
        be a compile-time constant, or a variable. When this data is
        variable, it may be specified with the field_specify method and
        the MPFQ_SIMD_GROUPSIZE tag." ],

#    # The deprecation thing comes from commit e610256, and affects both
#    # offset() and stride().
#'offset' => [ 'int <- int',
#        "TO BE DEPRECATED. In a context where the group size is a runtime
#        variable, it is not possible to assume that the element after the
#        one pointed to by x (of type elt*) is x+1. It might be something
#        further away.  The offset() calculation returns exactly this. The
#        k-th element after the one pointed to by x is at x+offset(k)"
#        ],
#'stride' => [ 'int <-', 'TO BE DEPRECATED. Alias for offset(1)' ],
    
'simd_hamming_weight'	=> [ 'int <- src_elt', "Returns the number of non-zero elements in the SIMD group" ],
'simd_find_first_set'	=> [ 'int <- src_elt', "Returns the index (counted from 0) of the first non-zero element in the SIMD group. Returns -1 if all elements are zero" ],
'simd_get_ui_at'	=> 'ulong <- src_elt int',
'simd_set_ui_at'	=> 'dst_elt int ulong',
'simd_add_ui_at'	=> 'dst_elt src_elt int ulong',
'simd_set_ui_all'	=> 'dst_elt ulong',

#restrict('+URE'),
#'elt_ur_simd_hamming_weight'	=> [ 'int <- src_elt_ur', "Returns the number of non-zero elements in the SIMD group" ],
#'elt_ur_simd_find_first_set'	=> [ 'int <- src_elt_ur', "Returns the index (counted from 0) of the first non-zero element in the SIMD group. Returns -1 if all elements are zero" ],
#'elt_ur_simd_get_ui_at'	=> 'ulong <- src_elt_ur int',
#'elt_ur_simd_set_ui_at'	=> 'dst_elt_ur int ulong',
#'elt_ur_simd_set_ui_all'	=> 'dst_elt_ur ulong',
#restrict('-URE'),

'add_dotprod' => [ 'dst_vec src_vec src_vec uint',
            "This takes two vectors of n elements, and adds to the
            output a _vector_ whose length is the SIMD group size of the
            current type.  If the SIMD group size is g, this operation is
            thus Transpose(U)*V, where U and V are matrices of size n*g."
            ],
            
# grmblblbl. how would we prototype a mul_constant ? (not mul_constant_ui ?)
'*mul_constant_ui'		=> 'dst_elt src_elt ulong',


hdr('Member templates related to SIMD operation'),

# This is ugly. See OO interface much deeper down. These functions are
# better accessed via the OO interface.

'#COMMON_ARGS'  => [ ],

'member_template_add_dotprod' => [ '0dst_field 1dst_field dst_vec 1src_vec src_vec uint',
            "[MEMBER TEMPLATE]
            This takes two vectors of n elements. It works with both the
            current type (denoted type 0, and one other type (denoted
            type 1). The first vector is made of elements relative to
            type 1 (variable identified by 1dst_field), with its proper
            SIMD group size. The second vector is made of elements of type 0 (yes).
            The output is a vector of elements of type 0, but its length
            is the SIMD group size of type 1.  If the SIMD group size of type 0
            is g, and the SIMD group size of type 1 is g', then this operation
            adds to the output Transpose(U)*V, where U and V are matrices of size n*g'
            and n*g, respectively.  The output is a matrix of size
            g'*g."], 
'member_template_addmul_tiny' => [ '0dst_field 1dst_field
                        1dst_vec src_vec 1src_vec uint',
            "[MEMBER TEMPLATE]
            Let the arguments be K0 K1 A1 A0 B1 n. This computes A1 +=
            A0*B, following the conventions detailed here.
            Let g0 denote the SIMD group size of K0 (respectively g1 for
            K1). A0 denotes an n*g0 matrix, and A1 an n*g1 matrix
            (equivalently, A0 and A1 are vectors of n SIMD elements or
            SIMD widths g0 and g1, respectively).  The argument B is
            expected to be a g0*g1 matrix (that is, a vector of length g0
            of SIMD elements of SIMD width g1 -- therefore relative to
            K1).  The function computes A1 += A0 * B." ],
'member_template_transpose' => [ '0dst_field 1dst_field
                    dst_vec 1src_vec',
            "[MEMBER TEMPLATE]
            This takes two vectors of n elements. It works with both the
            current type (denoted type 0), and one other type (denoted
            type 1).  Let g0 denote the SIMD group size of type 0
            (respectively g1 for type 1).  This function transposes the
            input vector of g0 elements of type 1, into a vector of g1
            elements of type 0.  Caveat: older versions of this code had
            the two last arguments swapped."],

'#COMMON_ARGS'  => [ 'dst_field' ],
                        

restrict('-SIMD'),

####################################


restrict("+OO"),
hdr('Object-oriented interface'),

# See simd/README.oo

'#COMMON_ARGS'  => [ ],

'oo_field_init' => [ 'magic_virtual_base_ptr',
            "field_init(f) sets f, which is an object of the abstract
            base class, to be an object of the current type.  Another,
            non-OO facade of the same object can be obtained by setting a
            dst_field variable to the value f->obj.  The companion to
            this constructor function is field_clear." ],
'oo_field_clear' => [ 'magic_virtual_base_ptr', "clears f." ],


# oo_field_init turns into a function like mpfq_u64k1_oo_field_init(v),
# which inits the virtual base with function pointers related to impl
# mpfq_u64k1.
#
# This is not the same as mpfq_vbase_oo_field_init_byfeatures, which
# does more. mpfq_vbase_oo_field_init_byfeatures (note that it is not
# prefixed by a specific implementation like mpfq_u64k1) selects the
# implementation to be used (say BLAH), and then calls
# BLAH_oo_field_init(v)
# (see simd/README.oo)

## This escapes the api even more, 
## 'oo_init_templates'    => 'mpfq_vbase_tmpl_ptr magic_virtual_base_ptr magic_virtual_base_ptr',

'#COMMON_ARGS'  => [ 'dst_field' ],

restrict("-OO"),


# It is normal for this file to end with a comma.
