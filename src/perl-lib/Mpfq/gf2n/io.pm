package Mpfq::gf2n::io;
use strict;
use warnings;

use Mpfq::engine::utils qw(
    ceildiv
);

sub code_for_asprint {
    my $opt = $_[0];
    return if $opt->{"no_gmp"};
    my $n = $opt->{'n'};
    my $w = $opt->{'w'};
    my $eltwidth = ceildiv $n, $w;
    my $proto = 'function(k,pstr,x)';
    my $code = <<EOF;
int type = k->io_type;
int i, n; 

// Numerical io.
if (type <= 16) {
    // allocate enough room for base 2 conversion.
    *pstr = (char *)mpfq_malloc_check(($n+1));

    unsigned long tmp[$eltwidth + 1];
    for (i = 0; i < $eltwidth; ++i)
        tmp[i] = x[i];

    // mpn_get_str() needs a non-zero most significant limb
    int msl = $eltwidth - 1;
    while ((msl > 0) && (tmp[msl] == 0))
        msl--;
    msl++;
    if ((msl == 1) && (tmp[0] == 0)) {
        (*pstr)[0] = '0';
        (*pstr)[1] = '\\0';
        return 1;
    }
    n = mpn_get_str((unsigned char*)(*pstr), type, tmp, msl);
    for (i = 0; i < n; ++i) {
        char c = (*pstr)[i] + '0';
        if (c > '9')
            c = c-'0'+'a'-10;
        (*pstr)[i] = c;
    }
    (*pstr)[n] = '\\0';

    // Remove leading 0s
    int shift = 0;
    while (((*pstr)[shift] == '0') && ((*pstr)[shift+1] != '\\0')) 
        shift++;
    if (shift>0) {
        memmove(*pstr, (*pstr) + shift, n + 1 - shift);
        n -= shift;
    }

    // Return '0' instead of empty string for zero element
    if ((*pstr)[0] == '\\0') {
        (*pstr)[0] = '0';
        (*pstr)[1] = '\\0';
        n = 1;
    }
    return n;
} 
// Polynomial io.
else {
    char c = (char)type;
    // allocate (more than) enough room for polynomial conversion.
    // Warning: this is for exponent that fit in 3 digits
    *pstr = (char *)mpfq_malloc_check((8*$n+1));
    {
        unsigned int j;
        int sth = 0;
        char *ptr = *pstr;
        for(j = 0 ; j < $n ; j++) {
            if (x[j/$w] >> (j % $w) & 1UL) {
            	if (sth) {
                    *ptr++ = ' ';
                    *ptr++ = '+';
                    *ptr++ = ' ';
                }
            	sth = 1;
            	if (j == 0) {
                    *ptr++ = '1';      
            	} else if (j == 1) {
                    *ptr++ = c;      
            	} else {
                    int ret = sprintf(ptr,"\%c^\%d",c,j);
                    ptr += ret;
            	}
            }
        }
        if (!sth) {
            *ptr++ = '0';
        }
        *ptr = '\\0';
        return ptr - *pstr;
    }
}
EOF
    return [ $proto, $code ];
}

sub code_for_sscan {
    my $opt = shift @_;
    return if $opt->{"no_gmp"};
    my $n = $opt->{'n'};
    my $w = $opt->{'w'};
    my $eltwidth = ceildiv $n, $w;
    my $proto = 'function(k,z,str)';
    my $code = <<EOF;
if (k->io_type <= 16) {
    unsigned char *tmp;
    int len = strlen(str);
    tmp = (unsigned char *)mpfq_malloc_check(len+1);
    int i;
    for (i = 0; i < len; ++i) {
        if (str[i] >= '0' && str[i] <= '9' && str[i] < '0' + k->io_type) {
            tmp[i] = str[i] - '0';
        } else if (str[i] >= 'a' && str[i] < 'a' + k->io_type - 10) {
            tmp[i] = str[i] + 10 - 'a';
        } else if (str[i] >= 'A' && str[i] < 'A' + k->io_type - 10) {
            tmp[i] = str[i] + 10 - 'A';
        } else {
            break;
        }
    }
    if (i == 0) {
        free(tmp);
        return 0;
    }
    len = i;
    tmp[len]='\\0';
    unsigned long *zz;
    // Allocate one limb per byte... very conservative.
    zz = (unsigned long *)mpfq_malloc_check(len*sizeof(unsigned long));
    int ret = mpn_set_str(zz, tmp, len, k->io_type);
    free(tmp);
    if (ret > $eltwidth) {
        free(zz);
        return 0;
    }
    mpfq_copy(z, zz, ret);
    mpfq_zero(z + ret, $eltwidth - ret);
    free(zz);
    return len;
} else {
    fprintf(stderr, "Polynomial io not implemented for reading\\n");
    return 0;
}
EOF
    return [ $proto, $code ];
}

sub code_for_fscan {
    my $opt = shift @_;
    return if $opt->{"no_gmp"};
    my $n = $opt->{'n'};
    my $w = $opt->{'w'};
    my $proto = 'function(k,file,z)';
    my $code = <<EOF;
char *tmp;
int allocated, len=0;
int c, start=0;
allocated=100;
tmp = (char *)mpfq_malloc_check(allocated);
for(;;) {
    c = fgetc(file);
    if (c==EOF)
        break;
    if (isspace((int)(unsigned char)c)) {
        if (start==0)
            continue;
        else
            break;
    } else {
        if (len==allocated) {
            allocated = len + allocated / 4;
            tmp = (char*)realloc(tmp, allocated);
        }
        tmp[len]=c;
        len++;
    }
}
if (len==allocated) {
    allocated+=1;
    tmp = (char*)realloc(tmp, allocated);
}
tmp[len]='\\0';
int ret=@!sscan(k,z,tmp);
free(tmp);
return ret ? len : 0;
EOF
    return [ $proto, $code ];
}

sub code_for_cxx_in {
    my $opt = shift @_;
    return if $opt->{"no_gmp"};
    my $n = $opt->{'n'};
    my $w = $opt->{'w'};
    my $proto = 'function(k,is,z)';
    my $code = <<EOF;
char *tmp;
int allocated, len=0;
int start=0;
allocated=100;
tmp = (char *)mpfq_malloc_check(allocated);
for(;;) {
    char c;
    if (!(is.get(c)))
        break;
    if (isspace(c)) {
        if (start==0)
            continue;
        else
            break;
    } else {
        if (len==allocated) {
            allocated = len + allocated / 4;
            tmp = (char*)realloc(tmp, allocated);
        }
        tmp[len]=c;
        len++;
    }
}
if (len==allocated) {
    allocated+=1;
    tmp = (char*)realloc(tmp, allocated);
}
tmp[len]='\\0';
int ret=@!sscan(k,z,tmp);
if (ret != len)
    is.setstate(std::ios::failbit);
free(tmp);
return is;
EOF
    return [ $proto, $code ];
}

sub init_handler {
    my ($opt) = @_;

    for my $t (qw/n w/) {
	return -1 unless exists $opt->{$t};
    }
    return {};
}

1;
# vim:set sw=4 sta et:
