package Mpfq::fixmp::longlong;

use strict;
use warnings;
use Carp;
use Data::Dumper;

use Exporter 'import';
our @EXPORT_OK = qw/arglist_unbox_n alter_hash/;

use Mpfq::engine::maketext qw/simple_indent/;

# {{{ utility: alter_hash
sub alter_hash {
    my $h0 = shift;
    confess unless ref $h0 eq 'HASH';
    my $h = {}; $h->{$_}=$h0->{$_} for keys %$h0;
    while (@_) {
        my $k = shift;
        my $v = shift;
        $h->{$k}=$v;
    }
    return $h;
}
# }}}

# {{{ utility: arglist_unbox_n
sub arglist_unbox_n {
    # $opthw is undefined or equal to 1 upon return.
    my ($opt, $fname) = @_;
    if (defined($opt->{'n'})) {
        my $P = $opt->{'n'};
        if ($opt->{'opthw'}) {
            $P--;
            $P .= "_5";
        }
        return $opt, $opt->{'n'}, $opt->{'opthw'}, $P;
    } else {
        die unless ref $fname eq '';
        $fname =~ /^(\d+)(_\d+)?/ or confess "bad fname: $fname";
        my $P=$1;
        $P .= $2 if $2;
        my $n=$1;
        my $opthw;
        $opthw = 1 if $2;
        $n++ if $opthw;
        return $opt, $n, $opthw, $P;
    }
}
# }}}

# all generated functions are prefixed by mpfq_fixmp_${P}, with $P being
# e.g. 1, or 2_5 ; This ${P} determines the data operation length, as
# well as ancillary data. Comments give data width in the following way,
# dependin on whether ${P} terminates with _5 (the "half-word" case) or
# not. When we write:
#   n words, this means n=1 for P=1, but n=3 for P=2_5.
#   nd words, this means n words, but with half top word in the half-word case.
#   hw, this means 1 for the half-word case, 0 otherwise.
#
# Examples:
#
#       mpfq_fixmp_1_add                n = 1, hw = 0, nd = 1
#       mpfq_fixmp_8_5_redc_ur          n = 9, hw = 1, nd = 8.5
# 
# Generated prototypes are documented here in comments, and are enforced
# by the api which is created on the fly within src/fixmp/gen_fixmp.pl.
# Comments in the data returned by the functions here also repeat the
# prototypes, but really the only authoritative source is
# src/fixmp/gen_fixmp.pl.

## {{{ addmul1.
# The same code generator is used for the functions listed below
# mp_limb_t addmul1(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n+1. Put (z+x*c) in z. Return carry bit.
# void addmul1_nc(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n+1. Put (z+x*c) in z. Carry bit is lost.
# mp_limb_t addmul1_shortz(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. Put (z+x*c) in z. Return carry word.
# mp_limb_t addmul05(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. c is 0.5 word. Put (z+x*c) in z. Return carry bit.
# void addmul05_nc(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. c is 0.5 word. Put (z+x*c) in z. Carry lost.
# void mul1(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n + 1. c is 1 word. Put x*c in z. No carry.
# void mul05(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. c is 0.5 word. Put x*c in z. No carry.
sub code_for__fixmp_addmul1 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);

    # This code generator recognizes several influential parameters:
    # small_c : assert that c fits in a half word.
    # discard_carry : for the _nc variants.
    # short_z : for the _shortz variant.
    my $n1 = $n;
    $n1-- if $opt->{'small_c'};

    my $rtype="";
    $rtype = "mp_limb_t <- " unless $opt->{'discard_carry'};
    if ($opt->{'only_mul1'}) {
        $rtype = "" unless $opt->{'short_z'};
    }

    my @regs=qw/lo carry/;
    unshift @regs, 'hi' if $n1>0;
    push @regs, 'buf' if $n1>0 && !$opt->{'only_mul1'};
    my $regs=join(", ", @regs);
    my $code = <<EOF;
    mp_limb_t $regs;
    carry = 0;
EOF

    for (my $i=0; $i < $n1; $i++) {
        $code .= <<EOF;
        mpfq_umul_ppmm(hi,lo,c,x[$i]);
        lo += carry;
        carry = (lo<carry) + hi;
EOF
        if (!$opt->{'only_mul1'}) {
            # addmul needs some extra work, of course. All of this
            # collapses to no-ops when doing only mul1.
            $code .= <<EOF;
            buf = z[$i];
            lo += buf;
            carry += (lo<buf);
EOF
        }
        $code .= <<EOF;
        z[$i] = lo;
EOF
    }
    if ($opt->{'short_z'}) {
        die unless $n == $n1;
        $code .= "return carry;\n";
    } elsif ($n == $n1) {
        # normal case
        if ($opt->{'only_mul1'}) {
            $code .= "z[$n] = carry;\n";
        } else {
            $code .= "z[$n] += carry;\n";
            $code .= "return (z[$n]<carry);\n" unless $opt->{'discard_carry'};
        }
    } else {
        die unless $opt->{'small_c'};
        # special case, where c is asserted to be smaller than a half
        # word (see below).
        # c and x[n1] are less than (2^32-1), and carry is less than
        # 2^32-1 too. So in the end we expect that there is no carry in
        # the addition below.
        $code .= <<EOF;
        lo = c*x[$n1] + carry;
        assert(lo >= carry);
EOF
        if ($opt->{'only_mul1'}) {
            $code .= "z[$n1] = lo;\n";
        } else {
            $code .= "z[$n1] += lo;\n";
            $code .= "return z[$n1] < lo;\n" unless $opt->{'discard_carry'};
        }
    }

    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(z, x, c)',
        #requirements => "${rtype}mp_limb_t* const-mp_limb_t* mp_limb_t",
        name => "${P}_addmul1",
    };
}

# This is similar to addmul1, except that we assert here that c fits in
# an half word.
# mp_limb_t addmul05(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. c is 0.5 word. Put (z+x*c) in z. Return carry bit.
sub code_for__fixmp_addmul05 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    confess "only for half-word code" unless $opthw;
    shift @_;
    my $x = code_for__fixmp_addmul1(alter_hash($opt, small_c => 1), @_);
    return alter_hash($x, name => "${P}_addmul05");
}

# void addmul1_nc(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n+1. Put (z+x*c) in z. Carry bit is lost.
sub code_for__fixmp_addmul1_nc {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my $x = code_for__fixmp_addmul1(alter_hash($opt, discard_carry => 1), @_);
    return alter_hash($x, name => "${P}_addmul1_nc");
}

# void addmul05_nc(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. c is 0.5 word. Put (z+x*c) in z. Carry lost.
sub code_for__fixmp_addmul05_nc {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    confess "only for half-word code" unless $opthw;
    shift @_;
    my $x = code_for__fixmp_addmul1(alter_hash($opt, small_c => 1, discard_carry => 1), @_);
    return alter_hash($x, name => "${P}_addmul05_nc");
}

# mp_limb_t addmul1_shortz(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. Put (z+x*c) in z. Return carry word.
sub code_for__fixmp_addmul1_shortz {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my $x = code_for__fixmp_addmul1(alter_hash($opt, short_z => 1), @_);
    return alter_hash($x, name => "${P}_addmul1_shortz");
}

# void mul1(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n + 1. c is 1 word. Put x*c in z. No carry.
sub code_for__fixmp_mul1 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my $x = code_for__fixmp_addmul1(alter_hash($opt, only_mul1=>1, discard_carry => 1), @_);
    return alter_hash($x, name => "${P}_mul1");
}

# void mul05(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. c is 0.5 word. Put x*c in z. No carry.
sub code_for__fixmp_mul05 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    confess "only for half-word code" unless $opthw;
    shift @_;
    my ($x, @tail) = code_for__fixmp_mul1(alter_hash($opt, small_c => 1), @_);
    return alter_hash($x, name => "${P}_mul05"), @tail;
}
# 

# }}}

# {{{ mul -- this is fallback code, to be based on addmul1_nc
# void mul(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#    x and y have $nd words, z has 2*n-hw. Put x*y in z.
sub code_for__fixmp_mul {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my $nn = 2*$n;
    my $n1 = $n;
    my $nn1 = $nn;
    $n1-- if $opthw;
    $nn1-- if $opthw;

    my $code = <<EOF;
    assert(z != x && z != y);
    for (int i = 0; i < $nn1; z[i++] = 0) ;
EOF

    my @subs;
    if ($n1) {
        push @subs, code_for__fixmp_addmul1_nc(@_);
    }

    for (my $i = 0; $i < $n1; $i++) {
        $code .= "  @!${P}_addmul1_nc (z + $i, x, y[$i]);\n";
    }
    if ($opthw) {
        push @subs, code_for__fixmp_addmul05_nc(@_);
        $code .= "  @!${P}_addmul05_nc (z + $n1, x, y[$n1]);\n";
    }

    $code = simple_indent($code);
    return {
        code => $code,
        name => "${P}_mul",
        kind => 'inline(z, x, y)',
        #requirements => 'mp_limb_t* const-mp_limb_t* const-mp_limb_t*',
    }, @subs;
}
# }}}

# {{{ shortmul
# void shortmul(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#    x and y have $nd words, z has n. Put the low n words of x*y in z.
sub code_for__fixmp_shortmul {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my $code = <<EOF;
    mpfq_zero(z, $n);
EOF

    my @subs=();
    for (my $i = 0; $i < $n-1; $i++) {
        my $j = $n-$i-1;
        push @subs,
        alter_hash(
            code_for__fixmp_addmul1_nc(
                alter_hash($opt,
                    n=>$j), @_),
        );
        $code .= "@!${j}_addmul1_nc (z+$i, x, y[$i]);\n";
        $code .= "  z[$n-1] += x[$j]*y[$i];\n";
    }
    $code .= "  z[$n-1] += x[0]*y[$n-1];\n";
    $code = simple_indent($code);
    return {
        code => $code,
        name => "${P}_shortmul",
        kind => 'inline(z, x, y)',
        #requirements => "mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
    }, @subs;
}
# }}}

# {{{ add, sub, add_ui, sub_ui and nc variants
# The same code generator is used for the functions listed below
# mp_limb_t add(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#     x, y, and z have nd words. Result to z. Return carry bit
# mp_limb_t sub(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#     x, y, and z have nd words. Result to z. Return borrow bit
# mp_limb_t add_ui(mp_limb_t * z, const mp_limb_t * x, mp_limb_t y)
#     x and z have nd words. y is 1 word. Result to z. Return carry bit
# mp_limb_t sub_ui(mp_limb_t * z, const mp_limb_t * x, mp_limb_t y)
#     x and z have nd words. y is 1 word. Result to z. Return borrow bit
# void add_nc(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#     x, y, and z have nd words. Result to z.
# void sub_nc(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#     x, y, and z have nd words. Result to z.
# void add_ui_nc(mp_limb_t * z, const mp_limb_t * x, mp_limb_t y)
#     x and z have nd words. y is 1 word. Result to z.
# void sub_ui_nc(mp_limb_t * z, const mp_limb_t * x, mp_limb_t y)
#     x and z have nd words. y is 1 word. Result to z.
#
sub code_for__fixmp_add {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);

    my $code = <<EOF;
        mp_limb_t r, s, t, cy, cy1, cy2;
        cy = 0;
EOF

    my $rtype="";
    $rtype = "mp_limb_t <- " unless $opt->{'discard_carry'};

    for (my $i=0; $i < $n; $i++) {
        $code .= <<EOF;
        r = x[$i];
        s = r + y[$i];
        cy1 = s < r;
        t = s + cy;
        cy2 = t < s;
        cy = cy1 | cy2;
        z[$i] = t;
EOF
    }
    $code .= "return cy;" unless $opt->{'discard_carry'};
    $code = simple_indent($code);
    return {
        code => $code,
        name => "${P}_add",
        kind => 'inline(z, x, y)',
        #requirements => $rtype . 'mp_limb_t* const-mp_limb_t* const-mp_limb_t*',
    };
}

# mp_limb_t add_ui(mp_limb_t * z, const mp_limb_t * x, mp_limb_t y)
#     x and z have nd words. y is 1 word. Result to z. Return carry bit
sub code_for__fixmp_add_ui {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);

    my $rtype="";
    $rtype = "mp_limb_t <- " unless $opt->{'discard_carry'};

    my $code = <<EOF;
        mp_limb_t r, s, t, cy, cy1, cy2;
        cy = 0;
        r = x[0];
        s = r + y;
        cy1 = s < r;
        t = s + cy;
        cy2 = t < s;
        cy = cy1 | cy2;
        z[0] = t;
EOF

    for (my $i=1; $i < $n; $i++) {
        $code .= <<EOF;
        s = x[$i];
        t = s + cy;
        cy = t < s;
        z[$i] = t;
EOF
    }
    $code .= "return cy;" unless $opt->{'discard_carry'};
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(z, x, y)',
        name => "${P}_add_ui",
        #requirements => $rtype . 'mp_limb_t* const-mp_limb_t* const-mp_limb_t',
    };
}

# mp_limb_t sub(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#     x, y, and z have nd words. Result to z. Return borrow bit
sub code_for__fixmp_sub {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);

    my $rtype="";
    $rtype = "mp_limb_t <- " unless $opt->{'discard_carry'};

    my $code = <<EOF;
    mp_limb_t r, s, t, cy, cy1, cy2;
    cy = 0;
EOF

    for (my $i=0; $i < $n; $i++) {
        $code .= <<EOF;
        r = x[$i];
        s = r - y[$i];
        cy1 = s > r;
        t = s - cy;
        cy2 = t > s;
        cy = cy1 | cy2;
        z[$i] = t;
EOF
    }
    $code .= "return cy;" unless $opt->{'discard_carry'};
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(z, x, y)',
        name => "${P}_sub",
        #requirements => $rtype . 'mp_limb_t* const-mp_limb_t* const-mp_limb_t*',
    };
}

# mp_limb_t sub_ui(mp_limb_t * z, const mp_limb_t * x, mp_limb_t y)
#     x and z have nd words. y is 1 word. Result to z. Return borrow bit
sub code_for__fixmp_sub_ui {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);

    my $rtype = "";
    $rtype = "mp_limb_t <- " unless $opt->{'discard_carry'};

    my $code = <<EOF;
        mp_limb_t r, s, t, cy, cy1, cy2;
        cy = 0;
        r = x[0];
        s = r - y;
        cy1 = s > r;
        t = s - cy;
        cy2 = t > s;
        cy = cy1 | cy2;
        z[0] = t;
EOF

    for (my $i=1; $i < $n; $i++) {
        $code .= <<EOF;
        s = x[$i];
        t = s - cy;
        cy = t > s;
        z[$i] = t;
EOF
    }
    $code .= "return cy;" unless $opt->{'discard_carry'};
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(z, x, y)',
        name => "${P}_sub_ui",
        #requirements => $rtype . 'mp_limb_t* const-mp_limb_t* const-mp_limb_t',
    };
}


# void add_nc(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#     x, y, and z have nd words. Result to z.
sub code_for__fixmp_add_nc {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my ($x, @tail) = code_for__fixmp_add(alter_hash($opt, discard_carry=>1), @_);
    return alter_hash($x, name => "${P}_add_nc"), @tail;
}

# void add_ui_nc(mp_limb_t * z, const mp_limb_t * x, mp_limb_t y)
#     x and z have nd words. y is 1 word. Result to z.
sub code_for__fixmp_add_ui_nc {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my ($x, @tail) = code_for__fixmp_add_ui(alter_hash($opt, discard_carry=>1), @_);
    return alter_hash($x, name => "${P}_add_ui_nc"), @tail;
}

# void sub_nc(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#     x, y, and z have nd words. Result to z.
sub code_for__fixmp_sub_nc {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my ($x, @tail) = code_for__fixmp_sub(alter_hash($opt, discard_carry=>1), @_);
    return alter_hash($x, name => "${P}_sub_nc"), @tail;
}

# void sub_ui_nc(mp_limb_t * z, const mp_limb_t * x, mp_limb_t y)
#     x and z have nd words. y is 1 word. Result to z.
sub code_for__fixmp_sub_ui_nc {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my ($x, @tail) = code_for__fixmp_sub_ui(alter_hash($opt, discard_carry=>1), @_);
    return alter_hash($x, name => "${P}_sub_ui_nc"), @tail;
}

# }}}

# {{{ sqr
# void sqr(mp_limb_t * z, const mp_limb_t * x)
#    x has $nd words, z has 2*n-hw. Put x*y in z.
sub code_for__fixmp_sqr {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my $nn = 2*$n;
    my $n1 = $n;
    my $nn1 = $nn;
    $n1-- if $opthw;
    $nn1-- if $opthw;
    my $code = <<EOF;
    mp_limb_t buf[$nn1] = {0,};
EOF
    my @subs;
    # Let X = \sum W^ix_i and S_i = \sum_{j<i} W^j x_j.
    # We have X^2 = \sum W^{2i}x_i^2 + 2 * \sum x_iS_i
    # We accumulate 2 * \sum x_iS_i in buf, put the squares in z, and add
    # that up.
    for(my $i = 1; $i < $n; $i++) {
        push @subs,
            alter_hash(
                code_for__fixmp_addmul1_nc(
                    alter_hash($opt, n=>$i), @_),
            );
        $code .= "@!${i}_addmul1_nc(buf + $i, x, x[$i]);\n";
    }
    for (my $i=0; $i < $n1; $i++) {
        $code .= "mpfq_umul_ppmm(z[2*$i+1], z[2*$i], x[$i], x[$i]);\n";
    }
    if ($n1 != $n) {
        $code .= "z[2*$n1] = x[$n1] * x[$n1];\n";
    }
    # TODO: How reasonable is it to do mpn_add_n here ?
    $code .= <<EOF;
    mpn_lshift(buf, buf, $nn1, 1);
    mpn_add_n(z, z, buf, $nn1);
EOF
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(z, x)',
        name => "${P}_sqr",
        #requirements => 'mp_limb_t* const-mp_limb_t*',
    }, @subs;
}
# }}}

# {{{ mod
# void mod(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * p)
#    x has 2*n-hw words. z and p have nd words, and the high word of p is non-zero. Put x mod p in z.
sub code_for__fixmp_mod {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my $n1 = $n;
    my $nn = 2*$n;
    my $nn1 = $nn;
    $n1-- if $opthw;
    $nn1-- if $opthw;
    my $code = <<EOF;
      mp_limb_t q[$n1+1], r[$n];
      assert (p[$n-1] != 0);
      mpn_tdiv_qr(q, r, 0, x, $nn1, p, $n);
      mpfq_copy(z, r, $n);
EOF
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(z, x, p)',
        name => "${P}_mod",
        #requirements => 'mp_limb_t* const-mp_limb_t* const-mp_limb_t*',
    };
}
# }}}

# {{{ cmp cmp_ui
# int cmp(const mp_limb_t * x, const mp_limb_t * y)
#    x and y have nd words. Return sign of difference x-y.
sub code_for__fixmp_cmp {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);

    my $code = <<EOF;
        for (int i = $n-1; i >= 0; --i) {
            if (x[i] > y[i]) return 1;
            if (x[i] < y[i]) return -1;
        }
        return 0;
EOF
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(x, y)',
        name => "${P}_cmp",
        #requirements => 'int <- const-mp_limb_t* const-mp_limb_t*',
    };
}

# int cmp_ui(const mp_limb_t * x, const mp_limb_t * y)
#    x and y have nd words. Return sign of difference x-y.
sub code_for__fixmp_cmp_ui {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);

    my $code;
    if ($n > 1) {
        $code = <<EOF;
        for (int i = $n-1; i > 0; --i) {
            if (x[i]) return 1;
        }
EOF
    }
    $code .= <<EOF;
        if (x[0]>y) return 1;
        if (x[0]<y) return -1;
        return 0;
EOF
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(x, y)',
        name => "${P}_cmp_ui",
        #requirements => 'int <- const-mp_limb_t* mp_limb_t',
    };
}
# }}}

# {{{ invmod
# int invmod(mp_limb_t * res, const mp_limb_t * x, const mp_limb_t * p)
#     x, z, and p have nd words. Put inverse of x mod p in z.
#     Return non-zero if an inverse could be found.
#     If no inverse could be found, return 0 and set z to zero.
sub code_for__fixmp_invmod1 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my $code = <<EOF;
  mp_limb_t a, b, u, v, fix;
  int t, lsh;

  a = *x;
  b = *p;

  if (a == 0 || a == b) {
    *z=0;
    return 0;
  }
  /* b must be odd and >a */

  fix = (b+1)>>1;

  assert (a < b);

  u = 1; v = 0; t = 0;
  
  /* compute u and t such that u*a_orig = 2^t mod b */

  /* we maintain:
   *    u*a_orig - (not kept)*b_orig = 2^t*a
   *    v*a_orig - (not kept)*b_orig = -2^t*b
   * a and b are both odd.
   * An update consists in reducing the largest by the smallest,
   * and then adjusting the valuation.  */

  lsh = mpfq_ctzl(a);
  a >>= lsh;
  t += lsh;
  v <<= lsh;
  do {
    do {
      b -= a; v += u;
      lsh = mpfq_ctzl(b);
      b >>= lsh;
      t += lsh;
      u <<= lsh;
    } while (a<b);
    if (a == b)
      break;
    do {
      a -= b; u += v;
      lsh = mpfq_ctzl(a);
      a >>= lsh;
      t += lsh;
      v <<= lsh;
    } while (b < a);
  } while (a != b);
  if (a != 1) {
    *z = a;
    return 0;
  }
  while (t>0) {
    mp_limb_t sig = u & 1UL;
    u >>= 1;
    if (sig)
      u += fix;
    --t;
  } 
  *z = u;
  return 1;
EOF
    return {
        code => $code,
        kind => 'inline(z, x, p)',
        name => "${P}_invmod",
        #requirements => 'int <- mp_limb_t* const-mp_limb_t* const-mp_limb_t*',
    };
}

sub code_for__fixmp_invmod {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    if ($n == 1) { return code_for__fixmp_invmod1(@_); }
    my @subs;
    push @subs,
        code_for__fixmp_add(@_),
	code_for__fixmp_sub(@_),
	code_for__fixmp_cmp(@_),
	code_for__fixmp_cmp_ui(@_),
	code_for__fixmp_long_lshift(@_),
	code_for__fixmp_long_rshift(@_),
        # code_for__fixmp_lshift(@_),
	code_for__fixmp_rshift(@_);
    my $code = <<EOF;
  mp_limb_t u[$n], v[$n], a[$n], b[$n], fix[$n];
  int i, t, lsh;

  mpfq_zero(u, $n);
  mpfq_zero(v, $n);
  mpfq_copy(a, x, $n);
  mpfq_copy(b, p, $n);
  u[0] = 1UL;
  
  if (@!${P}_cmp(a, v) == 0 || @!${P}_cmp(a, b) == 0) {
    mpfq_zero(res, $n);
    return 0;
  }

  @!${P}_add(fix, b, u);
  @!${P}_rshift(fix, 1);

  assert (@!${P}_cmp(a,b) < 0);

  t = 0;
  
  for(i = 0 ; !a[i] ; i++) ;
  assert (i < $n);
  lsh = mpfq_ctzl(a[i]);
  @!${P}_long_rshift(a, i, lsh);
  t += lsh + i*GMP_NUMB_BITS;
  @!${P}_long_lshift(v, i, lsh);

  do {
    do {
      @!${P}_sub(b, b, a);
      @!${P}_add(v, v, u);
      for(i = 0 ; !b[i] ; i++) ;
      assert (i < $n);
      lsh = mpfq_ctzl(b[i]);
      @!${P}_long_rshift(b, i, lsh);
      t += lsh + i*GMP_NUMB_BITS;
      @!${P}_long_lshift(u, i, lsh);
    } while (@!${P}_cmp(a,b) < 0);
    if (@!${P}_cmp(a, b) == 0)
      break;
    do {
      @!${P}_sub(a, a, b);
      @!${P}_add(u, u, v);
      for(i = 0 ; !a[i] ; i++) ;
      assert (i < $n);
      lsh = mpfq_ctzl(a[i]);
      @!${P}_long_rshift(a, i, lsh);
      t += lsh + i*GMP_NUMB_BITS;
      @!${P}_long_lshift(v, i, lsh);
    } while (@!${P}_cmp(b,a)<0);
  } while (@!${P}_cmp(a,b) != 0);
  {
    if (@!${P}_cmp_ui(a, 1) != 0) {
      mpfq_copy(res, a, $n);
      return 0;
    }
  }
  while (t>0) {
    mp_limb_t sig = u[0] & 1UL;
    @!${P}_rshift(u, 1);
    if (sig)
      @!${P}_add(u, u, fix);
    --t;
  }
  mpfq_copy(res, u, $n);
  return 1;
EOF
    return {
        code => $code,
        name => "${P}_invmod",
        kind => 'inline(res, x, p)',
        #requirements => 'int <- mp_limb_t* const-mp_limb_t* const-mp_limb_t*',
    }, @subs;
}
# }}}

# {{{ shifts
# void lshift(mp_limb_t * a, int cnt)
#    a has nd words. Shift it in place by cnt bits to the left.
#    The shift count cnt must not exceed the word size.
#    Note that no carry is returned for the bits shifted out.
sub code_for__fixmp_lshift {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my $code = <<EOF;
    if (!cnt) return;
EOF
    if ($n > 1) {
        $code .= <<EOF;
    int i;
    int dnt = GMP_NUMB_BITS - cnt;
    for (i = $n-1; i>0; --i) {
        a[i] <<= cnt;
        a[i] |= (a[i-1] >> dnt);
    }
EOF
    }
    $code .= <<EOF;
    a[0] <<= cnt;
EOF
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(a, cnt)',
        name => "${P}_lshift",
        #requirements => 'mp_limb_t* int',
    };
}

# void long_lshift(mp_limb_t * a, int off, int cnt)
#    a has nd words. Shift it in place by off words and cnt bits to the left.
#    The shift count cnt must not exceed the word size.
#    Note that no carry is returned for the bits shifted out.
sub code_for__fixmp_long_lshift {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    # We assume that this comes with an implicit promise that
    # 0 <= off < n
    my $code = "";
    if ($n > 1) {
        $code .= <<EOF;
            int i;
            if (cnt) {
                int dnt = GMP_NUMB_BITS - cnt;
                for (i = $n-1; i>off; --i) {
                    a[i] = (a[i-off]<<cnt) | (a[i-off-1]>>dnt);
                }
                a[off] = a[0]<<cnt;
            } else {
                mpfq_copyd(a + off, a, $n - off);
            }
            mpfq_zero(a, off);
EOF
    } else {
        # special case for n == 1
        $code .= <<EOF;
            if (cnt) {
                a[0] <<= cnt;
            }
EOF
    }
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(a, off!, cnt)',
        name => "${P}_long_lshift",
        #requirements => 'mp_limb_t* int int',
    };
}

# void rshift(mp_limb_t * a, int cnt)
#    a has nd words. Shift it in place by cnt bits to the right.
#    The shift count cnt must not exceed the word size.
#    Note that no carry is returned for the bits shifted out.
sub code_for__fixmp_rshift {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my $code = <<EOF;
    if (!cnt) return;
EOF
    if ($n > 1) {
        $code .= <<EOF;
    int i;
    int dnt = GMP_NUMB_BITS - cnt;
    for (i = 0; i < $n-1; ++i) {
        a[i] >>= cnt;
        a[i] |= (a[i+1] << dnt);
    }
EOF
    }
    $code .= <<EOF;
    a[$n-1] >>= cnt;
EOF
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(a, cnt)',
        name => "${P}_rshift",
        #requirements => 'mp_limb_t* int',
    };
}

# void long_lshift(mp_limb_t * a, int off, int cnt)
#    a has nd words. Shift it in place by off words and cnt bits to the right.
#    The shift count cnt must not exceed the word size.
#    Note that no carry is returned for the bits shifted out.
sub code_for__fixmp_long_rshift {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my $code = "";
    if ($n > 1) {
        $code .= <<EOF;
            if (cnt) {
                int dnt = GMP_NUMB_BITS - cnt;
                for (int i = 0; i < $n - off - 1; ++i) {
                    a[i] = (a[i+off]>>cnt) | (a[i+off+1]<<dnt);
                }
                a[$n-off-1] = a[$n-1]>>cnt;
            } else {
                mpfq_copyi(a, a + off, $n - off);
            }
            mpfq_zero(a + $n - off, off);
EOF
    } else {
        # special case for n == 1
        $code .= <<EOF;
            if (cnt) {
                a[0] >>= cnt;
            }
EOF
    }
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(a, off!, cnt)',
        name => "${P}_long_rshift",
        #requirements => 'mp_limb_t* int int',
    };
}
# }}}

# {{{ redc -- several variants.
# void redc(mp_limb_t * z, mp_limb_t * x, const mp_limb_t * mip, const mp_limb_t * p)
#    x has 2*n-hw words, z and p have nd words.
#    only one word is read from invp.
#    Assuming R=W^n is the redc modulus, we expect that x verifies:
#      x < R*p,
#    so that we have eventually z < p, z congruent to x/R mod p.
#    The contents of the area pointed by x are clobbered by this call.
#    Note also that x may alias z.
sub code_for__fixmp_redc_old {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    die "redc_old does not support half-word" if $opthw;
    # at least I haven't checked
 
    my $nn = 2*$n;
    my @subs;
    my $add2_code = "mpn_add_n(t, t, x, $nn)";
    if ($nn <= 9) {
        $add2_code = "@!${nn}_add(t, t, x)";
        push @subs, alter_hash(
                code_for__fixmp_add( alter_hash($opt, n=>$nn), @_),
                name => "${nn}_add");
    }

    my $code = <<EOF;
    mp_limb_t m[$n], t[2*$n], cy;

    @!${P}_shortmul(m, x, mip);
    @!${P}_mul(t, m, p);
    cy = $add2_code;
    if (cy || (@!${P}_cmp(t+$n, p) > 0)) {
        @!${P}_sub(z, t+$n, p);
    } else {
        mpfq_copy(z, t + $n, $n);
    }
    assert (@!${P}_cmp(z, p) < 0);
EOF
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(z, x, mip, p)',
        name => "${P}_redc",
        #requirements => "mp_limb_t* mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
    }, @subs;
}

# void redc(mp_limb_t * z, mp_limb_t * x, const mp_limb_t * mip, const mp_limb_t * p)
#    x has 2*n-hw words, z and p have nd words.
#    only one word is read from invp.
#    Assuming R=W^n is the redc modulus, we expect that x verifies:
#      x < R*p,
#    so that we have eventually z < p, z congruent to x/R mod p.
#    The contents of the area pointed by x are clobbered by this call.
#    Note also that x may alias z.
sub code_for__fixmp_redc1 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my @subs;
    my $code = <<EOF;
    mp_limb_t t = x[0]*mip[0];
EOF
    # redc() takes the result of a multiplication, which has 2n words in
    # the normal case, and 2n-1 words in the half-word case. Note that
    # redc_ur is needed for accumulations of products.
    if ($opthw) {
        push @subs, code_for__fixmp_addmul1_shortz(@_);
        $code .= <<EOF;
            mp_limb_t cy = @!${P}_addmul1_shortz(x, p, t);
            if (cy >= p[0]) {
                z[0] = cy - p[0];
            } else {
                z[0] = cy;
            }
EOF
    } else {
        push @subs, code_for__fixmp_addmul1(@_);
        $code .= <<EOF;
            mp_limb_t cy = @!${P}_addmul1(x, p, t);
            if (cy || (x[1]>=p[0])) {
                z[0] = x[1] - p[0];
            } else {
                z[0] = x[1];
            }
EOF
    }
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(z, x, mip, p)',
        name => "${P}_redc",
        #requirements => "mp_limb_t* mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
    }, @subs;
}

# void redc(mp_limb_t * z, mp_limb_t * x, const mp_limb_t * mip, const mp_limb_t * p)
#    x has 2*n-hw words, z and p have nd words.
#    only one word is read from invp.
#    Assuming R=W^n is the redc modulus, we expect that x verifies:
#      x < R*p,
#    so that we have eventually z < p, z congruent to x/R mod p.
#    The contents of the area pointed by x are clobbered by this call.
#    Note also that x may alias z.
sub code_for__fixmp_redc {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    if ($n == 1) { return code_for__fixmp_redc1(@_); }
    my $m = $n - 1;
    # The quadratic redc adds successive multiples of p to x, so that
    # eventually we have x divisible by (R=2^w)^n. See e.g.
    # Brent-Zimmermann for a complete analysis.
    #
    # Old code: carries of each addmul are set aside for a grouped
    # addition: Carry of addmul at i==0 corresponds to word [n+1], up to
    # carry of addmul at i==n-1 which corresponds to word [2n] (whence
    # this carry will really be handled as a carry). We do carry
    # trackback by an addmul of width n-1.
    #
    # Alternatively, we may do addmul1_shortz everywhere, and eventually
    # an addition of width n.
    #   for(i) x[i] = @!${P}_addmul1_shortz(x+i, p, x[i]*mip[0]);
    #   cy = @!${P}_add(x+n, x+n, x);
    #
    # Note that when we use the second option, the last addmul1_shortz
    # writes to [x+n-1..x+2n-1[ ; so that is perfectly acceptable for the
    # half-word case.
    my @subs;
    push @subs,
                code_for__fixmp_addmul1_shortz(@_),
                code_for__fixmp_cmp(@_),
                code_for__fixmp_sub(@_),
                code_for__fixmp_add(@_);
    my $code = <<EOF;
    mp_limb_t cy;
    for(int i = 0; i < $n; ++i) {
        mp_limb_t t = x[i]*mip[0];
        cy = @!${P}_addmul1_shortz(x+i, p, t);
        assert (x[i] == 0);
        x[i] = cy;
    }
EOF
    if ($opthw) {
        # we have limited space in x: only 2n-1 words.
        my $xhead = join(", ", ((map { "x[$_]" } ($n..2*$n-2)), 0));
        $code .= <<EOF;
            {
                mp_limb_t ret[$n] = { $xhead };
                cy = @!${P}_add(x, x, ret);
            }
EOF
    } else {
        $code .= "cy = @!${P}_add(x, x, x + $n);\n";
    }
    $code .= <<EOF;
    /* At this point, we have (x' denotes x + cy*W^n here)
     * x' <= R*p-1 + (W-1)*p*(1+W+...+W^{n-1}) and x = mod R.
     * x'/R < p + p
     */
    if (cy || @!${P}_cmp(x, p) >= 0) {
        @!${P}_sub(z, x, p);
    } else {
        mpfq_copy(z, x, $n);
    }
EOF
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(z, x, mip, p)',
        name => "${P}_redc",
        #requirements => "mp_limb_t* mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
    }, @subs;
}


# void redc(mp_limb_t * z, mp_limb_t * x, const mp_limb_t * mip, const mp_limb_t * p)
#    x has 2*n+1-hw words, z and p have nd words.
#    only one word is read from invp.
#    Assuming R=W^n is the redc modulus, we expect that x verifies:
#      x < W*W^nd*p = W^0.5*R*p for the hw case, W*R*p otherwise.
#    so that we have eventually z < p, z congruent to x/R mod p.
#    The contents of the area pointed by x are clobbered by this call.
#    Note also that x may alias z.
sub code_for__fixmp_redc_ur {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my @subs;
    my $code;
    if (!$opthw) {
        # normal case.
        push @subs,
                    code_for__fixmp_addmul1(@_),
                    code_for__fixmp_add(@_);
        $code .= <<EOF;
            mp_limb_t cy, q[2];
            for (int i = 0; i < $n; ++i) {
                mp_limb_t t = x[i]*mip[0];
                cy = @!${P}_addmul1(x+i, p, t);
                assert (x[i] == 0);
                x[i] = cy;
            }
            cy=@!${P}_add(x+$n+1, x+$n+1, x);
            /* At this point, we have (x' denotes x + cy*W^(n+1) here)
             * x' <= W*R*p-1 + (W-1)*p*(1+W+...+W^{n-1}) and x = mod R.
             * x'/R < (W+1)*p
             */
            if (cy) {
                /* x'/R-p < W*p, which fits in n+1 words */
                mpn_sub(x+$n,x+$n,$n+1,p,$n);
            }
            mpn_tdiv_qr(q, z, 0, x+$n, $n+1, p, $n);
EOF
    } else {
        push @subs,
                    code_for__fixmp_addmul1_shortz(@_),
                    code_for__fixmp_cmp(@_),
                    code_for__fixmp_sub(@_),
                    code_for__fixmp_add(@_);
        # We prefer to rewrite using addmul1_shortz as a workhorse.
        # This writes down better for the half-word case.
        # After clearing n low words, Carries of each addmul1 (bound to
        # words at position [n] to [2n-1]) are saved exactly in these
        # locations. In the hw case, the usable top words are [n..2n[.
        # This means that the plain add will work
        # like a charm.
        #

        $code .= <<EOF;
            mp_limb_t cy, q[1];
            for(int i = 0; i < $n; ++i) {
                mp_limb_t t = x[i]*mip[0];
                cy = @!${P}_addmul1_shortz(x+i, p, t);
                assert (x[i] == 0);
                x[i] = cy;
            }
            cy = @!${P}_add(x + $n, x, x + $n);
            /* At this point, we have (x' denotes x + cy*W^(n+1) here)
             * x' <= W^0.5*R*p-1 + (W-1)*p*(1+W+...+W^{n-1}) and x = mod R.
             * x'/R < (W^0.5+1)*p
             */
            if (cy) {
                /* x'/R-p < W^0.5*p, which fits in n words. */
                @!${P}_sub(x + $n, x + $n, p);
            }
            mpn_tdiv_qr(q, z, 0, x + $n, $n, p, $n);
EOF
    }
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(z, x, mip, p)',
        name => "${P}_redc_ur",
        #requirements => "mp_limb_t* mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
    }, @subs;
}

# }}}

# {{{ mgy enc/dec
# void mgy_encode(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * p)
#    x, z, and p have nd words.
#    Assuming R=W^n is the redc modulus, we compute z=R*x mod p. 
sub code_for__fixmp_mgy_encode {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my $nn = 2*$n;
    my $limbs = join(", ", ((0) x $n, map { "x[$_]" } (0..$n-1)));
    my $code;
    if (!$opthw) {
        $code = <<EOF;
        mp_limb_t t[$nn] = { $limbs };
        @!${P}_mod(z, t, p);
EOF
    } else {
        # half-word mgy encode is unfortunately quite different. At least
        # for now. We haven't settled yet on whether we use W^n or
        # W^(n-0.5) as the redc auxiliary base. Currently we do the
        # former, because it's less awkward for redc which is
        # speed-critical. This means that the data we compute above is one limb
        # too long compared to what @!mod can grok. Therefore we resort
        # to tdiv_qr, at least for the time being (note that mod uses
        # tdiv_qr too, so that's not much of an issue, in fact).
        $code = <<EOF;
        mp_limb_t t[$nn] = { $limbs };
        mp_limb_t qq[$n+1];
        mpn_tdiv_qr(qq, z, 0, t, $nn, p, $n);
EOF
    }
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(z, x, p)',
        name => "${P}_mgy_encode",
        #requirements => "mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
    };
}

# void mgy_decode(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * invR, const mp_limb_t * p)
#    x, z, invR and p have nd words.
#    Assuming R=W^n is the redc modulus, we compute z=x/R mod p. 
sub code_for__fixmp_mgy_decode {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my $nn = 2*$n;
    my $limbs = join(", ", (map { "x[$_]" } (0..$n-1)));
    my @subs = (code_for__fixmp_mul(@_), code_for__fixmp_mod(@_));
    # with redc we would do: 
    # mp_limb_t t[$nn] = { $limbs };
    # @!${P}_redc(z, t, p);
    my $code = <<EOF;
         mp_limb_t t[$nn];
         @!${P}_mul(t, x, invR);
         @!${P}_mod(z, t, p);
EOF
    $code = simple_indent($code);
    return {
        code => $code,
        kind => 'inline(z, x, invR, p)',
        name => "${P}_mgy_decode",
        #requirements => "mp_limb_t* const-mp_limb_t* const-mp_limb_t* const-mp_limb_t*",
    }, @subs;
}

# }}}

1;
