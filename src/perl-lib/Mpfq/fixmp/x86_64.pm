package Mpfq::fixmp::x86_64;

use strict;
use warnings;
use Carp;
use Data::Dumper;

# We're only using a utility function from there.
use Mpfq::fixmp::longlong qw/arglist_unbox_n alter_hash/;
use Mpfq::engine::maketext qw/simple_indent/;

# For many functions, we have the choice between giving the limbs of x
# and z as assembly constraints, or through a single pointer. Reading
# previously existing code, the former reportedly has the edge for small
# sizes. This is implemented as a tunable option for addmul

# {{{ addmul1
# mp_limb_t addmul1(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n+1 (or n if shortz). Put (z+x*c) in z. Return carry bit.
sub code_for__fixmp_addmul1_1 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my ($code, $asm, @outputs, @inputs, @clobbers);
    $code = '';
    $asm = <<EOF;
        mulq    %[mult]
        addq    %%rax, %[z0]
        adcq    \$0, %%rdx
EOF
    # Force x[0] to be in %rax on input. Since rax will also be touched
    # by the asm, we'll need to make sure that it appears as well as an
    # output operand with a matching constraint.
    push @inputs, "[x0]\"0\"(x[0])";    # -- must match 0 !! ---
    push @outputs, map { "[z$_]\"+m\"(z[$_])" } (0..1);
    push @inputs, "[mult]\"m\"(c)";
    if ($opt->{'short_z'}) {
        # mind the earlyclobber for the carry !
        unshift @outputs, "\"=a\"(junk)"; $code .= "mp_limb_t junk;\n";
        # discard z[1]
        pop @outputs;
        push @outputs, "\"=&d\"(carry)";
        # push @clobbers, map { "\"$_\"" } (qw/%rax/);
    } elsif ($opt->{'discard_carry'}) {
        unshift @outputs, "\"=a\"(junk)"; $code .= "mp_limb_t junk;\n";
        push @clobbers, map { "\"$_\"" } (qw/%rdx/);
        $asm .= <<EOF;
            addq    %%rdx, %[z1]
EOF
    } else {
        # Because %rax is marked as input for x0, there is no
        # earlyclobber needed for the carry -- that would not make sense.
        unshift @outputs, "\"=a\"(carry)";
        push @clobbers, map { "\"$_\"" } (qw/%rdx/);
        $asm .= <<EOF;
            xorq    %%rax, %%rax
            addq    %%rdx, %[z1]
            adcq    \$0, %%rax
EOF
    }
    $asm =~ s/^\s*(.*)$/\t"$1\\n"/gm;
    $code .= "__asm__ __volatile__(\n"
        . $asm
        . ': ' . join(", ", @outputs) . "\n"
        . ': ' . join(", ", @inputs) . "\n"
        . ': ' . join(", ", @clobbers) . ");\n";
    # gcc-4.2.1 doesn't seem to accept asm declarations ending with
    # ": )" in case there are no clobbers. The colon must be omitted
    # as well.
    $code =~ s/: (\);)$/$1/;
    my $rtype = "";
    if (!$opt->{'discard_carry'}) {
        $code = "mp_limb_t carry;\n" . $code . "return carry;";
        $rtype = "mp_limb_t <- ";
    }
    return {
        kind => 'inline(z, x, c)',
        #requirements => $rtype . 'mp_limb_t* const-mp_limb_t* mp_limb_t',
        name => "${P}_addmul1",
        code => $code,
    };
}

# mp_limb_t addmul05(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. c is 0.5 word. Put (z+x*c) in z. Return carry bit.
sub code_for__fixmp_addmul05_1 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my ($code, $asm, @outputs, @inputs, @clobbers);
    $code = '';
    $asm = "imulq    %[mult], %[x0]\n";
    push @outputs, "[z0]\"+m\"(z[0])";
    push @inputs, "[x0]\"0\"(x[0])";    # -- must match 0 !! ---
    push @inputs, "[mult]\"m\"(c)";
    unshift @outputs, "\"=a\"(junk)"; $code .= "mp_limb_t junk;\n";
    if ($opt->{'discard_carry'}) {
        $asm .= "addq    %[x0], %[z0]\n";
    } else {
        push @outputs, "\"=&d\"(carry)";
        $asm .= <<EOF;
            xorq    %%rdx, %%rdx
            addq    %%rax, %[z0]
            adcq    \$0, %%rdx
EOF
    }
    $asm =~ s/^\s*(.*)$/\t"$1\\n"/gm;
    $code .= "__asm__ __volatile__(\n"
        . $asm
        . ': ' . join(", ", @outputs) . "\n"
        . ': ' . join(", ", @inputs) . "\n"
        . ': ' . join(", ", @clobbers) . ");\n";
    # gcc-4.2.1 doesn't seem to accept asm declarations ending with
    # ": )" in case there are no clobbers. The colon must be omitted
    # as well.
    $code =~ s/: (\);)$/$1/;
    my $rtype = "";
    if (!$opt->{'discard_carry'}) {
        $code = "mp_limb_t carry;\n" . $code . "return carry;";
        $rtype = "mp_limb_t <- ";
    }
    return {
        kind => 'inline(z, x, c)',
        #requirements => $rtype . 'mp_limb_t* const-mp_limb_t* mp_limb_t',
        name => "${P}_addmul05",
        code => $code,
    };
}

# mp_limb_t addmul1(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n+1. Put (z+x*c) in z. Return carry bit.
sub code_for__fixmp_addmul1 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    if ($n == 1) {
        if (!$opt->{'small_c'}) {
            return code_for__fixmp_addmul1_1(@_);
        } else {
            return code_for__fixmp_addmul05_1(@_);
        }
    }
    my $direct = $opt->{'direct'};
    if (!defined($direct)) { $direct = $n < 5; }

    my $code = "";

    if (!$direct) {
        $code .= <<EOF;
            movq    %[z], %%rdi
            movq    %[x], %%rsi
EOF
    }
    my ($ix, $iz) = (0,0);

    my $gxi = sub { return $direct ? "%[x$ix]" : ($ix*8) . "(%%rsi)"; };
    my $gzi = sub { return $direct ? "%[z$iz]" : ($iz*8) . "(%%rdi)"; };

    my $xi = &$gxi();
    my $zi = &$gzi();

    $code .= "movq    $xi, %%rax\n";
    if ($n > 1) {
        # This is the *normal* startup code. Only the degenerate case
        # n==1 proceeds differently, in that it does not even enter the
        # main loop.
        $ix++;
        my $xi1 = &$gxi();
        $code .= <<EOF;
            mulq    %[mult]
            addq    %%rax, $zi
            movq    $xi1, %%rax
            adcq    \$0, %%rdx
            movq    %%rdx, %%rcx
EOF
    }

    for(my $i = 2 ; $i < $n ; $i++) {
        $ix++; $xi = &$gxi();
        $iz++; $zi = &$gzi();
        $code .= <<EOF;
            mulq    %[mult]
            addq    %%rax, %%rcx
            adcq    \$0, %%rdx
            movq    $xi, %%rax
            addq    %%rcx, $zi
            adcq    \$0, %%rdx
            movq    %%rdx, %%rcx
EOF
    }

    if ($n > 1) {
        $iz++; $zi = &$gzi();
    } elsif (!$opt->{'small_c'}) {
        $code .= "xorq    %%rcx, %%rcx\n";
    }

    if (!$opt->{'small_c'}) {
        $code .= <<EOF;
            mulq    %[mult]
            addq    %%rax, %%rcx
            adcq    \$0, %%rdx
            addq    %%rcx, $zi
            adcq    \$0, %%rdx
EOF
        if ($opt->{'short_z'}) {
            $code .= "movq    %%rdx, %[carry]\n";
        } else {
            $iz++; $zi = &$gzi();
            if (!$opt->{'discard_carry'}) {
                $code .= <<EOF;
                    xorq    %%rcx, %%rcx
                    addq    %%rdx, $zi
                    adcq    \$0, %%rcx
                    movq    %%rcx, %[carry]
EOF
            } else {
                $code .= "addq    %%rdx, $zi\n";
            }
        }
    } else {
        die "unexpected" unless $opthw;
        # small_c implies hw, and the combination implies short_z
        die "unexpected" if $opt->{'short_z'};
        if (!$opt->{'discard_carry'}) {
            $code .= <<EOF;
                imulq    %[mult], %%rax
                xorq    %%rdx, %%rdx
EOF
            if ($n > 1) {
                $code .= <<EOF;
                    addq    %%rcx, %%rax
                    adcq    \$0, %%rdx
EOF
            }
            $code .= <<EOF;
                addq    %%rax, $zi
                adcq    \$0, %%rdx
                movq    %%rdx, %[carry]
EOF
        } else {
            $code .= "imulq    %[mult], %%rax\n";
            $code .= "addq    %%rcx, %%rax\n" if $n > 1;
            $code .= "addq    %%rax, $zi\n";
        }
    }

    $code =~ s/^\s*(.*)$/\t"$1\\n"/gm;

    # Write down asm constraints.
    my (@inputs, @outputs, @clobbers);
    @inputs = ('[mult] "r" (c)');
    @clobbers = (map { "\"$_\"" } (qw/%rax %rcx %rdx/));
    if (!$opt->{'discard_carry'}) {
        push @outputs, "[carry]\"=g\"(carry)";
    }
    if ($direct) {
        push @outputs, map { "[z$_]\"+m\"(z[$_])" } (0..$n);
        pop @outputs if $opt->{'small_c'};
        push @inputs, map { "[x$_]\"m\"(x[$_])" } (0..$n-1);
    } else {
        push @outputs, '[z] "+m" (z)';
        push @inputs, '[x] "m" (x)';
        push @clobbers, map { "\"$_\"" } (qw/%rsi %rdi memory/);
    }
    $code = "__asm__ __volatile__(\n"
                     . $code
                     . ': ' . join(", ", @outputs) . "\n"
                     . ': ' . join(", ", @inputs) . "\n"
                     . ': ' . join(", ", @clobbers) . ");\n";
    my $rtype='';

    if (!$opt->{'discard_carry'}) {
        $code = "mp_limb_t carry;\n" . $code . "return carry;";
        $rtype = "mp_limb_t <- ";
    }


    return {
        kind => 'inline(z, x, c)',
        #requirements => $rtype . 'mp_limb_t* const-mp_limb_t* mp_limb_t',
        name => "${P}_addmul1",
        code => $code,
    };
}

# mp_limb_t addmul05(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. c is 0.5 word. Put (z+x*c) in z. Return carry bit.
sub code_for__fixmp_addmul05 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    confess "only for half-word code" unless $opthw;
    shift @_;
    my $x = code_for__fixmp_addmul1(alter_hash($opt, small_c => 1), @_);
    return alter_hash($x, name => "${P}_addmul05");
}

# void addmul1_nc(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n+1. Put (z+x*c) in z. Carry bit is lost.
sub code_for__fixmp_addmul1_nc {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my $x = code_for__fixmp_addmul1(alter_hash($opt, discard_carry => 1), @_);
    return alter_hash($x, name => "${P}_addmul1_nc");
}

# void addmul05_nc(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. c is 0.5 word. Put (z+x*c) in z. Carry lost.
sub code_for__fixmp_addmul05_nc {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    confess "only for half-word code" unless $opthw;
    shift @_;
    my $x = code_for__fixmp_addmul1(alter_hash($opt, small_c => 1, discard_carry => 1), @_);
    return alter_hash($x, name => "${P}_addmul05_nc");
}

# mp_limb_t addmul1_shortz(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. Put (z+x*c) in z. Return carry word.
sub code_for__fixmp_addmul1_shortz {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my $x = code_for__fixmp_addmul1(alter_hash($opt, short_z => 1), @_);
    return alter_hash($x, name => "${P}_addmul1_shortz");
}

# }}}

# {{{ add sub add_nc sub_nc
# mp_limb_t add(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#     x, y, and z have nd words. Result to z. Return carry bit
sub code_for__fixmp_add {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my ($code, $asm, @outputs, @inputs, @clobbers);
    my ($xi, $yi, $zi);
    my $direct = $opt->{'direct'};
    if (!defined($direct)) { $direct = $n < 5; }
    my ($infix, $op, $opc);
    if ($opt->{'subtract'}) {
        ($infix, $op, $opc) = qw/- sub sbb/;
    } else {
        ($infix, $op, $opc) = qw/+ add adc/;
    }
    if ($n == 1 && $opt->{'discard_carry'}) {
        $code = "*z = *x $infix *y;\n";
    } else {
        my ($ix, $iy, $iz) = (0, 0, 0);
        my $gxi = sub { return $direct ? "%[x$ix]" : ($ix*8) . "(%%rsi)"; };
        my $gyi = sub { return $direct ? "%[y$iy]" : ($iy*8) . "(%%rdx)"; };
        my $gzi = sub { return $direct ? "%[z$iz]" : ($iz*8) . "(%%rdi)"; };
        my $xi = &$gxi();
        my $yi = &$gyi();
        my $zi = &$gzi();
        $asm = "";
        if (!$direct) {
            $asm .= <<EOF;
                movq    %[z], %%rdi
                movq    %[x], %%rsi
                movq    %[y], %%rdx
EOF
            push @inputs, "[z]\"m\"(z)";
            push @inputs, "[x]\"m\"(x)", "[y]\"m\"(y)";
            push @clobbers, map { "\"$_\"" } (qw/%rdx %rsi %rdi memory/);
        } else {
            push @outputs, map { "[z$_]\"=m\"(z[$_])" } (0..$n-1);
            push @inputs, map { "[x$_]\"m\"(x[$_])" } (0..$n-1);
            push @inputs, map { "[y$_]\"m\"(y[$_])" } (0..$n-1);
        }
        if ($opt->{'discard_carry'}) {
            push @clobbers, map { "\"$_\"" } (qw/%rax/);
        } else {
            push @outputs, "\"=&a\"(carry)";
        }
        for(my $i = 0 ; $i < $n ; $i++) {
            $asm .= <<EOF;
                movq    $xi, %%rax
                ${opc}q    $yi, %%rax
                movq    %%rax, $zi
EOF
            $asm =~ s/$opc/$op/ if $i == 0;
            $ix++; $xi = &$gxi();
            $iy++; $yi = &$gyi();
            $iz++; $zi = &$gzi();
        }
        if (!$opt->{'discard_carry'}) {
            $asm .= "movq \$0, %%rax\n";
            $asm .= "adcq \$0, %%rax\n";
        }
    }

    if ($asm) {
        $asm =~ s/^\s*(.*)$/\t"$1\\n"/gm;
        $code = "__asm__ __volatile__(\n"
                         . $asm
                         . ': ' . join(", ", @outputs) . "\n"
                         . ': ' . join(", ", @inputs) . "\n"
                         . ': ' . join(", ", @clobbers) . ");\n";
        $code =~ s/: (\);)$/$1/;
        if (!$opt->{'discard_carry'}) {
            $code = "mp_limb_t carry;\n" . $code . "return carry;";
        }
        # gcc-4.2.1 doesn't seem to accept asm declarations ending with
        # ": )" in case there are no clobbers. The colon must be omitted
        # as well.
    }
    my $rtype = "";
    if (!$opt->{'discard_carry'}) {
        $rtype = "mp_limb_t <- " unless $opt->{'discard_carry'};
    }
    return {
        kind => 'inline(z, x, y)',
        #requirements => $rtype . 'mp_limb_t* const-mp_limb_t* const-mp_limb_t*',
        name => "${P}_$op",
        code => $code,
    };
}

# mp_limb_t sub(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#     x, y, and z have nd words. Result to z. Return borrow bit
sub code_for__fixmp_sub {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    return code_for__fixmp_add(alter_hash($opt, subtract => 1), @_);
}

# void add_nc(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#     x, y, and z have nd words. Result to z.
sub code_for__fixmp_add_nc {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my $x = code_for__fixmp_add(alter_hash($opt, discard_carry => 1), @_);
    return alter_hash($x, name => "${P}_add_nc");
}

# void sub_nc(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#     x, y, and z have nd words. Result to z.
sub code_for__fixmp_sub_nc {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    shift @_;
    my $x = code_for__fixmp_sub(alter_hash($opt, discard_carry => 1), @_);
    return alter_hash($x, name => "${P}_sub_nc");
}
# }}}

# {{{ mul
# void mul(mp_limb_t * z, const mp_limb_t * x, const mp_limb_t * y)
#    x and y have $nd words, z has 2*n-hw. Put x*y in z.
sub code_for__fixmp_mul {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);

    if ($n == 1) {
        my $code;
        if (!$opthw) {
            $code = <<EOF;
                mulq %[y0]
EOF
            $code =~ s/^\s*(.*)$/\t"$1\\n"/gm;
            $code = "__asm__ __volatile__(\n" . $code . <<EOF;
          : "=a" (z[0]), "=d" (z[1])
          : "0" (x[0]), [y0] "rm1" (y[0])
          );
EOF
        } else {
            $code = <<EOF;
                imulq %[y0], %%rax
EOF
            $code =~ s/^\s*(.*)$/\t"$1\\n"/gm;
            $code = "__asm__ __volatile__(\n" . $code . <<EOF;
          : "=a" (z[0])
          : "0" (x[0]), [y0] "rm" (y[0])
          );
EOF
        }
        return {
            kind => 'inline(z, x, y)',
            #requirements => 'mp_limb_t* const-mp_limb_t* const-mp_limb_t*',
            name => "${P}_mul",
            code => $code,
        };
    };

    # x : rsi
    # y : r8
    # z : rdi
    
    my $ix = 0;
    my $iy = 0;
    my $iz = 0;

    my $code = <<EOF;
    ### x*y[0]
    movq    %2, %%r8
    movq    %0, %%rdi
    movq    $iy(%%r8), %%r9
    movq    %1, %%rsi
    movq    $ix(%%rsi), %%rax
EOF
    $ix += 8;
    $code .= <<EOF;
    mulq    %%r9
    movq    %%rax, $iz(%%rdi)
    movq    $ix(%%rsi), %%rax
    movq    %%rdx, %%rcx
EOF
    for (my $i=1; $i<$n-1; $i++) {
        $ix += 8;
        $iz += 8;
        $code .= <<EOF;
        mulq    %%r9
        addq    %%rax, %%rcx
        adcq    \$0, %%rdx
        movq    $ix(%%rsi), %%rax
        movq    %%rcx, $iz(%%rdi)
        movq    %%rdx, %%rcx
EOF
    }
    $iz += 8;   # 8*($n-1)
    $code .= <<EOF;
    mulq    %%r9
    addq    %%rax, %%rcx
    adcq    \$0, %%rdx
    movq    %%rcx, $iz(%%rdi)
EOF
    $iz += 8;   # 8 * $n
    $code .= <<EOF;
    movq    %%rdx, $iz(%%rdi)
EOF

    my $nn = 2*$n;
    my $nn1 = $nn;
    $nn1-- if $opthw;

    for (my $i=$n+1; $i < $nn1; $i++) {
        $iz += 8;
        $code .= <<EOF;
        movq    \$0, $iz(%%rdi)
EOF
    }

    for (my $j=1; $j < $n; $j++) {
        $iy = 8*$j;
        $iz = 8*$j;
        $ix = 0;
        $code .= <<EOF;
        ### x*y[$j]
        movq    $iy(%%r8), %%r9
        movq    $ix(%%rsi), %%rax
        mulq    %%r9
EOF
        $ix += 8;
        $code .= <<EOF;
        addq    %%rax, $iz(%%rdi)
        movq    $ix(%%rsi), %%rax
        adcq    \$0, %%rdx
        movq    %%rdx, %%rcx
EOF

        for (my $i=1; $i<$n-1; $i++) {
            $ix += 8;
            $iz += 8;
            $code .= <<EOF;
            mulq    %%r9
            addq    %%rax, %%rcx
            adcq    \$0, %%rdx
            movq    $ix(%%rsi), %%rax
            addq    %%rcx, $iz(%%rdi)
            adcq    \$0, %%rdx
            movq    %%rdx, %%rcx
EOF

        }
        $iz += 8;
        $code .= <<EOF;
        mulq    %%r9
        addq    %%rax, %%rcx
        adcq    \$0, %%rdx
        addq    %%rcx, $iz(%%rdi)
EOF
        if ($j < $n-1 || !$opthw) {
            $iz += 8;
            $code .= <<EOF;
            adcq    \$0, %%rdx
            movq    %%rdx, $iz(%%rdi)
EOF
        }
    }
    $code =~ s/^\s*(.*)$/\t"$1\\n"/gm;
    $code = "__asm__ __volatile__(\n" . $code . <<EOF;
  : "+m" (z)
  : "m" (x), "m" (y)
  : "%rax", "%rcx", "%rdx", "%rsi", "%rdi", "%r8", "%r9", "memory");
EOF
    return {
        kind => 'inline(z, x, y)',
        #requirements => 'mp_limb_t* const-mp_limb_t* const-mp_limb_t*',
        name => "${P}_mul",
        code => $code,
    };
}
# }}}

# {{{ sqr
# void sqr(mp_limb_t * z, const mp_limb_t * x)
#    x has $nd words, z has 2*n-hw. Put x*y in z.
sub code_for__fixmp_sqr {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my $code;
    my $asm;
    my (@inputs, @outputs, @clobbers);

    if ($P eq '0_5') {
        $asm = "imulq %%rax, %%rax\n";
        @outputs = '"=a" (z[0])';
        @inputs = '"0" (x[0])';
        # @clobbers = '"cc"';       # really ?
    } elsif ($P eq '1') {
        $asm = "mulq %%rax\n";
        @outputs = ('"=a" (z[0])', '"=d" (z[1])');
        @inputs = '"0" (x[0])';
        # @clobbers = '"cc"';       # really ?
    } elsif ($P eq '1_5') { # {{{
        $asm = <<EOF;
            movq    %1, %%r8
            movq    %0, %%rdi
            movq    (%%r8), %%rax
            mulq    %%rax
            movq    %%rax, (%%rdi)
            movq    8(%%r8), %%rax
            movq    %%rdx, %%r9
            mulq    %%rax
            movq    %%rax, %%r10
            movq    (%%r8), %%rax
            mulq    8(%%r8)
            addq    %%rax, %%r9
            adcq    %%rdx, %%r10
            addq    %%rax, %%r9
            movq    %%r9, 8(%%rdi)
            adcq    %%rdx, %%r10
            movq    %%r10, 16(%%rdi)
EOF
        @outputs = '"+m" (z)';
        @inputs =  '"m" (x)';
        @clobbers = map { "\"$_\"" } qw/%rax %rdx %rdi %r8 %r9 %r10 %r11 memory/;
        # }}}
    } elsif ($P eq '2') { # {{{
        $asm = <<EOF;
            movq    %1, %%r8
            movq    %0, %%rdi
            movq    (%%r8), %%rax
            mulq    %%rax
            movq    %%rax, (%%rdi)
            movq    8(%%r8), %%rax
            movq    %%rdx, %%r9
            mulq    %%rax
            movq    %%rax, %%r10
            movq    (%%r8), %%rax
            movq    %%rdx, %%r11
            mulq    8(%%r8)
            addq    %%rax, %%r9
            adcq    %%rdx, %%r10
            adcq    \$0, %%r11
            addq    %%rax, %%r9
            movq    %%r9, 8(%%rdi)
            adcq    %%rdx, %%r10
            movq    %%r10, 16(%%rdi)
            adcq    \$0, %%r11
            movq    %%r11, 24(%%rdi)
EOF
        @outputs = '"+m" (z)';
        @inputs =  '"m" (x)';
        @clobbers = map { "\"$_\"" } qw/%rax %rdx %rdi %r8 %r9 %r10 %r11 memory/;
        # }}}
    } elsif ($P eq '2_5') { # {{{
        $asm = <<EOF;
            movq    %1, %%rsi
            movq    %0, %%rdi
            ### diagonal elements
            movq    (%%rsi), %%rax
            mulq    %%rax
            movq    %%rax, (%%rdi)
            movq    8(%%rsi), %%rax
            movq    %%rdx, 8(%%rdi)
            mulq    %%rax
            movq    %%rax, 16(%%rdi)
            movq    16(%%rsi), %%rax
            movq    %%rdx, 24(%%rdi)
            mulq    %%rax
            movq    %%rax, 32(%%rdi)
            ### precompute triangle
            ### x[0]*x[1,2]
            movq    (%%rsi), %%rcx
            movq    8(%%rsi), %%rax
            mulq    %%rcx
            movq    %%rax, %%r8
            movq    %%rdx, %%r9
            movq    16(%%rsi), %%rax
            mulq    %%rcx
            addq    %%rax, %%r9
            adcq    \$0, %%rdx
            movq    %%rdx, %%r10
            ### x[1]*x[2]
            movq    8(%%rsi), %%rax
            mulq    16(%%rsi)
            addq    %%rax, %%r10
            adcq    \$0, %%rdx
            ### Shift triangle
            addq    %%r8, %%r8
            adcq    %%r9, %%r9
            adcq    %%r10, %%r10
            adcq    %%rdx, %%rdx
            ### add shifted triangle to diagonal
            addq    %%r8, 8(%%rdi)
            adcq    %%r9, 16(%%rdi)
            adcq    %%r10, 24(%%rdi)
            adcq    %%rdx, 32(%%rdi)
EOF
        @outputs = '"+m" (z)';
        @inputs =  '"m" (x)';
        @clobbers = map { "\"$_\"" } qw/%rax %rcx %rdx %rsi %rdi %r8 %r9 %r10 memory/;
        # }}}
    } elsif ($P eq '3') { # {{{
        $asm = <<EOF;
            movq    %1, %%rsi
            movq    %0, %%rdi
            ### diagonal elements
            movq    (%%rsi), %%rax
            mulq    %%rax
            movq    %%rax, (%%rdi)
            movq    8(%%rsi), %%rax
            movq    %%rdx, 8(%%rdi)
            mulq    %%rax
            movq    %%rax, 16(%%rdi)
            movq    16(%%rsi), %%rax
            movq    %%rdx, 24(%%rdi)
            mulq    %%rax
            movq    %%rax, 32(%%rdi)
            movq    %%rdx, 40(%%rdi)
            ### precompute triangle
            ### x[0]*x[1,2]
            movq    (%%rsi), %%rcx
            movq    8(%%rsi), %%rax
            mulq    %%rcx
            movq    %%rax, %%r8
            movq    %%rdx, %%r9
            movq    16(%%rsi), %%rax
            mulq    %%rcx
            addq    %%rax, %%r9
            adcq    \$0, %%rdx
            movq    %%rdx, %%r10
            ### x[1]*x[2]
            movq    8(%%rsi), %%rax
            mulq    16(%%rsi)
            addq    %%rax, %%r10
            adcq    \$0, %%rdx
            ### Shift triangle
            addq    %%r8, %%r8
            adcq    %%r9, %%r9
            adcq    %%r10, %%r10
            adcq    %%rdx, %%rdx
            adcq    \$0, 40(%%rdi)
            ### add shifted triangle to diagonal
            addq    %%r8, 8(%%rdi)
            adcq    %%r9, 16(%%rdi)
            adcq    %%r10, 24(%%rdi)
            adcq    %%rdx, 32(%%rdi)
            adcq    \$0, 40(%%rdi)
EOF
        @outputs = '"+m" (z)';
        @inputs =  '"m" (x)';
        @clobbers = map { "\"$_\"" } qw/%rax %rcx %rdx %rsi %rdi %r8 %r9 %r10 memory/;
        # }}}
    } elsif ($P eq '3_5') { # {{{
        $asm = <<EOF;
            movq    %1, %%rsi
            movq    %0, %%rdi
            ### diagonal elements
            movq    (%%rsi), %%rax
            mulq    %%rax
            movq    %%rax, (%%rdi)
            movq    8(%%rsi), %%rax
            movq    %%rdx, 8(%%rdi)
            mulq    %%rax
            movq    %%rax, 16(%%rdi)
            movq    16(%%rsi), %%rax
            movq    %%rdx, 24(%%rdi)
            mulq    %%rax
            movq    %%rax, 32(%%rdi)
            movq    24(%%rsi), %%rax
            movq    %%rdx, 40(%%rdi)
            mulq    %%rax
            movq    %%rax, 48(%%rdi)
            ### precompute triangle
            ### x[0]*x[1:3]
            movq    (%%rsi), %%rcx
            movq    8(%%rsi), %%rax
            mulq    %%rcx
            movq    %%rax, %%r8
            movq    %%rdx, %%r9
            movq    16(%%rsi), %%rax
            mulq    %%rcx
            addq    %%rax, %%r9
            adcq    \$0, %%rdx
            movq    %%rdx, %%r10
            movq    24(%%rsi), %%rax
            mulq    %%rcx
            addq    %%rax, %%r10
            adcq    \$0, %%rdx
            movq    %%rdx, %%r11
            ### x[1]*x[2:3]
            movq    8(%%rsi), %%rcx
            movq    16(%%rsi), %%rax
            xorq    %%r12, %%r12
            mulq    %%rcx
            addq    %%rax, %%r10
            adcq    %%rdx, %%r11
            adcq    \$0, %%r12
            movq    24(%%rsi), %%rax
            mulq    %%rcx
            addq    %%rax, %%r11
            adcq    \$0, %%rdx
            addq    %%rdx, %%r12
            ### x[2]*x[3]
            movq    16(%%rsi), %%rax
            mulq    24(%%rsi)
            addq    %%rax, %%r12
            adcq    \$0, %%rdx
            ### Shift triangle
            addq    %%r8, %%r8
            adcq    %%r9, %%r9
            adcq    %%r10, %%r10
            adcq    %%r11, %%r11
            adcq    %%r12, %%r12
            adcq    %%rdx, %%rdx
            ### add shifted triangle to diagonal
            addq    %%r8, 8(%%rdi)
            adcq    %%r9, 16(%%rdi)
            adcq    %%r10, 24(%%rdi)
            adcq    %%r11, 32(%%rdi)
            adcq    %%r12, 40(%%rdi)
            adcq    %%rdx, 48(%%rdi)
EOF
        @outputs = '"+m" (z)';
        @inputs =  '"m" (x)';
        @clobbers = map { "\"$_\"" } qw/%rax %rcx %rdx %rsi %rdi %r8 %r9 %r10 %r11 %r12 memory/;
        # }}}
    } elsif ($P eq '4') { # {{{
        $asm = <<EOF;
            movq	%1, %%rsi
            movq	%0, %%rdi
            ### diagonal elements
            movq    (%%rsi), %%rax
            mulq	%%rax
            movq    %%rax, (%%rdi)
            movq    8(%%rsi), %%rax
            movq    %%rdx, 8(%%rdi)
            mulq	%%rax
            movq    %%rax, 16(%%rdi)
            movq	16(%%rsi), %%rax
            movq    %%rdx, 24(%%rdi)
            mulq    %%rax
            movq    %%rax, 32(%%rdi)
            movq	24(%%rsi), %%rax
            movq    %%rdx, 40(%%rdi)
            mulq    %%rax
            movq    %%rax, 48(%%rdi)
            movq    %%rdx, 56(%%rdi)
            ### precompute triangle
            ### x[0]*x[1:3]
            movq	(%%rsi), %%rcx
            movq	8(%%rsi), %%rax
            mulq	%%rcx
            movq	%%rax, %%r8
            movq	%%rdx, %%r9
            movq    16(%%rsi), %%rax
            mulq	%%rcx
            addq	%%rax, %%r9
            adcq	\$0, %%rdx
            movq	%%rdx, %%r10
            movq    24(%%rsi), %%rax
            mulq	%%rcx
            addq	%%rax, %%r10
            adcq	\$0, %%rdx
            movq	%%rdx, %%r11
            ### x[1]*x[2:3]
            movq	8(%%rsi), %%rcx
            movq	16(%%rsi), %%rax
            xorq	%%r12, %%r12
            mulq	%%rcx
            addq	%%rax, %%r10
            adcq	%%rdx, %%r11
            adcq	\$0, %%r12
            movq	24(%%rsi), %%rax
            mulq	%%rcx
            addq    %%rax, %%r11
            adcq	\$0, %%rdx
            addq    %%rdx, %%r12
            ### x[2]*x[3]
            movq	16(%%rsi), %%rax
            mulq	24(%%rsi)
            addq	%%rax, %%r12
            adcq	\$0, %%rdx
            ### Shift triangle
            addq	%%r8, %%r8
            adcq	%%r9, %%r9
            adcq	%%r10, %%r10
            adcq	%%r11, %%r11
            adcq	%%r12, %%r12
            adcq	%%rdx, %%rdx
            adcq	\$0, 56(%%rdi)
            ### add shifted triangle to diagonal
            addq	%%r8, 8(%%rdi)
            adcq	%%r9, 16(%%rdi)
            adcq	%%r10, 24(%%rdi)
            adcq	%%r11, 32(%%rdi)
            adcq	%%r12, 40(%%rdi)
            adcq	%%rdx, 48(%%rdi)
            adcq	\$0, 56(%%rdi)
EOF
        @outputs = '"+m" (z)';
        @inputs =  '"m" (x)';
        @clobbers = map { "\"$_\"" } qw/%rax %rcx %rdx %rsi %rdi %r8 %r9 %r10 %r11 %r12 memory/;
        # }}}
    } else {
        $code = "@!${P}_mul(z, x, x);\n";
    }

    if ($asm) {
        $asm =~ s/^\s*(.*)$/\t"$1\\n"/gm;
        $code = "__asm__ __volatile__(\n"
                         . $asm
                         . ': ' . join(", ", @outputs) . "\n"
                         . ': ' . join(", ", @inputs) . "\n"
                         . ': ' . join(", ", @clobbers) . ");\n";
        # gcc-4.2.1 doesn't seem to accept asm declarations ending with
        # ": )" in case there are no clobbers. The colon must be omitted
        # as well.
        $code =~ s/: (\);)$/$1/;
    }
    return {
        kind => 'inline(z, x)',
        #requirements => 'mp_limb_t* const-mp_limb_t*',
        name => "${P}_sqr",
        code => $code,
    };
}
# }}}

# {{{ mul1
# void mul1(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n + 1. c is 1 word. Put x*c in z. No carry.
sub code_for__fixmp_mul1_1 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my ($code, $asm, @outputs, @inputs, @clobbers);
    $asm = "mulq    %[mult]\n";
    push @outputs, "[z0]\"=a\"(z[0])", "[z1]\"=d\"(z[1])";
    push @inputs, map { "[x$_]\"0\"(x[$_])" } (0);
    push @inputs, "[mult]\"m\"(c)";
    $asm =~ s/^\s*(.*)$/\t"$1\\n"/gm;
    $code = "__asm__ __volatile__(\n"
        . $asm
        . ': ' . join(", ", @outputs) . "\n"
        . ': ' . join(", ", @inputs) . "\n"
        . ': ' . join(", ", @clobbers) . ");\n";
    # gcc-4.2.1 doesn't seem to accept asm declarations ending with
    # ": )" in case there are no clobbers. The colon must be omitted
    # as well.
    $code =~ s/: (\);)$/$1/;
    return {
        kind => 'inline(z, x, c)',
        #requirements => 'mp_limb_t* const-mp_limb_t* mp_limb_t',
        name => "${P}_mul1",
        code => $code,
    };
}

# void mul05(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. c is 0.5 word. Put x*c in z. No carry.
sub code_for__fixmp_mul05_1 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my ($code, $asm, @outputs, @inputs, @clobbers);
    $asm = "imulq    %[mult], %%rax\n";
    push @outputs, "[z0]\"=a\"(z[0])";
    push @inputs, "[x0]\"0\"(x[0])";
    push @inputs, "[mult]\"m\"(c)";
    $asm =~ s/^\s*(.*)$/\t"$1\\n"/gm;
    $code = "__asm__ __volatile__(\n"
        . $asm
        . ': ' . join(", ", @outputs) . "\n"
        . ': ' . join(", ", @inputs) . "\n"
        . ': ' . join(", ", @clobbers) . ");\n";
    # gcc-4.2.1 doesn't seem to accept asm declarations ending with
    # ": )" in case there are no clobbers. The colon must be omitted
    # as well.
    $code =~ s/: (\);)$/$1/;
    return {
        kind => 'inline(z, x, c)',
        #requirements => 'mp_limb_t* const-mp_limb_t* mp_limb_t',
        name => "${P}_mul05",
        code => $code,
    };
}

# void mul1(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n + 1. c is 1 word. Put x*c in z. No carry.
sub code_for__fixmp_mul1 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    if ($n == 1) {
        if (!$opt->{'small_c'}) {
            return code_for__fixmp_mul1_1(@_);
        } else {
            return code_for__fixmp_mul05_1(@_);
        }
    }
    my $direct = $opt->{'direct'};
    if (!defined($direct)) { $direct = $n < 5; }

    my $code = "";

    if (!$direct) {
        $code .= <<EOF;
            movq    %[z], %%rdi
            movq    %[x], %%rsi
EOF
    }
    my ($ix, $iz) = (0,0);

    my $gxi = sub { return $direct ? "%[x$ix]" : ($ix*8) . "(%%rsi)"; };
    my $gzi = sub { return $direct ? "%[z$iz]" : ($iz*8) . "(%%rdi)"; };

    my $xi = &$gxi();
    my $zi = &$gzi();

    $code .= "movq    $xi, %%rax\n";
    if ($n > 1) {
        # This is the *normal* startup code. Only the degenerate case
        # n==1 proceeds differently, in that it does not even enter the
        # main loop.
        $ix++;
        my $xi1 = &$gxi();
        $code .= <<EOF;
            mulq    %[mult]
            movq    %%rax, $zi
            movq    $xi1, %%rax
            movq    %%rdx, %%rcx
EOF
    }

    for(my $i = 2 ; $i < $n ; $i++) {
        $ix++; $xi = &$gxi();
        $iz++; $zi = &$gzi();
        $code .= <<EOF;
            mulq    %[mult]
            addq    %%rax, %%rcx
            adcq    \$0, %%rdx
            movq    $xi, %%rax
            movq    %%rcx, $zi
            movq    %%rdx, %%rcx
EOF
    }

    if ($n > 1) {
        $iz++; $zi = &$gzi();
    } elsif (!$opt->{'small_c'}) {
        $code .= "xorq    %%rcx, %%rcx\n";
    }

    if (!$opt->{'small_c'}) {
        $code .= <<EOF;
            mulq    %[mult]
            addq    %%rax, %%rcx
            adcq    \$0, %%rdx
            movq    %%rcx, $zi
EOF
            $iz++; $zi = &$gzi();
            $code .= "movq    %%rdx, $zi\n";
    } else {
        die "unexpected" unless $opthw;
        # small_c implies hw, and the combination implies short_z
        die "unexpected" if $opt->{'short_z'};
        $code .= "imulq    %[mult], %%rax\n";
        $code .= "addq    %%rcx, %%rax\n" if $n > 1;
        $code .= "movq    %%rax, $zi\n";
    }

    $code =~ s/^\s*(.*)$/\t"$1\\n"/gm;

    # Write down asm constraints.
    my (@inputs, @outputs, @clobbers);
    @inputs = ('[mult] "r" (c)');
    @clobbers = (map { "\"$_\"" } (qw/%rax %rcx %rdx/));
    if ($direct) {
        push @outputs, map { "[z$_]\"=m\"(z[$_])" } (0..$n);
        pop @outputs if $opt->{'small_c'};
        push @inputs, map { "[x$_]\"m\"(x[$_])" } (0..$n-1);
    } else {
        push @inputs, '[z] "m" (z)', '[x] "m" (x)';
        push @clobbers, map { "\"$_\"" } (qw/%rsi %rdi memory/);
    }
    $code = "__asm__ __volatile__(\n"
                     . $code
                     . ': ' . join(", ", @outputs) . "\n"
                     . ': ' . join(", ", @inputs) . "\n"
                     . ': ' . join(", ", @clobbers) . ");\n";
    return {
        kind => 'inline(z, x, c)',
        #requirements => 'mp_limb_t* const-mp_limb_t* mp_limb_t',
        name => "${P}_mul1",
        code => $code,
    };
}

# void mul05(mp_limb_t * z, const mp_limb_t * x, mp_limb_t c)
#    x has nd words, z has n. c is 0.5 word. Put x*c in z. No carry.
sub code_for__fixmp_mul05 {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    confess "only for half-word code" unless $opthw;
    shift @_;
    my $x = code_for__fixmp_mul1(alter_hash($opt, small_c => 1), @_);
    return alter_hash($x, name => "${P}_mul05");
}

# }}}

# TODO: provide mulredc1, maybe generalized.
# This code has been disabled since 2ba54dd ; why ? re-enabling now (post
# 32507a).
sub code_for__fixmp_mulredc {
    my ($opt, $n, $opthw, $P) = arglist_unbox_n(@_);
    my ($code, $asm, @outputs, @inputs, @clobbers);
    return unless $n==1 && !$opthw;
    $asm = <<EOF;
        movq    %[x0], %%rax
        mulq    %[y0]
        movq    %%rdx, %[z]
        imul    %[invp0], %%rax
        mulq    %[p0]
        addq    \$0xFFFFFFFFFFFFFFFF, %%rax // set carry if ax is not 0
        adcq    \$0, %[z]                   // this should not produce any carry
        movq    %[z], %%rax                 // ax = z
        subq    %[p0], %%rax                // ax -= p
        addq    %%rdx, %[z]                 // z += dx
        addq    %%rdx, %%rax                // ax += dx  (ax = z-p+dx)
        cmovc   %%rax, %[z]                 // z c_= ax
EOF
    push @outputs, "[z]\"=&r\"(z)";
    push @inputs, map { "[${_}0]\"rm\"(${_}\[0\])" } qw/x y p invp/;
    push @clobbers, map { "\"$_\"" } (qw/%rax %rdx/);
    $asm =~ s/^\s*(.*?)((?:\s*\/\/.*)?)$/\t"$1\\n"$2/gm;
    $code = "__asm__ __volatile__(\n"
        . $asm
        . ': ' . join(", ", @outputs) . "\n"
        . ': ' . join(", ", @inputs) . "\n"
        . ': ' . join(", ", @clobbers) . ");\n";
    $code = "mp_limb_t z;\n" . $code . "*pz = z;\n";
    $code = simple_indent($code);
    return {
        optional => 1,
        kind => 'inline(pz, x, y, p, invp)',
        #requirements => 'mp_limb_t* const-mp_limb_t* const-mp_limb_t* const-mp_limb_t* const-mp_limb_t*',
        name => "${P}_mulredc",
        code => $code,
    };
}

sub init_handler {
    my $opt = shift @_;
    my $features = $opt->{'features'};
    return "incompatible with this feature set" unless $opt->{'w'} == 64 && $features->{'gcc_inline_assembly'};
    return {};
}

1;

