package Mpfq::fixmp::i386;

use strict;
use warnings;

# This is empty. We might consider reviving code which used to exist (up
# to ded793a in src/fixmp/perl). That did only some of the addmul1's, and
# also add. However, it's probably simpler to copy-paste and adapt based
# on the 64-bit code.

sub init_handler {
    my $opt = shift @_;
    my $features = $opt->{'features'};
    return "incompatible with this feature set" unless $opt->{'w'} == 32 && $features->{'gcc_inline_assembly'};
    return {};
}

1;
