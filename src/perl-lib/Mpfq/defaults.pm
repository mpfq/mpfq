package Mpfq::defaults;

use Carp;
use Data::Dumper;
use strict;
use warnings;
use Exporter qw(import);

our @parents = qw//;

# sub code_for_field_specify { return [ 'macro(k!,dummy!,vp!)' , '' ]; }
# sub code_for_field_init { return [ 'macro(K!)', '' ]; }
# sub code_for_field_clear { return [ 'macro(K!)', '' ]; }
# sub code_for_field_setopt { return [ 'macro(f,x,y)' , '' ]; }
 
sub code_for_fprint {
    my $proto = 'function(k,file,x)';
    my $code = <<EOF;
char *str;
int rc;
@!asprint(k,&str,x);
rc = fprintf(file,"%s",str);
free(str);
return rc;
EOF
    return [ $proto, $code ];
}

sub code_for_cxx_out {
    my $proto = 'function(k,os,x)';
    my $code = <<EOF;
char *str;
@!asprint(k,&str,x);
os << str;
free(str);
return os;
EOF
    return [ $proto, $code ];
}

sub code_for_print {
    return [ 'macro(k,x)', '@!fprint(k,stdout,x)' ];
}

sub code_for_scan {
    return [ 'macro(k,x)', '@!fscan(k,stdin,x)' ];
}

sub code_for_impl_name {
    my $opt = shift;
    confess unless defined($opt->{'tag'});
    return [ 'macro()', qq{"$opt->{'tag'}"} ];
}


sub init_handler {
    # the api includes
    # ptrdiff_t --> stddef.h
    # FILE*     --> stdio.h
    return { includes => [qw/
	    <stddef.h>
	    <stdio.h>
	    "assert.h"/],
	'hpp:includes' => [ qw{
	    <istream>
	    <ostream>
	    } ],
    };
}
1;
