package Mpfq::defaults::vec::mul;

use strict;
use warnings;

# These are separated from the rest because the relevance of having these
# functions is essentially dependent on the availability of a mul()
# operation, and the companions mul_ur() and reduce()

sub code_for_vec_scal_mul {
    my $proto = 'inline(K!,w,u,c,n)';
    my $code = <<EOF;
    unsigned int i;
for(i = 0; i < n; i++) {
    @!src_elt x = @!vec_coeff_ptr_const(K, u, i);
    @!dst_elt y = @!vec_coeff_ptr(K, w, i);
    @!mul(K, y, x, c);
}
EOF
    return [ $proto, $code ];
}

sub code_for_vec_scal_mul_ur {
    my $proto = 'inline(K!,w,u,c,n)';
    my $code = <<EOF;
unsigned int i;
for(i = 0; i < n; i++) {
    @!src_elt x = @!vec_coeff_ptr_const(K, u, i);
    @!dst_elt_ur y = @!vec_ur_coeff_ptr(K, w, i);
    @!mul_ur(K, y, x, c);
}
EOF
    return [ $proto, $code ];
}

sub code_for_vec_reduce {
    my $proto = 'inline(K!,w,u,n)';
    my $code = <<EOF;
unsigned int i;
for(i = 0; i < n; i++) {
    @!dst_elt_ur x = @!vec_ur_coeff_ptr(K, u, i);
    @!dst_elt y = @!vec_coeff_ptr(K, w, i);
    @!reduce(K, y, x);
}
EOF
    return [ $proto, $code ];
}

1;
