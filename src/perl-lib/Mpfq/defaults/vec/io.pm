package Mpfq::defaults::vec::io;

use strict;
use warnings;

sub code_for_vec_asprint {
    my $opt = shift @_;
    my $proto = 'function(K!,pstr,w,n)';
    my $code = <<EOF;
if (n == 0) {
    *pstr = (char *)mpfq_malloc_check(4);
    sprintf(*pstr, "[ ]");
    return strlen(*pstr);
}
int alloc = 100;
int len = 0;
*pstr = (char *)mpfq_malloc_check(alloc);
(*pstr)[len++] = '[';
(*pstr)[len++] = ' ';
unsigned int i;
for(i = 0; i < n; i+=1) {
    if (i) {
        (*pstr)[len++] = ',';
        (*pstr)[len++] = ' ';
    }
    char *tmp;
    @!asprint(K, &tmp, w[i]);
    int ltmp = strlen(tmp);
    if (len+ltmp+4 > alloc) {
        alloc = len+ltmp+100 + alloc / 4;
        *pstr = (char *)realloc(*pstr, alloc);
    }
    strncpy(*pstr+len, tmp, alloc-len);
    len += ltmp;
    free(tmp);
}
(*pstr)[len++] = ' ';
(*pstr)[len++] = ']';
(*pstr)[len] = '\\0';
return len;
EOF
    return [ $proto, $code ];
}

sub code_for_vec_fprint {
    my $opt = shift @_; 
    my $n = $opt->{'n'};
    my $w = $opt->{'w'};
    my $proto = 'function(K!,file,w,n)';
    my $code = <<EOF;
char *str;
int rc;
@!vec_asprint(K,&str,w,n);
rc = fprintf(file,"%s",str);
free(str);
return rc;
EOF
    return [ $proto, $code ];
}

sub code_for_vec_cxx_out {
    my $opt = shift @_; 
    my $n = $opt->{'n'};
    my $w = $opt->{'w'};
    my $proto = 'function(K!,os,w,n)';
    my $code = <<EOF;
char *str;
@!vec_asprint(K,&str,w,n);
os << str;
free(str);
return os;
EOF
    return [ $proto, $code ];
}

sub code_for_vec_print {
    my $proto = 'function(K!,w,n)';
    my $code = <<EOF;
return @!vec_fprint(K,stdout,w,n);
EOF
    return [ $proto, $code ];
}

sub code_for_vec_sscan {
    my $proto = 'function(K!,w,n,str)';
    my $code = <<EOF;
// start with a clean vector
unsigned int nn;
int len = 0;
@!vec_reinit(K, w, *n, 0);
*n = nn = 0;
while (isspace((int)(unsigned char)str[len]))
    len++;
if (str[len] != '[')
    return 0;
len++;
while (isspace((int)(unsigned char)str[len]))
    len++;
if (str[len] == ']') {
    len++;
    return len;
}
unsigned int i = 0;
for (;;) {
    if (nn < i+1) {
        @!vec_reinit(K, w, nn, i+1);
        *n = nn = i+1;
    }
    int ret = @!sscan(K, @!vec_coeff_ptr(K, *w, i), str + len);
    if (!ret) {
        *n = 0; /* invalidate data ! */
        return 0;
    }
    i++;
    len += ret;
    while (isspace((int)(unsigned char)str[len]))
        len++;
    if (str[len] == ']') {
        len++;
        break;
    }
    if (str[len] != ',') {
        *n = 0; /* invalidate data ! */
        return 0;
    }
    len++;
    while (isspace((int)(unsigned char)str[len]))
        len++;
}
return len;
EOF
    return [ $proto, $code ];
}

sub code_for_vec_fscan {
    my $proto = 'function(K!,file,w,n)';
    my $code = <<EOF;
char *tmp;
int c;
int allocated, len=0;
allocated=100;
tmp = (char *)mpfq_malloc_check(allocated);
int nest = 0, mnest = 0;
for(;;) {
    c = fgetc(file);
    if (c==EOF) {
        free(tmp);
        return 0;
    }
    if (c == '[') {
        nest++, mnest++;
    }
    if (len==allocated) {
        allocated = len + 10 + allocated / 4;
        tmp = (char*)realloc(tmp, allocated);
    }
    tmp[len]=c;
    len++;
    if (c == ']') {
        nest--, mnest++;
    }
    if (mnest && nest == 0)
        break;
}
if (len==allocated) {
    allocated+=1;
    tmp = (char*)realloc(tmp, allocated);
}
tmp[len]='\\0';
int ret=@!vec_sscan(K,w,n,tmp);
free(tmp);
return ret;
EOF
    return [ $proto, $code ];
}

sub code_for_vec_cxx_in {
    my $proto = 'function(K!,is,w,n)';
    my $code = <<EOF;
char *tmp;
char c;
int allocated, len=0;
allocated=100;
tmp = (char *)mpfq_malloc_check(allocated);
int nest = 0, mnest = 0;
for(;;) {
    if (!is.get(c)) {
        free(tmp);
        return is;
    }
    if (c == '[') {
        nest++, mnest++;
    }
    if (len==allocated) {
        allocated = len + 10 + allocated / 4;
        tmp = (char*)realloc(tmp, allocated);
    }
    tmp[len]=c;
    len++;
    if (c == ']') {
        nest--, mnest++;
    }
    if (mnest && nest == 0)
        break;
}
if (len==allocated) {
    allocated+=1;
    tmp = (char*)realloc(tmp, allocated);
}
tmp[len]='\\0';
int ret=@!vec_sscan(K,w,n,tmp);
free(tmp);
if (ret != len)
    is.setstate(std::ios::failbit);
return is;
EOF
    return [ $proto, $code ];
}

sub code_for_vec_scan {
    return [ 'macro(K,w,n)', '@!vec_fscan(K,stdin,w,n)' ];
}

1;
