package Mpfq::defaults::vec::generic;

use strict;
use warnings;

##################################################################
# all functions below are written using @!vec_coeff_ptr and
# @!vec_coeff_ptr_const, and should thus be valid in all cases.

sub code_for_vec_setcoeff {
    my $proto = 'inline(K!,w,x,i)';
    my $code = <<EOF;
        @!dst_elt y = @!vec_coeff_ptr(K, w, i);
        @!set(K, y, x);
EOF
    return [ $proto, $code ];
}

sub code_for_vec_setcoeff_ui {
    my $proto = 'inline(K!,w,x,i)';
    my $code = <<EOF;
        @!dst_elt y = @!vec_coeff_ptr(K, w, i);
        @!set_ui(K, y, x);
EOF
    return [ $proto, $code ];
}

sub code_for_vec_getcoeff {
    my $proto = 'inline(K!,x,w,i)';
    my $code = <<EOF;
        @!src_elt y = @!vec_coeff_ptr_const(K, w, i);
        @!set(K, x, y);
EOF
    return [ $proto, $code ];
}

sub code_for_vec_ur_setcoeff {
    my $proto = 'inline(K!,w,x,i)';
    my $code = <<EOF;
        @!dst_elt_ur y = @!vec_ur_coeff_ptr(K, w, i);
        @!elt_ur_set(K, y, x);
EOF
    return [ $proto, $code ];
}

sub code_for_vec_ur_getcoeff {
    my $proto = 'inline(K!,x,w,i)';
    my $code = <<EOF;
        @!src_elt_ur y = @!vec_ur_coeff_ptr_const(K, w, i);
        @!elt_ur_set(K, x, y);
EOF
    return [ $proto, $code ];
}

sub code_for_vec_set {
    my $proto = 'inline(k!,w,u,n)';
    my $code = <<EOF;
        unsigned int i;
        for(i = 0; i < n; ++i) {
            @!src_elt x = @!vec_coeff_ptr_const(k, u, i);
            @!dst_elt y = @!vec_coeff_ptr(k, w, i);
            @!set(k, y, x);
        }
EOF
    return [ $proto, $code ];
}

sub code_for_vec_ur_set {
    my $proto = 'inline(k!,w,u,n)';
    my $code = <<EOF;
        unsigned int i;
        for(i = 0; i < n; ++i) {
            @!src_elt_ur x = @!vec_ur_coeff_ptr_const(k, u, i);
            @!dst_elt_ur y = @!vec_ur_coeff_ptr(k, w, i);
            @!elt_ur_set(k, y, x);
        }
EOF
    return [ $proto, $code ];
}

sub code_for_vec_ur_set_vec {
    my $proto = 'inline(K!,w,u,n)';
    my $code = <<EOF;
        unsigned int i;
        for(i = 0; i < n; ++i) {
            @!src_elt x = @!vec_coeff_ptr_const(K, u, i);
            @!dst_elt_ur y = @!vec_ur_coeff_ptr(K, w, i);
            @!elt_ur_set_elt(K, y, x);
        }
EOF
    return [ $proto, $code ];
}

sub code_for_vec_random {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i) {
        @!dst_elt x = @!vec_coeff_ptr(k, w, i);
	@!random(k, x, state);
    }
EOF
    return [ 'function(k, w, n, state)', $code ];
}

sub code_for_vec_random2 {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i) {
        @!dst_elt x = @!vec_coeff_ptr(k, w, i);
	@!random2(k, x, state);
    }
EOF
    return [ 'function(k, w, n, state)', $code ];
}

sub code_for_vec_cmp {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i) {
        @!src_elt x = @!vec_coeff_ptr_const(k, u, i);
        @!src_elt y = @!vec_coeff_ptr_const(k, v, i);
	int ret = @!cmp(k, x, y);
	if (ret != 0)
	    return ret;
    }
    return 0;
EOF
    return [ 'function(k, u, v, n)', $code ];
}

sub code_for_vec_is_zero {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i) {
        @!src_elt x = @!vec_coeff_ptr_const(k, v, i);
	if (!@!is_zero(k, x))
	    return 0;
    }
    return 1;
EOF
    return [ 'function(k, v, n)', $code ];
}

sub code_for_vec_find_first_set {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i) {
        @!src_elt x = @!vec_coeff_ptr_const(k, v, i);
	if (!@!is_zero(k, x))
	    return i;
    }
    return -1;
EOF
    return [ 'function(k, v, n)', $code ];
}

sub code_for_vec_hamming_weight {
    my $code = <<EOF;
    int w = 0;
    for (unsigned int i = 0; i < n; ++i) {
        @!src_elt x = @!vec_coeff_ptr_const(k, v, i);
	w += !@!is_zero(k, x);
    }
    return w;
EOF
    return [ 'function(k, v, n)', $code ];
}

sub code_for_vec_simd_find_first_set {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i) {
        @!src_elt x = @!vec_coeff_ptr_const(k, v, i);
	if (!@!is_zero(k, x))
	    return i * @!simd_groupsize(k) + @!simd_hamming_weight(k, x);
    }
    return -1;
EOF
    return [ 'function(k, v, n)', $code ];
}

sub code_for_vec_simd_hamming_weight {
    my $code = <<EOF;
    int w = 0;
    for (unsigned int i = 0; i < n; ++i) {
        @!src_elt x = @!vec_coeff_ptr_const(k, v, i);
	w += @!simd_hamming_weight(k, x);
    }
    return w;
EOF
    return [ 'function(k, v, n)', $code ];
}

1;
