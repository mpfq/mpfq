package Mpfq::defaults::vec::getset;

use strict;
use warnings;

sub code_for_vec_coeff_ptr {
    my $proto = 'inline(K!,v,i)';
    my $code = 'return v[i];';
    return [ $proto, $code ];
}

sub code_for_vec_subvec {
    my $proto = 'inline(K!,v,i)';
    my $code = 'return v+i;';
    return [ $proto, $code ];
}

sub code_for_vec_coeff_ptr_const { return code_for_vec_coeff_ptr(@_); }
sub code_for_vec_subvec_const { return code_for_vec_subvec(@_); }

# by defaults the type have the width known at compile time, so that
# pointer arithmetic is sufficient !
sub code_for_vec_ur_coeff_ptr { return code_for_vec_coeff_ptr(@_); }
sub code_for_vec_ur_coeff_ptr_const { return code_for_vec_coeff_ptr_const(@_); }
sub code_for_vec_ur_subvec { return code_for_vec_subvec(@_); }
sub code_for_vec_ur_subvec_const { return code_for_vec_subvec_const(@_); }

1;
