package Mpfq::defaults::pow;

use strict;
use warnings;

sub code_for_pow {
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $proto = 'inline(k,res,r,x,n)';
    my $code = <<EOF;
@!elt u, a;
long i, j, lead;     /* it is a signed type */
unsigned long mask;

/* get the correct (i,j) position of the most significant bit in x */
for(i = ((long)n)-1; i>=0 && x[i]==0; i--)
    ;
if (i < 0) {
    /* power zero gets 1 */
    @!set_ui(k, res, 1);
    return;
}
j = $w - 1;
mask = (1UL<<j);
for( ; (x[i]&mask)==0 ;j--, mask>>=1)
    ;
lead = i*$w+j;      /* Ensured. */

@!init(k, &u);
@!init(k, &a);
@!set(k, a, r);
for( ; lead > 0; lead--) {
    if (j-- == 0) {
        i--;
        j = $w-1;
        mask = (1UL<<j);
    } else {
        mask >>= 1;
    }
    if (x[i]&mask) {
        @!sqr(k, u, a);
        @!mul(k, a, u, r);
    } else {
        @!sqr(k, a,a);
    }
}
@!set(k, res, a);
@!clear(k, &u);
@!clear(k, &a);
EOF
    return [ $proto, $code ];
}

# Getting the correct way to reduce the exponent modulo 2, even when
# doing it roughly, is not exactly trivial.
sub code_for_powz {
    my $opt = shift @_;
    my $proto = 'function(k,y,x,z)';
    my $code = <<EOF;
    if (mpz_sgn(z) < 0) {
        mpz_t mz;
        mpz_init(mz);
        mpz_neg(mz, z);
        @!powz(k, y, x, mz);
        @!inv(k, y, y);
        mpz_clear(mz);
    } else if (mpz_sizeinbase(z, 2) > @!field_degree(k) * @!field_characteristic_bits(k)) {
        mpz_t zr;
        mpz_init(zr);
        mpz_t ppz;
        mpz_init(ppz);
        @!field_characteristic(k, ppz);
        mpz_pow_ui(ppz,ppz,@!field_degree(k));
        mpz_sub_ui(ppz,ppz,1);
        mpz_fdiv_r(zr, z, ppz);
        @!powz(k, y, x, zr);
        mpz_clear(ppz);
        mpz_clear(zr);
    } else {
        @!pow(k, y, x, z->_mp_d, mpz_size(z));
    }
EOF
    return [ $proto, $code ];
}

1;
