package Mpfq::fixmp;

use strict;
use warnings;

use Mpfq::engine::handler;

use Mpfq::fixmp::longlong;
use Mpfq::fixmp::x86_64;
use Mpfq::fixmp::i386;

# This is modified at runtime by gen_fixmp.pl
our @parents = qw/
    Mpfq::fixmp::longlong
    Mpfq::fixmp::x86_64
    Mpfq::fixmp::i386
/;

our @ISA = qw/Mpfq::engine::handler/;

our $resolve_conflicts = sub {
    my ($k, @packages) = @_;
    return @packages if scalar @packages == 1;
    my $h = {};
    $h->{$_}=1 for @packages;
    delete $h->{'Mpfq::fixmp::longlong'};
    return keys %$h;
};

sub new { return bless({}, shift); }

