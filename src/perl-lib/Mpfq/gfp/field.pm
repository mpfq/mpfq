package Mpfq::gfp::field;

use strict;
use warnings;

sub code_for_impl_max_characteristic_bits {
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $n = $opt->{'n'};
    my $opthw=$opt->{'opthw'}?1:0;
    return [ 'macro()', $n*$w-int($opthw*$w/2) ];
}

sub code_for_impl_max_degree { return [ 'macro()', '1' ]; }

sub code_for_field_degree { return [ 'macro(K!)', '1' ]; }

sub code_for_field_characteristic {
    my $proto = 'inline(k,z)';
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $code = <<EOF;
    mpz_set(z, k->p);
EOF
    return [ $proto, $code ];
}
sub code_for_field_characteristic_srcptr {
    return [ 'inline(k)', "return k->p;" ];
}


sub code_for_field_characteristic_bits {
    my $proto = 'inline(k)';
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $code = <<EOF;
    return mpz_sizeinbase(k->p, 2);
EOF
    return [ $proto, $code ];
}

sub code_fragment_set_bp_from_p {
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $n = $opt->{'n'};
    my $nn = $opt->{'nn'};
    my $code = <<EOF;
    {
        /* precompute bigmul_p = largest multiple of p that fits in an
         * elt_ur: p*Floor( (2^($nn*$w)-1)/p )
         */
        mpz_ui_pow_ui(k->bigmul_p, 2, $nn*$w);
        mpz_sub_ui(k->bigmul_p, k->bigmul_p, 1);
        mpz_fdiv_q(k->bigmul_p, k->bigmul_p, k->p);
        mpz_mul(k->bigmul_p, k->bigmul_p, k->p);
    }
EOF
    return $code;
}

sub code_for_field_specify {
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $n = $opt->{'n'};
    my $nn = $opt->{'nn'};
    my $code = '';
    $code .= <<EOF;
    if (dummy == MPFQ_PRIME_MPN) {
        fprintf(stderr, "MPFQ_PRIME_MPN is deprecated\\n");
        return;
    } else if (dummy == MPFQ_PRIME_MPZ) {
        mpz_srcptr p = (mpz_srcptr) vp;
        if (!(mpz_size(p) == $n)) {
            fprintf(stderr, "This binary requires the use of a $n-machine words prime number. Here, p spans \%zu machine words. Please adapt linalg/bwc/CMakeLists.txt accordingly and re-run\\n", mpz_size(p));
            abort();
        }
        mpz_set(k->p, p);
EOF
    local $_ = code_fragment_set_bp_from_p($opt, @_);
    s/^/\t/mg;
    $code .= $_;
    $code .= <<EOF;
    } else if (dummy == MPFQ_SIMD_GROUPSIZE && *(int*)vp == 1) {
        /* Do nothing, this is an admitted condition */
        return;
    } else {
        return;
    }
EOF
    return [ 'function(k,dummy!,vp)' , $code ]; 
}

sub code_for_field_init { 
    my $opt = shift @_;
    my $type = $opt->{'type'};
    my $code = <<EOF;
mpz_init(k->p);
mpz_init(k->bigmul_p);
k->io_base = 10;
mpz_init(k->factor);
k->ts_info.e=0;
EOF
    return [ 'inline(k)', $code ];
}

sub code_for_field_clear {
    my $opt = shift @_;
    my $type = $opt->{'type'};
    my $code = '';
    $code .= <<EOF;
    mpz_clear(k->p);
    mpz_clear(k->bigmul_p);
    if (k->ts_info.e > 0) {
        free(k->ts_info.hh);
        free(k->ts_info.z);
    }
    mpz_clear(k->factor);
EOF
    return [ 'function(k)', $code ];
}

sub code_for_field_setopt { return [ 'macro(f,x,y)' , '' ]; }

sub init_handler {
    my $types = {
        field      =>	'typedef mpfq_p_field @!field;',
        src_field  =>	'typedef mpfq_p_src_field @!src_field;',
        dst_field  =>	'typedef mpfq_p_dst_field @!dst_field;',
    };
    return { types => $types,
             includes=>[ qw{
              <limits.h>
              "mpfq_fixmp.h"
              "mpfq_gfp_common.h"
              }],
      };
}

1;
