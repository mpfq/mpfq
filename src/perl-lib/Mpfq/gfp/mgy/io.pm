package Mpfq::gfp::mgy::io;

use strict;
use warnings;

use Mpfq::gfp::io;
#use Mpfq::gfp::mgy::codec

our @parents = qw/Mpfq::gfp::io/;

# piggy-back on default code.
sub code_for_asprint {
    my ($normal, @gens) = Mpfq::gfp::io::code_for_asprint(@_);
    my ($kind,$code) = @$normal;
    chomp($code);
    die "fix me please" unless $code =~ s/(return \w+;)$//;
    my $ret = $1;
    chomp($code);
    die "fix me please" unless $kind =~ /^(\w+)\(k,pstr,x\)$/;
    $kind = "$1(k,pstr,x0)";
    chomp($code);
    $code = <<EOF;
@!elt x;
@!init(k, &x);
@!mgy_dec(k, x, x0);
$code
@!clear(k, &x);
$ret
EOF
    return [ $kind , $code], @gens;
}

# Note that because sscan uses @!set_mpz, it is not necessary to
# specialize it.

1;
