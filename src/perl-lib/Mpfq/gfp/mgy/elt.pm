package Mpfq::gfp::mgy::elt;

use strict;
use warnings;

use Mpfq::gfp::elt;

our @parents = qw/Mpfq::gfp::elt/;

sub code_for_set_ui {
    my $normal = Mpfq::gfp::elt::code_for_set_ui(@_);
    my ($kind,$code,@gens) = @$normal;
    die "fix me please" unless $kind =~ /^(\w+)\(k!?,r!?,x!?\)$/;
    $code .= "@!mgy_enc(k, r, r);\n";
    return [ $kind, $code ], @gens;
}

sub code_for_get_ui {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $opthw=$opt->{'opthw'};
    my $proto = 'inline(k!,x)';
    my $code = <<EOF;
mp_limb_t tmp[$n];
@!mgy_dec(k,tmp,x);
return tmp[0];
EOF

    return [ $proto, $code ];
}


sub code_for_set_mpn {
    my $normal = Mpfq::gfp::elt::code_for_set_mpn(@_);
    my ($kind,$code,@gens) = @$normal;
    die "fix me please" unless $kind =~ /^(\w+)\(k!?,r!?,x!?,n!?\)$/;
    $code .= "@!mgy_enc(k, r, r);\n";
    return [ $kind, $code ], @gens;
}



sub code_for_get_mpn {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $opthw=$opt->{'opthw'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k!,r,x)';
    my $code = "mpfq_fixmp_${nhw}_mgy_decode(r, x, k->mgy_info.invR, k->p->_mp_d);\n";
    return [ $proto, $code ];
}

sub code_for_get_mpz {
    my $normal = Mpfq::gfp::elt::code_for_get_mpz(@_);
    my ($kind,$code,@gens) = @$normal;
    die "fix me please" unless $kind =~ /^(\w+)\(k!?,z!?,y!?\)$/;
    $kind = "$1(k,z,x)";
    my $pre = <<EOF;
@!elt y;
@!mgy_dec(k,y,x);
EOF
    return [ $kind, $pre . $code], @gens;
}


sub code_for_cmp {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k!,x,y)';
    my $code = <<EOF;
mp_limb_t tmpx[$n],tmpy[$n];
@!mgy_dec(k,tmpx,x);
@!mgy_dec(k,tmpy,y);
return mpfq_fixmp_${nhw}_cmp(tmpx,tmpy);
EOF
    return [ $proto, $code ];

}


sub code_for_cmp_ui {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k!,x,y)';
    my $code = <<EOF;
mp_limb_t tmpx[$n];
@!mgy_dec(k,tmpx,x);
return mpfq_fixmp_${nhw}_cmp_ui(tmpx,y);
EOF
    return [ $proto, $code ];
}

sub code_for_add_ui {
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
@!elt yy;
@!init(k, &yy);
@!set_ui(k, yy, y);
@!add(k, z, x, yy);
@!clear(k, &yy);
EOF
    return [ $proto, $code ];
}

sub code_for_sub_ui {
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
@!elt yy;
@!init(k, &yy);
@!set_ui(k, yy, y);
@!sub(k, z, x, yy);
@!clear(k, &yy);
EOF
    return [ $proto, $code ];
}

sub code_for_mul {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $opthw=$opt->{'opthw'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $nn1 = 2*$n;
    $nn1-- if $opt->{'opthw'};
    my $type = $opt->{'type'};
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
#ifdef HAVE_native_mpfq_fixmp_${nhw}_mulredc
mpfq_fixmp_${nhw}_mulredc(z, x, y, k->p->_mp_d, k->mgy_info.invP);
#else
mp_limb_t tmp[$nn1];
mpfq_fixmp_${nhw}_mul(tmp, x, y);
mpfq_fixmp_${nhw}_redc(z, tmp, k->mgy_info.invP, k->p->_mp_d);
#endif
EOF
    return [ $proto, $code ];
}

sub code_for_sqr {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $opthw=$opt->{'opthw'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $nn1 = 2*$n;
    $nn1-- if $opt->{'opthw'};
    my $proto = 'inline(k,z,x)';
    my $type = $opt->{'type'};
    my $code = <<EOF;
#ifdef HAVE_native_mpfq_fixmp_${nhw}_mulredc
mpfq_fixmp_${nhw}_mulredc(z, x, x, k->p->_mp_d, k->mgy_info.invP);
#else
mp_limb_t tmp[$nn1];
mpfq_fixmp_${nhw}_sqr(tmp, x);
mpfq_fixmp_${nhw}_redc(z, tmp, k->mgy_info.invP, k->p->_mp_d);
#endif
EOF
    return [ $proto, $code ];
}

sub code_for_mul_ui {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $opthw = $opt->{'opthw'};
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
mp_limb_t tmpy[$n];
@!set_ui(k,tmpy,y);
@!mul(k,z,x,tmpy);
EOF
    return [ $proto, $code ];
}

sub code_for_inv {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $n2 = 2*$n;
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k,z,x)';
    my $type = $opt->{'type'};
    my $code = <<EOF;
mp_limb_t tmp[3*$n],q[$n2];
int ret=mpfq_fixmp_${nhw}_invmod(tmp+$n2, x, k->p->_mp_d);
if (!ret) {
    @!get_mpz(k, k->factor, tmp+$n2);
    mpfq_zero(z, $n);
} else {
    for(int i=0;i<$n2;tmp[i++]=0) ;
    mpn_tdiv_qr(q,z,0,tmp,3*$n,k->p->_mp_d,$n);
}
return ret;
EOF
    return [ $proto, $code ];
}

#sub code_for_elt_ur_set {
#    my $opt = shift @_;
#    my $n = $opt->{'n'};
#    my $n2 = 2*$n;
#    my $nn = $opt->{'nn'};
#    my $proto = 'inline(k,z,x)';
#    my $code = <<EOF;
#mp_limb_t tmp[3*$n], q[$n2];
#int i;
#for(i = 0; i < $n; ++i) {
#    tmp[i] = 0;
#    tmp[i+$n2]=x[i];
#}
#for(i = $n; i < $n2; ++i) 
#    tmp[i] = 0;
#mpn_tdiv_qr(q,z,0,tmp,3*$n,k->p->_mp_d,$n);
#for(i= $n; i< $nn;++i)
#    z[i] = 0;
#EOF
#    return [ $proto, $code ];
#}

sub code_for_elt_ur_set_ui {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $n2 = 2*$n;
    my $nn = $opt->{'nn'};
    my $proto = 'inline(k,r,x)';
    my $code = <<EOF;
mp_limb_t tmp[$n2+1], q[$n+1];
mpfq_zero(tmp, $n2);
tmp[$n2] = x;
mpn_tdiv_qr(q,r,0,tmp,$n2+1,k->p->_mp_d,$n);
mpfq_zero(r + $n, $nn - $n);
EOF
    return [ $proto, $code ];
}


sub code_for_reduce {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nn = $opt->{'nn'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $opthw = $opt->{'opthw'};
    my $w = $opt->{'w'};
    my $proto = 'inline(k,z,x)';
    my $type = $opt->{'type'};
    my $code =<<EOF;
if (x[$nn-1]>>($w-1)) {
    // negative number, add bigmul_p to make it positive before reduction
    mpn_add_n(x, x, k->bigmul_p->_mp_d, $nn);
}
mpfq_fixmp_${nhw}_redc_ur(z,x,k->mgy_info.invP,k->p->_mp_d);
EOF
    return [ $proto, $code ];
}


1;
