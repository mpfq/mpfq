package Mpfq::gfp::mgy::codec;

use strict;
use warnings;

# For the moment, the bulk of encoding / decoding work is within fixmp.h
# ; therefore this code is merely a trampoline.

sub code_for_mgy_enc {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $w = $opt->{'w'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k,z,x)';
    my $code = <<EOF;
mpfq_fixmp_${nhw}_mgy_encode(z, x, k->p->_mp_d);
EOF
    return [ $proto, $code ];
}

sub code_for_mgy_dec {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $w = $opt->{'w'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k,z,x)';
    my $code = <<EOF;
mpfq_fixmp_${nhw}_mgy_decode(z, x, k->mgy_info.invR, k->p->_mp_d);
EOF
    return [ $proto, $code ];
}

1;
