package Mpfq::gfp::elt;

use strict;
use warnings;

use Mpfq::defaults::flatdata;
use Mpfq::defaults::pow;
use Mpfq::engine::maketext qw/simple_indent/;

our @parents = qw/
    Mpfq::defaults::flatdata
    Mpfq::defaults::pow
/;

sub code_for_init {
    my $code = <<EOF;
assert(k);
assert(*x);
EOF
    return [ 'inline(k!,x!)', $code ];
}

sub code_for_clear {
    my $code = <<EOF;
assert(k);
assert(*x);
EOF
    return [ 'inline(k!,x!)', $code ];
}

# These five functions should anyway be inlined and transformed to block
# loads and stores by the compiler. TODO: should we do the same with
# the vec_ functions ? (currently we use vec/flatdata, which does memcpy
# -- implies a r!=s test, but OTOH potentially better for large transfers
# than what we can do here).
sub code_for_set {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    return [ 'inline(K!,r,s)', "mpfq_copy(r,s,$n);" ];
}

sub code_for_set_ur {
    my $opt = shift @_;
    my $nn = $opt->{'nn'};
    return [ 'inline(K!,r,s)', "mpfq_copy(r,s,$nn);" ];
}
sub code_for_set_zero {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    return [ 'inline(K!,r)', "mpfq_zero(r, $n);" ];
}

sub code_for_elt_ur_set_zero {
    my $opt = shift @_;
    my $nn = $opt->{'nn'};
    return [ 'inline(K!,r)', "mpfq_zero(r, $nn);" ];
}

sub code_for_elt_ur_set_elt {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nn = $opt->{'nn'};
    return [ 'inline(K!,r,s)', "mpfq_copy(r, s, $n); mpfq_zero(r+$n, $nn-$n);" ];
}

sub code_for_set_ui {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $proto = 'inline(k!,r,x)';
    my $code;
    if ($n == 1) {
       $code = <<EOF;
            ASSERT_FOR_STATIC_ANALYZER(mpz_getlimbn(k->p, 0) != 0);
            r[0] = x % mpz_getlimbn(k->p, 0);
EOF
    } else {
       $code = <<EOF;
            assert (r);
            r[0] = x;
            mpfq_zero(r + 1, $n - 1);
EOF
    }
    $code = simple_indent($code);
    return [ $proto, $code ];
}

sub code_for_get_ui {
    return [ 'inline(k!,x)', 'return x[0];' ];
}

sub code_for_set_mpn {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $proto = 'inline(k,r,x,n)';
    my $code = <<EOF;
if (n < $n) {
    mpfq_copy(r, x, n);
    mpfq_zero(r + n, $n - n);
} else {
    mp_limb_t tmp[n-$n+1];
    mpn_tdiv_qr(tmp, r, 0, x, n, k->p->_mp_d, $n);
}
EOF
    return [ $proto, $code ];
}

sub code_for_set_mpz {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $proto = 'inline(k,r,z)';
    my $code = <<EOF;
if (z->_mp_size < 0) {
    @!set_mpn(k, r, z->_mp_d, -z->_mp_size);
    @!neg(k, r, r);
} else {
    @!set_mpn(k, r, z->_mp_d, z->_mp_size);
}
EOF
    return [ $proto, $code ];
}

sub code_for_get_mpn {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $proto = 'inline(k!,r,x)';
    my $code = "mpfq_copy(r, x, $n);\n";
    return [ $proto, $code ];
}

sub code_for_get_mpz {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $w = $opt->{'w'};
    my $proto = 'inline(k!,z,y)';
    my $code = <<EOF;
int i; 
mpz_realloc2(z, $n*$w);
for (i = 0; i < $n; ++i)
    z->_mp_d[i] = y[i];
i = $n;
while (i>=1 && z->_mp_d[i-1] == 0)
    i--;
z->_mp_size = i;
EOF
    return [ $proto, $code ];
}

sub code_for_normalize {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k,x)';
    my $code = <<EOF;
if (mpfq_fixmp_${n}_cmp(x,k->p->_mp_d)>=0) {
  mp_limb_t q[$n+1];
  @!elt r;
  mpn_tdiv_qr(q, r, 0, x, $n, k->p->_mp_d, $n);
  @!set(k, x, r);
}
EOF
    return [ $proto, $code ];
}

sub code_for_cmp {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    return [ 'inline(k!,x,y)', "return mpfq_fixmp_${nhw}_cmp(x,y);" ];
}

sub code_for_cmp_ui {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k!,x,y)';
    return [ $proto, "return mpfq_fixmp_${nhw}_cmp_ui(x,y);" ];
}

sub code_for_random {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $proto = 'inline(k,x,state)';
    my $code = <<EOF;
  mpz_t z;
  mpz_init(z);
  mpz_urandomb(z, state, $n * GMP_LIMB_BITS);
  mpfq_copy(x, z->_mp_d, $n);
  mpz_clear(z);
@!normalize(k, x);
EOF
    return [ $proto, $code ];
}

sub code_for_random2 {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $proto = 'inline(k,x,state)';
    my $code = <<EOF;
  mpz_t z;
  mpz_init(z);
  mpz_rrandomb(z, state, $n * GMP_LIMB_BITS);
  mpfq_copy(x, z->_mp_d, $n);
  mpz_clear(z);
@!normalize(k, x);
EOF
    return [ $proto, $code ];
}

sub code_for_add {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k,z,x,y)';
    my $code;
    if (!$opt->{'opthw'}) {
      $code = <<EOF;
mp_limb_t cy;
cy = mpfq_fixmp_${nhw}_add(z, x, y);
if (cy || (mpfq_fixmp_${nhw}_cmp(z, k->p->_mp_d) >= 0))
    mpfq_fixmp_${nhw}_sub(z, z, k->p->_mp_d);
EOF
    } else {
      $code = <<EOF;
mpfq_fixmp_${nhw}_add(z, x, y);
if (mpfq_fixmp_${nhw}_cmp(z, k->p->_mp_d) >= 0)
    mpfq_fixmp_${nhw}_sub(z, z, k->p->_mp_d);
EOF
    }
    return [ $proto, $code ];
}

sub code_for_sub {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $w = $opt->{'w'};
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
mp_limb_t cy;
cy = mpfq_fixmp_${nhw}_sub(z, x, y);
if (cy) // negative result
    mpfq_fixmp_${nhw}_add(z, z, k->p->_mp_d);
EOF
    return [ $proto, $code ];
}

sub code_for_neg {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k,z,x)';
    my $code = <<EOF;
if (mpfq_fixmp_${nhw}_cmp_ui(x, 0))
    mpfq_fixmp_${nhw}_sub(z, k->p->_mp_d, x);
else {
    int i;
    for (i = 0; i < $n; ++i)
        z[i] = 0;
    }
EOF
    return [ $proto, $code ];
}

sub code_for_mul {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $nn1 = 2*$n;
    $nn1-- if $opt->{'opthw'};
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
mp_limb_t tmp[$nn1];
mpfq_fixmp_${nhw}_mul(tmp, x, y);
mpfq_fixmp_${nhw}_mod(z, tmp, k->p->_mp_d);
EOF
    return [ $proto, $code ];
}

sub code_for_sqr {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $nn1 = 2*$n;
    $nn1-- if $opt->{'opthw'};
    my $proto = 'inline(k,z,x)';
    my $code = <<EOF;
mp_limb_t tmp[$nn1];
mpfq_fixmp_${nhw}_sqr(tmp, x);
mpfq_fixmp_${nhw}_mod(z, tmp, k->p->_mp_d);
EOF
    return [ $proto, $code ];
}

# For n=0.5, this code 
sub code_for_mul_halfui {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    die "only for hw" unless $opt->{'opthw'};
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
mp_limb_t tmp[$n], q[2];
mpfq_fixmp_${nhw}_mul1(tmp,x,y);
mpn_tdiv_qr(q, z, 0, tmp, $n+1, k->p->_mp_d, $n);
EOF
    return {
        kind=>$proto, 
        code=>$code,
        name=>"mul_halfui",
        requirements=>"dst_field dst_elt src_elt ulong",
    };
}

sub code_for_mul_ui {
    my $opt = $_[0];
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
mp_limb_t tmp[$n+1], q[2];
mpfq_fixmp_${nhw}_mul1(tmp,x,y);
mpn_tdiv_qr(q, z, 0, tmp, $n+1, k->p->_mp_d, $n);
EOF
    my @other;
    push @other, code_for_mul_halfui(@_) if $opt->{'opthw'};
    return [ $proto, $code ], @other;
}

sub code_for_is_sqr {
    my $proto = 'inline(k,x)';
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    # make sure we use @!cmp_ui and not cmp_ui_$n directly, since these
    # are not equivalent in the montgomery case !
    my $code = <<EOF;
mp_limb_t pp[$n];
@!elt y;
mpfq_fixmp_${nhw}_sub_ui_nc(pp, k->p->_mp_d, 1);
mpfq_fixmp_${nhw}_rshift(pp, 1);
@!init(k, &y);
@!pow(k, y, x, pp, $n);
int res = @!cmp_ui(k, y, 1);
@!clear(k, &y);
if (res == 0)
    return 1;
else 
    return 0;
EOF
    return [ $proto, $code ];
}

sub code_for_init_ts {
    my $proto = 'function(k)';
    my $requirements = 'dst_field';
    my $name = 'init_ts';
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $w = $opt->{'w'};
    my $code = <<EOF;
gmp_randstate_t rstate;
gmp_randinit_default(rstate);

mp_limb_t pp[$n];
mp_limb_t *ptr = pp;
mp_limb_t s[$n];
mpfq_fixmp_${nhw}_sub_ui_nc(pp, k->p->_mp_d, 1);
int e = 0;
for( ; e < $n*$w && *ptr == 0 ; e+=$w, ptr++) ;
if (e >= $n*$w) abort();
int ee;
ee = mpfq_ctzl(*ptr);
e += ee;
if (e < $w) {
    mpfq_fixmp_${nhw}_rshift(pp, e);
} else {
    mpfq_fixmp_${nhw}_long_rshift(pp, e/$w, e%$w);
}
s[0] = 1UL;
int i;
for (i = 1; i <$n; ++i)
    s[i] = 0UL;
if (e-1 < $w) {
    mpfq_fixmp_${nhw}_lshift(s, e-1);
} else {
    mpfq_fixmp_${nhw}_long_lshift(s, (e-1)/$w, (e-1)%$w);
}
k->ts_info.e = e;

k->ts_info.z = mpfq_malloc_check($n*sizeof(mp_limb_t));
k->ts_info.hh = mpfq_malloc_check($n*sizeof(mp_limb_t));

@!elt z, r;
@!init(k, &z);
@!init(k, &r);
@!set_ui(k, r, 0);
do {
    @!random(k, z, rstate);
    @!pow(k, z, z, pp, $n);
    @!pow(k, r, z, s, $n);
    @!add_ui(k, r, r, 1);
} while (@!cmp_ui(k, r, 0)!=0);
@!set(k, (@!dst_elt)k->ts_info.z, z);
@!clear(k, &z);
@!clear(k, &r);

mpfq_fixmp_${nhw}_sub_ui_nc(pp, pp, 1);
mpfq_fixmp_${nhw}_rshift(pp, 1);
for (i = 0; i < $n; ++i)
    k->ts_info.hh[i] = pp[i];
gmp_randclear(rstate);
EOF
    return { kind=>$proto,
        requirements=>$requirements,
        name=>$name,
        code=>$code,};
}



sub code_for_sqrt {
    my $proto = 'function(k,z,a)';
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $code = <<EOF;
if (@!cmp_ui(k, a, 0) == 0) {
    @!set_ui(k, z, 0);
    return 1;
}
if (k->ts_info.e == 0)
    @!init_ts(k);
@!elt b, x, y;
@!init(k, &x);
@!init(k, &y);
@!init(k, &b);
mp_limb_t r = k->ts_info.e;
mp_limb_t s; //= (1UL<<(r-1)); not needed...
@!set(k, x, a);
@!set(k, y, (@!src_elt)k->ts_info.z);

@!pow(k, x, a, k->ts_info.hh, $n);
@!sqr(k, b, x);
@!mul(k, x, x, a);
@!mul(k, b, b, a);

@!elt t;
@!init(k, &t);
mp_limb_t m;
for(;;) {
    @!set(k, t, b);
    for(m=0; @!cmp_ui(k, t, 1)!=0; m++)
        @!sqr(k, t, t);
    assert(m<=r);
    
    if (m==0 || m==r)
        break;
    
    s = 1UL<<(r-m-1);
    r = m;
    
    @!pow(k, t, y, &s, 1);
    @!sqr(k, y, t);
    @!mul(k, x, x, t);
    @!mul(k, b, b, y);
}
@!set(k, z, x);
@!clear(k, &t);
@!clear(k, &x);
@!clear(k, &y);
@!clear(k, &b);
return (m==0);
EOF
    return [ $proto, $code ], code_for_init_ts($opt);
}


sub code_for_inv {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k,z,x)';
    my $type = $opt->{'type'};
    my $code = <<EOF;
int ret=mpfq_fixmp_${nhw}_invmod(z, x, k->p->_mp_d);
if (!ret)
    @!get_mpz(k, k->factor, z);
return ret;
EOF
    return [ $proto, $code ];
}

sub code_for_frobenius { return [ 'macro(k,x,y)', '@!set(k, x, y)']; }

sub code_for_add_ui {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k,z,x,y)';
    my $code;
    if (!$opt->{'opthw'}) {
      $code = <<EOF;
mp_limb_t cy;
cy = mpfq_fixmp_${nhw}_add_ui(z, x, y);
if (cy || (mpfq_fixmp_${nhw}_cmp(z, k->p->_mp_d) >= 0))
    mpfq_fixmp_${nhw}_sub(z, z, k->p->_mp_d);
EOF
    } else {
      $code = <<EOF;
mpfq_fixmp_${nhw}_add_ui(z, x, y);
if (mpfq_fixmp_${nhw}_cmp(z, k->p->_mp_d) >= 0)
    mpfq_fixmp_${nhw}_sub(z, z, k->p->_mp_d);
EOF
    }
    if ($n == 1) {
        # Do something special for n==1, since in that case the unsigned
        # long may be larger than p.
        my $code_pre = <<EOF;
ASSERT_FOR_STATIC_ANALYZER(mpz_getlimbn(k->p, 0) != 0);
y %= mpz_getlimbn(k->p, 0);
EOF
        $code = $code_pre . $code;
    }
    return [ $proto, $code ];
}

sub code_for_sub_ui {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
mp_limb_t cy;
cy = mpfq_fixmp_${nhw}_sub_ui(z, x, y);
if (cy) // negative result
    mpfq_fixmp_${nhw}_add(z, z, k->p->_mp_d);
EOF
    if ($n == 1) {
        # Do something special for n==1, since in that case the unsigned
        # long may be larger than p.
        my $code_pre = <<EOF;
ASSERT_FOR_STATIC_ANALYZER(mpz_getlimbn(k->p, 0) != 0);
y %= mpz_getlimbn(k->p, 0);
EOF
        $code = $code_pre . $code;
    }
    return [ $proto, $code ];
}

sub code_for_elt_ur_init {
    my $code = <<EOF;
assert(k);
assert(*x);
EOF
    return [ 'inline(k!,x!)', $code ];
}

sub code_for_elt_ur_clear {
    my $code = <<EOF;
assert(k);
assert(*x);
EOF
    return [ 'inline(k!,x!)', $code ];
}

sub code_for_elt_ur_set {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nn = $opt->{'nn'};
    my $proto = 'inline(k!,z,x)';
    my $code = "mpfq_copy(z, x, $nn);\n";
    return [ $proto, $code ];
}

sub code_for_elt_ur_set_ui {
    my $opt = shift @_;
    my $nn = $opt->{'nn'};
    my $proto = 'inline(k!,r,x)';
    my $code = <<EOF;
assert (r); 
r[0] = x;
mpfq_zero(r + 1, $nn - 1);
EOF
    return [ $proto, $code ];
}

sub code_for_elt_ur_add {
    my $opt = shift @_;
    my $nn = $opt->{'nn'};
    my $proto = 'inline(k!,z,x,y)';
    my $code = <<EOF;
mpn_add_n(z, x, y, $nn);
EOF
    return [ $proto, $code ];
}

sub code_for_elt_ur_sub {
    my $opt = shift @_;
    my $nn = $opt->{'nn'};
    my $proto = 'inline(k!,z,x,y)';
    my $code = <<EOF;
mpn_sub_n(z, x, y, $nn);
EOF
    return [ $proto, $code ];
}

sub code_for_elt_ur_neg {
    my $opt = shift @_;
    my $nn = $opt->{'nn'};
    my $proto = 'inline(k,z,x)';
    my $code = <<EOF;
@!elt_ur tmp;
@!elt_ur_init(k, &tmp);
mpfq_zero(tmp, $nn);
mpn_sub_n(z, tmp, x, $nn);
@!elt_ur_clear(k, &tmp);
EOF
    return [ $proto, $code ];
}


sub code_for_reduce {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nn = $opt->{'nn'};
    my $w = $opt->{'w'};
    my $proto = 'inline(k,z,x)';
    my $type = $opt->{'type'};
    my $code = <<EOF;
mp_limb_t q[$nn+1];
if (x[$nn-1]>>($w-1)) {
    // negative number, add bigmul_p to make it positive before reduction
    mpn_add_n(x, x, k->bigmul_p->_mp_d, $nn);
}
mpn_tdiv_qr(q, z, 0, x, $nn, k->p->_mp_d, $n);
EOF
    return [ $proto, $code ];
}

sub code_for_mul_ur {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $nn = $opt->{'nn'};
    my $proto = 'inline(k!,z,x,y)';
    my $nn1 = 2*$n;
    $nn1-- if $opt->{'opthw'};
    my $code = <<EOF;
mpfq_fixmp_${nhw}_mul(z, x, y);
mpfq_zero(z + $nn1, $nn - $nn1);
EOF
    return [ $proto, $code ]; #, code_for_fixmp_mul($opt) ];
}

sub code_for_sqr_ur {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $nhw=$n;
    $nhw = ($nhw - 1) . "_5" if $opt->{'opthw'};
    my $nn = $opt->{'nn'};
    my $proto = 'inline(k!,z,x)';
    my $nn1 = 2*$n;
    $nn1-- if $opt->{'opthw'};
    my $code = <<EOF;
mpfq_fixmp_${nhw}_sqr(z, x);
mpfq_zero(z + $nn1, $nn - $nn1);
EOF
    return [ $proto, $code ];
}

sub code_for_hadamard {
    my $opt = shift @_;
    my $proto = 'inline(k,x,y,z,t)';
    my $code = <<EOF;
@!elt tmp;
@!init(k, &tmp);
@!add(k, tmp, x, y);
@!sub(k, y, x, y);
@!set(k, x, tmp);
@!add(k, tmp, z, t);
@!sub(k, t, z, t);
@!set(k, z, tmp);
@!sub(k, tmp, x, z);
@!add(k, x, x, z);
@!add(k, z, y, t);
@!sub(k, t, y, t);
@!set(k, y, tmp);
@!clear(k, &tmp); 
EOF
    return [ $proto, $code ];
}

sub init_handler {
    my ($opt) = @_;
    my $n = $opt->{'n'};
    my $nn = $opt->{'nn'};
    my $types = {
        elt =>	"typedef unsigned long @!elt\[$n\];",
        dst_elt =>	"typedef unsigned long * @!dst_elt;",
        src_elt =>	"typedef const unsigned long * @!src_elt;",

        elt_ur =>	"typedef unsigned long @!elt_ur\[$nn\];",
        dst_elt_ur =>	"typedef unsigned long * @!dst_elt_ur;",
        src_elt_ur =>	"typedef const unsigned long * @!src_elt_ur;",
    };
    return { types => $types };
}

1;
