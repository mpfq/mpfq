package Mpfq::engine::postprocess;

use strict;
use warnings;
use Carp;

use Mpfq::engine::conf qw/parse_api_rhs/;
use Mpfq::engine::utils qw(debug);
use Data::Dumper;
use Carp;
use Exporter qw(import);
our @EXPORT_OK = qw(reformat_generated_code);


# Create several hashes, for the main function _and_ for the possible
# callees.
sub reformat_generated_code {

    my ($api, $f, @definitions) = @_;

    my @functions_to_ship = ();

    debug "2 reformatting code generated for $f";

    my $doc = <<EOF;
    # Rules for what is returned by code_for_xxx
    #
    # (1) Normal value for is an array (reference to a list, i.e. [])
    # with kind, code, and the sub-functions afterwards.
    #
    # (2) Another possibility is to return just a hash (reference to a
    # hash, i.e. {}), which specifies at least kind and code, but
    # possibly also some other values (pragmas, attributes). Note that
    # this precludes the use of sub functions.
    #
    # (3) Yet another option is a list with only hashes, the first one
    # being the leader function. This has the greatest flexibility.
    #
    # (4) Note that all sub-functions must be hashes, which specify
    # requirements and name, of course. The leader, on the contrary,
    # _must not_ specify its name & requirements. (see also (5)).
EOF
    # [This one isn't really documented yet...]
    #
    # (5) There's yet another special case, where subfunctions are
    # generated dynamically with generators from other modules. This is
    # an experimental feature. A sub-function may be specified as a
    # simple string "foo", which means that the engine will call
    # code_for_foo to generate the function "foo".
    #
    # [And this is *also* not documented. Currently used by fixmp, but
    # there could be better ways]
    #
    # More generally it is possible to specify "foo\@bar" so as to have
    # the engine call code_for_bar for generating the function foo.
    local $SIG{__DIE__} = sub { print "while reading:", Dumper(\@definitions); confess "Improperly formatted code definition for $f :
        @_\n$doc"; };
    my $leader = { name => $f, };
    $leader->{'requirements'} = $api->{'functions'}->{$f} unless $f =~ /^_/;
    my $def = shift @definitions;
    my @gens= @definitions;

    if (ref $def eq 'ARRAY') {
        die "Unsupported !" if ref $def->[0] ne '';
        $leader->{'kind'} = $def->[0],
        $leader->{'code'} = $def->[1],
    } elsif (ref $def eq 'HASH') {
        for my $k (keys %{$def}) {
            if (!defined($def->{'cheat'})
                && defined($leader->{$k})
                && !($k eq 'name' && $leader->{$k} eq $def->{$k}))
            {
                my $extra='';
                if ($k eq 'name') {
                    $extra = " (alternatively, we expect $leader->{$k}, not $def->{$k})";
                }
                print STDERR Dumper($leader->{$k});
                print STDERR Dumper(parse_api_rhs(undef, $def->{$k}));

                die "Field $k is forbidden in first code specification" . $extra;
            }
            $leader->{$k} = $def->{$k};
        }
    } else {
        die "returned value should be a list beginning with array or hash\n";
    }

    # The only occasion where this can happen is when we are calling
    # sub-functions from external modules.
    if (ref $leader->{'requirements'} eq '') {
        $leader->{'requirements'} = parse_api_rhs(undef, $leader->{'requirements'});
    }
    push @functions_to_ship, $leader;

    for my $sub (@gens) {

        debug "3 handling sub-function of $f";

        my $yell = "Improperly formatted code for $f";

        if (ref $sub eq '') {
            die "I think this branch is unused\n";
            debug "2 postponing generation of $sub as a child of $f";
            push @functions_to_ship, $sub;
            next;
        }

        die "$yell: return callees as hashes, please"
            unless ref $sub eq 'HASH';

        if (defined(my $tmpl = $sub->{'template'})) {
            die "$yell : clash for $sub->{'name'}: both template and requirements";
            my $rq = $api->{'functions'}->{$tmpl};
            if (!defined($rq)) {
                debug "4 parsing api rhs for $f // $sub->{'name'}\n";
                $rq = parse_api_rhs(undef, $tmpl);
            }
            $sub->{'requirements'} = $rq;
            delete $sub->{'template'};
        }
        for my $k (qw/kind code name requirements/) {
            $sub->{$k} = $sub->{$k};
            next if $sub->{$k};
            if ($k eq 'requirements') {
                # Does this exist ?
                $sub->{$k} = $api->{'functions'}->{$f} and next;
            }
            die("$yell : sub-function hash lacks $k\n" .
                Dumper($sub));
        }
        if (ref $sub->{'requirements'} eq '') {
            # if it's simply a string, then we consider that it merely
            # needs to be passed through parse_api_rhs
            $sub->{'requirements'} = parse_api_rhs(undef, $sub->{'requirements'});
        }

        push @functions_to_ship, $sub;
    }

    debug "2 done reformatting code generated for $f";
 
    return @functions_to_ship;
}

1;
