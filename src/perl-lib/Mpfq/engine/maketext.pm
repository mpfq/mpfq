package Mpfq::engine::maketext;

use warnings;
use strict;

use Mpfq::engine::conf qw/parse_api_rhs/;
use Mpfq::engine::utils qw/debug/;
use Data::Dumper;

use Storable qw/dclone/;
use Exporter qw(import);
use Carp;
our @EXPORT_OK=qw(build_parameter_list build_source_text simple_indent);


###################################################################
# This function transforms the ulong and uint shorthands in api.pl
# prototypes to fully expanded names. It also translates all types that
# have been defined in api->{'types'}
sub type_substitute_abbrv {
    my ($types_array,$t) = @_;
    for my $x (@{$types_array}) {
        $t =~ s/\b$x\b/@!$x/;
    }
    $t =~ s/\bulong\b/unsigned long/;
    $t =~ s/\buint\b/unsigned int/;
    return $t;
}

###################################################################
# The function build_source_text formats the code for a given function
# into separate text snippets for going in the .h and .c files.
# - the api hash
# - some typical data about the function: name, requirements, kind, code
#
# The data is returned as a hash of text strings, each going to one
# specific logical place. The key is the identifier of the logical
# location (so far, the following exist: h c i vh)

# build_parameter_list does part of the job of build_source_text, but
# actually limits its work to providing the pieces, and not fit them
# together.
sub build_parameter_list
{
    my ($api, $name, $req, $kind) = @_;
    confess "undefined name" unless defined $name;
    my $type_pool = $api->{'types'} || [];
    my $argh = sub { confess "Improperly formatted code definition for $name : @_"; };

    $req = parse_api_rhs(undef,$req) if ref $req eq '';
    die "req for $name are buggy" if !defined($req->{'args'});

    &$argh("undefined kind") unless defined $kind;
    my @params;
    my $type;
    if ($kind eq '') {
        $type='none';
        @params = map { "a$_" } (0..$#{$req->{'args'}});
    } else {
        &$argh("bad kind $kind") if $kind !~ /^(macro|inline|v?function)\((.*)\)\s*$/;
        @params = split /,\s*/, $2;
        $type = $1;
        confess("$name: $kind does not match the required arguments ["
            . join(' ', @{$req->{'args'}}) . "]")
        if (scalar @params != scalar @{$req->{'args'}});
    }

    my $rtype = $req->{'rtype'} || 'void';
    $rtype = type_substitute_abbrv $type_pool, $rtype;
    my $ds = "$rtype @!$name";
    my @proto_par=();
    my @impl_par=();
    for my $i (0..$#params) {
        my $t = $req->{'args'}->[$i];
        my $name = $params[$i];
        $t = type_substitute_abbrv $type_pool, $t;
        push @proto_par, $t;
        $name =~ s/!$/ MAYBE_UNUSED/;
        confess 'Variadic functions must have ... in prototype AND \$kind'
            if (($name eq '...') != ($t eq '...'));
        if ($name eq '...') {
            push @impl_par, "$t";
        } else {
            push @impl_par, "$t $name";
        }
    }

    my @macro_par = @params;
    for (@macro_par) { s/!$//; }

    return $type, $rtype, \@macro_par, \@proto_par, \@impl_par;
}

sub build_source_text
{
    my ($api, $h, $prefix) = @_;
    confess unless ref $h eq 'HASH';
    my $name = $h->{'name'};
    confess 'not a reference' unless $h->{'requirements'};
    my $requirements = $h->{'requirements'};
    if (ref $requirements ne 'HASH') {
        $requirements = parse_api_rhs(undef, $requirements);
    }
    $requirements = dclone($requirements);
    my $kind = $h->{'kind'};
    my $code = $h->{'code'};

    my $pre_name = "@!";

    my @post_subs = (
        [ qr/@!/, "$prefix" ]
    );

    if (defined(my $ta = $h->{'member_template_args'})) {
        # Some work to do here. Because the engine needs different names
        # for the different slots hosting member template instantiations,
        # the code in handler::create_code has to prefix member templates
        # with the list of arguments, and that even goes up to including
        # the implementation tag. Therefore, we will need to mangle the
        # name a bit differently.
        my $a = $requirements->{'args'};
        s/^(\d+)/@!$1/ for @$a;
        my $p0 = $prefix;
        # Take out the implementation tag form the prefix because it's
        # already in the code slot name (done by handler::create_code).
        $p0 =~ s/$ta->[0]_$// or die;
        for my $i (0..$#$ta) {
            # Now that $p0 does not include the tag, we may safely insert
            # the proper tag for each template argument.
            unshift @post_subs, [ qr/@!$i/, "$p0$ta->[$i]_" ];
        }
        # We used to do name mangling here. Now that it's done in
        # handler::create_code, it's no longer needed, really.
        # $p0 .= "${_}_" for @$ta;
        $pre_name = $p0;
    }

    my ($type, $rtype, $macro_par, $proto_par, $impl_par) = build_parameter_list($api, $name, $requirements, $kind);

    my $texts={};

    my $comment = "";

    if (defined(my $t = $h->{'comment'})) {
        my @t = split(/^/m, $t);

        my $x = shift @t;
        $comment .= "/* " . $x;
        for (@t) { $comment .= " * " . $_; }
        $comment .= " *\/\n";
    }

    if (defined($h->{'generator'})) {
        my $a = $h->{'generator'};
        local $SIG{__DIE__} = sub { confess "generator field for $name should be an reference to an array of references to arrays ot strings ; currently we have: " . Dumper($a) . "\nLast error: $@ @_"; };
        die unless ref $a eq 'ARRAY';
        my $t = join(";\n   ", map { join(", ", @$_); } @$a);
        $comment .= "/* $t */\n";
    }
    if (defined($h->{'c_generator'})) {
        my $a = $h->{'c_generator'};
        local $SIG{__DIE__} = sub { confess "c_generator field for $name should be an reference to an array of references to arrays ot strings ; currently we have: " . Dumper($a) . "\nLast error: $@ @_"; };
        die unless ref $a eq 'ARRAY';
        my $t = join(", ", map { join("->", @$_); } grep { scalar @$_ } @$a);
        $comment .= "/* Triggered by: $t */\n" if length $t;
    }


    if ($type eq 'macro') {
        my $ds .= "#define $pre_name$name(" . join(', ', @$macro_par) . ")\t";
        if (defined($code) && $code =~ /\n/m) {
            $code =~ s/$/\t\\/mg;
            $code =~ s/^/\t/mg;
            $code = "\t\\\n" . $code;
            $code =~ s/\t\\\s*$//g;
            $code =~ s/\t\\\s*$/\n/g;
        }
        $ds .= $code || '/**/';
        $ds .= "\n";
        $texts->{'h'} = $comment . $ds;
    } else {
        my $pl_proto = "(" . join(', ', @$proto_par) . ")";
        my $pl_impl = "(" . join(', ', @$impl_par) . ")";
        my $attributes="";
        my $a = $h->{'attributes'};
        if (defined($a)) {
            if (ref $a eq 'ARRAY') {
                $attributes = join(" ", @$a);
            } else {
                $attributes = $a;
            }
        }
        my $proto = "$rtype $pre_name$name$pl_proto$attributes;\n";
        my $impl = "$rtype $pre_name$name$pl_impl\n";
        $impl .= "{\n";
        if ($code) {
            my $foo = $code;
            $foo =~ s/^/\t/gm;
            $foo =~ s/^\s*#/#/gm;
            $impl .= $foo;
            $impl =~ s/\n*$/\n/s;
        }
        $impl .= "}\n\n";

        debug "2 Building $type code for $name";
        if ($type eq 'function') {
            my $pp = '';
            $pp = 'pp' if $name =~ /cxx_/;
            $texts->{'h' . $pp} = $proto;
            $texts->{'c' . $pp} = $comment . $impl;
        } elsif ($type eq 'inline') {
            if ($name =~ /cxx_/) {
                $texts->{'hpp'} = $comment . "static inline\n" . $impl;
            } else {
                $texts->{'h'} = "static inline\n" . $proto;
                $texts->{'i'} = $comment . "static inline\n" . $impl;
            }
        } else {
            $texts->{'vh'} = $proto;
            $texts->{'vc'} = $comment . $impl;
        }
    }
    for my $s (@post_subs) {
        my ($from, $to) = @$s;
        s/$from/$to/g for values %$texts;
    }
    return $texts;
}

sub simple_indent {
    my @text = split(/^/m, shift);
    my $level = 0;
    my $t = "";
    my $tab = "    ";
    for (@text) {
        s/^\s*//;
        s/\s*$//;
        if (/^\s*\}/) {
            $level--; $t = $tab x $level if $level >= 0;
        }
        s/^\s*/$t/;
        s/\s*$//;
        if (/\{$/) {
            $level++; $t = $tab x $level;
        }
    }
    return join("\n", @text);
}

1;
