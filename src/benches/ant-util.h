#ifndef ANT_UTIL_H_TAG
#define ANT_UTIL_H_TAG

/* Define some middle-level utility functions for ideal decomposition */

/* Invoke 64-bit arithmetic */

//#ifdef  __x86_64
//#include "mpfq/mpfq_p_1.h"
//#undef  MPFQ_LAST_GENERATED_TAG
//#define MPFQ_LAST_GENERATED_TAG p_1
//#include "mpfq_rename.h"
//#elif   __i386
//#include "mpfq/mpfq_p_2.h"
//#undef  MPFQ_LAST_GENERATED_TAG
//#define MPFQ_LAST_GENERATED_TAG p_2
//#include "mpfq_rename.h"
//#else
//#error  "Please improve this test"
//#endif

struct mpfq_TAG_mat_s {
    unsigned int m,n;
    mpfq_TAG_elt * x;
};
typedef struct mpfq_TAG_mat_s mpfq_TAG_mat[1];
typedef struct mpfq_TAG_mat_s * mpfq_TAG_mat_ptr;
typedef const struct mpfq_TAG_mat_s * mpfq_TAG_mat_dstptr;


#ifdef __cplusplus
extern "C" {
#endif

void mpfq_TAG_mat_init(mpfq_TAG_field K MAYBE_UNUSED, mpfq_TAG_mat M, unsigned int m, unsigned int n);
void mpfq_TAG_mat_clear(mpfq_TAG_field K, mpfq_TAG_mat M);
void mpfq_TAG_mat_random(mpfq_TAG_field K, mpfq_TAG_mat M, gmp_randstate_t rstate);
void mpfq_TAG_mat_print(mpfq_TAG_field K, mpfq_TAG_mat M);
void mpfq_TAG_mat_det_destroy(mpfq_TAG_field K, mpfq_TAG_dst_elt det, mpfq_TAG_mat M);
unsigned int mpfq_TAG_mat_rank_destroy(mpfq_TAG_field K, mpfq_TAG_mat M);
unsigned int mpfq_TAG_gauss(mpfq_TAG_field K);

#ifdef __cplusplus
}
#endif

#endif	/* ANT_UTIL_H_TAG */
