#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gmp.h>

#include "mpfq_TAG.h"
#include "ant-util_TAG.h"
//#include "../macros.h"

unsigned int mpfq_TAG_gauss(mpfq_TAG_field K)
    /* [ET, 20140314: these //TAG seem to be auto-replaced by bench.pl.
     * But they're apparently doing NOPs !! So let's comment out the
     * whole thing...
     */
{
//TAG   //  mp_limb_t *p MAYBE_UNUSED = K->p;
    mpfq_TAG_mat M;
    gmp_randstate_t rstate;

//TAG   //  mpfq_TAG_field_specify(K, MPFQ_PRIME_MPN, p);
    mpfq_TAG_mat_init(K, M, 5, 5);
    gmp_randinit_default(rstate);
	unsigned int r;

    for(int i = 0 ; i < 20 ; i++) {
        mpfq_TAG_mat_random(K, M, rstate);
//        mpfq_TAG_mat_print(K, M);

        r = mpfq_TAG_mat_rank_destroy(K, M);
//        printf("/* rank == %u */\n", r);
//        mpfq_TAG_mat_print(K, M);
//        printf("\n");
//        printf("\n");
//        printf("\n");
//        printf("\n");
//        printf("\n");
    }

    gmp_randclear(rstate);
    mpfq_TAG_mat_clear(K, M);
    return r;
}

static mpfq_TAG_dst_elt mpfq_TAG_mat_coeff(mpfq_TAG_field K MAYBE_UNUSED, mpfq_TAG_mat M, unsigned int i, unsigned int j)
{
    return M->x[i*M->n+j];
}

/* Add lambda times row i1 to row i0, starting only from column k */
static void mpfq_TAG_mat_addrow_k(mpfq_TAG_field K, mpfq_TAG_mat M, unsigned int i0, unsigned int i1, mpfq_TAG_src_elt lambda, unsigned int k)
{
    mpfq_TAG_elt t;
    mpfq_TAG_init(K, &t);
    for(unsigned int j = k ; j < M->m ; j++) {
        mpfq_TAG_mul(K, t, lambda, mpfq_TAG_mat_coeff(K, M, i1, j));
        mpfq_TAG_add(K, mpfq_TAG_mat_coeff(K, M, i0, j), mpfq_TAG_mat_coeff(K, M, i0, j), t);
    }
    mpfq_TAG_clear(K, &t);
}


/*
static void mpfq_TAG_mat_addrow(mpfq_TAG_field K, mpfq_TAG_mat M, unsigned int i0, unsigned int i1, mpfq_TAG_src_elt lambda)
{
    mpfq_TAG_mat_addrow_k(K, M, i0, i1, lambda, 0);
}
*/


/* Swap rows i0 and i1, starting only from column k */
static void mpfq_TAG_mat_swaprows_k(mpfq_TAG_field K, mpfq_TAG_mat M, unsigned int i0, unsigned int i1, unsigned int k)
{
    mpfq_TAG_elt t;
    mpfq_TAG_init(K, &t);
    for(unsigned int j = k ; j < M->m ; j++) {
        /* FIXME: Should we add a swap() to mpfq ? */
        mpfq_TAG_set(K, t, mpfq_TAG_mat_coeff(K, M, i0, j));
        mpfq_TAG_set(K, mpfq_TAG_mat_coeff(K, M, i0, j), mpfq_TAG_mat_coeff(K, M, i1, j));
        mpfq_TAG_set(K, mpfq_TAG_mat_coeff(K, M, i1, j), t);
    }
    mpfq_TAG_clear(K, &t);
}

/*
static void mpfq_TAG_mat_swaprows(mpfq_TAG_field K, mpfq_TAG_mat M, unsigned int i0, unsigned int i1)
{
    mpfq_TAG_mat_swaprows_k(K, M, i0, i1, 0);
}
*/


/* det isn't really the determinant, but rather the product of all
 * non-zero diagonal coefficients.
 *
 * return the matrix rank */
unsigned int mpfq_TAG_mat_gauss_helper(mpfq_TAG_field K, mpfq_TAG_dst_elt det, mpfq_TAG_mat M)
{
    unsigned int rank = 0;
    if (det) mpfq_TAG_set_ui(K, det, 1);
    for(unsigned int j = 0 ; j < M->n ; j++) {
        /* Find a non-zero element in this column. We intend to start at
         * i==rank, since all non-zero pivots have been moved to rows
         * above that.
         */
        unsigned int ipiv;
        mpfq_TAG_dst_elt piv;

        for(ipiv = rank ; ipiv < M->m ; ipiv++) {
            piv = mpfq_TAG_mat_coeff(K, M, ipiv, j);
            if (!mpfq_TAG_is_zero(K, piv))
                break;
        }

        if (ipiv < M->m) {
          /* Cancel this coefficient in all relevant rows */
          mpfq_TAG_elt lambda, mu;
          mpfq_TAG_init(K, &lambda);
          mpfq_TAG_init(K, &mu);
          mpfq_TAG_inv(K, lambda, piv);
          mpfq_TAG_neg(K, lambda, lambda);
          for(unsigned int i = 0 ; i < rank ; i++) {
              mpfq_TAG_mul(K, mu, mpfq_TAG_mat_coeff(K, M, i, j), lambda);
              mpfq_TAG_set_zero(K, mpfq_TAG_mat_coeff(K, M, i, j));
              mpfq_TAG_mat_addrow_k(K, M, i, ipiv, mu, j+1);
          }
          for(unsigned int i = ipiv + 1 ; i < M->m ; i++) {
              mpfq_TAG_mul(K, mu, mpfq_TAG_mat_coeff(K, M, i, j), lambda);
              mpfq_TAG_set_zero(K, mpfq_TAG_mat_coeff(K, M, i, j));
              mpfq_TAG_mat_addrow_k(K, M, i, ipiv, mu, j+1);
          }
          // mpfq_TAG_set_ui(K, piv, 1);
          if (ipiv != rank) {
              mpfq_TAG_mat_swaprows_k(K, M, rank, ipiv, j);
          }
          if (det) mpfq_TAG_mul(K, det, det, piv);
          rank++;
          mpfq_TAG_clear(K, &lambda);
          mpfq_TAG_clear(K, &mu);
        }
    }
    return rank;
}

/* destroys M */
void mpfq_TAG_mat_det_destroy(mpfq_TAG_field K, mpfq_TAG_dst_elt det, mpfq_TAG_mat M)
{
    if (M->m != M->n) {
        mpfq_TAG_set_zero(K, det);
        return;
    }
    unsigned int rank = mpfq_TAG_mat_gauss_helper(K, det, M);
    if (rank < M->n) {
        mpfq_TAG_set_zero(K, det);
    }
}

unsigned int mpfq_TAG_mat_rank_destroy(mpfq_TAG_field K, mpfq_TAG_mat M)
{
    return mpfq_TAG_mat_gauss_helper(K, NULL, M);
}

void mpfq_TAG_mat_print(mpfq_TAG_field K, mpfq_TAG_mat M)
{
    for(unsigned int i = 0 ; i < M->m ; i++) {
        printf("%c[ ",i?' ':'[');
        for(unsigned int j = 0 ; j < M->n ; j++) {
            if (j) printf(", ");
            mpfq_TAG_print(K, mpfq_TAG_mat_coeff(K, M, i, j));
        }
        printf("]%c\n", i==(M->m-1) ? ']' : ',');
    }
}

void mpfq_TAG_mat_random(mpfq_TAG_field K, mpfq_TAG_mat M, gmp_randstate_t rstate)
{
    for(unsigned int i = 0 ; i < M->m ; i++) {
        for(unsigned int j = 0 ; j < M->n ; j++) {
            mpfq_TAG_random(K, mpfq_TAG_mat_coeff(K, M, i, j), rstate);
        }
    }
}

void mpfq_TAG_mat_init(mpfq_TAG_field K MAYBE_UNUSED, mpfq_TAG_mat M, unsigned int m, unsigned int n)
{
    M->m = m;
    M->n = n;
    M->x = malloc(m*n*sizeof(mpfq_TAG_elt));
}

void mpfq_TAG_mat_clear(mpfq_TAG_field K MAYBE_UNUSED, mpfq_TAG_mat M)
{
    free(M->x);
//    memset(M, 0, sizeof(M));
}
