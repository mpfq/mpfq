#!/usr/bin/perl

use strict;
use warnings;

my $keep;
my $srcpath = "";
my $cflags = "-msse2 -msse4.2 -DNDEBUG -I../fixmp -I../gfp -I../gf2n/ -I../gf2n -I../include -O4 -W";
my $ldflags = "-lgmp";
my $tmpdir="/tmp";

while (scalar @ARGV && $ARGV[0] =~ /^-/) {
    my $x = shift @ARGV;
    if ($x eq '--keep') {
        $keep = 1;
    } elsif ($x eq '--srcpath') {
        $srcpath = shift @ARGV or die "Give me srcpath!\n";
    } elsif ($x eq '--cflags') {
        $cflags = shift @ARGV or die "Give me cflags!\n";
    } elsif ($x eq '--ldflags') {
        $ldflags = shift @ARGV or die "Give me ldflags!\n";
    } elsif ($x eq '--tmpdir') {
        $tmpdir = shift @ARGV or die "Give me tmpdir!\n";
        if (! -d $tmpdir) {
            mkdir $tmpdir or die "mkdir($tmpdir): $!";
        }
    } elsif ($x eq '--') {
        last;
    } else {
        die "Unexpected option $x\n";
    }
}


my @tags = ( 
  {TAG => "p_127_735", TAGP => "0", IMPLEM_FILE => "../gfp/mpfq_p_127_735.c"},
  {TAG => "p_127_1", TAGP => "0", IMPLEM_FILE => "../gfp/mpfq_p_127_1.c"},
  {TAG => "p_25519", TAGP => "0", IMPLEM_FILE => "../gfp/mpfq_p_25519.c"},
  {TAG => "p_1", TAGP => "0xD5FC2153", IMPLEM_FILE => "../gfp/mpfq_p_1.c"},
  {TAG => "p_0_5", TAGP => "0xD5FC2153", IMPLEM_FILE => "../gfp/mpfq_p_0_5.c"},
  {TAG => "p_2", TAGP => "0x6B1BC00DA369935A59364ED9", IMPLEM_FILE => "../gfp/mpfq_p_2.c"},
  {TAG => "p_1_5", TAGP => "0x6B1BC00DA369935A59364ED9", IMPLEM_FILE => "../gfp/mpfq_p_1_5.c"},
  {TAG => "p_3", TAGP => "0x3AE76B172E88D4C58B1EC661EC9EBC7104AC0321", IMPLEM_FILE => "../gfp/mpfq_p_3.c"},
  {TAG => "p_2_5", TAGP => "0x3AE76B172E88D4C58B1EC661EC9EBC7104AC0321", IMPLEM_FILE => "../gfp/mpfq_p_2_5.c"},
  {TAG => "p_4", TAGP => "0xEB3A23D78D71E235FB4CFA742583371F27221AFE1E9ED02774D4FDBD", IMPLEM_FILE => "../gfp/mpfq_p_4.c"},
  {TAG => "p_3_5", TAGP => "0xEB3A23D78D71E235FB4CFA742583371F27221AFE1E9ED02774D4FDBD", IMPLEM_FILE => "../gfp/mpfq_p_3_5.c"},
  {TAG => "p_5", TAGP => "0xC69D2969FA20C0072562403C66B626A5CCB5BF1CBA6DBFFA6D0070877AAD86908F7159AF", IMPLEM_FILE => "../gfp/mpfq_p_5.c"},
  {TAG => "p_4_5", TAGP => "0xC69D2969FA20C0072562403C66B626A5CCB5BF1CBA6DBFFA6D0070877AAD86908F7159AF", IMPLEM_FILE => "../gfp/mpfq_p_4_5.c"},
  {TAG => "p_6", TAGP => "0xFFD1B9289D35C50DFCE42193267198E9A8963B8284544257820251705B1409A00A4E56E12F4A20D37AB8F657", IMPLEM_FILE => "../gfp/mpfq_p_6.c"},
  {TAG => "p_5_5", TAGP => "0xFFD1B9289D35C50DFCE42193267198E9A8963B8284544257820251705B1409A00A4E56E12F4A20D37AB8F657", IMPLEM_FILE => "../gfp/mpfq_p_5_5.c"},
  {TAG => "p_7", TAGP => "0xC471BF8FD994F2D51A953CBC9ECF20A90CF30589D2E10535FAE08560BBDBFF834CF9BEDB65D38AE0ED1321CE52EA81EE25DDC14B", IMPLEM_FILE => "../gfp/mpfq_p_7.c"},
  {TAG => "p_6_5", TAGP => "0xC471BF8FD994F2D51A953CBC9ECF20A90CF30589D2E10535FAE08560BBDBFF834CF9BEDB65D38AE0ED1321CE52EA81EE25DDC14B", IMPLEM_FILE => "../gfp/mpfq_p_6_5.c"},
  {TAG => "p_8", TAGP => "0x17FBDB7F9BC45F29DD5AA544C61B344B35582AEF997CD2D4BBABC2F89D7BFBE4A2C508A960989FC0329E4C812B36111DE7FD03CF91421A66E1D53635", IMPLEM_FILE => "../gfp/mpfq_p_8.c"},
  {TAG => "p_7_5", TAGP => "0x17FBDB7F9BC45F29DD5AA544C61B344B35582AEF997CD2D4BBABC2F89D7BFBE4A2C508A960989FC0329E4C812B36111DE7FD03CF91421A66E1D53635", IMPLEM_FILE => "../gfp/mpfq_p_7_5.c"},
  {TAG => "p_9", TAGP => "0x8BE39F717E1B1D46F30296D19BF6F9F1ED99B748EBBE42F49933977B7ECF802E2640F150862030702608009AFD507EB28D7F072750A8A4DEEDB0CFE93420295B54586FB9", IMPLEM_FILE => "../gfp/mpfq_p_9.c"},
  {TAG => "p_8_5", TAGP => "0x8BE39F717E1B1D46F30296D19BF6F9F1ED99B748EBBE42F49933977B7ECF802E2640F150862030702608009AFD507EB28D7F072750A8A4DEEDB0CFE93420295B54586FB9", IMPLEM_FILE => "../gfp/mpfq_p_8_5.c"},
#  {TAG => "p_1", TAGP => "0x7648E6CD57FAAC37", #  IMPLEM_FILE => "../gfp/mpfq_p_1.c"},
#  {TAG => "p_2", TAGP => "0x25564CB42C55E730E58DF47CED19AA55", #  IMPLEM_FILE => "../gfp/mpfq_p_2.c"},
#  {TAG => "p_3", TAGP => "0x675060314A63D000DB3A404C03A5EEDAF050E13ED536DB0B", #  IMPLEM_FILE => "../gfp/mpfq_p_3.c"},
#  {TAG => "p_4", TAGP => "0x299E8E0CB293540AEBEC8D914A0E3F70CADF6E8B3628D9513EF52772406C1EF9", #  IMPLEM_FILE => "../gfp/mpfq_p_4.c"},
#  {TAG => "p_5", TAGP => "0x7D98B5A9851AEF01CB4F9AFFB79EAB5855D4494D9D37BF371C62E589CBC9F86CE30A698F592AF1C5", #  IMPLEM_FILE => "../gfp/mpfq_p_5.c"},
#  {TAG => "p_6", TAGP => "0x45C6D4A2377FEDE9D70376EDB663218B5672CC55FAFD718BAFE222C29DA4BD4D825E22C37A8C2B0C1A35CDC1AEB8F53", #  IMPLEM_FILE => "../gfp/mpfq_p_6.c"},
#  {TAG => "p_7", TAGP => "0x2317755911D47B81CE899257B2B28D52A47E8A10B1AB4F265B902CFA1E45BFD291A3FB05B615EAA5307D2E40EC3A94E78B6CA79DA6E6468D", #  IMPLEM_FILE => "../gfp/mpfq_p_7.c"},
#  {TAG => "p_8", TAGP => "0x138D854317BE6CC627E2058BC2DEEEDEB2F37CB5BC57FE181A0B53925EF2DF40BB50A71C1B1182E7054D9E48F3CE2AC9389DE2F8F377195AC31716C63E0F1527", #  IMPLEM_FILE => "../gfp/mpfq_p_8.c"},
#  {TAG => "p_9", TAGP => "0x70C7C3ACBCC7F83C5F1A18B92F629EA80B4CE86841B76EB8D71B784C441CA399FC19D68E30C5465B038E7B663328B7B0FD06A54592F84BA9E163E577B13F0B41A3DBB1D89AFF3879", #  IMPLEM_FILE => "../gfp/mpfq_p_9.c"},
  {TAG => "pm_1", TAGP => "0xD5FC2153", IMPLEM_FILE => "../gfp/mpfq_pm_1.c"},
  {TAG => "pm_0_5", TAGP => "0xD5FC2153", IMPLEM_FILE => "../gfp/mpfq_pm_0_5.c"},
  {TAG => "pm_2", TAGP => "0x6B1BC00DA369935A59364ED9", IMPLEM_FILE => "../gfp/mpfq_pm_2.c"},
  {TAG => "pm_1_5", TAGP => "0x6B1BC00DA369935A59364ED9", IMPLEM_FILE => "../gfp/mpfq_pm_1_5.c"},
  {TAG => "pm_3", TAGP => "0x3AE76B172E88D4C58B1EC661EC9EBC7104AC0321", IMPLEM_FILE => "../gfp/mpfq_pm_3.c"},
  {TAG => "pm_2_5", TAGP => "0x3AE76B172E88D4C58B1EC661EC9EBC7104AC0321", IMPLEM_FILE => "../gfp/mpfq_pm_2_5.c"},
  {TAG => "pm_4", TAGP => "0xEB3A23D78D71E235FB4CFA742583371F27221AFE1E9ED02774D4FDBD", IMPLEM_FILE => "../gfp/mpfq_pm_4.c"},
  {TAG => "pm_3_5", TAGP => "0xEB3A23D78D71E235FB4CFA742583371F27221AFE1E9ED02774D4FDBD", IMPLEM_FILE => "../gfp/mpfq_pm_3_5.c"},
  {TAG => "pm_5", TAGP => "0xC69D2969FA20C0072562403C66B626A5CCB5BF1CBA6DBFFA6D0070877AAD86908F7159AF", IMPLEM_FILE => "../gfp/mpfq_pm_5.c"},
  {TAG => "pm_4_5", TAGP => "0xC69D2969FA20C0072562403C66B626A5CCB5BF1CBA6DBFFA6D0070877AAD86908F7159AF", IMPLEM_FILE => "../gfp/mpfq_pm_4_5.c"},
  {TAG => "pm_6", TAGP => "0xFFD1B9289D35C50DFCE42193267198E9A8963B8284544257820251705B1409A00A4E56E12F4A20D37AB8F657", IMPLEM_FILE => "../gfp/mpfq_pm_6.c"},
  {TAG => "pm_5_5", TAGP => "0xFFD1B9289D35C50DFCE42193267198E9A8963B8284544257820251705B1409A00A4E56E12F4A20D37AB8F657", IMPLEM_FILE => "../gfp/mpfq_pm_5_5.c"},
  {TAG => "pm_7", TAGP => "0xC471BF8FD994F2D51A953CBC9ECF20A90CF30589D2E10535FAE08560BBDBFF834CF9BEDB65D38AE0ED1321CE52EA81EE25DDC14B", IMPLEM_FILE => "../gfp/mpfq_pm_7.c"},
  {TAG => "pm_6_5", TAGP => "0xC471BF8FD994F2D51A953CBC9ECF20A90CF30589D2E10535FAE08560BBDBFF834CF9BEDB65D38AE0ED1321CE52EA81EE25DDC14B", IMPLEM_FILE => "../gfp/mpfq_pm_6_5.c"},
  {TAG => "pm_8", TAGP => "0x17FBDB7F9BC45F29DD5AA544C61B344B35582AEF997CD2D4BBABC2F89D7BFBE4A2C508A960989FC0329E4C812B36111DE7FD03CF91421A66E1D53635", IMPLEM_FILE => "../gfp/mpfq_pm_8.c"},
  {TAG => "pm_7_5", TAGP => "0x17FBDB7F9BC45F29DD5AA544C61B344B35582AEF997CD2D4BBABC2F89D7BFBE4A2C508A960989FC0329E4C812B36111DE7FD03CF91421A66E1D53635", IMPLEM_FILE => "../gfp/mpfq_pm_7_5.c"},
  {TAG => "pm_9", TAGP => "0x8BE39F717E1B1D46F30296D19BF6F9F1ED99B748EBBE42F49933977B7ECF802E2640F150862030702608009AFD507EB28D7F072750A8A4DEEDB0CFE93420295B54586FB9", IMPLEM_FILE => "../gfp/mpfq_pm_9.c"},
  {TAG => "pm_8_5", TAGP => "0x8BE39F717E1B1D46F30296D19BF6F9F1ED99B748EBBE42F49933977B7ECF802E2640F150862030702608009AFD507EB28D7F072750A8A4DEEDB0CFE93420295B54586FB9", IMPLEM_FILE => "../gfp/mpfq_pm_8_5.c"},
#  {TAG => "pm_1", TAGP => "0x7648E6CD57FAAC37", #  IMPLEM_FILE => "../gfp/mpfq_pm_1.c"},
#  {TAG => "pm_2", TAGP => "0x25564CB42C55E730E58DF47CED19AA55", #  IMPLEM_FILE => "../gfp/mpfq_pm_2.c"},
#  {TAG => "pm_3", TAGP => "0x675060314A63D000DB3A404C03A5EEDAF050E13ED536DB0B", #  IMPLEM_FILE => "../gfp/mpfq_pm_3.c"},
#  {TAG => "pm_4", TAGP => "0x299E8E0CB293540AEBEC8D914A0E3F70CADF6E8B3628D9513EF52772406C1EF9", #  IMPLEM_FILE => "../gfp/mpfq_pm_4.c"},
#  {TAG => "pm_5", TAGP => "0x7D98B5A9851AEF01CB4F9AFFB79EAB5855D4494D9D37BF371C62E589CBC9F86CE30A698F592AF1C5", #  IMPLEM_FILE => "../gfp/mpfq_pm_5.c"},
#  {TAG => "pm_6", TAGP => "0x45C6D4A2377FEDE9D70376EDB663218B5672CC55FAFD718BAFE222C29DA4BD4D825E22C37A8C2B0C1A35CDC1AEB8F53", #  IMPLEM_FILE => "../gfp/mpfq_pm_6.c"},
#  {TAG => "pm_7", TAGP => "0x2317755911D47B81CE899257B2B28D52A47E8A10B1AB4F265B902CFA1E45BFD291A3FB05B615EAA5307D2E40EC3A94E78B6CA79DA6E6468D", #  IMPLEM_FILE => "../gfp/mpfq_pm_7.c"},
#  {TAG => "pm_8", TAGP => "0x138D854317BE6CC627E2058BC2DEEEDEB2F37CB5BC57FE181A0B53925EF2DF40BB50A71C1B1182E7054D9E48F3CE2AC9389DE2F8F377195AC31716C63E0F1527", #  IMPLEM_FILE => "../gfp/mpfq_pm_8.c"},
#  {TAG => "pm_9", TAGP => "0x70C7C3ACBCC7F83C5F1A18B92F629EA80B4CE86841B76EB8D71B784C441CA399FC19D68E30C5465B038E7B663328B7B0FD06A54592F84BA9E163E577B13F0B41A3DBB1D89AFF3879", #  IMPLEM_FILE => "../gfp/mpfq_pm_9.c"},

);

sub get_cpuinfo {
        open F, "</proc/cpuinfo" or die "no cpuinfo";
        my $cpuinfo = {};
        while (defined($_ = <F>)) {
                /^\s*$/ && next;
                /^([\w\s]*)\b\s*:\s*(.*)$/ || die "Bad format: $_";
                $cpuinfo->{$1} = $2;
        }
        close F;
        return $cpuinfo;
}

my $cpu = get_cpuinfo();
if (defined($cpu->{'cpu MHz'})) {
	print STDERR "CPU at $cpu->{'cpu MHz'} MHz\n";
}

my $i;

for ($i = 2; $i < 256; $i++) {
  my $field = {TAG => "2_" . $i, TAGP => "0", IMPLEM_FILE => "../gf2n/mpfq_2_" . $i . ".c"};
  push(@tags, $field);
}

my %corresp = ();
for my $x (@tags) {
	$corresp{$x->{TAG}} = $x;
}
if (scalar(@ARGV)) {
	@tags = map {$corresp{$_}} @ARGV;
}

my $cpt = 5;

for ($i = 0; $i < scalar(@tags); $i++) {

  if (!-f $tags[$i]{IMPLEM_FILE}) {
      print STDERR "$tags[$i]{IMPLEM_FILE} does not exist -- bench skipped\n";
      next;
  }

  my $f_in;
  my $f_out;
  my $line;
  
  my $file;
  my @file_list=("fieldop.c","ant-util.h","ant-util.c");
  my @ofn_list=();

  foreach $file (@file_list) {

    open $f_in, "${srcpath}/benches/" . $file or die "Can't open " . $file;
    my $ext = chop($file);
    $ext = chop($file) . $ext;

    if (($ext ne ".h") and ($ext ne ".c")) {
      die "Not correct extension";
    }
    my $ofn = "${tmpdir}/" . $file . "_" . $tags[$i]{TAG} . $ext;
    open $f_out, ">$ofn" or die "Can't open output file ${ofn}";
    
    while (defined($line = <$f_in>)) {
      $line =~ s/TAGP/"$tags[$i]{TAGP}"/g;
      $line =~ s/TAG/$tags[$i]{TAG}/g;
      print $f_out $line;
    }
  
    close $f_out;
    close $f_in;
    if ($ext eq ".c") {
      push(@ofn_list,$ofn);
    }
  }


  my $pipe;
  my $binary =  "${tmpdir}/bench-$i-$$";
  print $binary;
  my $cmd = join(' ', "gcc", "-o $binary",
	  	$cflags,$tags[$i]{IMPLEM_FILE},$tags[$i]{CFLAGS}||"",join(' ',@ofn_list),$ldflags);
  print STDERR $cmd . "\n";
  system $cmd;

  open $pipe, "$binary " . $cpt . " |";
  my %res = (
    "add" => 0,
    "sub" => 0,
    "mul_ur" => 0,
    "sqr_ur" => 0,
    "mul" => 0,
    "sqr" => 0,
    "inv" => 0,
    "gauss" => 0,
  );

  my $name;
  my $val;
  while (defined($line = <$pipe>)) {
    ($name, $val) = split(/ /, $line);
    $res{$name} += $val;
  }
  print "Result for mpfq_" . $tags[$i]{TAG} . " (in microseconds and cycles):\n";
  for my $x (keys(%res)) {
    my $s = $tags[$i]{TAG}. " " . $x . " " . $res{$x}/$cpt;
    if (defined($cpu->{'cpu MHz'})) {
	    my $ncyc = int($res{$x}/$cpt * $cpu->{'cpu MHz'});
	    $s .= " $ncyc";
    }
    print "$s\n";
  }

  unless ($keep) {
	  unlink @ofn_list;
	  unlink $binary;
  }
}


