
include_directories(${PROJECT_BINARY_DIR}/include)
include_directories(${PROJECT_BINARY_DIR}/gfp)
include_directories(${PROJECT_BINARY_DIR}/gfpe)
include_directories(${PROJECT_BINARY_DIR}/gf2n)
include_directories(${PROJECT_BINARY_DIR}/rns)
include_directories(${PROJECT_SOURCE_DIR}/gfp)
include_directories(${PROJECT_SOURCE_DIR}/gfpe)
include_directories(${PROJECT_SOURCE_DIR}/gf2n)
include_directories(${PROJECT_SOURCE_DIR}/rns)

set(simple_generator_extra_dependencies ../api.pl)

simple_generator(${CMAKE_CURRENT_BINARY_DIR}/mpfq_name_K.h ./mk_name_k.pl api_file=../api.pl name=K)

SET(ALL_PE_CHECKS)
SET(ALL_P_CHECKS)
SET(ALL_2_CHECKS)
SET(ALL_RNS_CHECKS)
add_custom_target(all_test_dependencies)

macro(gen_testcode tag)
    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/mpfq_test_${tag}.cpp
        COMMAND sed -e 's/TAG/${tag}/g' -e 's,PATH_TO_SRC_TEST_DIRECTORY,${CMAKE_CURRENT_SOURCE_DIR}/,' < mpfq_test.cpp.meta >
        ${CMAKE_CURRENT_BINARY_DIR}/mpfq_test_${tag}.cpp
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        DEPENDS mpfq_test.cpp.meta
        )
    add_executable(mpfq_test_${tag} EXCLUDE_FROM_ALL
        ${CMAKE_CURRENT_BINARY_DIR}/mpfq_test_${tag}.cpp
        ${CMAKE_CURRENT_BINARY_DIR}/mpfq_name_K.h
    )
    set(addflags "${test_cflags_${tag}} ${ARGN}")
    if(addflags STRGREATER "")
        set_target_properties(mpfq_test_${tag} PROPERTIES COMPILE_FLAGS "${addflags}")
    endif(addflags STRGREATER "")
    add_test(builddep_check-${tag}
        ${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR} --target mpfq_test_${tag}
    )
    add_dependencies(all_test_dependencies mpfq_test_${tag})
    add_test(NAME check-${tag}
        COMMAND  ${CMAKE_CURRENT_BINARY_DIR}/mpfq_test_${tag}
    )
    set_tests_properties (check-${tag} PROPERTIES DEPENDS builddep_check-${tag})
    target_link_libraries(mpfq_test_${tag} gmp mpfq_${tag})
endmacro(gen_testcode)

set(test_cflags_p_25519     "-DFIX_PRIME")
# register allocator can't cope with the inline assembly for these
# two fields.
set(test_cflags_p_127_735   "-DFIX_PRIME -O2")
set(test_cflags_p_127_1     "-DFIX_PRIME -O2")

foreach(line ${all_fields})
    STRING(REGEX REPLACE "^ *([^ ]+)(.*)" "\\1;\\2" taillist ${line})
    list(GET taillist 0 tag)
    if(tag MATCHES "pz")
         gen_testcode(${tag} "-DVARIABLE_SIZE_PRIME")
         list(APPEND ALL_P_CHECKS check-${tag})

#          set(etag "${tag}_e")
#          gen_testcode(${etag} "-DVARIABLE_SIZE_PRIME -DEXTENSION_OF_GFP -DBFIELD=${tag}")
#          target_link_libraries(mpfq_test_${etag} mpfq_${tag})
#          list(APPEND ALL_PE_CHECKS check-${etag})
    endif(tag MATCHES "pz")
    if(tag MATCHES "^pm?_[0-9]+$")
        gen_testcode(${tag})
        list(APPEND ALL_P_CHECKS check-${tag})

#         set(etag "${tag}_e")
#         gen_testcode(${etag} "-DEXTENSION_OF_GFP -DBFIELD=${tag}")
#         target_link_libraries(mpfq_test_${etag} mpfq_${tag})
#         list(APPEND ALL_PE_CHECKS check-${etag})
    endif(tag MATCHES "^pm?_[0-9]+$")
    if(tag MATCHES "^pm?_[0-9]+_5$")
        set(etag "${tag}_e")
        gen_testcode(${tag}  "-DHALF_WORD")
        list(APPEND ALL_P_CHECKS check-${tag})

#         gen_testcode(${etag} "-DHALF_WORD -DEXTENSION_OF_GFP -DBFIELD=${tag}")
#         target_link_libraries(mpfq_test_${etag} mpfq_${tag})
#         list(APPEND ALL_PE_CHECKS check-${etag})
    endif(tag MATCHES "^pm?_[0-9]+_5$")
    if(tag MATCHES "^2_[0-9]+$")
        gen_testcode(${tag} -DCHAR2)
        list(APPEND ALL_2_CHECKS check-${tag})
    endif(tag MATCHES "^2_[0-9]+$")
#     if(tag MATCHES "^rns_[0-9]+$")
# 	gen_testcode(${tag} "-DNOT_URE -DRNS")
# 	list(APPEND ALL_RNS_CHECKS check-${tag})
#     endif(tag MATCHES "^rns_[0-9]+$")
endforeach(line)

# add_custom_target(check-gfp)
# add_dependencies(check-gfp ${ALL_P_CHECKS})
# 
# add_custom_target(check-gfpe)
# add_dependencies(check-gfpe ${ALL_PE_CHECKS})
# 
# add_custom_target(check-gf2n)
# add_dependencies(check-gf2n ${ALL_2_CHECKS})
# 
# 
# add_custom_target(check-rns)
# add_dependencies(check-rns ${ALL_RNS_CHECKS})
# 
# add_custom_target(check)
# add_dependencies(check check-fixmp check-gfp check-gfpe check-gf2n)

add_custom_target(check COMMAND ${CMAKE_CTEST_COMMAND} -V $(ARGS))
