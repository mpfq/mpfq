package p25519;

use strict;
use warnings;

use Mpfq::engine::handler;

use Mpfq::gfp;

our @parents = qw/
    Mpfq::gfp
/;

our @ISA = qw/Mpfq::engine::handler/;

sub new { return bless({}, shift); }

sub code_for_impl_max_characteristic_bits { return [ 'macro()', '255' ]; }
sub code_for_impl_max_degree { return [ 'macro()', '1' ]; }

sub code_for_field_specify {
    return [ 'macro(k,dummy,vp)' , '' ]; 
}

sub code_for_field_init { 
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $n = $opt->{'n'};
    my $nn = $opt->{'nn'};
    my $base = &Mpfq::gfp::field::code_for_field_init($opt, @_);
    $base->[1] .= <<EOF;
mpz_init(k->factor);
k->io_base = 10;
k->ts_info.e=0;
mpz_ui_pow_ui(k->p, 2, 255);
mpz_sub_ui(k->p, k->p, 19);
EOF
    $base->[1] .= &Mpfq::gfp::field::code_fragment_set_bp_from_p($opt, @_);
    return $base;
}

## This reduce code does not work for a elt_ur, since it deals only with
## inputs of size 2*$n.
sub code_for_reduce_2n {
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $n = $opt->{'n'};
    my $nn = $opt->{'nn'};
    my $proto = 'inline(k,z,x)';
    my $code = <<EOF;
mp_limb_t tmp[$n+1];
mp_limb_t c;
int i;
for (i = 0; i < $n; ++i)
    tmp[i] = x[i];
tmp[$n] = 0UL;
mpfq_fixmp_${n}_addmul1_nc(tmp, x+$n, 38UL);

c = (tmp[$n] << 1) | (tmp[$n-1] >> ($w-1));
c *= 19UL;

tmp[$n-1] &= ((-1UL) >> 1);  // kill last bit.
c = mpn_add_1(z, tmp, $n, c);
assert (c == 0UL);

if (mpfq_fixmp_${n}_cmp(z,k->p->_mp_d)>=0)
    mpfq_fixmp_${n}_sub(z, z, k->p->_mp_d);
EOF
    return { 'kind'=>$proto,
        'code'=>$code,
        'name'=>'reduce_2n',
        'requirements'=>'dst_field dst_elt mp_limb_t*'};
}

sub code_for_mul {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
mp_limb_t tmp[2*$n];
mpfq_fixmp_${n}_mul(tmp, x, y);
@!reduce_2n(k, z, tmp);
EOF
    return [ $proto, $code ], code_for_reduce_2n($opt);
}

sub code_for_sqr {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $proto = 'inline(k,z,x)';
    my $code = <<EOF;
mp_limb_t tmp[2*$n];
mpfq_fixmp_${n}_sqr(tmp, x);
@!reduce_2n(k, z, tmp);
EOF
    return [ $proto, $code ];
}

sub code_for_add {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $code = <<EOF;
#if defined(x86_64) && defined(__GNUC__)
  __asm__ volatile (
    "   ### Add s1 and s2 to tmp=[r8,r9,r10,r11]\\n"
    "   movq    %1, %%rax\\n"
    "   movq    %2, %%r15\\n"
    "   movq    (%%rax), %%r8\\n"
    "   addq    (%%r15), %%r8\\n"
    "   movq    8(%%rax), %%r9\\n"
    "   adcq    8(%%r15), %%r9\\n"
    "   movq    16(%%rax), %%r10\\n"
    "   adcq    16(%%r15), %%r10\\n"
    "   movq    24(%%rax), %%r11\\n"
    "   adcq    24(%%r15), %%r11\\n"

    "   ### Subtract p to tmp and put a copy of tmp in tmp2=[r12,r13,r14,r15]\\n"
    "   ### (in fact, add 2^256-p)\\n"
    "   movq    %%r8, %%r12\\n"
    "   addq    \$19, %%r8\\n"
    "   movq    %%r9, %%r13\\n"
    "   adcq    \$0, %%r9\\n"
    "   movq    %%r10, %%r14\\n"
    "   adcq    \$0, %%r10\\n"
    "   movq    \$9223372036854775808, %%rax\\n"
    "   movq    %%r11, %%r15\\n"
    "   adcq    %%rax, %%r11\\n"
    
    "   ### CMOVS and copy to result\\n"
    "   movq    %0, %%rax\\n"
    "   cmovnc  %%r12, %%r8\\n"
    "   cmovnc  %%r13, %%r9\\n"
    "   cmovnc  %%r14, %%r10\\n"
    "   cmovnc  %%r15, %%r11\\n"
    "   movq    %%r8, (%%rax)\\n"
    "   movq    %%r9, 8(%%rax)\\n"
    "   movq    %%r10, 16(%%rax)\\n"
    "   movq    %%r11, 24(%%rax)\\n"
  : "+m" (z)
  : "m" (x), "m" (y)
  : "%rax", "%r8", "%r9", "%r10", "%r11", "%r12", "%r13", "%r14", "%r15", "memory");
#else
  mpfq_fixmp_${n}_add_nc(z, x, y);
  if (mpfq_fixmp_${n}_cmp(z, k->p->_mp_d)>=0) 
    mpfq_fixmp_${n}_sub_nc(z, z, k->p->_mp_d);
#endif
EOF
    return [ 'inline(k!,z,x,y)', $code ];
}

sub code_for_sub {
    my $opt = shift @_;
    my $n = $opt->{'n'};
    my $code = <<EOF;
#if defined(x86_64) && defined(__GNUC__)
  __asm__ volatile (
    "   ### Sub y to x and put result in tmp=[r8,r9,r10,r11]\\n"
    "   movq    %1, %%rax\\n"
    "   movq    %2, %%r15\\n"
    "   xorq    %%r12, %%r12\\n"
    "   xorq    %%r13, %%r13\\n"
    "   xorq    %%r14, %%r14\\n"
    "   movq    (%%rax), %%r8\\n"
    "   subq    (%%r15), %%r8\\n"
    "   movq    8(%%rax), %%r9\\n"
    "   sbbq    8(%%r15), %%r9\\n"
    "   movq    16(%%rax), %%r10\\n"
    "   sbbq    16(%%r15), %%r10\\n"
    "   movq    24(%%rax), %%r11\\n"
    "   sbbq    24(%%r15), %%r11\\n"
    "   ### Get p or 0 according to carry\\n"
    "   movq    \$0, %%r15\\n"
    "   movq    %3, %%rax\\n"
    "   cmovc   (%%rax), %%r12\\n"
    "   cmovc   8(%%rax), %%r13\\n"
    "   cmovc   16(%%rax), %%r14\\n"
    "   cmovc   24(%%rax), %%r15\\n"

    "   ### add p or 0\\n"
    "   movq    %0, %%rax\\n"
    "   addq    %%r12, %%r8\\n"
    "   adcq    %%r13, %%r9\\n"
    "   adcq    %%r14, %%r10\\n"
    "   adcq    %%r15, %%r11\\n"
    "   movq    %%r8, (%%rax)\\n"
    "   movq    %%r9, 8(%%rax)\\n"
    "   movq    %%r10, 16(%%rax)\\n"
    "   movq    %%r11, 24(%%rax)\\n"
  : "+m" (z)
  : "m" (x), "m" (y), "m" (k->p->_mp_d)
  : "%rax", "%r8", "%r9", "%r10", "%r11", "%r12", "%r13", "%r14", "%r15", "memory");
#else
  mp_limb_t cy;
  cy = mpfq_fixmp_${n}_sub(z, x, y);
  if (cy)
    mpfq_fixmp_${n}_add_nc(z, z, k->p->_mp_d);
#endif
EOF
    return [ 'inline(k,z,x,y)', $code ];
}

1;
