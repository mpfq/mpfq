package pz;

use strict;
use warnings;

use Mpfq::engine::handler;
use Mpfq::engine::maketext qw/simple_indent/;

use Mpfq::gfp::field;
use Mpfq::defaults;
use Mpfq::defaults::pow;
use Mpfq::defaults::poly;
use Mpfq::defaults::vec::io;
use Mpfq::defaults::vec::generic;
use Mpfq::defaults::vec::mul;

our @parents = qw/
    Mpfq::gfp::field
    Mpfq::defaults
    Mpfq::defaults::pow
    Mpfq::defaults::poly
    Mpfq::defaults::vec::io
    Mpfq::defaults::vec::generic
    Mpfq::defaults::vec::mul
/;

our @ISA = qw/Mpfq::engine::handler/;

sub new { return bless({}, shift); }

sub code_for_impl_max_characteristic_bits { return [ 'macro()', 'UINT_MAX' ]; }
sub code_for_impl_max_degree { return [ 'macro()', '1' ]; }
sub code_for_field_degree { return [ 'macro(k)', '1' ]; }
sub code_for_field_setopt { return [ 'macro(k,x,y)' , '' ]; }
sub code_for_frobenius { return [ 'macro(k,x,y)' , '@!set(k,x,y)' ]; }

sub code_for_field_specify {
    my $opt = shift @_;
    my $w = $opt->{'w'};
    my $n = $opt->{'n'};
    my $nn = $opt->{'nn'};
    my $code = '';
    $code .= <<EOF;
    if (dummy == MPFQ_PRIME_MPN) {
        fprintf(stderr, "MPFQ_PRIME_MPN is deprecated\\n");
        return;
    } else if (dummy == MPFQ_PRIME_MPZ) {
        mpz_srcptr p = (mpz_srcptr) vp;
        mpz_set(k->p, p);
        if (!(mpz_size(k->p) > 0)) abort();
EOF
    local $_ = Mpfq::gfp::field::code_fragment_set_bp_from_p($opt, @_);
    s/^/\t/mg;
    $code .= $_;
    $code .= <<EOF;
    } else if (dummy == MPFQ_SIMD_GROUPSIZE && *(int*)vp == 1) {
        /* Do nothing, this is an admitted condition */
        return;
    } else {
        return;
    }
EOF
    return [ 'function(k,dummy!,vp)' , $code ]; 
}

sub code_for_vec_coeff_ptr {
    my $proto = 'inline(K!,v,i)';
    my $code = 'return v + i * mpz_size(K->p);';
    return [ $proto, $code ];
}

sub code_for_vec_coeff_ptr_const { return code_for_vec_coeff_ptr(@_); }

sub code_for_vec_subvec {
    my $proto = 'inline(K!,v,i)';
    my $code = 'return v + i * mpz_size(K->p);';
    return [ $proto, $code ];
}

sub code_for_vec_subvec_const { return code_for_vec_subvec(@_); }


sub code_for_vec_ur_coeff_ptr {
    my $proto = 'inline(K!,v,i)';
    my $code = 'return v + i * mpz_size(K->bigmul_p);';
    return [ $proto, $code ];
}

sub code_for_vec_ur_coeff_ptr_const { return code_for_vec_ur_coeff_ptr(@_); }

sub code_for_vec_ur_subvec {
    my $proto = 'inline(K!,v,i)';
    my $code = 'return v + i * mpz_size(K->bigmul_p);';
    return [ $proto, $code ];
}

sub code_for_vec_ur_subvec_const { return code_for_vec_ur_subvec(@_); }


sub code_for_init {
    my $code = <<EOF;
    *x = (@!elt) mpfq_malloc_check(mpz_size(k->p) * sizeof(mp_limb_t));
EOF
    return [ 'function(k, x)', $code ];
}

sub code_for_clear {
    my $code = <<EOF;
    mpfq_free(*x, mpz_size(k->p) * sizeof(mp_limb_t));
    *x = NULL;
EOF
    return [ 'function(k!, x)', $code ];
}

sub code_for_set {
    my $code = <<EOF;
    mpfq_copy(z, x, mpz_size(k->p));
EOF
    return [ 'inline(k, z, x)', $code ];
}

sub code_for_set_ui {
    my $code = <<EOF;
    ASSERT_FOR_STATIC_ANALYZER(mpz_size(k->p) > 1 || mpz_getlimbn(k->p, 0) > 0);
    z[0] = mpz_size(k->p) == 1 ? x0 % mpz_getlimbn(k->p, 0) : x0;
    if (mpz_size(k->p) >= 1) {
        /* Sigh. mpz_size(k->p) is a size_t. As such, it is bounded by
         * SIZE_MAX, and gcc 12+ sees it as such. It is apparently not
         * possible to use this bound in a memset directly.
         */
#if GNUC_VERSION_ATLEAST(7,1,0)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstringop-overflow"
#endif
        mpfq_zero(z + 1, (mpz_size(k->p) - 1));
#if GNUC_VERSION_ATLEAST(7,1,0)
#pragma GCC diagnostic pop
#endif
    }
EOF
    return [ 'inline(k, z, x0)', $code ];
}

sub code_for_set_zero {
    my $code = <<EOF;
    mpfq_zero(z, (mpz_size(k->p)));
EOF
    return [ 'inline(k, z)', $code ];
}

sub code_for_get_ui {
    my $code = <<EOF;
    return x[0];
EOF
    return [ 'inline(k!, x)', $code ];
}

sub code_for_set_mpn {
    my $code = <<EOF;
    if (n < mpz_size(k->p)) {
	mpfq_copy(z, x, n);
	mpfq_zero(z + n, (mpz_size(k->p) - n));
    } else {
	mp_limb_t *tmp;
	tmp = (mp_limb_t *) mpfq_malloc_check((n + 1 - mpz_size(k->p)) * sizeof(mp_limb_t));
	mpn_tdiv_qr(tmp, z, 0, x, n, k->p->_mp_d, mpz_size(k->p));
	mpfq_free(tmp, (n + 1 - mpz_size(k->p)) * sizeof(mp_limb_t));
    }
EOF
    $code = simple_indent($code);
    return [ 'function(k, z, x, n)', $code ];
}

sub code_for_set_mpz {
    my $code = <<EOF;
    if (mpz_sgn(x) < 0) {
	@!set_mpn(k, z, x->_mp_d, -x->_mp_size);
	@!neg(k, z, z);
    } else {
	@!set_mpn(k, z, x->_mp_d, x->_mp_size);
    }
EOF
    $code = simple_indent($code);
    return [ 'function(k, z, x)', $code ];
}

sub code_for_get_mpn {
    my $code = <<EOF;
    mpfq_copy(z, x, mpz_size(k->p));
EOF
    return [ 'inline(k, z, x)', $code ];
}

sub code_for_get_mpz {
    my $code = <<EOF;
    int n = mpz_size(k->p);
    mpz_set_ui(z, x[n - 1]);
    for (int i = n - 2; i >= 0; --i) {
	mpz_mul_2exp(z, z, 64);
	mpz_add_ui(z, z, x[i]);
    }
EOF
    $code = simple_indent($code);
    return [ 'inline(k, z, x)', $code ];
}

sub code_for_random {
    my $code = <<EOF;
    mpz_t zz;
    mpz_init(zz);
    mpz_urandomb(zz, state, mpz_size(k->p) * GMP_LIMB_BITS);
    @!set_mpz(k, z, zz);
    mpz_clear(zz);
EOF
    return [ 'function(k, z, state)', $code ];
}

sub code_for_random2 {
    my $code = <<EOF;
    mpz_t zz;
    mpz_init(zz);
    mpz_rrandomb(zz, state, mpz_size(k->p) * GMP_LIMB_BITS);
    @!set_mpz(k, z, zz);
    mpz_clear(zz);
EOF
    return [ 'function(k, z, state)', $code ];
}

sub code_for_add {
    my $code = <<EOF;
    mp_limb_t cy;
    cy = mpn_add_n(z, x, y, mpz_size(k->p));
    if (cy || mpn_cmp(z, k->p->_mp_d, mpz_size(k->p)) >= 0)
	mpn_sub_n(z, z, k->p->_mp_d, mpz_size(k->p));
EOF
    return [ 'function(k, z, x, y)', $code ];
}

sub code_for_sub {
    my $code = <<EOF;
    mp_limb_t cy;
    cy = mpn_sub_n(z, x, y, mpz_size(k->p));
    if (cy)
	mpn_add_n(z, z, k->p->_mp_d, mpz_size(k->p));
EOF
    return [ 'function(k, z, x, y)', $code ];
}

sub code_for_neg {
    my $code = <<EOF;
    if (@!is_zero(k, x))
	@!set_zero(k, z);
    else
	mpn_sub_n(z, k->p->_mp_d, x, mpz_size(k->p));
EOF
    return [ 'function(k, z, x)', $code ];
}

sub code_for_mul {
    my $code = <<EOF;
    @!elt_ur zz;
    @!elt_ur_init(k, &zz);
    @!mul_ur(k, zz, x, y);
    @!reduce(k, z, zz);
    @!elt_ur_clear(k, &zz);
EOF
    return [ 'function(k, z, x, y)', $code ];
}

sub code_for_sqr {
    my $code = <<EOF;
    @!elt_ur zz;
    @!elt_ur_init(k, &zz);
    @!sqr_ur(k, zz, x);
    @!reduce(k, z, zz);
    @!elt_ur_clear(k, &zz);
EOF
    return [ 'function(k, z, x)', $code ];
}

sub code_for_is_sqr {
    my $code = <<EOF;
    mpz_t a, p;
    mpz_init(a);
    mpz_init(p);
    @!field_characteristic(k, p);
    @!get_mpz(k, a, x);
    int ret = mpz_jacobi(a, p);
    mpz_clear(a);
    mpz_clear(p);
    return (ret >= 0);
EOF
    return [ 'function(k, x)', $code ];
}

# sub code_for_sqrt {
#     my $code = <<EOF;
#     fprintf(stderr, "Not Implemented!\\n");
#     exit(1);
# EOF
#     return [ 'function(k!, z!, x!)', $code ];
# }
# 
sub code_for_pow {
    my $code = <<EOF;
    mpz_t p, xx, nn;
    mpz_init(p);
    mpz_init(xx);
    @!field_characteristic(k, p);
    @!get_mpz(k, xx, x);
    nn->_mp_d = n;
    nn->_mp_size = nl;
    nn->_mp_alloc = nl;
    mpz_powm(xx, xx, nn, p);
    @!set_mpz(k, z, xx);
    mpz_clear(p);
    mpz_clear(xx);
EOF
    return [ 'function(k, z, x, n, nl)', $code ];
}

sub code_for_add_ui {
    my $code = <<EOF;
    mp_limb_t cy;
    cy = mpn_add_1(z, x, mpz_size(k->p), y);
    if (cy || mpn_cmp(z, k->p->_mp_d, mpz_size(k->p)) >= 0)
	mpn_sub_n(z, z, k->p->_mp_d, mpz_size(k->p));
EOF
    return [ 'function(k, z, x, y)', $code ];
}

sub code_for_sub_ui {
    my $code = <<EOF;
    mp_limb_t cy;
    cy = mpn_sub_1(z, x, mpz_size(k->p), y);
    if (cy)
	mpn_add_n(z, z, k->p->_mp_d, mpz_size(k->p));
EOF
    return [ 'function(k, z, x, y)', $code ];
}

sub code_for_mul_ui {
    my $code = <<EOF;
    mpz_t xx, p;
    mpz_init(xx);
    mpz_init(p);
    @!field_characteristic(k, p);
    @!get_mpz(k, xx, x);
    mpz_mul_ui(xx, xx, y);
    mpz_tdiv_r(xx, xx, p);
    @!set_mpz(k, z, xx);
    mpz_clear(xx);
    mpz_clear(p);
EOF
    return [ 'function(k, z, x, y)', $code ];
}

sub code_for_inv {
    my $code = <<EOF;
    mpz_t xx, p;
    mpz_init(xx);
    mpz_init(p);
    @!field_characteristic(k, p);
    @!get_mpz(k, xx, x);
    int ret = mpz_invert(xx, xx, p);
    @!set_mpz(k, z, xx);
    mpz_clear(xx);
    mpz_clear(p);
    if (!ret) {
	if (@!is_zero(k, x)) {
            @!set_ui(k, z, 0);
	    return 0;
        } else {
	    fprintf(stderr, "Not implemented\\n");
	    exit(1);
	}
    }
    return 1;
EOF
    $code = simple_indent($code);
    return [ 'function(k, z, x)', $code ];
}

sub code_for_elt_ur_init {
    my $code = <<EOF;
    *x = (@!elt_ur) mpfq_malloc_check(mpz_size(k->bigmul_p) * sizeof(mp_limb_t));
EOF
    return [ 'function(k, x)', $code ];
}

sub code_for_elt_ur_clear {
    my $code = <<EOF;
    mpfq_free(*x, mpz_size(k->bigmul_p) * sizeof(mp_limb_t));
    *x = NULL;
EOF
    return [ 'function(k!, x)', $code ];
}

sub code_for_elt_ur_set {
    my $code = <<EOF;
    mpfq_copy(z, x, mpz_size(k->bigmul_p));
EOF
    return [ 'inline(k, z, x)', $code ];
}

sub code_for_elt_ur_set_elt {
    my $code = <<EOF;
    mpfq_copy(z, x, mpz_size(k->p));
    mpfq_zero(z + mpz_size(k->p), (mpz_size(k->bigmul_p) - mpz_size(k->p)));
EOF
    return [ 'inline(k, z, x)', $code ];
}

sub code_for_elt_ur_set_zero {
    my $code = <<EOF;
    mpfq_zero(z, (mpz_size(k->bigmul_p)));
EOF
    return [ 'inline(k, z)', $code ];
}

sub code_for_elt_ur_set_ui {
    my $code = <<EOF;
    z[0] = x0;
    mpfq_zero(z + 1, (mpz_size(k->bigmul_p) - 1));
EOF
    return [ 'inline(k, z, x0)', $code ];
}

sub code_for_elt_ur_add {
    my $code = <<EOF;
    mpn_add_n(z, x, y, mpz_size(k->bigmul_p));
EOF
    return [ 'function(k, z, x, y)', $code ];
}

sub code_for_elt_ur_sub {
    my $code = <<EOF;
    mpn_sub_n(z, x, y, mpz_size(k->bigmul_p));
EOF
    return [ 'function(k, z, x, y)', $code ];
}

sub code_for_elt_ur_neg {
    my $code = <<EOF;
#if GMP_VERSION_ATLEAST(5, 0, 0)
    mpn_neg(z, x, mpz_size(k->bigmul_p));
#else
  mp_size_t i;
  mp_size_t n = mpz_size(k->bigmul_p);

  for (i = 0; i < n; i++)
    z[i] = ~x[i];
  mpn_add_1 (z, z, n, 1);
#endif
EOF
    return [ 'function(k, z, x)', $code ];
}

sub code_for_mul_ur {
    my $code = <<EOF;
    mpn_mul_n(z, x, y, mpz_size(k->p));
    z[mpz_size(k->bigmul_p) - 1] = 0;
EOF
    return [ 'function(k, z, x, y)', $code ];
}

sub code_for_sqr_ur {
    my $code = <<EOF;
    mpn_sqr(z, x, mpz_size(k->p));
    z[mpz_size(k->bigmul_p) - 1] = 0;
EOF
    return [ 'function(k, z, x)', $code ];
}

sub code_for_reduce {
    my $code = <<EOF;
//    int neg = 0;
    if (x[mpz_size(k->bigmul_p) - 1] >> (GMP_LIMB_BITS - 1)) {	// negative input
//        neg = 1;
//        @!elt_ur_neg(k, x, x);
	mpn_add_n(x, x, k->bigmul_p->_mp_d, mpz_size(k->bigmul_p));
    }
    mp_limb_t *tmp;
    tmp = (mp_limb_t *) mpfq_malloc_check((mpz_size(k->bigmul_p) + 1) * sizeof(mp_limb_t));
    mpn_tdiv_qr(tmp, z, 0, x, mpz_size(k->bigmul_p), k->p->_mp_d, mpz_size(k->p));
//    if (neg)
//        @!neg(k, z, z);
    mpfq_free(tmp, (mpz_size(k->bigmul_p) + 1) * sizeof(mp_limb_t));
EOF
    $code = simple_indent($code);
    return [ 'function(k, z, x)', $code ];
}

sub code_for_normalize {
    my $code = <<EOF;
    mp_limb_t tmp;
    mpn_tdiv_qr(&tmp, z, 0, z, mpz_size(k->p), k->p->_mp_d, mpz_size(k->p));
EOF
    return [ 'function(k, z)', $code ];
}

sub code_for_cmp {
    my $code = <<EOF;
    return mpn_cmp(x, y, mpz_size(k->p));
EOF
    return [ 'inline(k, x, y)', $code ];
}

sub code_for_cmp_ui {
    my $code = <<EOF;
    for (int i = mpz_size(k->p) - 1; i > 0; --i) {
	if (x[i]) return 1;
    }
    return (x[0] > y0) - (x[0] < y0);
EOF
    return [ 'inline(k, x, y0)', $code ];
}

sub code_for_is_zero {
    my $code = <<EOF;
    for (unsigned int i = 0; i < mpz_size(k->p); ++i)
	if (x[i] != 0)
	    return 0;
    return 1;
EOF
    return [ 'inline(k, x)', $code ];
}

sub code_for_asprint {
    my $code = <<EOF;
    // deal with 0
    if (@!is_zero(k, x)) {
	char * s = *pstr = (char *) malloc(2);
	s[0] = '0';
	s[1] = '\\0';
	return 1;
    }
    // allocate enough room for base 2 conversion.
    *pstr = (char *) malloc(((mpz_size(k->p)) * 64 + 1));
    mp_limb_t *tmp;
    tmp = (mp_limb_t *) mpfq_malloc_check((mpz_size(k->p)) * sizeof(mp_limb_t));
    int tl = mpz_size(k->p) - 1;
    while (x[tl] == 0)		// x is non-zero, no need to test tl>0
	tl--;
    for (int i = 0; i <= tl; ++i)
	tmp[i] = x[i];
    int n = mpn_get_str((unsigned char *) (*pstr), k->io_base, tmp, tl + 1);
    mpfq_free(tmp, (mpz_size(k->p)) * sizeof(mp_limb_t));
    for (int i = 0; i < n; ++i)
	(*pstr)[i] += '0';
    (*pstr)[n] = '\\0';
    // remove leading 0s
    int shift = 0;
    while (((*pstr)[shift] == '0') && ((*pstr)[shift + 1] != '\\0'))
	shift++;
    if (shift) {
        memmove(*pstr, (*pstr) + shift, n + 1 - shift);
        n -= shift;
    }
    return n;
EOF
    return [ 'function(k, pstr, x)', $code ];
}

sub code_for_fprint {
    my $code = <<EOF;
    char *str;
    int rc;
    @!asprint(k, &str, x);
    rc = fprintf(file, "%s", str);
    free(str);
    return rc;
EOF
    return [ 'function(k, file, x)', $code ];
}

sub code_for_cxx_out {
    my $code = <<EOF;
    char *str;
    @!asprint(k, &str, x);
    os << str;
    free(str);
    return os;
EOF
    return [ 'function(k, os, x)', $code ];
}

sub code_for_sscan {
    my $code = <<EOF;
    mpz_t zz;
    mpz_init(zz);
    int nread;
    if (gmp_sscanf(str, "%Zd%n", zz, &nread) != 1) {
	mpz_clear(zz);
	return 0;
    }
    @!set_mpz(k, z, zz);
    mpz_clear(zz);
    return nread;
EOF
    return [ 'function(k, z, str)', $code ];
}

sub code_for_fscan {
    my $code = <<EOF;
    mpz_t zz;
    mpz_init(zz);
    int nread;
    if (gmp_fscanf(file, "%Zd%n", zz, &nread) != 1) {
	mpz_clear(zz);
	return 0;
    }
    @!set_mpz(k, x, zz);
    mpz_clear(zz);
    return nread;
EOF
    return [ 'function(k, file, x)', $code ];
}

sub code_for_cxx_in {
    my $code = <<EOF;
    mpz_t zz;
    mpz_init(zz);
    if (is >> zz) {
        @!set_mpz(k, x, zz);
    }
    mpz_clear(zz);
    return is;
EOF
    return [ 'function(k, is, x)', $code ];
}

sub code_for_vec_init {
    my $code = <<EOF;
    *v = (@!vec) mpfq_malloc_check((mpz_size(k->p)) * n * sizeof(mp_limb_t));
EOF
    return [ 'function(k, v, n)', $code ];
}

sub code_for_vec_reinit {
    my $code = <<EOF;
    *v = (@!vec) mpfq_realloc_check(*v, (mpz_size(k->p)) * n * sizeof(mp_limb_t), (mpz_size(k->p)) * m * sizeof(mp_limb_t));
EOF
    return [ 'function(k, v, n!, m)', $code ];
}

sub code_for_vec_clear {
    my $code = <<EOF;
    mpfq_free(*v, (mpz_size(k->p)) * n * sizeof(mp_limb_t));
EOF
    return [ 'function(k!, v, n!)', $code ];
}

sub code_for_vec_set {
    my $code = <<EOF;
    if (v != w)
	memmove(w, v, n * mpz_size(k->p) * sizeof(mp_limb_t));
EOF
    return [ 'function(k, w, v, n)', $code ];
}

sub code_for_vec_set_zero {
    my $code = <<EOF;
    if (n) mpfq_zero(w, n * mpz_size(k->p));
EOF
    return [ 'function(k, w, n)', $code ];
}

sub code_for_vec_setcoeff {
    my $code = <<EOF;
    @!set(k, w + i * mpz_size(k->p), x);
EOF
    return [ 'function(k, w, x, i)', $code ];
}

sub code_for_vec_setcoeff_ui {
    my $code = <<EOF;
    @!set_ui(k, w + i * mpz_size(k->p), x0);
EOF
    return [ 'function(k, w, x0, i)', $code ];
}

sub code_for_vec_getcoeff {
    my $code = <<EOF;
    @!set(k, z, v + i * mpz_size(k->p));
EOF
    return [ 'function(k, z, v, i)', $code ];
}

sub code_for_vec_add {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i)
	@!add(k, w + i * mpz_size(k->p), u + i * mpz_size(k->p), v + i * mpz_size(k->p));
EOF
    return [ 'function(k, w, u, v, n)', $code ];
}

sub code_for_vec_neg {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i)
	@!neg(k, w + i * mpz_size(k->p), v + i * mpz_size(k->p));
EOF
    return [ 'function(k, w, v, n)', $code ];
}

sub code_for_vec_sub {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i)
	@!sub(k, w + i * mpz_size(k->p), u + i * mpz_size(k->p), v + i * mpz_size(k->p));
EOF
    return [ 'function(k, w, u, v, n)', $code ];
}

sub code_for_vec_rev {
    my $code = <<EOF;
    unsigned int nn = n >> 1;
    @!elt tmp;
    @!init(k, &tmp);
    for (unsigned int i = 0; i < n - 1 - i; ++i) {
	@!set(k, tmp, v + i * mpz_size(k->p));
	@!set(k, w + i * mpz_size(k->p), v + (n - 1 - i) * mpz_size(k->p));
	@!set(k, w + (n - 1 - i) * mpz_size(k->p), tmp);
    }
    if (n & 1)
	@!set(k, w + nn * mpz_size(k->p), v + nn * mpz_size(k->p));
    @!clear(k, &tmp);
EOF
    return [ 'function(k, w, v, n)', $code ];
}

sub code_for_vec_conv {
    my $code = <<EOF;
    @!vec_ur tmp;
    @!vec_ur_init(k, &tmp, m + n - 1);
    @!vec_conv_ur(k, tmp, u, n, v, m);
    @!vec_reduce(k, w, tmp, m + n - 1);
    @!vec_ur_clear(k, &tmp, m + n - 1);
EOF
    return [ 'function(k, w, u, n, v, m)', $code ];
}

sub code_for_vec_asprint {
    my $code = <<EOF;
    if (n == 0) {
	*pstr = (char *) malloc(4);
	sprintf(*pstr, "[ ]");
	return strlen(*pstr);
    }
    int alloc = 100;
    int len = 0;
    *pstr = (char *) malloc(alloc);
    if (!*pstr) abort();
    (*pstr)[len++] = '[';
    (*pstr)[len++] = ' ';
    for (unsigned int i = 0; i < n; ++i) {
	if (i) {
	    (*pstr)[len++] = ',';
	    (*pstr)[len++] = ' ';
	}
	char *tmp;
	@!asprint(k, &tmp, v + i * mpz_size(k->p));
	int ltmp = strlen(tmp);
	if (len + ltmp + 4 > alloc) {
	    alloc = len + ltmp + 100;
	    *pstr = (char *) realloc(*pstr, alloc);
	}
        strncpy(*pstr+len, tmp, alloc-len);
	len += ltmp;
	free(tmp);
    }
    (*pstr)[len++] = ' ';
    (*pstr)[len++] = ']';
    (*pstr)[len] = '\\0';
    return len;
EOF
    return [ 'function(k, pstr, v, n)', $code ];
}

sub code_for_vec_fprint {
    my $code = <<EOF;
    char *str;
    int rc;
    @!vec_asprint(k, &str, v, n);
    rc = fprintf(file, "%s", str);
    free(str);
    return rc;
EOF
    return [ 'function(k, file, v, n)', $code ];
}

sub code_for_vec_ur_init {
    my $code = <<EOF;
    *v = (@!vec) mpfq_malloc_check((mpz_size(k->bigmul_p)) * n * sizeof(mp_limb_t));
EOF
    return [ 'function(k, v, n)', $code ];
}

sub code_for_vec_ur_reinit {
    my $code = <<EOF;
    *v = (@!vec) mpfq_realloc_check(*v, (mpz_size(k->bigmul_p)) * n * sizeof(mp_limb_t), (mpz_size(k->bigmul_p)) * m * sizeof(mp_limb_t));
EOF
    return [ 'function(k, v, n!, m)', $code ];
}

sub code_for_vec_ur_clear {
    my $code = <<EOF;
    mpfq_free(*v, (mpz_size(k->bigmul_p)) * n * sizeof(mp_limb_t));
EOF
    return [ 'function(k!, v, n!)', $code ];
}

sub code_for_vec_ur_set {
    my $code = <<EOF;
    if (v != w)
	memmove(w, v, n * mpz_size(k->bigmul_p) * sizeof(mp_limb_t));
EOF
    return [ 'function(k, w, v, n)', $code ];
}

sub code_for_vec_ur_set_zero {
    my $code = <<EOF;
    mpfq_zero(w, n * mpz_size(k->bigmul_p));
EOF
    return [ 'function(k, w, n)', $code ];
}

sub code_for_vec_ur_setcoeff {
    my $code = <<EOF;
    @!elt_ur_set(k, w + i * mpz_size(k->bigmul_p), x);
EOF
    return [ 'function(k, w, x, i)', $code ];
}

sub code_for_vec_ur_setcoeff_ui {
    my $code = <<EOF;
    @!elt_ur_set_ui(k, w + i * mpz_size(k->bigmul_p), x0);
EOF
    return [ 'function(k, w, x0, i)', $code ];
}

sub code_for_vec_ur_getcoeff {
    my $code = <<EOF;
    @!elt_ur_set(k, z, v + i * mpz_size(k->bigmul_p));
EOF
    return [ 'function(k, z, v, i)', $code ];
}

sub code_for_vec_ur_set_vec {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i)
	@!elt_ur_set_elt(k, w + i * mpz_size(k->bigmul_p), v + i * mpz_size(k->p));
EOF
    return [ 'function(k, w, v, n)', $code ];
}

sub code_for_vec_ur_add {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i)
	@!elt_ur_add(k, w + i * mpz_size(k->bigmul_p), u + i * mpz_size(k->bigmul_p),
			      v + i * mpz_size(k->bigmul_p));
EOF
    return [ 'function(k, w, u, v, n)', $code ];
}

sub code_for_vec_ur_neg {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i)
	@!elt_ur_neg(k, w + i * mpz_size(k->bigmul_p), v + i * mpz_size(k->bigmul_p));
EOF
    return [ 'function(k, w, v, n)', $code ];
}

sub code_for_vec_ur_sub {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i)
	@!elt_ur_sub(k, w + i * mpz_size(k->bigmul_p), u + i * mpz_size(k->bigmul_p),
			      v + i * mpz_size(k->bigmul_p));
EOF
    return [ 'function(k, w, u, v, n)', $code ];
}

sub code_for_vec_ur_rev {
    my $code = <<EOF;
    unsigned int nn = n >> 1;
    @!elt_ur tmp;
    @!elt_ur_init(k, &tmp);
    for (unsigned int i = 0; i < nn; ++i) {
	@!elt_ur_set(k, tmp, v + i * mpz_size(k->bigmul_p));
	@!elt_ur_set(k, w + i * mpz_size(k->bigmul_p), v + (n - 1 - i) * mpz_size(k->bigmul_p));
	@!elt_ur_set(k, w + (n - 1 - i) * mpz_size(k->bigmul_p), tmp);
    }
    if (n & 1)
	@!elt_ur_set(k, w + nn * mpz_size(k->bigmul_p), v + nn * mpz_size(k->bigmul_p));
    @!elt_ur_clear(k, &tmp);
EOF
    return [ 'function(k, w, v, n)', $code ];
}

# The model for generating ancillary functions with the interface is
# init_ts in gfp/perl/elt.pm
sub code_for_vec_conv_ur_ks {
    my $code = <<EOF;
    // compute base as a power 2^GMP_NUMB_BITS
    // This is the least number of words that can accomodate
    //     log_2( (p-1)^2 * min(n,m) )
    mpz_t p;
    mpz_init(p);
    @!field_characteristic(k, p);
    mpz_sub_ui(p, p, 1);
    mpz_mul(p, p, p);
    mpz_mul_ui(p, p, MIN(m, n));

    long nbits = mpz_sizeinbase(p, 2);
    unsigned long nwords = 1 + ((nbits - 1) / GMP_NUMB_BITS);
    nbits = GMP_NUMB_BITS * nwords;
    mpz_clear(p);
    assert(mpz_size(k->bigmul_p) >= nwords);

    // Create big integers
    mp_limb_t *U, *V;
    U = (mp_limb_t *) mpfq_malloc_check(n * nwords * sizeof(mp_limb_t));
    V = (mp_limb_t *) mpfq_malloc_check(m * nwords * sizeof(mp_limb_t));
    mpfq_zero(U, n * nwords);
    for (unsigned int i = 0; i < n; ++i)
	@!get_mpn(k, U + i * nwords, u + i * mpz_size(k->p));
    mpfq_zero(V, m * nwords);
    for (unsigned int i = 0; i < m; ++i)
	@!get_mpn(k, V + i * nwords, v + i * mpz_size(k->p));
    // Mul !
    mp_limb_t *W;
    W = (mp_limb_t *) mpfq_malloc_check((n + m) * nwords * sizeof(mp_limb_t));
    if (n>=m)
        mpn_mul(W, U, n * nwords, V, m * nwords);
    else
        mpn_mul(W, V, m * nwords, U, n * nwords);
    // Put coeffs in w
    mpfq_zero(w, (n + m - 1) * mpz_size(k->bigmul_p));
    for (unsigned int i = 0; i < n + m - 1; ++i)
	mpfq_copy(w + i * mpz_size(k->bigmul_p), W + i * nwords, nwords);
    mpfq_free(U, n * nwords * sizeof(mp_limb_t));
    mpfq_free(V, m * nwords * sizeof(mp_limb_t));
    mpfq_free(W, (m+n) * nwords * sizeof(mp_limb_t));
EOF
    return { kind=>'function(k, w, u, n, v, m)',
        requirements=>"field dst_vec_ur src_vec uint src_vec uint",
        name=>"vec_conv_ur_ks",
        code=>$code};

}

sub code_for_vec_conv_ur_n {
    my $code = <<EOF;
    @!vec_conv_ur_ks(k, w, u, n, v, n);
EOF
    return [ 'function(k, w, u, v, n)', $code], code_for_vec_conv_ur_ks(@_);
}

sub code_for_vec_conv_ur {
    my $code = <<EOF;
    @!vec_conv_ur_ks(k, w, u, n, v, m);
EOF
    return [ 'function(k, w, u, n, v, m)', $code], code_for_vec_conv_ur_ks(@_);
}

sub code_for_vec_reduce {
    my $code = <<EOF;
    for (unsigned int i = 0; i < n; ++i)
	@!reduce(k, w + i * mpz_size(k->p), v + i * mpz_size(k->bigmul_p));
EOF
    return [ 'function(k, w, v, n)', $code ];
}

sub code_for_elt_stride { return [ 'inline(k!)', 'return mpz_size(k->p) * sizeof(mp_limb_t);' ]; }
sub code_for_elt_ur_stride { return [ 'inline(k!)', 'return mpz_size(k->bigmul_p) * sizeof(mp_limb_t);' ]; }

sub code_for_vec_elt_stride {
    return [ 'macro(k, n)', "((n) * @!elt_stride((k)))" ];
}
sub code_for_vec_ur_elt_stride {
    return [ 'macro(k, n)', "((n) * @!elt_ur_stride((k)))" ];
}

sub init_handler {
    my $types = {
	field => "typedef mpfq_p_field @!field;",
	src_field => "typedef mpfq_p_src_field @!src_field;",
	dst_field => "typedef mpfq_p_dst_field @!dst_field;",

	elt => "typedef mp_limb_t * @!elt;",
	dst_elt => "typedef mp_limb_t * @!dst_elt;",
	src_elt => "typedef const mp_limb_t * @!src_elt;",

	elt_ur => "typedef mp_limb_t * @!elt_ur;",
	dst_elt_ur => "typedef mp_limb_t * @!dst_elt_ur;",
	src_elt_ur => "typedef const mp_limb_t * @!src_elt_ur;",


	vec => "typedef mp_limb_t * @!vec;",
	dst_vec => "typedef mp_limb_t * @!dst_vec;",
	src_vec => "typedef const mp_limb_t * @!src_vec;",

	vec_ur => "typedef mp_limb_t * @!vec_ur;",
	dst_vec_ur => "typedef mp_limb_t * @!dst_vec_ur;",
	src_vec_ur => "typedef const mp_limb_t * @!src_vec_ur;",
    };
    return { types => $types,
             'c:includes'=>[ qw{
              <limits.h>
              }],
             includes=>[ qw{
              "mpfq_gfp_common.h"
              }],
           };
}
1;
