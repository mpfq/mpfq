	.file	"testmul.c"
# GNU C (GCC) version 4.8.2 20131219 (prerelease) (x86_64-unknown-linux-gnu)
#	compiled by GNU C version 4.8.2 20131219 (prerelease), GMP version 5.1.3, MPFR version 3.1.2-p5, MPC version 1.0.1
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passées:  testmul.c -mtune=generic -march=x86-64 -fverbose-asm
# options autorisées:  -faggressive-loop-optimizations
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg -fcommon
# -fdelete-null-pointer-checks -fdwarf2-cfi-asm -fearly-inlining
# -feliminate-unused-debug-types -ffunction-cse -fgcse-lm -fgnu-runtime
# -fident -finline-atomics -fira-hoist-pressure -fira-share-save-slots
# -fira-share-spill-slots -fivopts -fkeep-static-consts
# -fleading-underscore -fmath-errno -fmerge-debug-strings
# -fmove-loop-invariants -fpeephole -fprefetch-loop-arrays
# -freg-struct-return -fsched-critical-path-heuristic
# -fsched-dep-count-heuristic -fsched-group-heuristic -fsched-interblock
# -fsched-last-insn-heuristic -fsched-rank-heuristic -fsched-spec
# -fsched-spec-insn-heuristic -fsched-stalled-insns-dep -fshow-column
# -fsigned-zeros -fsplit-ivs-in-unroller -fstrict-volatile-bitfields
# -fsync-libcalls -ftrapping-math -ftree-coalesce-vars -ftree-cselim
# -ftree-forwprop -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pta
# -ftree-reassoc -ftree-scev-cprop -ftree-slp-vectorize
# -ftree-vect-loop-version -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -maccumulate-outgoing-args -malign-stringops -mfancy-math-387
# -mfp-ret-in-387 -mfxsr -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4
# -mpush-args -mred-zone -msse -msse2 -msse4.2 -mtls-direct-seg-refs

	.text
	.type	mul_1, @function
mul_1:
.LFB22:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)	# z, z
	movq	%rsi, -16(%rbp)	# x, x
	movq	%rdx, -24(%rbp)	# y, y
	movq	-8(%rbp), %rax	# z, tmp62
	leaq	8(%rax), %rsi	#, D.4509
	movq	-16(%rbp), %rax	# x, tmp63
	movq	(%rax), %rcx	# *x_3(D), D.4510
	movq	-24(%rbp), %rax	# y, tmp64
	movq	(%rax), %rdx	# *y_5(D), D.4510
	movq	%rcx, %rax	# D.4510, tmp65
#APP
# 43 "test.h" 1
	    mulq %rdx	# D.4510

# 0 "" 2
#NO_APP
	movq	-8(%rbp), %rcx	# z, tmp67
	movq	%rax, (%rcx)	# tmp65, *z_1(D)
	movq	%rdx, (%rsi)	# tmp66, *_2
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22:
	.size	mul_1, .-mul_1
	.type	mul_2, @function
mul_2:
.LFB23:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	pushq	%r15	#
	pushq	%r14	#
	pushq	%r13	#
	pushq	%r12	#
	pushq	%rbx	#
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -48(%rbp)	# z, z
	movq	%rsi, -56(%rbp)	# x, x
	movq	%rdx, -64(%rbp)	# y, y
	movq	-48(%rbp), %rax	# z, tmp72
	addq	$8, %rax	#, D.4513
	movq	%rax, %rbx	# D.4513, D.4513
	movq	-48(%rbp), %rax	# z, tmp73
	leaq	16(%rax), %r15	#, D.4513
	movq	-48(%rbp), %rax	# z, tmp74
	leaq	24(%rax), %r14	#, D.4513
	movq	-56(%rbp), %rax	# x, tmp75
	movq	(%rax), %r10	# *x_5(D), D.4514
	movq	-56(%rbp), %rax	# x, tmp76
	addq	$8, %rax	#, D.4515
	movq	(%rax), %r11	# *_7, D.4514
	movq	-64(%rbp), %rax	# y, tmp77
	movq	(%rax), %r12	# *y_9(D), D.4514
	movq	-64(%rbp), %rax	# y, tmp78
	addq	$8, %rax	#, D.4515
	movq	(%rax), %r13	# *_11, D.4514
	movq	-48(%rbp), %rax	# z, tmp79
	movq	(%rax), %rsi	# *z_1(D), D.4514
	movq	%rbx, -72(%rbp)	# D.4513, %sfp
	movq	%rbx, %rax	# D.4513, D.4513
	movq	(%rax), %rcx	# *_2, D.4514
	movq	(%r15), %rdx	# *_3, D.4514
	movq	(%r14), %rax	# *_4, D.4514
	movq	%rsi, %r9	# D.4514, tmp80
	movq	%rcx, %r8	# D.4514, tmp81
	movq	%rdx, %rdi	# D.4514, tmp82
	movq	%rax, %rsi	# D.4514, tmp83
#APP
# 54 "test.h" 1
	    movq %r10, %rax	# D.4514
    mulq %r12	# D.4514
    movq %rax, %rcx
    movq %rcx, %r9	# tmp80
    movq %rdx, %r8	# tmp81
    movq %r11, %rax	# D.4514
    mulq %r13	# D.4514
    movq %rax, %rdi	# tmp82
    movq %rdx, %rsi	# tmp83
    movq %r10, %rax	# D.4514
    mulq %r13	# D.4514
    movq %rax, %rbx
    movq %rdx, %rcx
    movq %r11, %rax	# D.4514
    mulq %r12	# D.4514
    addq %rbx, %r8	# tmp81
    adcq %rcx, %rdi	# tmp82
    adcq $0, %rsi	# tmp83
    addq %rax, %r8	# tmp81
    adcq %rdx, %rdi	# tmp82
    adcq $0, %rsi	# tmp83

# 0 "" 2
#NO_APP
	movq	-48(%rbp), %rax	# z, tmp84
	movq	%r9, (%rax)	# tmp80, *z_1(D)
	movq	-72(%rbp), %rax	# %sfp, D.4513
	movq	%r8, (%rax)	# tmp81, *_2
	movq	%rdi, (%r15)	# tmp82, *_3
	movq	%rsi, (%r14)	# tmp83, *_4
	popq	%rbx	#
	popq	%r12	#
	popq	%r13	#
	popq	%r14	#
	popq	%r15	#
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23:
	.size	mul_2, .-mul_2
	.type	mul_3, @function
mul_3:
.LFB24:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)	# z, z
	movq	%rsi, -16(%rbp)	# x, x
	movq	%rdx, -24(%rbp)	# y, y
#APP
# 85 "test.h" 1
	  ### START OF MUL
  ### x*y[0]
    movq	-24(%rbp), %r8	# y
    movq    -8(%rbp), %rdi	# z
    movq	(%r8), %r9
    movq    -16(%rbp), %rsi	# x
    movq    (%rsi), %rax
    mulq    %r9
    movq    %rax, (%rdi)
    movq    8(%rsi), %rax
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    movq    %rcx, 8(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    %rcx, 16(%rdi)
    movq    %rdx, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
  ### x*y[1]
    movq	8(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 8(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 16(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 32(%rdi)
  ### x*y[2]
    movq	16(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 16(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 40(%rdi)

# 0 "" 2
#NO_APP
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24:
	.size	mul_3, .-mul_3
	.type	mul_4, @function
mul_4:
.LFB25:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)	# z, z
	movq	%rsi, -16(%rbp)	# x, x
	movq	%rdx, -24(%rbp)	# y, y
#APP
# 161 "test.h" 1
	  ### START OF MUL
  ### x*y[0]
    movq	-24(%rbp), %r8	# y
    movq    -8(%rbp), %rdi	# z
    movq	(%r8), %r9
    movq    -16(%rbp), %rsi	# x
    movq    (%rsi), %rax
    mulq    %r9
    movq    %rax, (%rdi)
    movq    8(%rsi), %rax
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    movq    %rcx, 8(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    movq    %rcx, 16(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    %rcx, 24(%rdi)
    movq    %rdx, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
  ### x*y[1]
    movq	8(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 8(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 16(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 40(%rdi)
  ### x*y[2]
    movq	16(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 16(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 48(%rdi)
  ### x*y[3]
    movq	24(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 24(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 56(%rdi)

# 0 "" 2
#NO_APP
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25:
	.size	mul_4, .-mul_4
	.type	mul_5, @function
mul_5:
.LFB26:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)	# z, z
	movq	%rsi, -16(%rbp)	# x, x
	movq	%rdx, -24(%rbp)	# y, y
#APP
# 286 "test.h" 1
	  ### START OF MUL
  ### x*y[0]
    movq	-24(%rbp), %r8	# y
    movq    -8(%rbp), %rdi	# z
    movq	(%r8), %r9
    movq    -16(%rbp), %rsi	# x
    movq    (%rsi), %rax
    mulq    %r9
    movq    %rax, (%rdi)
    movq    8(%rsi), %rax
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    movq    %rcx, 8(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    movq    %rcx, 16(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    movq    %rcx, 24(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    %rcx, 32(%rdi)
    movq    %rdx, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
  ### x*y[1]
    movq	8(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 8(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 16(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 48(%rdi)
  ### x*y[2]
    movq	16(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 16(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 56(%rdi)
  ### x*y[3]
    movq	24(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 24(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 64(%rdi)
  ### x*y[4]
    movq	32(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 32(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 72(%rdi)

# 0 "" 2
#NO_APP
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26:
	.size	mul_5, .-mul_5
	.type	mul_6, @function
mul_6:
.LFB27:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)	# z, z
	movq	%rsi, -16(%rbp)	# x, x
	movq	%rdx, -24(%rbp)	# y, y
#APP
# 474 "test.h" 1
	  ### START OF MUL
  ### x*y[0]
    movq	-24(%rbp), %r8	# y
    movq    -8(%rbp), %rdi	# z
    movq	(%r8), %r9
    movq    -16(%rbp), %rsi	# x
    movq    (%rsi), %rax
    mulq    %r9
    movq    %rax, (%rdi)
    movq    8(%rsi), %rax
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    movq    %rcx, 8(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    movq    %rcx, 16(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    movq    %rcx, 24(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    movq    %rcx, 32(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    %rcx, 40(%rdi)
    movq    %rdx, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 88(%rdi)
  ### x*y[1]
    movq	8(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 8(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 16(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 56(%rdi)
  ### x*y[2]
    movq	16(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 16(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 64(%rdi)
  ### x*y[3]
    movq	24(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 24(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 72(%rdi)
  ### x*y[4]
    movq	32(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 32(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 80(%rdi)
  ### x*y[5]
    movq	40(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 40(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 88(%rdi)

# 0 "" 2
#NO_APP
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27:
	.size	mul_6, .-mul_6
	.type	mul_7, @function
mul_7:
.LFB28:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)	# z, z
	movq	%rsi, -16(%rbp)	# x, x
	movq	%rdx, -24(%rbp)	# y, y
#APP
# 739 "test.h" 1
	  ### START OF MUL
  ### x*y[0]
    movq	-24(%rbp), %r8	# y
    movq    -8(%rbp), %rdi	# z
    movq	(%r8), %r9
    movq    -16(%rbp), %rsi	# x
    movq    (%rsi), %rax
    mulq    %r9
    movq    %rax, (%rdi)
    movq    8(%rsi), %rax
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    movq    %rcx, 8(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    movq    %rcx, 16(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    movq    %rcx, 24(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    movq    %rcx, 32(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    movq    %rcx, 40(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    %rcx, 48(%rdi)
    movq    %rdx, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 104(%rdi)
  ### x*y[1]
    movq	8(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 8(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 16(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 64(%rdi)
  ### x*y[2]
    movq	16(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 16(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 72(%rdi)
  ### x*y[3]
    movq	24(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 24(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 80(%rdi)
  ### x*y[4]
    movq	32(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 32(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 88(%rdi)
  ### x*y[5]
    movq	40(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 40(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 88(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 96(%rdi)
  ### x*y[6]
    movq	48(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 48(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 88(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 96(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 104(%rdi)

# 0 "" 2
#NO_APP
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28:
	.size	mul_7, .-mul_7
	.type	mul_8, @function
mul_8:
.LFB29:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)	# z, z
	movq	%rsi, -16(%rbp)	# x, x
	movq	%rdx, -24(%rbp)	# y, y
#APP
# 1095 "test.h" 1
	  ### START OF MUL
  ### x*y[0]
    movq	-24(%rbp), %r8	# y
    movq    -8(%rbp), %rdi	# z
    movq	(%r8), %r9
    movq    -16(%rbp), %rsi	# x
    movq    (%rsi), %rax
    mulq    %r9
    movq    %rax, (%rdi)
    movq    8(%rsi), %rax
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    movq    %rcx, 8(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    movq    %rcx, 16(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    movq    %rcx, 24(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    movq    %rcx, 32(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    movq    %rcx, 40(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    movq    %rcx, 48(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    %rcx, 56(%rdi)
    movq    %rdx, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 112(%rdi)
	movq	$0, 120(%rdi)
  ### x*y[1]
    movq	8(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 8(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 16(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 72(%rdi)
  ### x*y[2]
    movq	16(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 16(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 80(%rdi)
  ### x*y[3]
    movq	24(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 24(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 88(%rdi)
  ### x*y[4]
    movq	32(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 32(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 88(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 96(%rdi)
  ### x*y[5]
    movq	40(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 40(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 88(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 96(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 104(%rdi)
  ### x*y[6]
    movq	48(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 48(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 88(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 96(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 104(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 112(%rdi)
  ### x*y[7]
    movq	56(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 56(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 88(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 96(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 104(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 112(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 120(%rdi)

# 0 "" 2
#NO_APP
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE29:
	.size	mul_8, .-mul_8
	.type	mul_9, @function
mul_9:
.LFB30:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)	# z, z
	movq	%rsi, -16(%rbp)	# x, x
	movq	%rdx, -24(%rbp)	# y, y
#APP
# 1556 "test.h" 1
	  ### START OF MUL
  ### x*y[0]
    movq	-24(%rbp), %r8	# y
    movq    -8(%rbp), %rdi	# z
    movq	(%r8), %r9
    movq    -16(%rbp), %rsi	# x
    movq    (%rsi), %rax
    mulq    %r9
    movq    %rax, (%rdi)
    movq    8(%rsi), %rax
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    movq    %rcx, 8(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    movq    %rcx, 16(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    movq    %rcx, 24(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    movq    %rcx, 32(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    movq    %rcx, 40(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    movq    %rcx, 48(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    64(%rsi), %rax
    movq    %rcx, 56(%rdi)
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    %rcx, 64(%rdi)
    movq    %rdx, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 112(%rdi)
	movq	$0, 120(%rdi)
	movq	$0, 128(%rdi)
	movq	$0, 136(%rdi)
  ### x*y[1]
    movq	8(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 8(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 16(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    64(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 80(%rdi)
  ### x*y[2]
    movq	16(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 16(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 24(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    64(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 88(%rdi)
  ### x*y[3]
    movq	24(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 24(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 32(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    64(%rsi), %rax
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 88(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 96(%rdi)
  ### x*y[4]
    movq	32(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 32(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 40(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    64(%rsi), %rax
    addq    %rcx, 88(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 96(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 104(%rdi)
  ### x*y[5]
    movq	40(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 40(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 48(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 88(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    64(%rsi), %rax
    addq    %rcx, 96(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 104(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 112(%rdi)
  ### x*y[6]
    movq	48(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 48(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 56(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 88(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 96(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    64(%rsi), %rax
    addq    %rcx, 104(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 112(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 120(%rdi)
  ### x*y[7]
    movq	56(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 56(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 64(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 88(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 96(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 104(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    64(%rsi), %rax
    addq    %rcx, 112(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 120(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 128(%rdi)
  ### x*y[8]
    movq	64(%r8), %r9
    movq    (%rsi), %rax
    mulq    %r9
    addq    %rax, 64(%rdi)
    movq    8(%rsi), %rax
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    16(%rsi), %rax
    addq    %rcx, 72(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    24(%rsi), %rax
    addq    %rcx, 80(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    32(%rsi), %rax
    addq    %rcx, 88(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    40(%rsi), %rax
    addq    %rcx, 96(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    48(%rsi), %rax
    addq    %rcx, 104(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    56(%rsi), %rax
    addq    %rcx, 112(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    movq    64(%rsi), %rax
    addq    %rcx, 120(%rdi)
    adcq    $0, %rdx
    movq    %rdx, %rcx
    mulq    %r9
    addq    %rax, %rcx
    adcq    $0, %rdx
    addq    %rcx, 128(%rdi)
    adcq    $0, %rdx
    movq    %rdx, 136(%rdi)

# 0 "" 2
#NO_APP
	popq	%rbp	#
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30:
	.size	mul_9, .-mul_9
	.section	.rodata
.LC1:
	.string	"Time for size 1 is %f \n"
	.text
	.globl	test_mul_1
	.type	test_mul_1, @function
test_mul_1:
.LFB31:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$80, %rsp	#,
	leaq	-64(%rbp), %rax	#, tmp66
	movl	$1, %esi	#,
	movq	%rax, %rdi	# tmp66,
	call	__gmpn_random2	#
	leaq	-80(%rbp), %rax	#, tmp67
	movl	$1, %esi	#,
	movq	%rax, %rdi	# tmp67,
	call	__gmpn_random2	#
	call	clock	#
	movq	%rax, -16(%rbp)	# tmp68, t
	movl	$0, -4(%rbp)	#, i
	jmp	.L11	#
.L12:
	leaq	-80(%rbp), %rdx	#, tmp69
	leaq	-64(%rbp), %rcx	#, tmp70
	leaq	-32(%rbp), %rax	#, tmp71
	movq	%rcx, %rsi	# tmp70,
	movq	%rax, %rdi	# tmp71,
	call	mul_1	#
	leaq	-64(%rbp), %rdx	#, tmp72
	leaq	-80(%rbp), %rcx	#, tmp73
	leaq	-48(%rbp), %rax	#, tmp74
	movq	%rcx, %rsi	# tmp73,
	movq	%rax, %rdi	# tmp74,
	call	mul_1	#
	addl	$1, -4(%rbp)	#, i
.L11:
	cmpl	$9999999, -4(%rbp)	#, i
	jle	.L12	#,
	call	clock	#
	subq	-16(%rbp), %rax	# t, tmp76
	movq	%rax, -16(%rbp)	# tmp76, t
	cvtsi2ssq	-16(%rbp), %xmm0	# t, D.4523
	movss	.LC0(%rip), %xmm1	#, tmp77
	divss	%xmm1, %xmm0	# tmp77, D.4523
	unpcklps	%xmm0, %xmm0	# D.4523, D.4523
	cvtps2pd	%xmm0, %xmm0	# D.4523, D.4524
	movl	$.LC1, %edi	#,
	movl	$1, %eax	#,
	call	printf	#
	leaq	-48(%rbp), %rcx	#, tmp78
	leaq	-32(%rbp), %rax	#, tmp79
	movl	$1, %edx	#,
	movq	%rcx, %rsi	# tmp78,
	movq	%rax, %rdi	# tmp79,
	call	__gmpn_cmp	#
	testl	%eax, %eax	# D.4521
	jne	.L13	#,
	movl	$1, %eax	#, D.4521
	jmp	.L15	#
.L13:
	movl	$0, %eax	#, D.4521
.L15:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE31:
	.size	test_mul_1, .-test_mul_1
	.section	.rodata
.LC2:
	.string	"Time for size 2 is %f \n"
	.text
	.globl	test_mul_2
	.type	test_mul_2, @function
test_mul_2:
.LFB32:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$112, %rsp	#,
	leaq	-96(%rbp), %rax	#, tmp66
	movl	$2, %esi	#,
	movq	%rax, %rdi	# tmp66,
	call	__gmpn_random2	#
	leaq	-112(%rbp), %rax	#, tmp67
	movl	$2, %esi	#,
	movq	%rax, %rdi	# tmp67,
	call	__gmpn_random2	#
	call	clock	#
	movq	%rax, -16(%rbp)	# tmp68, t
	movl	$0, -4(%rbp)	#, i
	jmp	.L17	#
.L18:
	leaq	-112(%rbp), %rdx	#, tmp69
	leaq	-96(%rbp), %rcx	#, tmp70
	leaq	-48(%rbp), %rax	#, tmp71
	movq	%rcx, %rsi	# tmp70,
	movq	%rax, %rdi	# tmp71,
	call	mul_2	#
	leaq	-96(%rbp), %rdx	#, tmp72
	leaq	-112(%rbp), %rcx	#, tmp73
	leaq	-80(%rbp), %rax	#, tmp74
	movq	%rcx, %rsi	# tmp73,
	movq	%rax, %rdi	# tmp74,
	call	mul_2	#
	addl	$1, -4(%rbp)	#, i
.L17:
	cmpl	$9999999, -4(%rbp)	#, i
	jle	.L18	#,
	call	clock	#
	subq	-16(%rbp), %rax	# t, tmp76
	movq	%rax, -16(%rbp)	# tmp76, t
	cvtsi2ssq	-16(%rbp), %xmm0	# t, D.4528
	movss	.LC0(%rip), %xmm1	#, tmp77
	divss	%xmm1, %xmm0	# tmp77, D.4528
	unpcklps	%xmm0, %xmm0	# D.4528, D.4528
	cvtps2pd	%xmm0, %xmm0	# D.4528, D.4529
	movl	$.LC2, %edi	#,
	movl	$1, %eax	#,
	call	printf	#
	leaq	-80(%rbp), %rcx	#, tmp78
	leaq	-48(%rbp), %rax	#, tmp79
	movl	$2, %edx	#,
	movq	%rcx, %rsi	# tmp78,
	movq	%rax, %rdi	# tmp79,
	call	__gmpn_cmp	#
	testl	%eax, %eax	# D.4526
	jne	.L19	#,
	movl	$1, %eax	#, D.4526
	jmp	.L21	#
.L19:
	movl	$0, %eax	#, D.4526
.L21:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE32:
	.size	test_mul_2, .-test_mul_2
	.section	.rodata
.LC3:
	.string	"Time for size 3 is %f \n"
	.text
	.globl	test_mul_3
	.type	test_mul_3, @function
test_mul_3:
.LFB33:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$176, %rsp	#,
	leaq	-144(%rbp), %rax	#, tmp66
	movl	$3, %esi	#,
	movq	%rax, %rdi	# tmp66,
	call	__gmpn_random2	#
	leaq	-176(%rbp), %rax	#, tmp67
	movl	$3, %esi	#,
	movq	%rax, %rdi	# tmp67,
	call	__gmpn_random2	#
	call	clock	#
	movq	%rax, -16(%rbp)	# tmp68, t
	movl	$0, -4(%rbp)	#, i
	jmp	.L23	#
.L24:
	leaq	-176(%rbp), %rdx	#, tmp69
	leaq	-144(%rbp), %rcx	#, tmp70
	leaq	-64(%rbp), %rax	#, tmp71
	movq	%rcx, %rsi	# tmp70,
	movq	%rax, %rdi	# tmp71,
	call	mul_3	#
	leaq	-144(%rbp), %rdx	#, tmp72
	leaq	-176(%rbp), %rcx	#, tmp73
	leaq	-112(%rbp), %rax	#, tmp74
	movq	%rcx, %rsi	# tmp73,
	movq	%rax, %rdi	# tmp74,
	call	mul_3	#
	addl	$1, -4(%rbp)	#, i
.L23:
	cmpl	$9999999, -4(%rbp)	#, i
	jle	.L24	#,
	call	clock	#
	subq	-16(%rbp), %rax	# t, tmp76
	movq	%rax, -16(%rbp)	# tmp76, t
	cvtsi2ssq	-16(%rbp), %xmm0	# t, D.4533
	movss	.LC0(%rip), %xmm1	#, tmp77
	divss	%xmm1, %xmm0	# tmp77, D.4533
	unpcklps	%xmm0, %xmm0	# D.4533, D.4533
	cvtps2pd	%xmm0, %xmm0	# D.4533, D.4534
	movl	$.LC3, %edi	#,
	movl	$1, %eax	#,
	call	printf	#
	leaq	-112(%rbp), %rcx	#, tmp78
	leaq	-64(%rbp), %rax	#, tmp79
	movl	$3, %edx	#,
	movq	%rcx, %rsi	# tmp78,
	movq	%rax, %rdi	# tmp79,
	call	__gmpn_cmp	#
	testl	%eax, %eax	# D.4531
	jne	.L25	#,
	movl	$1, %eax	#, D.4531
	jmp	.L27	#
.L25:
	movl	$0, %eax	#, D.4531
.L27:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE33:
	.size	test_mul_3, .-test_mul_3
	.section	.rodata
.LC4:
	.string	"Time for size 4 is %f \n"
	.text
	.globl	test_mul_4
	.type	test_mul_4, @function
test_mul_4:
.LFB34:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$208, %rsp	#,
	leaq	-176(%rbp), %rax	#, tmp66
	movl	$4, %esi	#,
	movq	%rax, %rdi	# tmp66,
	call	__gmpn_random2	#
	leaq	-208(%rbp), %rax	#, tmp67
	movl	$4, %esi	#,
	movq	%rax, %rdi	# tmp67,
	call	__gmpn_random2	#
	call	clock	#
	movq	%rax, -16(%rbp)	# tmp68, t
	movl	$0, -4(%rbp)	#, i
	jmp	.L29	#
.L30:
	leaq	-208(%rbp), %rdx	#, tmp69
	leaq	-176(%rbp), %rcx	#, tmp70
	leaq	-80(%rbp), %rax	#, tmp71
	movq	%rcx, %rsi	# tmp70,
	movq	%rax, %rdi	# tmp71,
	call	mul_4	#
	leaq	-176(%rbp), %rdx	#, tmp72
	leaq	-208(%rbp), %rcx	#, tmp73
	leaq	-144(%rbp), %rax	#, tmp74
	movq	%rcx, %rsi	# tmp73,
	movq	%rax, %rdi	# tmp74,
	call	mul_4	#
	addl	$1, -4(%rbp)	#, i
.L29:
	cmpl	$9999999, -4(%rbp)	#, i
	jle	.L30	#,
	call	clock	#
	subq	-16(%rbp), %rax	# t, tmp76
	movq	%rax, -16(%rbp)	# tmp76, t
	cvtsi2ssq	-16(%rbp), %xmm0	# t, D.4538
	movss	.LC0(%rip), %xmm1	#, tmp77
	divss	%xmm1, %xmm0	# tmp77, D.4538
	unpcklps	%xmm0, %xmm0	# D.4538, D.4538
	cvtps2pd	%xmm0, %xmm0	# D.4538, D.4539
	movl	$.LC4, %edi	#,
	movl	$1, %eax	#,
	call	printf	#
	leaq	-144(%rbp), %rcx	#, tmp78
	leaq	-80(%rbp), %rax	#, tmp79
	movl	$4, %edx	#,
	movq	%rcx, %rsi	# tmp78,
	movq	%rax, %rdi	# tmp79,
	call	__gmpn_cmp	#
	testl	%eax, %eax	# D.4536
	jne	.L31	#,
	movl	$1, %eax	#, D.4536
	jmp	.L33	#
.L31:
	movl	$0, %eax	#, D.4536
.L33:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE34:
	.size	test_mul_4, .-test_mul_4
	.section	.rodata
.LC5:
	.string	"Time for size 5 is %f \n"
	.text
	.globl	test_mul_5
	.type	test_mul_5, @function
test_mul_5:
.LFB35:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$272, %rsp	#,
	leaq	-224(%rbp), %rax	#, tmp66
	movl	$5, %esi	#,
	movq	%rax, %rdi	# tmp66,
	call	__gmpn_random2	#
	leaq	-272(%rbp), %rax	#, tmp67
	movl	$5, %esi	#,
	movq	%rax, %rdi	# tmp67,
	call	__gmpn_random2	#
	call	clock	#
	movq	%rax, -16(%rbp)	# tmp68, t
	movl	$0, -4(%rbp)	#, i
	jmp	.L35	#
.L36:
	leaq	-272(%rbp), %rdx	#, tmp69
	leaq	-224(%rbp), %rcx	#, tmp70
	leaq	-96(%rbp), %rax	#, tmp71
	movq	%rcx, %rsi	# tmp70,
	movq	%rax, %rdi	# tmp71,
	call	mul_5	#
	leaq	-224(%rbp), %rdx	#, tmp72
	leaq	-272(%rbp), %rcx	#, tmp73
	leaq	-176(%rbp), %rax	#, tmp74
	movq	%rcx, %rsi	# tmp73,
	movq	%rax, %rdi	# tmp74,
	call	mul_5	#
	addl	$1, -4(%rbp)	#, i
.L35:
	cmpl	$9999999, -4(%rbp)	#, i
	jle	.L36	#,
	call	clock	#
	subq	-16(%rbp), %rax	# t, tmp76
	movq	%rax, -16(%rbp)	# tmp76, t
	cvtsi2ssq	-16(%rbp), %xmm0	# t, D.4543
	movss	.LC0(%rip), %xmm1	#, tmp77
	divss	%xmm1, %xmm0	# tmp77, D.4543
	unpcklps	%xmm0, %xmm0	# D.4543, D.4543
	cvtps2pd	%xmm0, %xmm0	# D.4543, D.4544
	movl	$.LC5, %edi	#,
	movl	$1, %eax	#,
	call	printf	#
	leaq	-176(%rbp), %rcx	#, tmp78
	leaq	-96(%rbp), %rax	#, tmp79
	movl	$5, %edx	#,
	movq	%rcx, %rsi	# tmp78,
	movq	%rax, %rdi	# tmp79,
	call	__gmpn_cmp	#
	testl	%eax, %eax	# D.4541
	jne	.L37	#,
	movl	$1, %eax	#, D.4541
	jmp	.L39	#
.L37:
	movl	$0, %eax	#, D.4541
.L39:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE35:
	.size	test_mul_5, .-test_mul_5
	.section	.rodata
.LC6:
	.string	"Time for size 6 is %f \n"
	.text
	.globl	test_mul_6
	.type	test_mul_6, @function
test_mul_6:
.LFB36:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$304, %rsp	#,
	leaq	-256(%rbp), %rax	#, tmp66
	movl	$6, %esi	#,
	movq	%rax, %rdi	# tmp66,
	call	__gmpn_random2	#
	leaq	-304(%rbp), %rax	#, tmp67
	movl	$6, %esi	#,
	movq	%rax, %rdi	# tmp67,
	call	__gmpn_random2	#
	call	clock	#
	movq	%rax, -16(%rbp)	# tmp68, t
	movl	$0, -4(%rbp)	#, i
	jmp	.L41	#
.L42:
	leaq	-304(%rbp), %rdx	#, tmp69
	leaq	-256(%rbp), %rcx	#, tmp70
	leaq	-112(%rbp), %rax	#, tmp71
	movq	%rcx, %rsi	# tmp70,
	movq	%rax, %rdi	# tmp71,
	call	mul_6	#
	leaq	-256(%rbp), %rdx	#, tmp72
	leaq	-304(%rbp), %rcx	#, tmp73
	leaq	-208(%rbp), %rax	#, tmp74
	movq	%rcx, %rsi	# tmp73,
	movq	%rax, %rdi	# tmp74,
	call	mul_6	#
	addl	$1, -4(%rbp)	#, i
.L41:
	cmpl	$9999999, -4(%rbp)	#, i
	jle	.L42	#,
	call	clock	#
	subq	-16(%rbp), %rax	# t, tmp76
	movq	%rax, -16(%rbp)	# tmp76, t
	cvtsi2ssq	-16(%rbp), %xmm0	# t, D.4548
	movss	.LC0(%rip), %xmm1	#, tmp77
	divss	%xmm1, %xmm0	# tmp77, D.4548
	unpcklps	%xmm0, %xmm0	# D.4548, D.4548
	cvtps2pd	%xmm0, %xmm0	# D.4548, D.4549
	movl	$.LC6, %edi	#,
	movl	$1, %eax	#,
	call	printf	#
	leaq	-208(%rbp), %rcx	#, tmp78
	leaq	-112(%rbp), %rax	#, tmp79
	movl	$6, %edx	#,
	movq	%rcx, %rsi	# tmp78,
	movq	%rax, %rdi	# tmp79,
	call	__gmpn_cmp	#
	testl	%eax, %eax	# D.4546
	jne	.L43	#,
	movl	$1, %eax	#, D.4546
	jmp	.L45	#
.L43:
	movl	$0, %eax	#, D.4546
.L45:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE36:
	.size	test_mul_6, .-test_mul_6
	.section	.rodata
.LC7:
	.string	"Time for size 7 is %f \n"
	.text
	.globl	test_mul_7
	.type	test_mul_7, @function
test_mul_7:
.LFB37:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$368, %rsp	#,
	leaq	-304(%rbp), %rax	#, tmp66
	movl	$7, %esi	#,
	movq	%rax, %rdi	# tmp66,
	call	__gmpn_random2	#
	leaq	-368(%rbp), %rax	#, tmp67
	movl	$7, %esi	#,
	movq	%rax, %rdi	# tmp67,
	call	__gmpn_random2	#
	call	clock	#
	movq	%rax, -16(%rbp)	# tmp68, t
	movl	$0, -4(%rbp)	#, i
	jmp	.L47	#
.L48:
	leaq	-368(%rbp), %rdx	#, tmp69
	leaq	-304(%rbp), %rcx	#, tmp70
	leaq	-128(%rbp), %rax	#, tmp71
	movq	%rcx, %rsi	# tmp70,
	movq	%rax, %rdi	# tmp71,
	call	mul_7	#
	leaq	-304(%rbp), %rdx	#, tmp72
	leaq	-368(%rbp), %rcx	#, tmp73
	leaq	-240(%rbp), %rax	#, tmp74
	movq	%rcx, %rsi	# tmp73,
	movq	%rax, %rdi	# tmp74,
	call	mul_7	#
	addl	$1, -4(%rbp)	#, i
.L47:
	cmpl	$9999999, -4(%rbp)	#, i
	jle	.L48	#,
	call	clock	#
	subq	-16(%rbp), %rax	# t, tmp76
	movq	%rax, -16(%rbp)	# tmp76, t
	cvtsi2ssq	-16(%rbp), %xmm0	# t, D.4553
	movss	.LC0(%rip), %xmm1	#, tmp77
	divss	%xmm1, %xmm0	# tmp77, D.4553
	unpcklps	%xmm0, %xmm0	# D.4553, D.4553
	cvtps2pd	%xmm0, %xmm0	# D.4553, D.4554
	movl	$.LC7, %edi	#,
	movl	$1, %eax	#,
	call	printf	#
	leaq	-240(%rbp), %rcx	#, tmp78
	leaq	-128(%rbp), %rax	#, tmp79
	movl	$7, %edx	#,
	movq	%rcx, %rsi	# tmp78,
	movq	%rax, %rdi	# tmp79,
	call	__gmpn_cmp	#
	testl	%eax, %eax	# D.4551
	jne	.L49	#,
	movl	$1, %eax	#, D.4551
	jmp	.L51	#
.L49:
	movl	$0, %eax	#, D.4551
.L51:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE37:
	.size	test_mul_7, .-test_mul_7
	.section	.rodata
.LC8:
	.string	"Time for size 8 is %f \n"
	.text
	.globl	test_mul_8
	.type	test_mul_8, @function
test_mul_8:
.LFB38:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$400, %rsp	#,
	leaq	-336(%rbp), %rax	#, tmp66
	movl	$8, %esi	#,
	movq	%rax, %rdi	# tmp66,
	call	__gmpn_random2	#
	leaq	-400(%rbp), %rax	#, tmp67
	movl	$8, %esi	#,
	movq	%rax, %rdi	# tmp67,
	call	__gmpn_random2	#
	call	clock	#
	movq	%rax, -16(%rbp)	# tmp68, t
	movl	$0, -4(%rbp)	#, i
	jmp	.L53	#
.L54:
	leaq	-400(%rbp), %rdx	#, tmp69
	leaq	-336(%rbp), %rcx	#, tmp70
	leaq	-144(%rbp), %rax	#, tmp71
	movq	%rcx, %rsi	# tmp70,
	movq	%rax, %rdi	# tmp71,
	call	mul_8	#
	leaq	-336(%rbp), %rdx	#, tmp72
	leaq	-400(%rbp), %rcx	#, tmp73
	leaq	-272(%rbp), %rax	#, tmp74
	movq	%rcx, %rsi	# tmp73,
	movq	%rax, %rdi	# tmp74,
	call	mul_8	#
	addl	$1, -4(%rbp)	#, i
.L53:
	cmpl	$9999999, -4(%rbp)	#, i
	jle	.L54	#,
	call	clock	#
	subq	-16(%rbp), %rax	# t, tmp76
	movq	%rax, -16(%rbp)	# tmp76, t
	cvtsi2ssq	-16(%rbp), %xmm0	# t, D.4558
	movss	.LC0(%rip), %xmm1	#, tmp77
	divss	%xmm1, %xmm0	# tmp77, D.4558
	unpcklps	%xmm0, %xmm0	# D.4558, D.4558
	cvtps2pd	%xmm0, %xmm0	# D.4558, D.4559
	movl	$.LC8, %edi	#,
	movl	$1, %eax	#,
	call	printf	#
	leaq	-272(%rbp), %rcx	#, tmp78
	leaq	-144(%rbp), %rax	#, tmp79
	movl	$8, %edx	#,
	movq	%rcx, %rsi	# tmp78,
	movq	%rax, %rdi	# tmp79,
	call	__gmpn_cmp	#
	testl	%eax, %eax	# D.4556
	jne	.L55	#,
	movl	$1, %eax	#, D.4556
	jmp	.L57	#
.L55:
	movl	$0, %eax	#, D.4556
.L57:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE38:
	.size	test_mul_8, .-test_mul_8
	.section	.rodata
.LC9:
	.string	"Time for size 9 is %f \n"
	.text
	.globl	test_mul_9
	.type	test_mul_9, @function
test_mul_9:
.LFB39:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$464, %rsp	#,
	leaq	-384(%rbp), %rax	#, tmp66
	movl	$9, %esi	#,
	movq	%rax, %rdi	# tmp66,
	call	__gmpn_random2	#
	leaq	-464(%rbp), %rax	#, tmp67
	movl	$9, %esi	#,
	movq	%rax, %rdi	# tmp67,
	call	__gmpn_random2	#
	call	clock	#
	movq	%rax, -16(%rbp)	# tmp68, t
	movl	$0, -4(%rbp)	#, i
	jmp	.L59	#
.L60:
	leaq	-464(%rbp), %rdx	#, tmp69
	leaq	-384(%rbp), %rcx	#, tmp70
	leaq	-160(%rbp), %rax	#, tmp71
	movq	%rcx, %rsi	# tmp70,
	movq	%rax, %rdi	# tmp71,
	call	mul_9	#
	leaq	-384(%rbp), %rdx	#, tmp72
	leaq	-464(%rbp), %rcx	#, tmp73
	leaq	-304(%rbp), %rax	#, tmp74
	movq	%rcx, %rsi	# tmp73,
	movq	%rax, %rdi	# tmp74,
	call	mul_9	#
	addl	$1, -4(%rbp)	#, i
.L59:
	cmpl	$9999999, -4(%rbp)	#, i
	jle	.L60	#,
	call	clock	#
	subq	-16(%rbp), %rax	# t, tmp76
	movq	%rax, -16(%rbp)	# tmp76, t
	cvtsi2ssq	-16(%rbp), %xmm0	# t, D.4563
	movss	.LC0(%rip), %xmm1	#, tmp77
	divss	%xmm1, %xmm0	# tmp77, D.4563
	unpcklps	%xmm0, %xmm0	# D.4563, D.4563
	cvtps2pd	%xmm0, %xmm0	# D.4563, D.4564
	movl	$.LC9, %edi	#,
	movl	$1, %eax	#,
	call	printf	#
	leaq	-304(%rbp), %rcx	#, tmp78
	leaq	-160(%rbp), %rax	#, tmp79
	movl	$9, %edx	#,
	movq	%rcx, %rsi	# tmp78,
	movq	%rax, %rdi	# tmp79,
	call	__gmpn_cmp	#
	testl	%eax, %eax	# D.4561
	jne	.L61	#,
	movl	$1, %eax	#, D.4561
	jmp	.L63	#
.L61:
	movl	$0, %eax	#, D.4561
.L63:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE39:
	.size	test_mul_9, .-test_mul_9
	.section	.rodata
.LC10:
	.string	"Test 1 failed "
.LC11:
	.string	"Test 2 failed "
.LC12:
	.string	"Test 3 failed "
.LC13:
	.string	"Test 4 failed "
.LC14:
	.string	"Test 5 failed "
.LC15:
	.string	"Test 6 failed "
.LC16:
	.string	"Test 7 failed "
.LC17:
	.string	"Test 8 failed "
.LC18:
	.string	"Test 9 failed "
	.text
	.globl	main
	.type	main, @function
main:
.LFB40:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	subq	$32, %rsp	#,
	movl	%edi, -20(%rbp)	# argc, argc
	movq	%rsi, -32(%rbp)	# argv, argv
	movl	$0, %eax	#,
	call	test_mul_1	#
	movl	%eax, -4(%rbp)	# tmp60, r
	cmpl	$0, -4(%rbp)	#, r
	jne	.L65	#,
	movl	$.LC10, %edi	#,
	call	puts	#
.L65:
	movl	$0, %eax	#,
	call	test_mul_2	#
	movl	%eax, -4(%rbp)	# tmp61, r
	cmpl	$0, -4(%rbp)	#, r
	jne	.L66	#,
	movl	$.LC11, %edi	#,
	call	puts	#
.L66:
	movl	$0, %eax	#,
	call	test_mul_3	#
	movl	%eax, -4(%rbp)	# tmp62, r
	cmpl	$0, -4(%rbp)	#, r
	jne	.L67	#,
	movl	$.LC12, %edi	#,
	call	puts	#
.L67:
	movl	$0, %eax	#,
	call	test_mul_4	#
	movl	%eax, -4(%rbp)	# tmp63, r
	cmpl	$0, -4(%rbp)	#, r
	jne	.L68	#,
	movl	$.LC13, %edi	#,
	call	puts	#
.L68:
	movl	$0, %eax	#,
	call	test_mul_5	#
	movl	%eax, -4(%rbp)	# tmp64, r
	cmpl	$0, -4(%rbp)	#, r
	jne	.L69	#,
	movl	$.LC14, %edi	#,
	call	puts	#
.L69:
	movl	$0, %eax	#,
	call	test_mul_6	#
	movl	%eax, -4(%rbp)	# tmp65, r
	cmpl	$0, -4(%rbp)	#, r
	jne	.L70	#,
	movl	$.LC15, %edi	#,
	call	puts	#
.L70:
	movl	$0, %eax	#,
	call	test_mul_7	#
	movl	%eax, -4(%rbp)	# tmp66, r
	cmpl	$0, -4(%rbp)	#, r
	jne	.L71	#,
	movl	$.LC16, %edi	#,
	call	puts	#
.L71:
	movl	$0, %eax	#,
	call	test_mul_8	#
	movl	%eax, -4(%rbp)	# tmp67, r
	cmpl	$0, -4(%rbp)	#, r
	jne	.L72	#,
	movl	$.LC17, %edi	#,
	call	puts	#
.L72:
	movl	$0, %eax	#,
	call	test_mul_9	#
	movl	%eax, -4(%rbp)	# tmp68, r
	cmpl	$0, -4(%rbp)	#, r
	jne	.L74	#,
	movl	$.LC18, %edi	#,
	call	puts	#
.L74:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE40:
	.size	main, .-main
	.section	.rodata
	.align 4
.LC0:
	.long	1232348160
	.ident	"GCC: (GNU) 4.8.2 20131219 (prerelease)"
	.section	.note.GNU-stack,"",@progbits
