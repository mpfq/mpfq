#!/usr/bin/env perl

use strict;
use warnings;
use List::Util qw[min max];





sub mul($) {
  my $k = $_[0];
  
  if ($k == 1) {
    my $code = <<EOF;
static void 
mul_1(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    mulq %3\\n"
  : "=a" (z[0]), "=d" (z[1])
  : "0" (x[0]), "rm1" (y[0])
  : "cc");
}
EOF
    return $code;
  }
  elsif ($k == 2) {
    my $code = <<EOF;
static void 
mul_2(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    movq %5, %%rax\\n"
   "    mulq %7\\n"
   "    movq %%rax, %2\\n"
   "    movq %%rdx, %3\\n"
   "    movq %4, %%rax\\n"
   "    mulq %6\\n"
   "    movq %%rax, %0\\n"
   "    movq %%rdx, %1\\n"
   "    movq %4, %%rax\\n"
   "    mulq %7\\n"
   "    addq %%rax, %1\\n"
   "    adcq %%rdx, %2\\n"
   "    adcq \$0, %3\\n"
   "    movq %5, %%rax\\n"
   "    mulq %6\\n"
   "    addq %%rax, %1\\n"
   "    adcq %%rdx, %2\\n"
   "    adcq \$0, %3\\n"
  : "=&rm" (z[0]), "=&r" (z[1]), "=&r" (z[2]), "=&r" (z[3])
  : "r" (x[0]), "r" (x[1]), "r" (y[0]), "r" (y[1])
  : "%rax", "%rdx", "cc");
}
EOF
    return $code;
  }
  elsif ($k == 53) {
    my $code = <<EOF;
static void 
mul_2(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    movq %4, %%rax\\n"
   "    mulq %6\\n"
   "    movq %%rax, %0\\n"
   "    movq %%rdx, %1\\n"
   "    movq %4, %%rax\\n"
   "    mulq %7\\n"
   "    addq %%rax, %1\\n"
   "    xorq %2,  %2\\n"
   "    adcq %%rdx, %2\\n"
   "    xorq %3, %3\\n"
   "    adcq \$0, %3\\n"
   "    movq %5, %%rax\\n"
   "    mulq %6\\n"
   "    addq %%rax, %1\\n"
   "    adcq %%rdx, %2\\n"
   "    adcq \$0, %3\\n"
   "    movq %5, %%rax\\n"
   "    mulq %7\\n"
   "    addq %%rax, %2\\n"
   "    adcq %%rdx, %3\\n"
  : "=&rm" (z[0]), "=&rm" (z[1]), "=&rm" (z[2]), "=&rm" (z[3])
  : "rm" (x[0]), "rm" (x[1]), "rm" (y[0]), "rm" (y[1])
  : "%rax", "%rdx", "cc");
}
EOF
    return $code;
  }
  elsif ($k == 49) {
    my $code = <<EOF;
static void 
mul_2(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    movq %4, %%rax\\n"
   "    mulq %6\\n"
   "    movq %%rax, %%rcx\\n"
   "    movq %%rcx, %0\\n"
   "    movq %%rdx, %1\\n"
   "    movq %5, %%rax\\n"
   "    mulq %7\\n"
   "    movq %%rax, %2\\n"
   "    movq %%rdx, %3\\n"
   "    movq %4, %%rax\\n"
   "    mulq %7\\n"
   "    movq %%rax, %%rbx\\n"
   "    movq %%rdx, %%rcx\\n"
   "    movq %5, %%rax\\n"
   "    mulq %6\\n"
   "    addq %%rbx, %1\\n"
   "    adcq %%rcx, %2\\n"
   "    adcq \$0, %3\\n"
   "    addq %%rax, %1\\n"
   "    adcq %%rdx, %2\\n"
   "    adcq \$0, %3\\n"
  : "+rm" (z[0]), "+r" (z[1]), "+r" (z[2]), "+r" (z[3])
  : "r" (x[0]), "r" (x[1]), "r" (y[0]), "r" (y[1])
  : "%rax", "%rbx", "%rcx", "%rdx", "cc");
}
EOF
    return $code;
  }
  elsif ($k == 22) {
    my $code = <<EOF;
static void 
mul_2(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    movq %4, %%rax\\n"
   "    mulq %6\\n"
   "    movq %%rax, %%rcx\\n"
   "    movq %%rcx, %0\\n"
   "    movq %%rdx, %1\\n"
   "    movq %5, %%rax\\n"
   "    mulq %7\\n"
   "    movq %%rax, %2\\n"
   "    movq %%rdx, %3\\n"
   "    movq %4, %%rax\\n"
   "    mulq %7\\n"
   "    movq %%rax, %%rbx\\n"
   "    movq %%rdx, %%rcx\\n"
   "    movq %5, %%rax\\n"
   "    mulq %6\\n"
   "    addq %%rbx, %1\\n"
   "    adcq %%rcx, %2\\n"
   "    adcq \$0, %3\\n"
   "    addq %%rax, %1\\n"
   "    adcq %%rdx, %2\\n"
   "    adcq \$0, %3\\n"
  : "+rm" (z[0]), "+r" (z[1]), "+r" (z[2]), "+r" (z[3])
  : "r" (x[0]), "r" (x[1]), "r" (y[0]), "r" (y[1])
  : "%rax", "%rbx", "%rcx", "%rdx", "cc");
}
EOF
    return $code;
  }
  elsif ($k == 72) {
   my $code_init1 = <<EOF;
static void
mul_$k(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "  ### START OF MUL\\n"
   "  ### x*y[0]\\n"
   "    movq	%2, %%r8\\n"
   "    movq    %0, %%rdi\\n"
   "    movq	(%%r8), %%r9\\n"
   "    movq    %1, %%rsi\\n"
   "    movq    (%%rsi), %%rax\\n"
   "    mulq    %%r9\\n"
   "    movq    %%rax, (%%rdi)\\n"
   "    movq    8(%%rsi), %%rax\\n"
   "    movq    %%rdx, %%rcx\\n"
EOF

  my $loop1_code = <<EOF;
   "    mulq    %%r9\\n"
   "    addq    %%rax, %%rcx\\n"
   "    adcq    \$0, %%rdx\\n"
   "    movq    __xi, %%rax\\n"
   "    movq    %%rcx, __zi\\n"
   "    movq    %%rdx, %%rcx\\n"
EOF

  my $final1_code = <<EOF;
   "    mulq    %%r9\\n"
   "    addq    %%rax, %%rcx\\n"
   "    adcq    \$0, %%rdx\\n"
   "    movq    %%rcx, __zi\\n"
   "    movq    %%rdx, __zend\\n"
EOF

  my $str;
  $str = "" . (8*($k-1)) . "(%%rdi)"; $final1_code =~ s/__zi/$str/g;
  $str = "" . (8*$k) . "(%%rdi)"; $final1_code =~ s/__zend/$str/g;
 
  my $i;
  my $j;

  my $code = $code_init1;
  for ($i=1; $i<$k-1; $i++) {
    my $code_i = $loop1_code;
    $str = "".(8*($i+1))."(%%rsi)"; $code_i =~ s/__xi/$str/g;
    $str = "".(8*$i)."(%%rdi)"; $code_i =~ s/__zi/$str/g;
    $code = $code . $code_i;
  }
  $code = $code . $final1_code;

  for ($i=$k+1; $i < 2*$k; $i++) {
    $j = 8*$i;
    my $raz = <<EOF;
   "	movq	\$0, $j(%%rdi)\\n"
EOF
    $code = $code . $raz;
  }

  for ($j=1; $j < $k; $j++) {
    my $j8 = 8*$j;


  my $code_init2 = <<EOF;
   "  ### x*y[$j]\\n"
   "    movq	$j8(%%r8), %%r9\\n"
   "    movq    (%%rsi), %%rax\\n"
   "    mulq    %%r9\\n"
   "    addq    %%rax, $j8(%%rdi)\\n"
   "    movq    8(%%rsi), %%rax\\n"
   "    adcq    \$0, %%rdx\\n"
   "    movq    %%rdx, %%rcx\\n"
EOF

  my $loop2_code = <<EOF;
   "    mulq    %%r9\\n"
   "    addq    %%rax, %%rcx\\n"
   "    adcq    \$0, %%rdx\\n"
   "    movq    __xi, %%rax\\n"
   "    addq    %%rcx, __zi\\n"
   "    adcq    \$0, %%rdx\\n"
   "    movq    %%rdx, %%rcx\\n"
EOF

  my $final2_code = <<EOF;
   "    mulq    %%r9\\n"
   "    addq    %%rax, %%rcx\\n"
   "    adcq    \$0, %%rdx\\n"
   "    addq    %%rcx, __zi\\n"
   "    adcq    \$0, %%rdx\\n"
   "    movq    %%rdx, __zend\\n"
EOF

  $str = "" . (8*($k+$j-1)) . "(%%rdi)"; $final2_code =~ s/__zi/$str/g;
  $str = "" . (8*($k+$j)) . "(%%rdi)"; $final2_code =~ s/__zend/$str/g;
 

  $code = $code . $code_init2;
  for ($i=1; $i<$k-1; $i++) {
    my $code_i = $loop2_code;
    $str = "".(8*($i+1))."(%%rsi)"; $code_i =~ s/__xi/$str/g;
    $str = "".(8*($i+$j))."(%%rdi)"; $code_i =~ s/__zi/$str/g;
    $code = $code . $code_i;
  }
  $code = $code . $final2_code;
  }

  my $final_final = <<EOF;
  : "+m" (z)
  : "m" (x), "m" (y)
  : "%rax", "%rcx", "%rdx", "%rsi", "%rdi", "%r8", "%r9", "%r10", "memory");
}
EOF
  $code = $code . $final_final;
  return $code;
  }


my $code_init = <<EOF;
static void
mul_$k(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "  ### START OF MUL\\n"
   "    movq	%2, %%rsi\\n"
   "    movq    %1, %%rdi\\n"
   "    movq    %0, %%rcx\\n"
   "    movq	(%%rsi), %%rax\\n"
   "    movq    (%%rdi), %%r8\\n"
   "    mulq    %%r8\\n"
   "    movq    %%rax, %%r11\\n"
   "    movq    %%r11, (%%rcx)\\n"
   "    movq    %%rdx, %%r9\\n"
   "    xorq    %%r10, %%r10\\n"
   "    xorq    %%r11, %%r11\\n"
EOF

  my $i;
  my $j;
  my $str;
  my $r9;
  my $r10;
  my $r11;
  my $code=$code_init;

  for ($i=1; $i<$k; $i++) {
    if ($i%3==1) {
      $r9=9; $r10=10; $r11=11;
    } elsif ($i%3==2) {
      $r9=10; $r10=11; $r11=9;
    } else {
      $r9=11; $r10=9; $r11=10;
    }

    my $code_init_loop=<<EOF;
   "    movq    __ak__, %%rax\\n"
   "    mulq    %%r8\\n"
   "    addq    %%rax, %%r${r9}\\n"
   "    adcq    %%rdx, %%r${r10}\\n"
   "    adcq    \$0, %%r${r11}\\n"
EOF
    my $code_loop=<<EOF;
   "    movq    __ak__, %%r8\\n"
   "    movq    __bk__, %%rax\\n"
   "    mulq    %%r8\\n"
   "    addq    %%rax, %%r${r9}\\n"
   "    adcq    %%rdx, %%r${r10}\\n"
   "    adcq    \$0, %%r${r11}\\n"
EOF
    if ($i%2==1) {
      $str= "".(8*$i)."(%%rsi)";
      $code_init_loop =~ s/__ak__/$str/g;
    }
    else {
      $str= "".(8*$i)."(%%rdi)";
      $code_init_loop =~ s/__ak__/$str/g;
    }
    $code=$code.$code_init_loop;


    for ($j=1; $j<=$i; $j++) {
      my $code_j=$code_loop;
      if ($i%2==1) {
        $str="".(8*($i-$j))."(%%rsi)";  $code_j =~ s/__ak__/$str/g;
        $str="".(8*$j)."(%%rdi)";   $code_j =~ s/__bk__/$str/g;
      }
      else {
        $str="".(8*($i-$j))."(%%rdi)";  $code_j =~ s/__ak__/$str/g;
        $str="".(8*$j)."(%%rsi)";   $code_j =~ s/__bk__/$str/g;
      }
      $code=$code . $code_j;
    }
    my $code_final_loop=<<EOF;
   "    movq    %%r${r9}, __zk__\\n"
   "    xorq    %%r${r9}, %%r${r9}\\n"
EOF
    $str = "".(8*$i)."(%%rcx)"; $code_final_loop =~ s/__zk__/$str/g;
    $code=$code . $code_final_loop;
  }

  my $code_between_loop=<<EOF;
   "    movq    __bk__, %%r8\\n"    
EOF
  if ($k%2==1) {
    $str="".(8*($k-1))."(%%rdi)";
  } else {
    $str="".(8*($k-1))."(%%rsi)";
  }
  $code_between_loop=~ s/__bk__/$str/g;
  $code=$code.$code_between_loop;

  for ($i=$k; $i<2*$k-1; $i++) {
    if ($i%3==1) {
      $r9=9; $r10=10; $r11=11;
    } elsif ($i%3==2) {
      $r9=10; $r10=11; $r11=9;
    } else {
      $r9=11; $r10=9; $r11=10;
    }
    my $code_init_loop=<<EOF;
   "    movq    __ak__, %%rax\\n"
   "    mulq    %%r8\\n"
   "    addq    %%rax, %%r${r9}\\n"
   "    adcq    %%rdx, %%r${r10}\\n"
   "    adcq    \$0, %%r${r11}\\n"
EOF
    my $code_loop=<<EOF;
   "    movq    __ak__, %%r8\\n"
   "    movq    __bk__, %%rax\\n"
   "    mulq    %%r8\\n"
   "    addq    %%rax, %%r${r9}\\n"
   "    adcq    %%rdx, %%r${r10}\\n"
   "    adcq    \$0, %%r${r11}\\n"
EOF
    if ($i%2==1) {
      $str= "".(8*($i-$k+1))."(%%rsi)";
      $code_init_loop =~ s/__ak__/$str/g;
    }
    else {
      $str= "".(8*($i-$k+1))."(%%rdi)";
      $code_init_loop =~ s/__ak__/$str/g;
    }
    $code=$code.$code_init_loop;


    for ($j=$i-$k+2; $j<=$k-1; $j++) {
      my $code_j=$code_loop;
      if ($i%2==1) {
        $str="".(8*$j)."(%%rsi)";  $code_j =~ s/__ak__/$str/g;
        $str="".(8*($i-$j))."(%%rdi)";   $code_j =~ s/__bk__/$str/g;
      }
      else {
        $str="".(8*$j)."(%%rdi)";  $code_j =~ s/__ak__/$str/g;
        $str="".(8*($i-$j))."(%%rsi)";   $code_j =~ s/__bk__/$str/g;
      }
      $code=$code . $code_j;
    }

    my $code_final_loop=<<EOF;
   "    movq    %%r${r9}, __zk__\\n"
   "    xorq    %%r${r11}, %%r${r11}\\n"
EOF
    $str = "".(8*$i)."(%%rcx)"; $code_final_loop =~ s/__zk__/$str/g;
    $code=$code . $code_final_loop;
  }
  my $code_final=<<EOF;
   "	movq    %%r${r9}, __zk__\\n"
  : "+m" (z)
  : "m" (x), "m" (y)
  : "%rax", "%rcx", "%rdx", "%rsi", "%rdi", "%r8", "%r9", "%r10", "%r11", "memory");
}
EOF
  $str = "".(8*(2*$k-1))."(%%rcx)"; $code_final =~ s/__zk__/$str/g;
  $code=$code . $code_final;

  return $code;
}


print <<EOF;
#ifndef __MP_X86_64_H__
#define __MP_X86_64_H__

#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>

EOF

my $i;
for ($i=1; $i <= 9; $i++) {
  print "#define HAVE_NATIVE_MUL_$i 1\n";
  print mul($i);
  print "\n";
}


print "#endif  /* __MP_X86_64_H__ */\n";
