#!/usr/bin/env perl

use strict;
use warnings;


sub test_mul($){
  my $k=$_[0];

  print <<EOF;

int test_mul_$k() {
  int i;
  mp_limb_t z1[2*$k],z2[2*$k],x[$k],y[$k];
  mpn_random2(x,$k);
  mpn_random2(y,$k);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_$k(z1,x,y);
    mul_$k(z2,y,x);
  }
  t=clock()-t;
  printf("Time for size $k is %f \\n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,$k)){
    return 1;
  } else {
    return 0;
  }
}


EOF
}

print <<EOF;
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <time.h>
#include "test.h"

EOF

my $i;
for($i=1;$i<=9;$i++){
  test_mul($i);
}


print <<EOF;
int main(int argc,char** argv) {
  int r;
EOF

for($i=1;$i<=9;$i++){
  print <<EOF;
  r=test_mul_$i();
  if (r==0) {
    printf("Test $i failed \\n");
  }

EOF
}

print <<EOF;

}
EOF



