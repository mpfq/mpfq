#!/usr/bin/env perl

use strict;
use warnings;



my @tags = ( "{3590070611UL}", 
"{11775104716989943513UL, 1796980749UL}", 
"{17050272432866984737UL, 3353163867485029985UL, 988244759UL}", 
"{2206429735340539325UL, 2703064808292489982UL, 10192176153193413236UL, 3946456023UL}", 
"{8839869599038134703UL, 13433604345807532167UL, 7401145531175911196UL, 18023616845661225020UL, 3332188521UL}", 
"{3407572160728331863UL, 6562881139957978849UL, 9535319254660960624UL, 2770163375132785538UL, 11328177101588406675UL, 4291934504UL}", 
"{5974730715497873739UL, 7337360916571431374UL, 13536694044389392091UL, 15195432347125187936UL, 11443401088610534793UL, 15678423199871483068UL, 3295788943UL}", 
"{10466957513140287029UL, 3113695012517118927UL, 6960488871932873857UL, 11347940645492033705UL, 11059946596038591224UL, 14275060941503081199UL, 11224200804726121796UL, 402381695UL}", 
"{3756047561460838329UL, 5812076596499107817UL, 18253228595075811111UL, 9664778058569613466UL, 9137663104656666960UL, 16987088462797838203UL, 11238444737618360136UL, 9086888863712057041UL, 2346950513UL}" )



sub addmul_si_ur($) {
  my $k=$_[0]

  print <<EOF;
void mpfq_p_$k_addmul_si_ur(mpfq_p_$k_dst_field MAYBE_UNUSED K, mpfq_p_$k_dst_elt w, mpfq_p_$k_src_elt  u, long v){
mpfq_p_$k_elt_ur s;
mpfq_p_$k_elt vx;
mpfq_p_$k_elt_ur_init(K, &s);
mpfq_p_$k_init(K, &vx);
    if (v>0) {
        mpfq_p_$k_set_ui(K, vx, v);
        mpfq_p_$k_mul_ur(K, s, u, vx);
        mpfq_p_$k_elt_ur_add(K, w, w, s);
    } else {
        mpfq_p_$k_set_ui(K, vx, -v);
        mpfq_p_$k_mul_ur(K, s, u, vx);
        mpfq_p_$k_elt_ur_sub(K, w, w, s);
    }
    mpfq_p_$k_clear(K, &vx);
    mpfq_p_$k_elt_ur_clear(K, &s);
}
EOF
}


sub bench_addmul_si_ur($) {
  my $k=$_[0]

  print <<EOF;
int bench_addmul_si_ur_$k() {
gmp_randstate rstate;
unsigned long seed =(unsigned long) time (NULL);
gmp_randinit_mt(rstate);
gmp_randseed_ui(rstate, seed); 

mpfq_p_$k_field K;
mp_limb_t p[$k] = @tags[$k+1];
mpfq_p_$k_field_init(K);
mpfq_p_$k_field_specify(K, MPFQ_PRIME_MPN, p);

mpfq_p_$k_elt_ur eri,ere;
mpfq_p_$k_dst_vec v;
long l[100000];
mpfq_p_$k_elt_ur_init(K, &eri);
mpfq_p_$k_elt_ur_init(K, &ere);
mpfq_p_$k_vec_init(K, &v, 100000);




}

EOF
}


print <<EOF;
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gmp.h>
#include <time.h>
#include "mpfq.h"

EOF

my $i;
for($i=1; $i<=9; $i++) {
  addmul_si_ur($i);
}

my $i;
for($i=1; $i<=9; $i++) {
  bench_addmul_si_ur($i);
}

  print <<EOF;
int main(int argc, char** argv) {
  int r;
EOF

for($i=1; $i=9;$i++) {
  print <<EOF;
  r=bench_addmul_si_ur_$i();
  if (r==0) {
    printf("Test $i failed \\n");
  }

EOF
}

print <<EOF;

}
EOF



 


}
