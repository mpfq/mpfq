#!/usr/bin/env perl

use strict;
use warnings;





sub mul($) {
  my $k = $_[0];
  
  if ($k == 1) {
    my $code = <<EOF;
static void 
mul_$k(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    movq %5, %%rax\\n"
   "    mulq %7\\n"
   "    movq %%rax, %2\\n"
   "    movq %%rdx, %3\\n"
   "    movq %4, %%rax\\n"
   "    mulq %6\\n"
   "    movq %%rax, %0\\n"
   "    movq %%rdx, %1\\n"
   "    movq %4, %%rax\\n"
   "    mulq %7\\n"
   "    addq %%rax, %1\\n"
   "    adcq %%rdx, %2\\n"
   "    adcq \$0, %3\\n"
   "    movq %5, %%rax\\n"
   "    mulq %6\\n"
   "    addq %%rax, %1\\n"
   "    adcq %%rdx, %2\\n"
   "    adcq \$0, %3\\n"
  : "=&rm" (z[0]), "=&r" (z[1]), "=&r" (z[2]), "=&r" (z[3])
  : "r" (x[0]), "r" (x[1]), "r" (y[0]), "r" (y[1])
  : "%rax", "%rdx", "cc");
}
EOF
    return $code;
  }
  elsif ($k == 5) {
    my $code = <<EOF;
static void 
mul_$k(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    movq %5, %%rax\\n"
   "    mulq %6\\n"
   "    movq %%rax, %1\\n"
   "    movq %%rdx, %2\\n"
   "    movq %4, %%rax\\n"
   "    mulq %7\\n"
   "    movq %%rax, %%rcx\\n"
   "    movq %%rdx, %%r8\\n"
   "    movq %5, %%rax\\n"
   "    mulq %7\\n"
   "    addq %%rcx, %1\\n"
   "    adcq %%r8, %2\\n"
   "    movq %%rdx, %2\\n"
   "    adcq \$0, %3\\n"
   "    addq %%rax, %2\\n"
   "    adcq \$0, %3\\n"
   "    movq %4, %%rax\\n"
   "    mulq %6\\n"
   "    movq %%rax, %0\\n"
   "    addq %%rdx, %1\\n"
   "    adcq \$0, %2\\n"
   "    adcq \$0, %3\\n"
  : "=&rm" (z[0]), "=&r" (z[1]), "=&r" (z[2]), "=&r" (z[3])
  : "r" (x[0]), "r" (x[1]), "r" (y[0]), "r" (y[1])
  : "%rax", "%rcx", "%rdx", "%r8", "cc");
}
EOF
    return $code;
  }
  elsif ($k == 3) {
    my $code = <<EOF;
static void 
mul_$k(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    movq %4, %%rax\\n"
   "    mulq %6\\n"
   "    movq %%rax, %%rcx\\n"
   "    movq %%rcx, %0\\n"
   "    movq %%rdx, %1\\n"
   "    movq %5, %%rax\\n"
   "    mulq %7\\n"
   "    movq %%rax, %2\\n"
   "    movq %%rdx, %3\\n"
   "    movq %4, %%rax\\n"
   "    mulq %7\\n"
   "    movq %%rax, %%rbx\\n"
   "    movq %%rdx, %%rcx\\n"
   "    movq %5, %%rax\\n"
   "    mulq %6\\n"
   "    addq %%rbx, %1\\n"
   "    adcq %%rcx, %2\\n"
   "    adcq \$0, %3\\n"
   "    addq %%rax, %1\\n"
   "    adcq %%rdx, %2\\n"
   "    adcq \$0, %3\\n"
  : "+rm" (z[0]), "+r" (z[1]), "+r" (z[2]), "+r" (z[3])
  : "r" (x[0]), "r" (x[1]), "r" (y[0]), "r" (y[1])
  : "%rax", "%rbx", "%rcx", "%rdx", "cc");
}
EOF
    return $code;
  }
  elsif ($k == 4) {
    my $code = <<EOF;
static void 
mul_$k(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    movq %4, %%rax\\n"
   "    mulq %6\\n"
   "    movq %%rax, %%rcx\\n"
   "    movq %%rcx, %0\\n"
   "    movq %%rdx, %1\\n"
   "    movq %5, %%rax\\n"
   "    mulq %7\\n"
   "    movq %%rax, %2\\n"
   "    movq %%rdx, %3\\n"
   "    movq %4, %%rax\\n"
   "    mulq %7\\n"
   "    movq %%rax, %%rbx\\n"
   "    movq %%rdx, %%rcx\\n"
   "    movq %5, %%rax\\n"
   "    mulq %6\\n"
   "    addq %%rbx, %1\\n"
   "    adcq %%rcx, %2\\n"
   "    adcq \$0, %3\\n"
   "    addq %%rax, %1\\n"
   "    adcq %%rdx, %2\\n"
   "    adcq \$0, %3\\n"
  : "+rm" (z[0]), "+r" (z[1]), "+r" (z[2]), "+r" (z[3])
  : "r" (x[0]), "r" (x[1]), "r" (y[0]), "r" (y[1])
  : "%rax", "%rbx", "%rcx", "%rdx", "cc");
}
EOF
    return $code;
  }
  elsif ($k == 2) {
    my $code_init1 = <<EOF;
static void
mul_$k(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "  ### START OF MUL\\n"
   "  ### x*y[0]\\n"
   "    movq	%2, %%r8\\n"
   "    movq    %0, %%rdi\\n"
   "    movq	(%%r8), %%r9\\n"
   "    movq    %1, %%rsi\\n"
   "    movq    (%%rsi), %%rax\\n"
   "    mulq    %%r9\\n"
   "    movq    %%rax, (%%rdi)\\n"
   "    movq    8(%%rsi), %%rax\\n"
   "    movq    %%rdx, %%rcx\\n"
EOF

  my $loop1_code = <<EOF;
   "    mulq    %%r9\\n"
   "    addq    %%rax, %%rcx\\n"
   "    adcq    \$0, %%rdx\\n"
   "    movq    __xi, %%rax\\n"
   "    movq    %%rcx, __zi\\n"
   "    movq    %%rdx, %%rcx\\n"
EOF

  my $final1_code = <<EOF;
   "    mulq    %%r9\\n"
   "    addq    %%rax, %%rcx\\n"
   "    adcq    \$0, %%rdx\\n"
   "    movq    %%rcx, __zi\\n"
   "    movq    %%rdx, __zend\\n"
EOF

  my $str;
  $str = "" . (8*($k-1)) . "(%%rdi)"; $final1_code =~ s/__zi/$str/g;
  $str = "" . (8*$k) . "(%%rdi)"; $final1_code =~ s/__zend/$str/g;
 
  my $i;
  my $j;

  my $code = $code_init1;
  for ($i=1; $i<$k-1; $i++) {
    my $code_i = $loop1_code;
    $str = "".(8*($i+1))."(%%rsi)"; $code_i =~ s/__xi/$str/g;
    $str = "".(8*$i)."(%%rdi)"; $code_i =~ s/__zi/$str/g;
    $code = $code . $code_i;
  }
  $code = $code . $final1_code;

  for ($i=$k+1; $i < 2*$k; $i++) {
    $j = 8*$i;
    my $raz = <<EOF;
   "	movq	\$0, $j(%%rdi)\\n"
EOF
    $code = $code . $raz;
  }

  for ($j=1; $j < $k; $j++) {
    my $j8 = 8*$j;


  my $code_init2 = <<EOF;
   "  ### x*y[$j]\\n"
   "    movq	$j8(%%r8), %%r9\\n"
   "    movq    (%%rsi), %%rax\\n"
   "    mulq    %%r9\\n"
   "    addq    %%rax, $j8(%%rdi)\\n"
   "    movq    8(%%rsi), %%rax\\n"
   "    adcq    \$0, %%rdx\\n"
   "    movq    %%rdx, %%rcx\\n"
EOF

  my $loop2_code = <<EOF;
   "    mulq    %%r9\\n"
   "    addq    %%rax, %%rcx\\n"
   "    adcq    \$0, %%rdx\\n"
   "    movq    __xi, %%rax\\n"
   "    addq    %%rcx, __zi\\n"
   "    adcq    \$0, %%rdx\\n"
   "    movq    %%rdx, %%rcx\\n"
EOF

  my $final2_code = <<EOF;
   "    mulq    %%r9\\n"
   "    addq    %%rax, %%rcx\\n"
   "    adcq    \$0, %%rdx\\n"
   "    addq    %%rcx, __zi\\n"
   "    adcq    \$0, %%rdx\\n"
   "    movq    %%rdx, __zend\\n"
EOF

  $str = "" . (8*($k+$j-1)) . "(%%rdi)"; $final2_code =~ s/__zi/$str/g;
  $str = "" . (8*($k+$j)) . "(%%rdi)"; $final2_code =~ s/__zend/$str/g;
 

  $code = $code . $code_init2;
  for ($i=1; $i<$k-1; $i++) {
    my $code_i = $loop2_code;
    $str = "".(8*($i+1))."(%%rsi)"; $code_i =~ s/__xi/$str/g;
    $str = "".(8*($i+$j))."(%%rdi)"; $code_i =~ s/__zi/$str/g;
    $code = $code . $code_i;
  }
  $code = $code . $final2_code;
}

  my $final_final = <<EOF;
  : "+m" (z)
  : "m" (x), "m" (y)
  : "%rax", "%rcx", "%rdx", "%rsi", "%rdi", "%r8", "%r9", "%r10", "memory");
}
EOF
  $code = $code . $final_final;
  return $code;
}
}


print <<EOF;
#ifndef __MP_X86_64_H__
#define __MP_X86_64_H__

#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>

EOF

my $i;
for ($i=1; $i <= 5; $i++) {
  print "#define HAVE_NATIVE_MUL_$i 1\n";
  print mul($i);
  print "\n";
}


print "#endif  /* __MP_X86_64_H__ */\n";
