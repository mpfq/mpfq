#ifndef __MP_X86_64_H__
#define __MP_X86_64_H__

#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>

#define HAVE_NATIVE_MUL_1 1
static void 
mul_1(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    movq %5, %%rax\n"
   "    mulq %7\n"
   "    movq %%rax, %2\n"
   "    movq %%rdx, %3\n"
   "    movq %4, %%rax\n"
   "    mulq %6\n"
   "    movq %%rax, %0\n"
   "    movq %%rdx, %1\n"
   "    movq %4, %%rax\n"
   "    mulq %7\n"
   "    addq %%rax, %1\n"
   "    adcq %%rdx, %2\n"
   "    adcq $0, %3\n"
   "    movq %5, %%rax\n"
   "    mulq %6\n"
   "    addq %%rax, %1\n"
   "    adcq %%rdx, %2\n"
   "    adcq $0, %3\n"
  : "=&rm" (z[0]), "=&r" (z[1]), "=&r" (z[2]), "=&r" (z[3])
  : "r" (x[0]), "r" (x[1]), "r" (y[0]), "r" (y[1])
  : "%rax", "%rdx", "cc");
}

#define HAVE_NATIVE_MUL_2 1
static void
mul_2(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "  ### START OF MUL\n"
   "  ### x*y[0]\n"
   "    movq	%2, %%r8\n"
   "    movq    %0, %%rdi\n"
   "    movq	(%%r8), %%r9\n"
   "    movq    %1, %%rsi\n"
   "    movq    (%%rsi), %%rax\n"
   "    mulq    %%r9\n"
   "    movq    %%rax, (%%rdi)\n"
   "    movq    8(%%rsi), %%rax\n"
   "    movq    %%rdx, %%rcx\n"
   "    mulq    %%r9\n"
   "    addq    %%rax, %%rcx\n"
   "    adcq    $0, %%rdx\n"
   "    movq    %%rcx, 8(%%rdi)\n"
   "    movq    %%rdx, 16(%%rdi)\n"
   "	movq	$0, 24(%%rdi)\n"
   "  ### x*y[1]\n"
   "    movq	8(%%r8), %%r9\n"
   "    movq    (%%rsi), %%rax\n"
   "    mulq    %%r9\n"
   "    addq    %%rax, 8(%%rdi)\n"
   "    movq    8(%%rsi), %%rax\n"
   "    adcq    $0, %%rdx\n"
   "    movq    %%rdx, %%rcx\n"
   "    mulq    %%r9\n"
   "    addq    %%rax, %%rcx\n"
   "    adcq    $0, %%rdx\n"
   "    addq    %%rcx, 16(%%rdi)\n"
   "    adcq    $0, %%rdx\n"
   "    movq    %%rdx, 24(%%rdi)\n"
  : "+m" (z)
  : "m" (x), "m" (y)
  : "%rax", "%rcx", "%rdx", "%rsi", "%rdi", "%r8", "%r9", "%r10", "memory");
}

#define HAVE_NATIVE_MUL_3 1
static void 
mul_3(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    movq %4, %%rax\n"
   "    mulq %6\n"
   "    movq %%rax, %%rcx\n"
   "    movq %%rcx, %0\n"
   "    movq %%rdx, %1\n"
   "    movq %5, %%rax\n"
   "    mulq %7\n"
   "    movq %%rax, %2\n"
   "    movq %%rdx, %3\n"
   "    movq %4, %%rax\n"
   "    mulq %7\n"
   "    movq %%rax, %%rbx\n"
   "    movq %%rdx, %%rcx\n"
   "    movq %5, %%rax\n"
   "    mulq %6\n"
   "    addq %%rbx, %1\n"
   "    adcq %%rcx, %2\n"
   "    adcq $0, %3\n"
   "    addq %%rax, %1\n"
   "    adcq %%rdx, %2\n"
   "    adcq $0, %3\n"
  : "+rm" (z[0]), "+r" (z[1]), "+r" (z[2]), "+r" (z[3])
  : "r" (x[0]), "r" (x[1]), "r" (y[0]), "r" (y[1])
  : "%rax", "%rbx", "%rcx", "%rdx", "cc");
}

#define HAVE_NATIVE_MUL_4 1
static void 
mul_4(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    movq %4, %%rax\n"
   "    mulq %6\n"
   "    movq %%rax, %%rcx\n"
   "    movq %%rcx, %0\n"
   "    movq %%rdx, %1\n"
   "    movq %5, %%rax\n"
   "    mulq %7\n"
   "    movq %%rax, %2\n"
   "    movq %%rdx, %3\n"
   "    movq %4, %%rax\n"
   "    mulq %7\n"
   "    movq %%rax, %%rbx\n"
   "    movq %%rdx, %%rcx\n"
   "    movq %5, %%rax\n"
   "    mulq %6\n"
   "    addq %%rbx, %1\n"
   "    adcq %%rcx, %2\n"
   "    adcq $0, %3\n"
   "    addq %%rax, %1\n"
   "    adcq %%rdx, %2\n"
   "    adcq $0, %3\n"
  : "+rm" (z[0]), "+r" (z[1]), "+r" (z[2]), "+r" (z[3])
  : "r" (x[0]), "r" (x[1]), "r" (y[0]), "r" (y[1])
  : "%rax", "%rbx", "%rcx", "%rdx", "cc");
}

#define HAVE_NATIVE_MUL_5 1
static void 
mul_5(mp_limb_t *z, const mp_limb_t *x, const mp_limb_t *y)
{
  __asm__ volatile(
   "    movq %5, %%rax\n"
   "    mulq %6\n"
   "    movq %%rax, %1\n"
   "    movq %%rdx, %2\n"
   "    movq %4, %%rax\n"
   "    mulq %7\n"
   "    movq %%rax, %%rcx\n"
   "    movq %%rdx, %%r8\n"
   "    movq %5, %%rax\n"
   "    mulq %7\n"
   "    addq %%rcx, %1\n"
   "    adcq %%r8, %2\n"
   "    movq %%rdx, %2\n"
   "    adcq $0, %3\n"
   "    addq %%rax, %2\n"
   "    adcq $0, %3\n"
   "    movq %4, %%rax\n"
   "    mulq %6\n"
   "    movq %%rax, %0\n"
   "    addq %%rdx, %1\n"
   "    adcq $0, %2\n"
   "    adcq $0, %3\n"
  : "=&rm" (z[0]), "=&r" (z[1]), "=&r" (z[2]), "=&r" (z[3])
  : "r" (x[0]), "r" (x[1]), "r" (y[0]), "r" (y[1])
  : "%rax", "%rcx", "%rdx", "%r8", "cc");
}

#endif  /* __MP_X86_64_H__ */
