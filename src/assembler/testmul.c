#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <time.h>
#include "test.h"


int test_mul_1() {
  int i;
  mp_limb_t z1[2*1],z2[2*1],x[1],y[1];
  mpn_random2(x,1);
  mpn_random2(y,1);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_1(z1,x,y);
    mul_1(z2,y,x);
  }
  t=clock()-t;
  printf("Time for size 1 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,1)){
    return 1;
  } else {
    return 0;
  }
}



int test_mul_2() {
  int i;
  mp_limb_t z1[2*2],z2[2*2],x[2],y[2];
  mpn_random2(x,2);
  mpn_random2(y,2);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_2(z1,x,y);
    mul_2(z2,y,x);
  }
  t=clock()-t;
  printf("Time for size 2 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,2)){
    return 1;
  } else {
    return 0;
  }
}



int test_mul_3() {
  int i;
  mp_limb_t z1[2*3],z2[2*3],x[3],y[3];
  mpn_random2(x,3);
  mpn_random2(y,3);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_3(z1,x,y);
    mul_3(z2,y,x);
  }
  t=clock()-t;
  printf("Time for size 3 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,3)){
    return 1;
  } else {
    return 0;
  }
}



int test_mul_4() {
  int i;
  mp_limb_t z1[2*4],z2[2*4],x[4],y[4];
  mpn_random2(x,4);
  mpn_random2(y,4);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_4(z1,x,y);
    mul_4(z2,y,x);
  }
  t=clock()-t;
  printf("Time for size 4 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,4)){
    return 1;
  } else {
    return 0;
  }
}



int test_mul_5() {
  int i;
  mp_limb_t z1[2*5],z2[2*5],x[5],y[5];
  mpn_random2(x,5);
  mpn_random2(y,5);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_5(z1,x,y);
    mul_5(z2,y,x);
  }
  t=clock()-t;
  printf("Time for size 5 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,5)){
    return 1;
  } else {
    return 0;
  }
}



int test_mul_6() {
  int i;
  mp_limb_t z1[2*6],z2[2*6],x[6],y[6];
  mpn_random2(x,6);
  mpn_random2(y,6);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_6(z1,x,y);
    mul_6(z2,y,x);
  }
  t=clock()-t;
  printf("Time for size 6 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,6)){
    return 1;
  } else {
    return 0;
  }
}



int test_mul_7() {
  int i;
  mp_limb_t z1[2*7],z2[2*7],x[7],y[7];
  mpn_random2(x,7);
  mpn_random2(y,7);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_7(z1,x,y);
    mul_7(z2,y,x);
  }
  t=clock()-t;
  printf("Time for size 7 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,7)){
    return 1;
  } else {
    return 0;
  }
}



int test_mul_8() {
  int i;
  mp_limb_t z1[2*8],z2[2*8],x[8],y[8];
  mpn_random2(x,8);
  mpn_random2(y,8);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_8(z1,x,y);
    mul_8(z2,y,x);
  }
  t=clock()-t;
  printf("Time for size 8 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,8)){
    return 1;
  } else {
    return 0;
  }
}



int test_mul_9() {
  int i;
  mp_limb_t z1[2*9],z2[2*9],x[9],y[9];
  mpn_random2(x,9);
  mpn_random2(y,9);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_9(z1,x,y);
    mul_9(z2,y,x);
  }
  t=clock()-t;
  printf("Time for size 9 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,9)){
    return 1;
  } else {
    return 0;
  }
}


int main(int argc,char** argv) {
  int r;
  r=test_mul_1();
  if (r==0) {
    printf("Test 1 failed \n");
  }

  r=test_mul_2();
  if (r==0) {
    printf("Test 2 failed \n");
  }

  r=test_mul_3();
  if (r==0) {
    printf("Test 3 failed \n");
  }

  r=test_mul_4();
  if (r==0) {
    printf("Test 4 failed \n");
  }

  r=test_mul_5();
  if (r==0) {
    printf("Test 5 failed \n");
  }

  r=test_mul_6();
  if (r==0) {
    printf("Test 6 failed \n");
  }

  r=test_mul_7();
  if (r==0) {
    printf("Test 7 failed \n");
  }

  r=test_mul_8();
  if (r==0) {
    printf("Test 8 failed \n");
  }

  r=test_mul_9();
  if (r==0) {
    printf("Test 9 failed \n");
  }


}
