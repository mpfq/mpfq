#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <time.h>
#include "test2.h"


int test_mul_1() {
  int i;
  mp_limb_t z1[2*2],z2[2*2],x[2],y[2];
  mpn_random2(x,2);
  mpn_random2(y,2);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_1(z1,x,y);
    mul_1(z2,y,x);
  }
  t=clock()-t;
  printf("Time for 1 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,2)){
    return 1;
  } else {
    return 0;
  }
}



int test_mul_2() {
  int i;
  mp_limb_t z1[2*2],z2[2*2],x[2],y[2];
  mpn_random2(x,2);
  mpn_random2(y,2);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_2(z1,x,y);
    mul_2(z2,y,x);
  }
  t=clock()-t;
  printf("Time for 2 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,2)){
    return 1;
  } else {
    return 0;
  }
}



int test_mul_3() {
  int i;
  mp_limb_t z1[2*2],z2[2*2],x[2],y[2];
  mpn_random2(x,2);
  mpn_random2(y,2);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_3(z1,x,y);
    mul_3(z2,y,x);
  }
  t=clock()-t;
  printf("Time for 3 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,2)){
    return 1;
  } else {
    return 0;
  }
}



int test_mul_4() {
  int i;
  mp_limb_t z1[2*2],z2[2*2],x[2],y[2];
  mpn_random2(x,2);
  mpn_random2(y,2);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_4(z1,x,y);
    mul_4(z2,y,x);
  }
  t=clock()-t;
  printf("Time for 4 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,2)){
    return 1;
  } else {
    return 0;
  }
}



int test_mul_5() {
  int i;
  mp_limb_t z1[2*2],z2[2*2],x[2],y[2];
  mpn_random2(x,2);
  mpn_random2(y,2);
  clock_t t;
  t=clock();
  for(i=0;i<10000000;i++) {
    mul_5(z1,x,y);
    mul_5(z2,y,x);
  }
  t=clock()-t;
  printf("Time for 5 is %f \n",((float) t)/CLOCKS_PER_SEC);
  if (!mpn_cmp(z1,z2,2)){
    return 1;
  } else {
    return 0;
  }
}


int main(int argc,char** argv) {
  int r;
  r=test_mul_1();
  if (r==0) {
    printf("Test 1 failed \n");
  }

  r=test_mul_2();
  if (r==0) {
    printf("Test 2 failed \n");
  }

  r=test_mul_3();
  if (r==0) {
    printf("Test 3 failed \n");
  }

  r=test_mul_4();
  if (r==0) {
    printf("Test 4 failed \n");
  }

  r=test_mul_5();
  if (r==0) {
    printf("Test 5 failed \n");
  }


}
