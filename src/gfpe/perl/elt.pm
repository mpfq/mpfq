package elt;

use strict;
use warnings;
unshift @INC, '.';

sub code_for_set_zero {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $code = <<EOF;
r->size=0;
EOF
    return [ 'inline(K!,r)', $code ];
}


sub code_for_is_zero {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $code = <<EOF;
return mpfq_${btag}_vec_is_zero(K->kbase, r[0].c, r[0].size);
EOF
    return [ 'inline(K,r)', $code ];
}

sub code_for_vec_set_zero {
    my $opt = shift @_;
    my $code = <<EOF;
unsigned int i;
for(i=0;i<n;++i)
 @!set_zero(K,r[i]);
EOF
    return [ 'inline(K!,r,n)', $code ];
}

sub code_for_elt_stride { return [ 'macro(k)', 'sizeof(@!elt)' ]; }
sub code_for_elt_ur_stride { return [ 'macro(k)', 'sizeof(@!elt_ur)' ]; }
sub code_for_vec_elt_stride {
    return [ 'macro(k, n)', "((n) * @!elt_stride((k)))" ];
}
sub code_for_vec_ur_elt_stride {
    return [ 'macro(k, n)', "((n) * @!elt_ur_stride((k)))" ];
}

#############


sub code_for_init {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $code = <<EOF;
assert(k);
assert(k->kbase);
assert(k->P);
mpfq_${btag}_poly_init(k->kbase, *x, (k->deg)-1);
EOF
    return [ 'inline(k,x)', $code ];
}

sub code_for_clear {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $code = <<EOF;
mpfq_${btag}_poly_clear(k->kbase, *x);
EOF
    return [ 'inline(k,x)', $code ];
}

sub code_for_set {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'inline(k!,r,x)';
    my $code = <<EOF;
mpfq_${btag}_poly_set(k->kbase, r, x);
EOF
    return [ $proto, $code ];
}

sub code_for_set_ui {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'inline(k,r,x)';
    my $code = <<EOF;
mpfq_${btag}_poly_setcoeff_ui(k->kbase, r, x, 0);
r->size=1;
EOF
    return [ $proto, $code ];
}

sub code_for_get_ui {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    return [ 'inline(k!,x)', "return mpfq_${btag}_get_ui(k->kbase, mpfq_${btag}_vec_coeff_ptr_const(k->kbase, x->c, 0));" ];
}

sub code_for_set_mpn {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'function(k,r,x,n)';
    my $code = <<EOF;
mpfq_${btag}_elt aux;
mpfq_${btag}_init(k->kbase, &aux);
mpfq_${btag}_set_mpn(k->kbase, aux, x, n);
mpfq_${btag}_poly_setcoeff(k->kbase, r, aux, 0);
r->size=1;
mpfq_${btag}_clear(k->kbase, &aux);
EOF
    return [ $proto, $code ];
}

sub code_for_set_mpz {
    my $proto = 'function(k,r,z)';
    my $code = <<EOF;
if (z->_mp_size < 0) {
    @!set_mpn(k, r, z->_mp_d, -z->_mp_size);
    @!neg(k, r, r);
} else {
    @!set_mpn(k, r, z->_mp_d, z->_mp_size);
}
EOF
    return [ $proto, $code ];
}

sub code_for_get_mpn {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'function(k!,r,x)';
    my $code = <<EOF;
mpfq_${btag}_elt aux;
mpfq_${btag}_init(k->kbase, &aux);
mpfq_${btag}_poly_getcoeff(k->kbase, aux, x, 0);
mpfq_${btag}_get_mpn(k->kbase, r, aux);
mpfq_${btag}_clear(k->kbase, &aux);
EOF
    return [ $proto, $code ];
}

sub code_for_get_mpz {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'function(k!,z,x)';
    my $code = <<EOF;
mpfq_${btag}_elt aux;
mpfq_${btag}_init(k->kbase, &aux);
mpfq_${btag}_poly_getcoeff(k->kbase, aux, x, 0);
mpfq_${btag}_get_mpz(k->kbase, z, aux);
mpfq_${btag}_clear(k->kbase, &aux);
EOF
    return [ $proto, $code ];
}

sub code_for_cmp {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    return [ 'inline(k!,x,y)', "return mpfq_${btag}_poly_cmp(k->kbase,x,y);" ];
}

sub code_for_cmp_ui {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'function(k!,x,y)';
    my $code = <<EOF;
if (x->size == 0) {
    if (y>0) 
        return 1;
    else if (((long int) y)<0)
        return -1;
    else
        return 0;
}
if (x->size > 1) {
    return 1;
}
mpfq_${btag}_elt yy;
mpfq_${btag}_init(k->kbase, &yy);
mpfq_${btag}_poly_getcoeff(k->kbase, yy, x, 0);
int ret = mpfq_${btag}_cmp_ui(k->kbase, yy, y);
mpfq_${btag}_clear(k->kbase, &yy);
return ret;
EOF
    return [ $proto, $code ];
}

sub code_for_random {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'function(k,x, state)';
    my $code = <<EOF;
mpfq_${btag}_poly_random(k->kbase, x, k->deg-1, state);
EOF
    return [ $proto, $code ];
}

sub code_for_random2 {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'function(k,x, state)';
    my $code = <<EOF;
mpfq_${btag}_poly_random2(k->kbase, x, k->deg-1, state);
EOF
    return [ $proto, $code ];
}

sub code_for_add {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
mpfq_${btag}_poly_add(k->kbase, z, x, y);
EOF
    return [ $proto, $code ];
}

sub code_for_sub {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
mpfq_${btag}_poly_sub(k->kbase, z, x, y);
EOF
    return [ $proto, $code ];
}

sub code_for_neg {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'inline(k,z,x)';
    my $code = <<EOF;
mpfq_${btag}_poly_neg(k->kbase, z, x);
EOF
    return [ $proto, $code ];
}

sub code_for_mul {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'function(k,z,x,y)';
    my $code = <<EOF;
mpfq_${btag}_poly tmp;
mpfq_${btag}_poly_init(k->kbase, tmp, 2*k->deg-1);
mpfq_${btag}_poly_mul(k->kbase, tmp, x, y);
if (k->invrevP != NULL)
    mpfq_${btag}_poly_mod_pre(k->kbase, z, tmp, k->P, k->invrevP);
else
    mpfq_${btag}_poly_divmod(k->kbase, NULL, z, tmp, k->P);
mpfq_${btag}_poly_clear(k->kbase, tmp);
EOF
    return [ $proto, $code ];
}

sub code_for_sqr {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'function(k,z,x)';
    my $code = <<EOF;
mpfq_${btag}_poly tmp;
mpfq_${btag}_poly_init(k->kbase, tmp, 2*k->deg-1);
mpfq_${btag}_poly_mul(k->kbase, tmp, x, x);
if (k->invrevP != NULL)
    mpfq_${btag}_poly_mod_pre(k->kbase, z, tmp, k->P, k->invrevP);
else
    mpfq_${btag}_poly_divmod(k->kbase, NULL, z, tmp, k->P);
mpfq_${btag}_poly_clear(k->kbase, tmp);
EOF
    return [ $proto, $code ];
}

sub code_for_init_ts {
    my $proto = 'function(k)';
    my $requirements = 'dst_field';
    my $name = 'init_ts';
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $w = $opt->{'w'};
    my $code = <<EOF;
mpz_t ppz;
mpz_init_set(ppz,k->kbase->p);
mpz_pow_ui(ppz,ppz,k->deg);
mpz_sub_ui(ppz,ppz,1);

unsigned int val = mpz_scan1(ppz, 0);
mpz_div_2exp(ppz, ppz, val);

mpz_t s;
mpz_init(s);
mpz_ui_pow_ui(s, 2, val-1);

k->ts_info.e = val;
k->ts_info.z = malloc(k->deg*mpz_size(k->kbase->p)*sizeof(mp_limb_t));

/* Find a generating element for the Sylow subgroup. */
gmp_randstate_t rstate;
gmp_randinit_default(rstate);
@!elt z, r;
@!init(k, &z);
@!init(k, &r);
@!set_ui(k, r, 0);
do {
    @!random(k, z, rstate);
    @!powz(k, z, z, ppz);
    @!powz(k, r, z, s);
    @!add_ui(k, r, r, 1);
} while (@!cmp_ui(k, r, 0)!=0);
@!set(k, (@!dst_elt)k->ts_info.z, z);
@!clear(k, &z);
@!clear(k, &r);
gmp_randclear(rstate);

mpz_sub_ui(ppz, ppz, 1);
mpz_div_2exp(ppz, ppz, 1);

// mpz_clear(ppz);
// ugly: don't clear ppz; reuse it !
k->ts_info.hhl = mpz_size(ppz);
k->ts_info.hh = ppz->_mp_d;

mpz_clear(s);
EOF
    return { kind=>$proto,
        requirements=>$requirements,
        name=>$name,
        code=>$code};
}


sub code_for_is_sqr {
    my $proto = 'inline(k,x)';
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $code = <<EOF;
mpz_t ppz;
mpz_init_set(ppz,k->kbase->p);
mpz_pow_ui(ppz,ppz,k->deg);
mpz_sub_ui(ppz,ppz,1);
mpz_div_2exp(ppz, ppz, 1);
@!elt y;
@!init(k, &y);
@!powz(k, y, x, ppz);
int res = @!cmp_ui(k, y, 1);
@!clear(k, &y);
mpz_clear(ppz);
return res == 0;
EOF
    return [ $proto, $code ], code_for_init_ts($opt);
}


sub code_for_sqrt {
    my $proto = 'function(k,z,a)';
    my $opt = shift @_;
    my $code = <<EOF;
if (@!cmp_ui(k, a, 0) == 0) {
    @!set_ui(k, z, 0);
    return 1;
}
if (k->ts_info.e == 0)
    @!init_ts(k);
@!elt b, x, y;
@!init(k, &x);
@!init(k, &y);
@!init(k, &b);
mp_limb_t r = k->ts_info.e;
mp_limb_t s; //= (1UL<<(r-1)); not needed...
@!set(k, x, a);
@!set(k, y, (@!src_elt)k->ts_info.z);

@!pow(k, x, a, k->ts_info.hh, k->ts_info.hhl);
@!sqr(k, b, x);
@!mul(k, x, x, a);
@!mul(k, b, b, a);

@!elt t;
@!init(k, &t);
mp_limb_t m;
for(;;) {
    @!set(k, t, b);
    for(m=0; @!cmp_ui(k, t, 1)!=0; m++)
        @!sqr(k, t, t);
    assert(m<=r);
    
    if (m==0 || m==r)
        break;
    
    s = 1UL<<(r-m-1);
    r = m;
    
    @!pow(k, t, y, &s, 1);
    @!sqr(k, y, t);
    @!mul(k, x, x, t);
    @!mul(k, b, b, y);
}
@!set(k, z, x);
@!clear(k, &t);
@!clear(k, &x);
@!clear(k, &y);
@!clear(k, &b);
return (m==0);
EOF
    return [ $proto, $code, code_for_init_ts($opt) ];
}

sub code_for_mul_ui {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'function(k,z,x,y)';
    my $code = <<EOF;
mpfq_${btag}_elt aux;
mpfq_${btag}_init(k->kbase, &aux);
unsigned int i;
for (i = 0; i < x->size; ++i) {
    mpfq_${btag}_poly_getcoeff(k->kbase, aux, x, i);
    mpfq_${btag}_mul_ui(k->kbase, aux, aux, y);
    mpfq_${btag}_poly_setcoeff(k->kbase, z, aux, i);
}
z->size = x->size;
z->size = 1 + mpfq_${btag}_poly_deg(k->kbase, z);
mpfq_${btag}_clear(k->kbase, &aux);
EOF
    return [ $proto, $code ];
}

sub code_for_inv {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'function(k,z,x)';
    my $code = <<EOF;
mpfq_${btag}_poly tmp1, tmp2, tmp3;
int ret;
mpfq_${btag}_poly_init(k->kbase, tmp1, 0);
mpfq_${btag}_poly_init(k->kbase, tmp2, 0);
mpfq_${btag}_poly_init(k->kbase, tmp3, 0);
mpfq_${btag}_poly_xgcd(k->kbase, tmp1, tmp2, tmp3, x, k->P);
if ((tmp1->size != 1) || 
    (mpfq_${btag}_cmp_ui(k->kbase, mpfq_${btag}_vec_coeff_ptr_const(k->kbase, tmp1->c, 0), 1) != 0)) {
    @!set_ui(k, z, 0);
    ret = 0;
} else {
    mpfq_${btag}_poly_set(k->kbase, z, tmp2);
    ret = 1;
}
mpfq_${btag}_poly_clear(k->kbase, tmp1);
mpfq_${btag}_poly_clear(k->kbase, tmp2);
mpfq_${btag}_poly_clear(k->kbase, tmp3);
return ret;
EOF
    return [ $proto, $code ];
}

sub code_for_frobenius {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'function(k,x,y)';
    my $code = <<EOF;
@!powz(k, x, y, k->kbase->p);
EOF
    return [ $proto, $code ];
}

sub code_for_add_ui {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
mpfq_${btag}_poly_add_ui(k->kbase, z, x, y);
EOF
    return [ $proto, $code ];
}

sub code_for_sub_ui {
    my $opt = shift @_;
    my $btag = $opt->{'basetag'};
    my $proto = 'inline(k,z,x,y)';
    my $code = <<EOF;
mpfq_${btag}_poly_sub_ui(k->kbase, z, x, y);
EOF
    return [ $proto, $code ];
}

1;
