# This file is parsed by cmake to define all fields which are built with
# the "make" command.
#
# all fields with tag p_* or pm_* trigger generation of the corresponding
# ${tag}_e extension field type
#
#
# This file is only for quick builds with the BUILD_ALL_FIELDS
# environment variable unset. Otherwise the larger file FieldList.txt is
# used
prime
p_25519   type=25519
p_127_735 type=127_735
p_127_1   type=127_1
 p_0_5    type=plain n=0.5
pm_0_5    type=mgy   n=0.5
 p_1      type=plain n=1
pm_1      type=mgy   n=1
pz        type=pz
rns_1
rns_2
rns_4
rns_7
rns_9
2_23
2_64
2_128
